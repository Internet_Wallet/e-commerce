//
//  OKPayModel.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

 public struct OKPayModel {
    
     var countryCode : String?
    
     var destination : String?
    
     var amount : Double?
    
    var geoTuple : (lat: String,long: String)?
    
     var urlType : URLType?
    
     var applicationType : ApplicationType?
    
     var paymentType : PaymentType?
    
     var hashKey : String?
    
    public init(code: String, destination: String, amount: Double, key: String, paymentTouple: (url: URLType, app: ApplicationType, pay: PaymentType), geoTuple: (lat: String, long: String)) {
        self.countryCode = code
        self.destination = destination
        self.amount      = amount
        self.hashKey     = key
        
        //touple initialisation for app
        self.urlType         = paymentTouple.url
        self.applicationType = paymentTouple.app
        self.paymentType     = paymentTouple.pay
        self.geoTuple        = geoTuple

    }
    
}
