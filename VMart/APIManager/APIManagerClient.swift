//
//  APIManagerClient.swift
//  NopCommerce
//
//  Created by BS-125 on 11/5/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation

class APIManagerClient: NSObject {
    
    var homePageProductArray            = [GetHomePageProducts]()
    var homePageMenuFactureArray        = [HomePageMenuFacture]()
    var parentCategoriesArray           = [GetAllCategories]()
    var leftMenuCategoriesArray         = [GetAllCategories]()
    var homePageCategories              = [HomePageCategories]()
    var categoryDetailsInfo             = CategoryDetailsInfo()
    var categoryArray                   = NSMutableArray()
    var subCategoryArray                = NSMutableArray()
    var shoppingCartCount               = 0
    var AppStartUp : AppStartUpModel?   = nil
    var isRequestedToReloadOnlyOnce: Bool = false
    var versioncode = 1
    var DeviceTypeId = 5 // FOR IOS 5
    var medicineRequestId = 0 // 0 for create new request
    
    // Mohit
    enum UrlTypes {
        case productionUrl, testUrl, stagingUrl
    }
    
    let serverUrl: UrlTypes = .productionUrl
    let baseURl_doctors = "http://13.251.91.165/api"
    
    var base_url                         = ""
    var base_url_sendSMS                   = ""
    var NST_KEY_Value       = ""
    var NST_SECRET_Value    = ""
    var secretKey = ""
    var productionMode = false
    var merchantID = ""
    var currencyCode = "104"
    var ProductionAndStagingPrivateKeyfor2c2p = ""
    
    func twoCtwoPCredential(urlTo: UrlTypes){
        switch urlTo {
        case .testUrl:
            secretKey =  "YDRbw14OtHw3"
            productionMode = false
            merchantID = "JT02"
            ProductionAndStagingPrivateKeyfor2c2p = "MIINHwYJKoZIhvcNAQcDoIINEDCCDQwCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAK5oNQEQSadi/YEQYtC3R1qsl7TUvXb5q02Pp3OqZXJkBzlFoq7jCN+1Cw5zia0PEdNW2wSy+Q+e4NHDpWk8ZIoC/bDzd68XdK8Cm/1ajmZxCfs0i0t0dcRXG0s1ksACaS89VuUjcdTHYMwnkEr8cRIlvCGINoevCFoUhSDcTgFc5Nn0hevDWvJuqsVRS6KiQJw/QTSRoiIx/rxzpXTINOagYbMd51Yo6+o7BwZorTHvFNU4QvecQcKJLokS1AhxMr2C0m1kQIH5Syp27u20X5z5sRQGqLPfFLhOXf35MBTmFjFt/bgPYnD5zHntUyGVTkksGbTdGRgD2muN82w/2pkwggtVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECHLlH8wI8ZiVgIILMFPXWK+POI98ZF8xoRvpKPmhc14juAoCxARCh1k0sW/brtusMygZ9MZccl3yVla7ha3wkOYSXPsh/X1BGpF+FBFE8Qhx84GLjwojlMuhCGPN4etCvBGl1P7j/RZchTxdUcX7LSpbs3dtlrqor1PK/byLrx/fmnNweTQzB8ebJ6rTg9krn+nVCGR4+Nma0j8EwmDfQe2+YSUXle51kDin5lcKcpKGgKhBxc1DfRc6kbN7C/VOv2ndfORF+HHDwKVJY+TSrkccQuC/Bk1EEYuwnHEQd5NTtEyQcDWrxSAEZ/YgyRgwc44yWFHtspFXbRXv3d6agtg6lesXztuiWDP6rcTSuZkC72crEH9okls7drjATxq7zQWl9JDsJR8OhBhMdvW63TfHK2SkUx9uwWnsL6VVJ4v/XHGxyg5JWbeg0EFoPHJK3d46IwTRuIG+1GTpSyS6Y7p0A0FYJ+66sShRSrOEerDfG8Rvjd3jXSyauw4qHqwHdDuLDfOQG2jRtk4vkvo4Vu0V/giUNnhZzqyk2WQGntAzEjH4ib1KuHyQdfsiQX8NEBK048m+yv6TmHoMtwzRkuXjpiUHzFkt5FHGn7CR2rT5t8xrhgy34ijXIjE6nlD7lV0TtblQpK3a4buNsEGF5JJsRhNvd8G3TDLkyQj1M/opLwmPf9u9p4CKixNmqa3IFVfNk41CuSPStmepg0nFRsaC6NkDYNfsIK0vLb8PiEhkgi9+DsjH+RByc80TXKXDTjEKGlTUc4pYhadVROfpV3hr9ZD+yVcMb2SE19LFELUgKLsWPcgywJvuNMK0fCju1ZgiLnJ5oADwizBhHCGQWYaAlsyCTWfKdC9Iy11ATrHwWlO6Q8OXNhOY/c3tUUtr0Leww1FIl1mIExRp5vlqizOVJC9vjqEPcPdzi80I8yZ4AtyZ0wtvHL7gUXn9lHYaeTT+TOPHUVf6DFaugiaQOzB+PGYhZFdIJzwCmn+tkawKLSSZrc9GvARK95wHLHVTS0jE0bHJho9uyt5QeUJtB8bnjtTazSO68nOFw6bcM8GYqO17D+2t+b2hk/d/n1aECF4hQ2KJOHS4KtcAVBrQYm623T1l7eBEhN3eFnm0s3RyKFbAgnAaLrbq/zqPDbZ4HWvZaBC1Iy2Qlkc3eOV40YFrZfQ+1D0JuvEXae3i/yIWq73pxmz0uYYhXGVn+hWh4r2Cx2RKd1I8Js1SFPm8gz6+PaYLMUed1RgSzsycB7MCqWDYeM04T7DDKb7KYwOQWmHQfOiLcq0p9G4itWuh73j0jmIBf0SNH4MNDhnR0bY2yQ9jbnNOFVr9RhmLoubtUbAebcKLVjTtmCcDm78Os4pa74x0TU+phHQY9eA4O+sNhc13gBer28CNh9VOgbKSAIAGusrShshqyfI22fwiPiJnQXhh2OlbG2BZNIxS0jPFwvyxOqwuq+0xln/lOBzm3Tg8WrrxfwHEHoEWzEjp+ppGaRg16RGb2qoEg96luGxv0rIyAx8lrphzPvt9a9G7V755a9Tnwm0VoaPud+x5j2VbbGEaWAULcU8u6q3Msiui8R2BN910PUIJunGrWHcNqodUXIVp1hlK4bi4tAnXc2fabV+zuyBBRktBaLH7f6J1Ico4ZOt9YadgewIvvynPGGzGVrWG3FPNbazoVbXncKiWFEKgScQJITsKy2Wb6sMvINaZUV28vNXb0S4MYkXC1RhuyAsWslslzkTu1po2Y0QdgG06OEVmBALVcNZ0A5k01UU3cwWgPNL6RsDlQvoS0bKBZoFbIQGWsbc3W95eQX3PSVA4Lb0VOyCSpcsLLGSImm59l920A4JxJj4Bd8zu7f8B0p034oK0xsmE9bWzrmu8VDhPqmXyM9ojf0iGkTKvAkjtdz+ePKjmU3ufrW8wdXRl5cjgRXEU0hXpHn+a73hClXpyusahgRE9TyGy6u3ho9s+zELEtIRhWktFKBCbMP62KKiXgFaTRPc3YhCY1poS2xJU3jFcpfw4YzNiX5S2/Bp39jn589jaVfiDA/s4b9Ss8vBAPvSSFO+oCVfsoUzQIRkbaxFnTH7sGtP6ASDg7nWHF67NHtOlyalCrZEh8DYAwoLrpoDZg4W5QzS/ET2/eHaNzfclknlzA9rWjKAGUt9pCi37eWquPb3RCoaUU5+178Z1S3hIYEE2kx+sXFD0+xgZ4Rz5mdagsEgxNuTQXnThn/jPAMXz8zwi0CYgl0IgVoOvUxsvuHiQbZ0zNz5w2DRonBSTO9o1SRSZrxyv5GfXsfdx3CsyKNpheBVFgRPRPpX459QZBZ3/M642BLJRb+oqUqnUNDSN+DxjDtcVdF83RMJqTR5ej7qi/oQb7uMqlDC3fwE3mPJ7BHjkdZkd4tKvj5UFwjlVCuRKtFlu+G1k/faoAtu9qXabdh95Vgv/nFaUBACdy+ZaHMDAJESxdhsksYfzYCEvnBkmX4hH1htd/4k4uG7mdfNxnZ0u/wV6FhvPp+996g9hRMPAhfQvg7Q61eC5FbgPwYzN/TYmzBQA1LqiL63opEYvFX0D/FalS+kpPzVnttT7R1Xg4czAi+bXDH5//yrgK4IJO0KZIHK9RiOYHPyWU0Ag3JTsCQHgdxhrSX1d6S/7/TVuCWKscPN/xiCjb4p5hOyXuaOGMTFARfuRB3x/5/sgfzfxEVQwfb0DDBVsOO9rh6GzIyt5hIh8EFoAzjxeJjylCgyLx0h71ycYiH2jm5IPAQ41+jMC/YTLY0aKdSxDXePR+2f1ATGS9dng+UN0qvLaLb8eIxWDWh83peOgAZCKcmRnBt+WPO3jBLiuxvV7RIjSg0dgyqBceGaVO3WMJZ0lXsMjrknEkBx4NNFRAxtmlQRVXTx8at7nSwhHZf0yHsPRjqHYyxIOx0lRf8JG/YlMNVTarR50XrWXf5Y6EAsY2gm2Q1dgblOAzIRtJz3ZX1ijfKktQKXt9j3K2K+7CQdqHqbFKWl40rAq4EvX/X35wq6rm4AX8sO9qjAVHjtpY50SmMpXLGWoxRreNsh3Xl/Nb2yiN/luiljA/FFZO11++MB9mtS+RVxl0zI9MH99dDhrLmC46azc5aSFXuR6b2twjntsNr04ys+KFi6iJU4vMTLWAOtLQMqtWNRbucNJsoMX+gQqqnqHrn7YaO5rE7GJIz09DxWjAcE2DHqJDFwmB42rmB5d8AH8DQXD7clcZ4hk2f+wF5G7JrwXRHalFsHpb8mgr6lJBJhPkJ5XS4OVR/QAg4nx0qXGadPeaYKx6VlCaHpHiQuy4+lt7gZSgeplK37Os6tPlvdkSmkoXN2aX4XnSlSs/fGfxV/P8jCUVfJVpHJDC/ORcg90AfiuH6a5K2/zpJscadexZADa2rNeYnJ3OGZ0HAJOHLAjt3vTPpHa9DqWwPn8W7jJNkwIiGaritWnrKQc0KIdlfaybAAkghr+mOKiZYH9NLTP0ik1wVtefhdcjR0nY5dn+vivKBMUhW7F2LrG8xIMKE5O0FELGyhGA7AjCxOm3oOq4cPkPax/5nldje/LlT6cM/xdQ5Af0rBASvdZrmWHCV5bIv4c1EeWD1R6aBY7XPJ6FvsJWBJ1rPGPtjHvU/g8+c/s+x86TvAcOstp7p+wLTWGtqE+wo4UHl8JvrzKM7cxSjIszqvM9rTR8IsMnfqk4MQHczytTzgVebrxVZJs6h2iuwIUVCus6EA1uw9wmReoxIsMfFx9yNz9GmAQcPiVScCGKfGJGkMjaDUnQSia8dAJDP+WvITXScEyIUqH2OlzGRrb6SSbh9uYXwwAeC0fPg4zYOpTOrfC8lTW8re30iuMcNGi"
        case .productionUrl:
            secretKey = "524EE9FC87F9832814708365A49703407879BAFD3490BF099FFAD0D395BA55B4"
            productionMode = true
            merchantID = "104104000000270"
            ProductionAndStagingPrivateKeyfor2c2p = "MIINhwYJKoZIhvcNAQcDoIINeDCCDXQCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAHtW5fAcUK+u6as+ZQkHQtFnUXXRefHY4BiAt8oWocAjqXPB8T+XV+eEloPFc01qiYW3/xRKWlDxFaaK+0Ru8E7tD6GenGJMtIjaa7YMyMnQyWMUkcDq8ykqkfy4ln2ON9Me5/dYUgKIIfteBWsZ02xs02KEnZdNxIagD/qxBHsoACANZDmB18BFY55UP9OF+P/yo1SuPVuyWYIqxpREh+MqF8zGNvvok8iFn0wfgfncC+uccdS+aocWdvMoTcpbz97OcQTThrgfbyJCyLIdifXkYYgK01Oa/ehpncsFrvRohPnrwje8MWjgBPd92tpl75XEPhEjPdJwnnkAgbq2GiIwggu9BgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECGou2maju0VlgIILmOiUJ/a4wRhGaWiVG5OdwsguRJYOvNNJGvdweGFO4RtkNkyXsv5mRRmmLznPTU3V7whn6xC02zW6isv7p5okdmX+AaFKbjN9pA2jJpfRNj3RUx7aIqeLrOTnAxmyyJ4hvRO5Ak83P+OAedMK4BnT2ohJfeYuH8hpBCEoKaLaUnocwO2c6Ifq6hCwel7npSVvqfSPjOlyn8KFSsknSWWnXLJD8ZPSxsz3aIOzbDnmtsQVUgLZoYddDJHirWKaCbq7hSHv7hhkGyyvMsZCQrlRmMuQn9lPQstEBPUR2HacYrxIa8TIu+uNf5QsjamD7APRQBv+PfZn7etx4UlEdGTzYT5xn18LuzRfhIvs4YakNjrSdwjDcdqlVX6I/0KCiUPL/9fxFedrrXti6mKAJPv2eCf+3/kO5Ql6GtmFSFfhHcmj0vnTcnsMqecPwKG9nHB0lEs1DYNeShLZPiZyZJ/xH3kN7PGv7PZO5jR8QxPKbZACDOuggqfKCfkDa08yMO6e6svlOuPrpuOLKj9RvWQ1J21EujiMs8mKdmJJtmoH6dTQb+SnyPM15i+8lYt+ahaQqpsL2emrsK7SsHkjg+7RKcygeSxYkzd3ZK6GV5rE8XaVleZ1Ey4QKXOzpcfOwDwBdpnUiFf/cnirCPBFVzVh9Wpj9SVRjeadipHqGpkUMaeGVIDtYD9LGCpQY+m1wu16g1cJ6ne8yYTBmyX1qiV2Hl17ZlCznbFc3L7Y+2zkTG5e7+qKOKYKxD0kA4q5JlzCE4Rewe2R4ecqz3LcaCAzE6ZN3OoTetJa2MmAdtyrsqhrsMnOK9kVyHbbRdXtnAeqkOVCYQzdUDjaO+05hxA+sxFaQ5svJPLK0kzRQKFbv3X2Dv8kaX6qh7PaoLmPRpQ2/2oE4Kdp7LeN7VFGA5S6nU/7yoEelTgk94vJpgazCZqfAohNv3JxZhTX+WKgi+FyKJY4TyAzGOjOyGqx03M+SPxB8j/lXk2nn/tp4kk5Mq8xsgqrj2SoXuFBrREpzm+8AJhh2JugrUOUCxn/YiQhKnBTwipENoQsunJAQUyU8q7NJsB4DKLszffECRJm2MhNsr0dHXl/ErnmvCDf9DT2eZIhelChoAj6ogDoOzPrcUhP/Yo9+6+ywezIM+apWvepQWmeDyYWovwXRUL5S/z9HBn8+RrUEq/5y/xQpThJJlbfgbdm1JfGqXzQ5Z7OrLYVqf9mvaFgegbOxhhYNepkpfvE6OlaiIGPb01TSNiIA3RHF9vH3p1Chcbs44+2C/evvx53oKlE+jzHFMs1YRwncu/EXPepjq+cCNVcSxXMH71pGVlcsjZ5me3Ba0NAAPcJJwRpVtkigXdYkWKRgB7goJQ1rKW95ktESj/H+FtQgImXJSUE2gT+JzgUB9IO6RG4ZT4hM2UwQ4MsXHLo399OYn7JHbKf5L80TDO/IXFOZ4xtXJCpGSJ4dHMuwWjpuXtSlx3oc6gVdqp+f+MLM08C2s/1uKXgmm21B48kFkDG+yDSd36mGKj5V7R61tKMOt97+Z3aKuPw8DBedtZbYgIf/77UvQjaTqyL3Ws69NeWxQGZ/BLzEgxjIsvQwGX18KcwfGfGST2Zuq4GAb09Zxq8sekmLPyJgpGS/RlbcofBDaOwsARXapGxq6oe4HHV0eE/4gTaMt6uizrN+xlftPU2SBEalGX3SxxhcXLGcLY5h6FDiVsqBeFcUppcIJ0o3iaNqjSe2U5B0zyRPFAe6Fm82SKjbaWNv7fAGsp/y3ovu++KjqD37iyn8w0RK3y3hXnKZZ3BP6BylqkqXYmTI/D877lFqoVW/3UwRHpUfkgX+gZS6gniXjAxz4ws56E1Fg33ypxMKJW8srce1OUQxkgO/VR0+lI/iIOXmznIGm4wzEBwAdizMe3adMiXTJExCoBtd9rIoavOIJUKDD8SADxQHgbqwbrMUZFX7uN+PT1FJFMQi7Ss1zoZCSUtYRWJFjsdHcvIkiZzBNUWaDQHMtyHhUivVziKCZSskZR0M021WriWFBwVGABdPUdqnTmM7yksyy7/eKqhNFSyXgmFxRaRNJrzv1k4QAn/cUka4o7znmHYKamAw/S+9MhXYg+lhO7Jj9Ve+fmbUm9VHOJlSshwrrMQ/Mh0TBrjERwufu6osFvmeQv2y8Gf/FsV+Uj4Hz7QUp6V1sfWQx8IU16n/ad6d9/uoSJJ9yqfKp6PdY6QmUnw/ijqSDXvvOlb9Qby+2GBSN+db55uXvQ6AeMV9L9w7E3xsGz02tk46eqiomBFJsKJbeykq6DvsqzZdTzVVoYKCOsho4VFZ7JdATmifpslDuBl0joTAGKoX4laPK6saKMSLYZCUG4XaPRxLC621gowzJSBOpQApiZWCQnDRfreN+5q9OIHv2SKvNvk1uHVA3YUiLlyQ/qwIp9XIg8ScT/rw2Mr72aO583HqB/jXE9W9o9pYtHAOwViO7iJyGW2uheXTQAzX2dfbpOHoqf0adzzNmJTQpztLibbBa/VOt4HPCipZU4zjurkKm3zyNoALLVNcc91X6UzIUXh6rgkwBP8fiNVIS8YH59S9PInVWFwC27AohCHzW9I5lMba9+Oqe5aSfyDwHxO7miHGCp97ULEpakyessuSQQ0Yvgjd0C+az6lVV9IijqetV9l6sDtjsXs44/Od9keEeA51kL2xnwMh7xT6mQ9vJTesTGP1erYQzprIkazzxaXtPSjETIAnuFmPyq+eGPWl3NaluaSD4WA2GaLPu1hOLDGrSkRWZaa9YgTWE7icyYHyk9jNo8VmIogBuUafewCtIXBrhS94PJM62zWGkuCVRS5N6FyK8+8+Qp4eSf3eA0efLw5hD4vY23IkpQMgYjvTfGI/eHLh/Ain22UWYv3eA4/r3tAx/r4lZCnmjvp18GIXbgnZtaiEiWq3jTUvxNGiOseu16C7fAiQIE6VrFES9bJJJrm6q8DnKdbpO14sHeykSoHV8h2Utp82YeQnj0s+vedkyr86oViVPrGbVlGjc8vqhc2qnPsGAxWXHwZkIcPaqQOIBzub9GzA5I5jlZapHPomPyHLkfHLQUfMk8bGKChmNOOrAjIWK5v088eB7lPRia4D/KZEzho1zExATl6OaLXtBO/c1d/JY+DXQ32UscMvw5VwQBR66I7yCP6s+uErA9nWs/lfIfj3MU4oct9nH3M3D/rZ8bu0SUFZC5J2A4LedofaZnY8puTb0rVvYH0nBsjtPzhToQo67ixipS8EEP44NvhmE0dra63QkUo6I7cX4+e3sty8hbHaUPxyi+BRzXTm7PuGGLkKf4KbXAxwZ4ZNsOVqFtoduyZt74x5xbA5W6+pv5z/9tnhU9NJazPzQ5wnG4WuyVAWWrscvm/F1pmjRzhXUsM41xfRnlyJrpmqPsUJ1ShdTf6v/j6w2VJp2z7X48Qnbd+95TqYfWA2HNXWDmX6h/dftMPOVEsAr27Oirfl4c3goDWpN5DVJWW6MUd6vXClVk4f75d1lECclTZJSFh1hEg1TliQfdPkvoV9r593fXOBEQAigf9PaUSzOf4sqyJj9/IXxICs60q/lp8zjAqqzAG3cySmwPdV6/ibja1QSokB/JUXqJa5jmmM55t7GWc5WXTdSyQBr8Aq5W2wttHL54HY8FrCbkadDEd4iKJysuIJBkjRrCCAZr9hM9YhfvLhNIRmgcvxqKONaHgeRO/BhdO4XDwplj+T5X52GqbfWOd6z0uR5fDInJTRNuHrQ5mh3OxrfevRgA6hK+m5XGfZ0k0naQZSauZMZV3a7M5MTrOyRZ0iciUqTHmD889vTYbtOVDV4uo21onKHeg5iEq5hN3zLhRlmweAiS5fNsESQeKXEvCT67t0aK2wZHKXKz8hdlHiZh9i9NhBGvOPvOIyyq3D9bXet7ZYI4KChSnLctQ7LGGXeyRbJToX3ciK8t5tTCqEpKzJkoDXss="
        case .stagingUrl:
            secretKey = "4C18A036BD22DC37F7412BB648E6D88C554EFBB8C4D9194FE3C19FAF64BAA519"
            productionMode = false
            merchantID = "104104000000270"
            ProductionAndStagingPrivateKeyfor2c2p = "MIINHwYJKoZIhvcNAQcDoIINEDCCDQwCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAK5oNQEQSadi/YEQYtC3R1qsl7TUvXb5q02Pp3OqZXJkBzlFoq7jCN+1Cw5zia0PEdNW2wSy+Q+e4NHDpWk8ZIoC/bDzd68XdK8Cm/1ajmZxCfs0i0t0dcRXG0s1ksACaS89VuUjcdTHYMwnkEr8cRIlvCGINoevCFoUhSDcTgFc5Nn0hevDWvJuqsVRS6KiQJw/QTSRoiIx/rxzpXTINOagYbMd51Yo6+o7BwZorTHvFNU4QvecQcKJLokS1AhxMr2C0m1kQIH5Syp27u20X5z5sRQGqLPfFLhOXf35MBTmFjFt/bgPYnD5zHntUyGVTkksGbTdGRgD2muN82w/2pkwggtVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECHLlH8wI8ZiVgIILMFPXWK+POI98ZF8xoRvpKPmhc14juAoCxARCh1k0sW/brtusMygZ9MZccl3yVla7ha3wkOYSXPsh/X1BGpF+FBFE8Qhx84GLjwojlMuhCGPN4etCvBGl1P7j/RZchTxdUcX7LSpbs3dtlrqor1PK/byLrx/fmnNweTQzB8ebJ6rTg9krn+nVCGR4+Nma0j8EwmDfQe2+YSUXle51kDin5lcKcpKGgKhBxc1DfRc6kbN7C/VOv2ndfORF+HHDwKVJY+TSrkccQuC/Bk1EEYuwnHEQd5NTtEyQcDWrxSAEZ/YgyRgwc44yWFHtspFXbRXv3d6agtg6lesXztuiWDP6rcTSuZkC72crEH9okls7drjATxq7zQWl9JDsJR8OhBhMdvW63TfHK2SkUx9uwWnsL6VVJ4v/XHGxyg5JWbeg0EFoPHJK3d46IwTRuIG+1GTpSyS6Y7p0A0FYJ+66sShRSrOEerDfG8Rvjd3jXSyauw4qHqwHdDuLDfOQG2jRtk4vkvo4Vu0V/giUNnhZzqyk2WQGntAzEjH4ib1KuHyQdfsiQX8NEBK048m+yv6TmHoMtwzRkuXjpiUHzFkt5FHGn7CR2rT5t8xrhgy34ijXIjE6nlD7lV0TtblQpK3a4buNsEGF5JJsRhNvd8G3TDLkyQj1M/opLwmPf9u9p4CKixNmqa3IFVfNk41CuSPStmepg0nFRsaC6NkDYNfsIK0vLb8PiEhkgi9+DsjH+RByc80TXKXDTjEKGlTUc4pYhadVROfpV3hr9ZD+yVcMb2SE19LFELUgKLsWPcgywJvuNMK0fCju1ZgiLnJ5oADwizBhHCGQWYaAlsyCTWfKdC9Iy11ATrHwWlO6Q8OXNhOY/c3tUUtr0Leww1FIl1mIExRp5vlqizOVJC9vjqEPcPdzi80I8yZ4AtyZ0wtvHL7gUXn9lHYaeTT+TOPHUVf6DFaugiaQOzB+PGYhZFdIJzwCmn+tkawKLSSZrc9GvARK95wHLHVTS0jE0bHJho9uyt5QeUJtB8bnjtTazSO68nOFw6bcM8GYqO17D+2t+b2hk/d/n1aECF4hQ2KJOHS4KtcAVBrQYm623T1l7eBEhN3eFnm0s3RyKFbAgnAaLrbq/zqPDbZ4HWvZaBC1Iy2Qlkc3eOV40YFrZfQ+1D0JuvEXae3i/yIWq73pxmz0uYYhXGVn+hWh4r2Cx2RKd1I8Js1SFPm8gz6+PaYLMUed1RgSzsycB7MCqWDYeM04T7DDKb7KYwOQWmHQfOiLcq0p9G4itWuh73j0jmIBf0SNH4MNDhnR0bY2yQ9jbnNOFVr9RhmLoubtUbAebcKLVjTtmCcDm78Os4pa74x0TU+phHQY9eA4O+sNhc13gBer28CNh9VOgbKSAIAGusrShshqyfI22fwiPiJnQXhh2OlbG2BZNIxS0jPFwvyxOqwuq+0xln/lOBzm3Tg8WrrxfwHEHoEWzEjp+ppGaRg16RGb2qoEg96luGxv0rIyAx8lrphzPvt9a9G7V755a9Tnwm0VoaPud+x5j2VbbGEaWAULcU8u6q3Msiui8R2BN910PUIJunGrWHcNqodUXIVp1hlK4bi4tAnXc2fabV+zuyBBRktBaLH7f6J1Ico4ZOt9YadgewIvvynPGGzGVrWG3FPNbazoVbXncKiWFEKgScQJITsKy2Wb6sMvINaZUV28vNXb0S4MYkXC1RhuyAsWslslzkTu1po2Y0QdgG06OEVmBALVcNZ0A5k01UU3cwWgPNL6RsDlQvoS0bKBZoFbIQGWsbc3W95eQX3PSVA4Lb0VOyCSpcsLLGSImm59l920A4JxJj4Bd8zu7f8B0p034oK0xsmE9bWzrmu8VDhPqmXyM9ojf0iGkTKvAkjtdz+ePKjmU3ufrW8wdXRl5cjgRXEU0hXpHn+a73hClXpyusahgRE9TyGy6u3ho9s+zELEtIRhWktFKBCbMP62KKiXgFaTRPc3YhCY1poS2xJU3jFcpfw4YzNiX5S2/Bp39jn589jaVfiDA/s4b9Ss8vBAPvSSFO+oCVfsoUzQIRkbaxFnTH7sGtP6ASDg7nWHF67NHtOlyalCrZEh8DYAwoLrpoDZg4W5QzS/ET2/eHaNzfclknlzA9rWjKAGUt9pCi37eWquPb3RCoaUU5+178Z1S3hIYEE2kx+sXFD0+xgZ4Rz5mdagsEgxNuTQXnThn/jPAMXz8zwi0CYgl0IgVoOvUxsvuHiQbZ0zNz5w2DRonBSTO9o1SRSZrxyv5GfXsfdx3CsyKNpheBVFgRPRPpX459QZBZ3/M642BLJRb+oqUqnUNDSN+DxjDtcVdF83RMJqTR5ej7qi/oQb7uMqlDC3fwE3mPJ7BHjkdZkd4tKvj5UFwjlVCuRKtFlu+G1k/faoAtu9qXabdh95Vgv/nFaUBACdy+ZaHMDAJESxdhsksYfzYCEvnBkmX4hH1htd/4k4uG7mdfNxnZ0u/wV6FhvPp+996g9hRMPAhfQvg7Q61eC5FbgPwYzN/TYmzBQA1LqiL63opEYvFX0D/FalS+kpPzVnttT7R1Xg4czAi+bXDH5//yrgK4IJO0KZIHK9RiOYHPyWU0Ag3JTsCQHgdxhrSX1d6S/7/TVuCWKscPN/xiCjb4p5hOyXuaOGMTFARfuRB3x/5/sgfzfxEVQwfb0DDBVsOO9rh6GzIyt5hIh8EFoAzjxeJjylCgyLx0h71ycYiH2jm5IPAQ41+jMC/YTLY0aKdSxDXePR+2f1ATGS9dng+UN0qvLaLb8eIxWDWh83peOgAZCKcmRnBt+WPO3jBLiuxvV7RIjSg0dgyqBceGaVO3WMJZ0lXsMjrknEkBx4NNFRAxtmlQRVXTx8at7nSwhHZf0yHsPRjqHYyxIOx0lRf8JG/YlMNVTarR50XrWXf5Y6EAsY2gm2Q1dgblOAzIRtJz3ZX1ijfKktQKXt9j3K2K+7CQdqHqbFKWl40rAq4EvX/X35wq6rm4AX8sO9qjAVHjtpY50SmMpXLGWoxRreNsh3Xl/Nb2yiN/luiljA/FFZO11++MB9mtS+RVxl0zI9MH99dDhrLmC46azc5aSFXuR6b2twjntsNr04ys+KFi6iJU4vMTLWAOtLQMqtWNRbucNJsoMX+gQqqnqHrn7YaO5rE7GJIz09DxWjAcE2DHqJDFwmB42rmB5d8AH8DQXD7clcZ4hk2f+wF5G7JrwXRHalFsHpb8mgr6lJBJhPkJ5XS4OVR/QAg4nx0qXGadPeaYKx6VlCaHpHiQuy4+lt7gZSgeplK37Os6tPlvdkSmkoXN2aX4XnSlSs/fGfxV/P8jCUVfJVpHJDC/ORcg90AfiuH6a5K2/zpJscadexZADa2rNeYnJ3OGZ0HAJOHLAjt3vTPpHa9DqWwPn8W7jJNkwIiGaritWnrKQc0KIdlfaybAAkghr+mOKiZYH9NLTP0ik1wVtefhdcjR0nY5dn+vivKBMUhW7F2LrG8xIMKE5O0FELGyhGA7AjCxOm3oOq4cPkPax/5nldje/LlT6cM/xdQ5Af0rBASvdZrmWHCV5bIv4c1EeWD1R6aBY7XPJ6FvsJWBJ1rPGPtjHvU/g8+c/s+x86TvAcOstp7p+wLTWGtqE+wo4UHl8JvrzKM7cxSjIszqvM9rTR8IsMnfqk4MQHczytTzgVebrxVZJs6h2iuwIUVCus6EA1uw9wmReoxIsMfFx9yNz9GmAQcPiVScCGKfGJGkMjaDUnQSia8dAJDP+WvITXScEyIUqH2OlzGRrb6SSbh9uYXwwAeC0fPg4zYOpTOrfC8lTW8re30iuMcNGi"
        }
    }
    
    func urlEnable(urlTo: UrlTypes) {
        switch urlTo {
        case .testUrl:
            base_url                         = "http://69.160.4.149:1003/api"
            base_url_sendSMS                   = "http://69.160.4.149:1003/api/customer/VerifyMobileNumber"
            NST_KEY_Value               = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value            = "bm5xS6V5"
        case .stagingUrl:
            base_url                         = "http://69.160.4.149:1003/api"
            base_url_sendSMS                   = "http://69.160.4.149:1003/api/customer/VerifyMobileNumber"
            NST_KEY_Value               = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value            = "bm5xS6V5"
        case .productionUrl:
            base_url                         = "https://zayok.com/api"
            base_url_sendSMS                   = "https://zayok.com/api/customer/VerifyMobileNumber"
            NST_KEY_Value               = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value            = "bm9wS2V5"
        }
    }
    
    //    var NST                              = "eyJhbGciOiJIUzM4NCJ9.eyJOU1RfS0VZIjoibm01d1UzUmhkR2x2YmxSdmEyVnUifQ.WDWHYv4Z_iH6yxaZlpvfZ21b1nKXb1MDOUzQ-wfa3mG1dR9xs0k3zyq0S3Wvr6Gm"
    //    var Token                            =  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJDdXN0b21lcklkIjo1MTQ1LCJleHAiOjE1NTQxNTAyNDAuMH0.B0p62K8fwnQ-NlU2UvyHCIjpLA8GGC_XGLLPtDAWtck"
    //    var DeviceId                         =  "e2c6dc5905805b36"
    
    var baseURL                         = "https://apps.nop-station.com/api"
    var defaultBaseUrl                  = "https://apps.nop-station.com/api"
    
    
    class var sharedInstance:APIManagerClient {
        return Static.instance
    }
    private struct Static {
        static let instance = Static.createInstance()
        static func createInstance()->APIManagerClient {
            
            let myInstance = APIManagerClient()
            return myInstance
        }
    }
    override init() {
        
    }
    func gethomePageProduct() -> [GetHomePageProducts] {
        return self.homePageProductArray
    }
}

/*
 Name of the claim: "nst"
 Claim value: {"NST_KEY" : "ABCD"}
 Secret: "DEEF"
 Network header name of the encrypted token: "NST"
 */
