//
//  AddAddress.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/8/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

enum tapGestureType {
    case townShip
    case state
    case country
    case none
}

enum LiftOptions {
    case yesLift
    case noLift
    case none
}

protocol VMAddNewAddressProtocol: class {
    func addNewAddress()
}

class AddAddress: VMLoginBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var fullNameTF: SubSkyFloatTextField! {
        didSet {
            self.fullNameTF.placeholder = self.fullNameTF.placeholder?.localized
        }
    }
    @IBOutlet weak var address1TF: SubSkyFloatTextField! {
        didSet {
            self.address1TF.placeholder = self.address1TF.placeholder?.localized
        }
    }
    @IBOutlet weak var address2TF: SubSkyFloatTextField! {
        didSet {
            self.address2TF.placeholder = self.address2TF.placeholder?.localized
        }
    }
    @IBOutlet weak var houseNoTF: SubSkyFloatTextField! {
        didSet {
            self.houseNoTF.placeholder = self.houseNoTF.placeholder?.localized
        }
    }
    @IBOutlet weak var floorTF: SubSkyFloatTextField! {
        didSet {
            self.floorTF.placeholder = self.floorTF.placeholder?.localized
        }
    }
    @IBOutlet weak var roomNoTF: SubSkyFloatTextField! {
        didSet {
            self.roomNoTF.placeholder = self.roomNoTF.placeholder?.localized
        }
    }
    @IBOutlet weak var phNumberTF: UneditableTextField! {
        didSet {
            self.phNumberTF.text = "+95 09"
            self.phNumberTF.placeholder = self.phNumberTF.placeholder?.localized
        }
    }
    @IBOutlet weak var stateTitleLabel: UILabel! {
        didSet {
            self.stateTitleLabel.text = self.stateTitleLabel.text?.localized
            self.stateTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var liftoptionLabel: UILabel!{
        didSet {
            self.liftoptionLabel.text = self.liftoptionLabel.text?.localized
            self.liftoptionLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var townshipTitleLabel: UILabel! {
        didSet {
            self.townshipTitleLabel.text = self.townshipTitleLabel.text?.localized
            self.townshipTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var stateValueLabel: UILabel! {
        didSet {
            self.stateValueLabel.text = self.stateValueLabel.text?.localized
            self.stateValueLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var townshipValueLabel: UILabel! {
        didSet {
            self.townshipValueLabel.text = self.townshipValueLabel.text?.localized
            self.townshipValueLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var liftYesBtn: UIButton! {
        didSet {
            liftYesBtn.setTitle((liftYesBtn.titleLabel?.text ?? "").localized, for: .normal)
            liftYesBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var liftNoBtn: UIButton! {
        didSet {
            liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            liftNoBtn.setTitle((liftNoBtn.titleLabel?.text ?? "").localized, for: .normal)
            liftNoBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var continueBtn: UIButton! {
        didSet {
            continueBtn.setTitle((continueBtn.titleLabel?.text ?? "").localized, for: .normal)
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    //Color
    @IBOutlet weak var colorSelectButton: UIButton! {
        didSet {
            colorSelectButton.setTitle((colorSelectButton.titleLabel?.text ?? "").localized, for: .normal)
            colorSelectButton.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var colorSelectLabel: UILabel! {
        didSet {
            colorSelectLabel.text = "Select rope color".localized
            self.colorSelectLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var colorSelectValueLabel: UILabel! {
        didSet {
            colorSelectValueLabel.text = "Black".localized
            self.colorSelectValueLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var selectColorTableView: UITableView! {
        didSet {
            self.selectColorTableView.isHidden = true
            self.selectColorTableView.layer.borderWidth = 1.0
            self.selectColorTableView.layer.borderColor = UIColor.gray.cgColor
        }
    }
    @IBOutlet weak var ropeStack: UIStackView!
    @IBOutlet weak var constraintTopMobileNumber: NSLayoutConstraint!
    
    //MARK: - Properties
    var aPIManager = APIManager()
    var screenFrom: String? = "Other"
    var defaultAddress: GenericBillingAddress?
    var liftOption: LiftOptions = .none
    var countryObject = (code: "60", name: "Myanmar")
    var selectedState = (code: "", name: "")
    var townshipObject = (code: "", name: "")
    weak var delegate: VMAddNewAddressProtocol?
    let colorArray = ["Black", "Red", "Blue", "Green", "Yellow"]
    var result: (min: Int, max: Int, operator: String, isRejected: Bool, color: String)?
    
    private var continueView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240.0/255.0, alpha: 1)
        btn.setTitle("CONTINUE".localized, for: .normal)
        if let myFont = UIFont(name: "Zawgyi-One", size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(submitFromToolbar), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Address".localized
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        initializeView()
        getBillform()
        setupToolbar()
        self.selectedYesLiftBtn()
        if let screenFrm = screenFrom {
            if screenFrm == "DashBoard" || screenFrm == "Cart" {
                self.addBack()
            } else if screenFrm == "Edit Address" {
                self.navigationItem.title = "Edit Address".localized
                self.addBack()
                self.updateUI()
            } else if screenFrm == "Other" {
                self.addBack()
                loadFreshScreen()
            }
            else if screenFrm == "DashBoard / "{
                self.addBack()
                loadFreshScreen()
            }
        }
    }
    
    
    func addBack(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
    }
    
    
    override func hideKeyboard() {
        self.view.endEditing(true)
        self.selectColorTableView.isHidden = true
    }
    
    //MARK: - Target Methods
    @objc func dismissScreen() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "DashBoard":
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            case "Cart","Other","Edit Address":
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            default:
                DispatchQueue.main.async {
                    //                    self.navigationController?.popViewController(animated: true)
                    self.performSegue(withIdentifier: "unwindAddToHome", sender: self)
                }
            }
        }
    }
    
    @objc func submitFromToolbar() {
        addNewAddressAction(UIButton())
    }
    
    //MARK: - Methods
    private func updateUI() {
        if let defaultAddr = defaultAddress {
            fullNameTF.text = defaultAddr.firstName
            address1TF.text = defaultAddr.address1
            address2TF.text = defaultAddr.address2
            if let houseNo = defaultAddr.houseNo {
                houseNoTF.text = houseNo.replacingOccurrences(of: "House No ", with: "")
            }
            if let floorNo = defaultAddr.floorNo {
                floorTF.text = floorNo.replacingOccurrences(of: "Floor No ", with: "")
            }
            if let roomNo = defaultAddr.roomNo {
                roomNoTF.text = roomNo.replacingOccurrences(of: "Room No ", with: "")
            }
            townshipValueLabel.text = defaultAddr.cityName
            stateValueLabel.text = defaultAddr.stateProvinceName
            if let lift = defaultAddr.isLift {
                if lift == 1 {
                    liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                    liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
                    liftOption = .yesLift
                    ropeStack.isHidden = true
                    constraintTopMobileNumber.constant = -66
                    self.view.layoutIfNeeded()
                } else {
                    liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
                    liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                    liftOption = .noLift
                    ropeStack.isHidden = false
                    constraintTopMobileNumber.constant = 19
                    self.view.layoutIfNeeded()
                }
            } else {
                liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
                liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
                liftOption = .none
                self.selectedYesLiftBtn()
            }
            if let phNumber = defaultAddr.phoneNumber, phNumber.count > 0 {
                if phNumber.hasPrefix("0095") {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 6), with: "+95 09")
                } else if phNumber.hasPrefix("+95") {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95 09")
                } else {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 1), with: "+95 ")
                }
            } else {
                phNumberTF.text = "+95 09"
            }
            
            self.colorSelectValueLabel.text = defaultAddr.ropeColor
            self.townshipObject = (code:"\(defaultAddr.cityId ?? 0)", name: (defaultAddr.cityName ?? ""))
            self.selectedState = (code: "\(defaultAddr.stateProvinceId ?? 0)", name: (defaultAddr.stateProvinceName ?? ""))
        }
    }
    
    private func phNoToUIPhNo(phStr: String) -> String {
        if phStr.hasPrefix("0095") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95 09")
        } else if phStr.hasPrefix("+95") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95 09")
        } else if phStr.hasPrefix("09"){
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 0), with: "+95 ")
        } else if phStr.hasPrefix("9") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 0), with: "+95 0")
        } else {
            return "+95 09"
        }
    }
    
    private func initializeView() {
        if let firstName = VMLoginModel.shared.firstName {
            self.fullNameTF.text = firstName
        }
    }
    
    private func setupToolbar() {
        fullNameTF.inputAccessoryView = continueView
        address1TF.inputAccessoryView = continueView
        address2TF.inputAccessoryView = continueView
        houseNoTF.inputAccessoryView = continueView
        floorTF.inputAccessoryView = continueView
        roomNoTF.inputAccessoryView = continueView
        phNumberTF.inputAccessoryView = continueView
    }
    
    func showViewController() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "DashBoard":
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            case "Cart":
                self.showPaymentScreen()
            case "Other", "Edit Address":
                DispatchQueue.main.async {
                    self.delegate?.addNewAddress()
                    self.navigationController?.popViewController(animated: true)
                }
            default:
                break
            }
        }
    }
    
    private func isMandatoryFilled() -> Bool {
        var isFilled = true
        if fullNameTF.text == "" {
            //            self.showAlert(status: "", messageText: "Full Name is Required".localized)
            AppUtility.showToastlocal(message: "Full Name is Required".localized, view: self.view)
            isFilled = false
        } else if let fName = fullNameTF.text, fName.count < 3 {
            //            self.showAlert(status: "", messageText: "Exact Full Name is Required".localized)
            AppUtility.showToastlocal(message: "Exact Full Name is Required".localized, view: self.view)
            isFilled = false
        } else if address1TF.text == "" {
            //            self.showAlert(status: "", messageText: "Street Address is Required".localized)
            AppUtility.showToastlocal(message: "Street Address is Required".localized, view: self.view)
            isFilled = false
        } else if let address = address1TF.text, address.count > 0 && address.count < 4 {
            //            self.showAlert(status: "", messageText: "Exact Address is Required".localized)
            AppUtility.showToastlocal(message: "Exact Address is Required".localized, view: self.view)
            isFilled = false
        } else if stateValueLabel.text == "" || stateValueLabel.text == "Select State/ Division" {
            //            self.showAlert(status: "", messageText: "State/ Division is Required".localized)
            AppUtility.showToastlocal(message: "State/ Division is Required".localized, view: self.view)
            isFilled = false
        } else if townshipValueLabel.text == "" || townshipValueLabel.text == "Select Township" {
            //            self.showAlert(status: "", messageText: "Township is Required".localized)
            AppUtility.showToastlocal(message: "Township is Required".localized, view: self.view)
            isFilled = false
        } else if let ph = phNumberTF.text, ph.count > 5 {
            let phno = String(ph.dropFirst().dropFirst().dropFirst().dropFirst())
            if let safeResult = result {
                if safeResult.isRejected || phno.count < (safeResult.min) || phno.count > (safeResult.max) {
                    //                    self.showAlert(status: "", messageText: "Invalid phone number".localized)
                    AppUtility.showToastlocal(message: "Invalid phone number".localized, view: self.view)
                    isFilled = false
                }
            } else {
                let result = myanmarValidation(phno)
                if result.isRejected || phno.count < (result.min) || phno.count > (result.max) {
                    //                    self.showAlert(status: "", messageText: "Invalid phone number".localized)
                    AppUtility.showToastlocal(message: "Invalid phone number".localized, view: self.view)
                    isFilled = false
                }
            }
        }
        return isFilled
    }
    
    private func showPaymentScreen() {
        if let addAddressViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "Payment_ID") as? Payment {
            self.navigationController?.pushViewController(addAddressViewController, animated: true)
        }
    }
    
    private func showTapGestureVC(type: tapGestureType) {
        self.view.endEditing(true)
        if let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "StateTownshipListing") as? StateTownshipListing {
            viewController.delegate = self
            viewController.selectedType = type
            if type == .state {
                viewController.countryObject = self.countryObject
            } else if type == .townShip {
                viewController.countryObject = self.selectedState
            }
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    private func isStateSelected() -> Bool {
        if self.selectedState.name == "" {
            return false
        }
        return true
    }
    
    private func loadFreshScreen() {
        fullNameTF.text = VMLoginModel.shared.firstName ?? ""
        phNumberTF.text = phNoToUIPhNo(phStr: defaultAddress?.phoneNumber ?? "")
        self.ropeStack.isHidden = true
        self.liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
        self.liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        self.constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Button Action Methods
    @IBAction func selectColorButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AddAddressToRopeColorVCSegue", sender: self)
        //        if selectColorTableView.isHidden {
        //            selectColorTableView.isHidden = false
        //        } else {
        //            selectColorTableView.isHidden = true
        //        }
    }
    
    @IBAction func addNewAddressAction(_ sender: UIButton) {
        self.continueBtn.isUserInteractionEnabled = false
        if isMandatoryFilled() {
            updateUserProfile()
            self.continueBtn.isUserInteractionEnabled = true
        } else {
            self.continueBtn.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func stateTapAction(_ sender: UITapGestureRecognizer) {
        showTapGestureVC(type: tapGestureType.state)
    }
    
    @IBAction func townshipTapAction(_ sender: UITapGestureRecognizer) {
        if !isStateSelected() {
            let alert  = UIAlertController(title: "", message: "Please select State/ Division".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        showTapGestureVC(type: tapGestureType.townShip)
    }
    
    @IBAction func liftYesButtonAction(_sender: UIButton) {
        liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .yesLift
        ropeStack.isHidden = true
        constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    func selectedYesLiftBtn() {
        liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .yesLift
        ropeStack.isHidden = true
        constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    @IBAction func liftNoButtonAction(_sender: UIButton) {
        liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .noLift
        ropeStack.isHidden = false
        constraintTopMobileNumber.constant = 19
        self.view.layoutIfNeeded()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddAddressToRopeColorVCSegue" {
            if let vc = segue.destination as? RopeColorVC {
                vc.delegate = self
            }
        }
    }
}

extension AddAddress: RopeColorDelegate {
    func updateRopeColor(data: String) {
        colorSelectValueLabel.text = data
    }
}

//MARK: - StateTownshipDelegate
extension AddAddress: StateTownshipDelegate {
    func selectedStateTownshipMethod(type: tapGestureType, selectedText: (String, String)) {
        switch type {
        case .state:
            if self.selectedState.name != selectedText.0 {
                self.selectedState = selectedText
                self.stateValueLabel.text = self.selectedState.name
                self.townshipValueLabel.text = "Select Township"
                self.townshipObject = (code: "", name: "")
                self.showTapGestureVC(type: .townShip)
            }
        case .townShip:
            self.townshipObject = selectedText
            self.townshipValueLabel.text = selectedText.1
            self.houseNoTF.becomeFirstResponder()
        case .none, .country:
            break
        }
    }
}

//MARK: - Api
extension AddAddress {
    private func updateUserProfile() {
        let urlString = String(format: "%@/checkout/checkoutsaveadress/1", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = AppUtility.JSONStringFromAnyObject(value: self.addNewAddrParams() as AnyObject)
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true, addAddress: "Add Address") { [weak self](response, success, _) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["StatusCode"] as? Int, code == 200 {
                            self?.showAlert(status: "Success".localized)
                        } else {
                            self?.showAlert(status: "Failure".localized, messageText: "Try again".localized)
                        }
                    }
                }
            }
        }
    }
    
    func addNewAddrParams() -> [[String:Any]] {
        var paramDicArray = [[String: Any]]()
        var paramDic            = [String:Any]()
        paramDic["key"] = "BillingNewAddress.FirstName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.LastName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Email"
        paramDic["value"] = VMLoginModel.shared.email ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Company"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CountryId"
        paramDic["value"] = countryObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.StateProvinceId"
        paramDic["value"] = selectedState.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CityId"
        paramDic["value"] = townshipObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address1"
        paramDic["value"] = address1TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address2"
        paramDic["value"] = address2TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.PhoneNumber"
        if let mobNo = phNumberTF.text {
            paramDic["value"] = (mobNo as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "0095")
            paramDicArray.append(paramDic)
        }
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FaxNumber"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.HouseNo"
        paramDic["value"] = "House No \(houseNoTF.text ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FloorNo"
        paramDic["value"] = "Floor No \(floorTF.text ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RoomNo"
        paramDic["value"] = "Room No \(roomNoTF.text ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RopeColor"
        paramDic["value"] = colorSelectValueLabel.text
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.IsLiftOption"
        paramDic["value"] = (liftOption == .yesLift) ? true : ((liftOption == .noLift) ? false : nil)
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Longitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLongitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Latitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLatitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Id"
        if let screenFrm = self.screenFrom, screenFrm == "Edit Address", let defaultAdd = defaultAddress{
            paramDic["value"] = "\(defaultAdd.id ?? 0)"
        } else {
            
        }
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        return paramDicArray
    }
    
    private func showAlert(message: String) {
        if message == "Your details updated successfully".localized {
            DispatchQueue.main.async {
                
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: message, withDescription: "Updated successfully".localized, onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.dismiss(animated: true, completion: nil)
                })
                alertVC.addAction(action: [ok])
            }
        } else {
            self.showAlert(alertTitle: "Warning!".localized, description: message)
        }
    }
    
    
    private func showAlert(status: String, messageText: String? = "") {
        DispatchQueue.main.async {
            if status == "Success" {
                var message = ""
                if let screenFrm = self.screenFrom, screenFrm == "Edit Address" {
                    message = "Delivery Address Successfully Updated."
                } else {
                    message = "Address added successfully"
                }
                VMLoginModel.shared.addressAdded = true
                //                self.showAlert(alertTitle: "Success".localized, description: message.localized)
                
                
                
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Success", withDescription: message.localized, onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.showViewController()
                })
                alertVC.addAction(action: [ok])
                
                
                //                let alert  = UIAlertController(title: "Success".localized, message: message.localized, preferredStyle: .alert)
                //                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (alert) in
                //                    self.showViewController()
                //                }))
                //                self.present(alert, animated: true, completion: nil)
                
            } else {
                //                self.showAlert(alertTitle: "Required".localized, description: messageText?.localized ?? "")
                
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "", withDescription: messageText?.localized ?? "", onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    
                })
                alertVC.addAction(action: [ok])
                
                //                let alert  = UIAlertController(title: "", message: messageText, preferredStyle: .alert)
                //                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                //                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func getBillform() {
        let urlString = String(format: "%@/checkout/billingform", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self] (response, success, _) in
            
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        if let newAddress = json["NewAddress"] as? Dictionary<String, Any> {
                            if let firstObj = newAddress["AvailableCountries"] as? NSArray, firstObj.count > 1 {
                                if let countryObj = firstObj.lastObject as? Dictionary<String, Any> {
                                    self?.countryObject.code = countryObj["Value"].safelyWrappingString()
                                    self?.countryObject.name = countryObj["Text"].safelyWrappingString()
                                }
                            }
                        }
                    } else {
                        self?.showAlert(status: "Failure".localized, messageText: "No Records found".localized)
                    }
                }
            }
        }
    }
}

//MARK: - UITextFieldDelegate
extension AddAddress: UITextFieldDelegate, PhValidationProtocol {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case self.fullNameTF:
            let onlyAlphabetsSpace = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            let invertedSet = onlyAlphabetsSpace.inverted
            let filteredSet = string.components(separatedBy: invertedSet).joined(separator: "")
            if string != filteredSet {
                return false
            }
            if text.count == 1 {
                if string == " " {
                    return false
                }
            }
            if text.count > 40 {
                return false
            }
            if string == "" || string == " " {
                let _ = Validations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                return false
            }
            break
        case self.phNumberTF:
            if !text.hasPrefix("+95 09") {
                return false
            }
            if text.count > 17 {
                self.view.endEditing(true)
                return false
            }
            let phno = String(text.dropFirst().dropFirst().dropFirst().dropFirst())
            result = myanmarValidation(phno)
            //            print("PayToValidations getNumberRangeValidation: \(getNumberRangeValidation("0968313693"))")
            //            print("myanmar Validation method: \(myanmarValidation("0968313693"))")
            if result!.isRejected {
                //                self.showAlert(status: "", messageText: "Invalid mobile number".localized)
                AppUtility.showToastlocal(message: "Invalid mobile number".localized, view: self.view)
                //                self.view.endEditing(true)
                return false
            }
            if text.count  > (result!.max + 3) {
                textField.text = text 
                self.view.endEditing(true)
                return false
            }
        //            return  self.phonValidation(textField, shouldChangeCharactersIn: range, replacementString: string)
        case address1TF, address2TF:
            if text.count > 80 {
                return false
            }
        case houseNoTF, floorTF, roomNoTF:
            if text.count > 6 {
                return false
            }
        default:
            break
        }
        return true
    }
    
    
    
    
    /*
     func phonValidation(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String)-> Bool{
     
     let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
     let mobileNumberAcceptableCharacters = "0123456789"
     let objectValidation = PayToValidations().getNumberRangeValidation(text)
     let textCount  = text.count
     let validCount = objectValidation.max
     
     let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
     let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
     
     if string != filteredSet {
     return false
     }
     
     let char = string.cString(using: String.Encoding.utf8)!
     let isBackSpace = strcmp(char, "\\b")
     
     if (isBackSpace == -92) {
     if textField.text == "09" {
     
     return (countryObject.code == "+95") ? false : true
     }
     
     }
     
     if countryObject.code == "+95" {
     if !text.hasPrefix("09") {
     return false
     }
     }
     
     // Normal Validations
     if PayToValidations().checkRejectedNumber(prefix: text) == true {
     return rejectNumberAlgorithm()
     }
     
     
     
     
     if countryObject.code == "+95" {
     if validCount == textCount {
     self.phNumberTF.text = text
     
     return false
     }
     if textCount > validCount {
     
     return false
     }
     }
     return true
     }
     
     
     private func rejectNumberAlgorithm() -> Bool {
     return false
     }
     */
}

//MARK: - UITableViewDelegate, UITableViewDataSource
//extension AddAddress: UITableViewDelegate, UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 40.0
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return colorArray.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
//        cell.textLabel?.text = colorArray[indexPath.row].localized
//        cell.textLabel?.font = UIFont(name: appFont, size: 15.0)!
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        colorSelectValueLabel.text = colorArray[indexPath.row]
//        self.selectColorTableView.isHidden = true
//    }
//}

//MARK: - UneditableTextField
class UneditableTextField: SubSkyFloatTextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: 90 , height: bounds.height)
    }
    
}

class SubSkyFloatTextField: SkyFloatingLabelTextField {
    
}

class Validations {
    class func trimSuccessiveSpaces( textField: UITextField, range: NSRange, string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        var commaRestrStr = text
        
        if string == " " {
            var boundValue = range.upperBound + 1
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
                boundValue = range.lowerBound
            }
            if text.contains("  ") {
                boundValue = range.lowerBound
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if range.upperBound == range.lowerBound {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            } else {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: range.lowerBound) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            }
        } else if string == "" {
            let boundValue = range.lowerBound
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
        }
        return false
    }
}



