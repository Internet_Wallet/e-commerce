//
//  SDKBuildType.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 1/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

enum SDKBuildType:String {
    case CORE_SDK = "PGW Core SDK (Non-UI)"
    case CORE_UI_SDK = "PGW Core UI SDK (UI)"
}
