//
//  FeatureCategoryHelper.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit

class FeatureCategoryHelper {
    func getList(sdkBuildType:SDKBuildType) -> [Feature] {
        
        var arrayList:[Feature] = [Feature]()
        
        if sdkBuildType == SDKBuildType.CORE_SDK {
            
            arrayList.append(Feature(id: 1, icon: "card_ico", name: "Credit card payment (Non 3DS)", desc: "Normal payment without 3DS"))
            arrayList.append(Feature(id: 2, icon: "card_ico", name: "Credit card payment (3DS)", desc: "Normal payment with 3DS"))
            arrayList.append(Feature(id: 3, icon: "card_ico", name: "Card tokenization payment", desc: "Normal payment with card tokenization"))
            arrayList.append(Feature(id: 4, icon: "card_ico", name: "Credit card payment with card token", desc: "Normal payment with card token"))
            arrayList.append(Feature(id: 5, icon: "card_ico", name: "IPP (Installment payment plan)", desc: "Installment payment"))
            arrayList.append(Feature(id: 6, icon: "card_ico", name: "RPP (Recurring payment plan)", desc: "Recurring payment"))
            
            arrayList.append(Feature(id: 7, icon: "baseline_list_alt_black", name: "Retrieve payment options", desc: "For retrieve enabled payment channel list"))
            arrayList.append(Feature(id: 8, icon: "baseline_list_alt_black", name: "Retrieve payment option details", desc: "For retrieve payment details by payment channel"))
        } else {
            //Unknown sdk build type, so do nothing.
        }
        
        return arrayList
    }
    
    func redirect(viewController:UIViewController, feature:Feature) {
        
        switch feature.id {
        case 1:
            let creditCardNon3DS:CreditCardNon3DS = CreditCardNon3DS(viewController: viewController)
            creditCardNon3DS.execute()

            break
        case 2:
            let creditCard3DS:CreditCard3DS = CreditCard3DS(viewController: viewController)
            creditCard3DS.execute()
            
            break
        case 3:
            let creditCardTokenization:CreditCardTokenization = CreditCardTokenization(viewController: viewController)
            creditCardTokenization.execute()
            
            break
        case 4:
            let creditCardWithToken:CreditCardWithToken = CreditCardWithToken(viewController: viewController)
            creditCardWithToken.execute()
            
            break
        case 5:
            let installmentPaymentPlan:InstallmentPaymentPlan = InstallmentPaymentPlan(viewController: viewController)
            installmentPaymentPlan.execute()
            
            break
        case 6:
            let recurringPaymentPlan:RecurringPaymentPlan = RecurringPaymentPlan(viewController: viewController)
            recurringPaymentPlan.execute()
            
            break
        case 7:
            let retrievePaymentOptions:RetrievePaymentOptions = RetrievePaymentOptions(viewController: viewController)
            retrievePaymentOptions.execute()
            
            break
        case 8:
            let retrievePaymentOptionDetails:RetrievePaymentOptionDetails = RetrievePaymentOptionDetails(viewController: viewController)
            retrievePaymentOptionDetails.execute()
            
            break
        default:
            let creditCardNon3DS:CreditCardNon3DS = CreditCardNon3DS(viewController: viewController)
            creditCardNon3DS.execute()
            
            break
        }
    }
    
}

