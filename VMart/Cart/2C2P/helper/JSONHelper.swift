//
//  JSONHelper.swift
//  PGWSDK
//
//  Created by yan feng liu on 9/6/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import Foundation

class JSONHelper {
    //String
    static func option(input:String?, defaultValue:String) -> String {
        if input == nil {
            return defaultValue
        } else {
            return input!
        }
    }
    
    //Int
    static func option(input:Int?, defaultValue:Int) -> Int {
        if input == nil {
            return defaultValue
        } else {
            return input!
        }
    }
    
    //Double
    static func option(input:Double?, defaultValue:Double) -> Double {
        if input == nil {
            return defaultValue
        } else {
            return input!
        }
    }

    //[String:Any]
    static func option(input:[String: Any]?) -> [String: Any] {
        if input == nil {
            return [String: Any]()
        } else {
            return input!
        }
    }
    
    //[[String: Any]]
    static func option(input:[[String: Any]]?) -> [[String: Any]] {
        if input == nil {
            return [[String: Any]]()
        } else {
            return input!
        }
    }

}
