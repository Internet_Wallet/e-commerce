//
//  MerchantServerSimulator.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class MerchantServerSimulator {
    
    /**
     * IMPORTANT : MerchantServerSimulator just for simulator how merchant server work on demo app, and DO NOT call payment token API directly from your application.
     */
    
    private var httpHelper:HTTPHelper
    private var paymentGatewayHelper:PaymentGatewayHelper
    private var stringHelper:StringHelper
    
    init() {
        
        httpHelper = HTTPHelper()
        paymentGatewayHelper = PaymentGatewayHelper()
        stringHelper = StringHelper()
    }
    
    /**
     * @param paymentTokenRequest
     * @param success
     * @param failure
     *
     * Reference : https://developer.2c2p.com/docs/mobile-v4-how-to-integrate
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-token-request-parameters-
     */
    func getPaymentToken(paymentTokenRequest:PaymentTokenRequest, success:@escaping response, failure:@escaping failure) {
        
        //Request information
        let apiVersion:String = "10.0"
        let nonceStr:String = UUID
        
        //Merchant's account information
        let mid:String = "JT01" //Get MerchantID when opening account with 2C2P
        let secretKey:String = "7jYcp4FxFdf0" //Get SecretKey from 2C2P PGW dashboard
        
        //Transaction information
        let desc:String = "2 days 1 night hotel room"
        let invoiceNo:String = String(Int(Date().timeIntervalSince1970))
        let currencyCode:String = "SGD"
        let amount:String = "000000001000"
        
        //Set common values for all payments
        paymentTokenRequest.version = apiVersion
        paymentTokenRequest.nonceStr = nonceStr
        paymentTokenRequest.merchantID = mid
        paymentTokenRequest.desc = desc
        paymentTokenRequest.invoiceNo = invoiceNo
        paymentTokenRequest.currencyCode = currencyCode
        paymentTokenRequest.amount = amount
        
        //Generate signature
        guard let paymentTokenRequestJson:String = ToolKit.jsonStringFrom(dic: paymentTokenRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        paymentTokenRequest.signature = self.paymentGatewayHelper.generateSignature(secretKey: secretKey, jsonString: paymentTokenRequestJson)
        
        guard let finalPaymentTokenRequestJson:String = ToolKit.jsonStringFrom(dic: paymentTokenRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        print("getPaymentToken JSON: \(finalPaymentTokenRequestJson)")
        
        guard let encodedPaymentTokenRequestJson:String = self.stringHelper.base64Encode(input: finalPaymentTokenRequestJson) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
                
        self.httpHelper.requestPaymentToken(encodedJson: encodedPaymentTokenRequestJson, success: success, failure: failure)
    }
    
    /**
     * @param paymentInquiryRequest
     * @param success
     * @param failure
     *
     * Reference : https://developer.2c2p.com/docs/mobile-v4-payment-inquiry-api
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-inquiry-response-parameters-
     */
    func inquiryPaymentResult(paymentInquiryRequest:PaymentInquiryRequest, success:@escaping response, failure:@escaping failure) {
        
        //Request information
        let apiVersion:String = "1.1"
        
        //Merchant's account information
        let mid:String = "JT01" //Get MerchantID when opening account with 2C2P
        let secretKey:String = "7jYcp4FxFdf0" //Get SecretKey from 2C2P PGW dashboard
        
        //Set common values for all payments
        paymentInquiryRequest.version = apiVersion
        paymentInquiryRequest.merchantID = mid
        
        //Generate signature
        guard let paymentInquiryRequestJson:String = ToolKit.jsonStringFrom(dic: paymentInquiryRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        paymentInquiryRequest.signature = self.paymentGatewayHelper.generateSignature(secretKey: secretKey, jsonString: paymentInquiryRequestJson)
        
        guard let finalPaymentInquiryRequestJson:String = ToolKit.jsonStringFrom(dic: paymentInquiryRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        print("inquiryPaymentResult JSON: \(finalPaymentInquiryRequestJson)")
        
        guard let encodedPaymentInquiryRequestJson:String = self.stringHelper.base64Encode(input: finalPaymentInquiryRequestJson) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        self.httpHelper.requestPaymentInquiry(encodedJson: encodedPaymentInquiryRequestJson, success: success, failure: failure)
    }
    
}
