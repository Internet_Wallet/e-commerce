//
//  PaymentTokenRequest.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class PaymentTokenRequest {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-token-request-parameters-
     *
     * Version : 10.0
     * {
     *    "version": "10.0",
     *    "merchantID": "",
     *    "invoiceNo": "",
     *    "desc": "",
     *    "amount": "",
     *    "currencyCode": "",
     *    "paymentChannel": "",
     *    "userDefined1": "",
     *    "userDefined2": "",
     *    "userDefined3": "",
     *    "userDefined4": "",
     *    "userDefined5": "",
     *    "interestType": "",
     *    "productCode": "",
     *    "recurring": "",
     *    "invoicePrefix": "",
     *    "recurringAmount": "",
     *    "allowAccumulate": "",
     *    "maxAccumulateAmt": "",
     *    "recurringInterval": "",
     *    "recurringCount": "",
     *    "chargeNextDate": "",
     *    "promotion": "",
     *    "request3DS": "",
     *    "statementDescriptor": "",
     *    "nonceStr": "",
     *    "signature": ""
     * }
     */
    
    var version:String?
    var merchantID:String?
    var invoiceNo:String?
    var desc:String?
    var amount:String?
    var currencyCode:String?
    var paymentChannel:String?
    var userDefined1:String?
    var userDefined2:String?
    var userDefined3:String?
    var userDefined4:String?
    var userDefined5:String?
    var interestType:String?
    var productCode:String?
    var recurring:String?
    var invoicePrefix:String?
    var recurringAmount:String?
    var allowAccumulate:String?
    var maxAccumulateAmt:String?
    var recurringInterval:String?
    var recurringCount:String?
    var chargeNextDate:String?
    var promotion:String?
    var request3DS:String?
    var statementDescriptor:String?
    var nonceStr:String?
    var signature:String?
    
    func toJSON() -> [String:String] {
        var json:[String:String]
        
        json = [
            "version"             : JSONHelper.option(input: self.version, defaultValue: ""),
            "merchantID"          : JSONHelper.option(input: self.merchantID, defaultValue: ""),
            "invoiceNo"           : JSONHelper.option(input: self.invoiceNo, defaultValue: ""),
            "desc"                : JSONHelper.option(input: self.desc, defaultValue: ""),
            "amount"              : JSONHelper.option(input: self.amount, defaultValue: ""),
            "currencyCode"        : JSONHelper.option(input: self.currencyCode, defaultValue: ""),
            "paymentChannel"      : JSONHelper.option(input: self.paymentChannel, defaultValue: ""),
            "userDefined1"        : JSONHelper.option(input: self.userDefined1, defaultValue: ""),
            "userDefined2"        : JSONHelper.option(input: self.userDefined2, defaultValue: ""),
            "userDefined3"        : JSONHelper.option(input: self.userDefined3, defaultValue: ""),
            "userDefined4"        : JSONHelper.option(input: self.userDefined4, defaultValue: ""),
            "userDefined5"        : JSONHelper.option(input: self.userDefined5, defaultValue: ""),
            "interestType"        : JSONHelper.option(input: self.interestType, defaultValue: ""),
            "productCode"         : JSONHelper.option(input: self.productCode, defaultValue: ""),
            "recurring"           : JSONHelper.option(input: self.recurring, defaultValue: ""),
            "invoicePrefix"       : JSONHelper.option(input: self.invoicePrefix, defaultValue: ""),
            "recurringAmount"     : JSONHelper.option(input: self.recurringAmount, defaultValue: ""),
            "allowAccumulate"     : JSONHelper.option(input: self.allowAccumulate, defaultValue: ""),
            "maxAccumulateAmt"    : JSONHelper.option(input: self.maxAccumulateAmt, defaultValue: ""),
            "recurringInterval"   : JSONHelper.option(input: self.recurringInterval, defaultValue: ""),
            "recurringCount"      : JSONHelper.option(input: self.recurringCount, defaultValue: ""),
            "chargeNextDate"      : JSONHelper.option(input: self.chargeNextDate, defaultValue: ""),
            "promotion"           : JSONHelper.option(input: self.promotion, defaultValue: ""),
            "request3DS"          : JSONHelper.option(input: self.request3DS, defaultValue: ""),
            "statementDescriptor" : JSONHelper.option(input: self.statementDescriptor, defaultValue: ""),
            "nonceStr"            : JSONHelper.option(input: self.nonceStr, defaultValue: ""),
            "signature"           : JSONHelper.option(input: self.signature, defaultValue: "")
        ]
        
        return json
    }
}
