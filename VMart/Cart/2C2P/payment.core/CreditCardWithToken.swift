//
//  CreditCardWithToken.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class CreditCardWithToken: BasePayment {
    
    /**
     *  Reference : https://developer.2c2p.com/docs/mobile-v4-payment-with-card-token
     *              https://developer.2c2p.com/docs/mobile-v4-api-parameters#card-token-payment-builder
     */
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "CC"
        request.request3DS = "N"
        
        return request
    }
    
    override func execute() {
        
        self.progressDialog.message = Constants.common_message_payment_token_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 1 : Get payment token
        self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Submit payment
            self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 2: Set card token
        let cardToken:String = "00106181629310424179"
        
        //Step 3: Construct credit card request.
        let cardTokenPayment:CreditCardPayment = CardTokenPaymentBuilder(cardToken: cardToken)
                                                    .securityCode("123")
                                                    .build()
        
        //Step 4: Construct transaction request.
        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
                                                    .withCreditCardPayment(cardTokenPayment)
                                                    .build()
        
        //Step 5: Execute payment request.
        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest, success: { (response:TransactionResultResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    
    override func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 6: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
}
