//
//  RecurringPaymentPlan.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class RecurringPaymentPlan: BasePayment {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-recurring-payment-plan
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#credit-card-payment-builder
     */
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "CC"
        request.request3DS = "N"
        
        //Enable RPP payment option
        request.recurring = "Y" //Enable / Disable RPP option
        request.invoicePrefix = "demo" + String(Int(Date().timeIntervalSince1970)) //RPP transaction invoice prefix
        request.recurringAmount = "000000000100" //Recurring amount
        request.allowAccumulate = "Y" //Allow failed authorization to be accumulated
        request.maxAccumulateAmt = "000000001000" //Maximum threshold of total accumulated amount
        request.recurringInterval = "5" //Recurring interval by no of days
        request.recurringCount = "3" //Number of Recurring occurance
        request.chargeNextDate = self.getChargeNextDate() //The first day to start recurring charges. format DDMMYYYY
        
        return request
    }
    
    
    override func execute() {
        
        self.progressDialog.message = Constants.common_message_payment_token_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 1 : Get payment token
        self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Submit payment
            self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 2: Construct credit card request.
        let creditCardPayment:CreditCardPayment = CreditCardPaymentBuilder(pan: "4111111111111111")
                                                    .expiryMonth(12)
                                                    .expiryYear(2019)
                                                    .securityCode("123")
                                                    .build()
        
        //Step 3: Construct transaction request.
        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
                                                    .withCreditCardPayment(creditCardPayment)
                                                    .build()
        
        //Step 4: Execute payment request.
        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest, success: { (response:TransactionResultResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    
    override func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 5: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }    
    
    private func getChargeNextDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        
        let nextDate = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: Date())
        
        return dateFormatter.string(from: nextDate!)
    }
    
    
}
