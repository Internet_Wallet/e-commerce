//
//  SDKBuildTypeViewController.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 1/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit

class SDKBuildTypeViewController: UIViewController {
    
    private var sdkBuildTypeList:[SDKBuildType] = [SDKBuildType]()

    @IBOutlet weak var sdkBuildTypeTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initData()
        
        self.sdkBuildTypeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.hiddenExtraCellLine(tableView: self.sdkBuildTypeTableView)
    }
    
    private func initData() {
        
        self.sdkBuildTypeList.append(SDKBuildType.CORE_SDK)
//        self.sdkBuildTypeList.append(SDKBuildType.CORE_UI_SDK)  //For future use
    }
    
    private func hiddenExtraCellLine(tableView:UITableView) {
        
        let view:UIView = UIView()
        view.backgroundColor = UIColor.white
        tableView.tableFooterView = view
    }

}

//MARK: - UITableViewDataSource & UITableViewDelegate
extension SDKBuildTypeViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sdkBuildTypeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let currentSDKBuildType:SDKBuildType = self.sdkBuildTypeList[indexPath.row]
        
        cell.textLabel?.text = currentSDKBuildType.rawValue
        cell.textLabel?.textColor = UIColor.darkGray
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sdkBuildType:SDKBuildType = self.sdkBuildTypeList[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let featureCategoryTVC:FeatureCategoryTableViewController =
            storyboard.instantiateViewController(withIdentifier: "FeatureCategoryTableViewController") as! FeatureCategoryTableViewController
        featureCategoryTVC.sdkBuildType = sdkBuildType
        self.navigationController?.pushViewController(featureCategoryTVC, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
