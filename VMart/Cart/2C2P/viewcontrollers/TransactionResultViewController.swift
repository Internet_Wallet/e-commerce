//
//  TransactionResultViewController.swift
//  PGW4
//
//  Created by yan feng liu on 29/7/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import UIKit
import PGW

class TransactionResultViewController: UIViewController {
    
    @IBOutlet weak var transactionResultHeader: UILabel!
    @IBOutlet weak var transactionResultMessage: UILabel!
    
    @IBOutlet weak var invoiceNo: CopyableLabel!
    @IBOutlet weak var amount: CopyableLabel!
    @IBOutlet weak var dateTime: CopyableLabel!
    @IBOutlet weak var paymentType: CopyableLabel!
    @IBOutlet weak var maskedPan: CopyableLabel!
    @IBOutlet weak var message: CopyableLabel!
    @IBOutlet weak var responseMessageExpand: CopyableLabel!
    
    @IBOutlet weak var containerView: UIScrollView!
    
    var extraLength:CGFloat = 460
    
    var paymentInquiryResponse:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initData()
    }
    
    private func initData() {
        
        guard let decodedRes:String = ToolKit.base64DecodedString(encodedString: self.paymentInquiryResponse) else { return }
        
        guard let jsonObject:[String:Any] = ToolKit.dicFrom(jsonString: decodedRes) else { return }
        
        guard let responseCode:String = jsonObject[PGW.Constants.JSON_NAME_RESP_CODE] as? String else { return }
        
        if(responseCode != APIResponseCode.API_SUCCESS) {
                        
            self.transactionResultHeader.text = Constants.transaction_result_header_failed
            self.transactionResultHeader.textColor = UIColor.red
            
            self.transactionResultMessage.text = Constants.transaction_result_message_failed
            
            self.message.textColor = UIColor.red
        }
        
        self.invoiceNo.text = jsonObject[PGW.Constants.JSON_NAME_INVOICE_NO] as? String
        self.amount.text = jsonObject[PGW.Constants.JSON_NAME_AMOUNT] as? String
        self.dateTime.text = jsonObject[Constants.JSON_NAME_TRANSACTION_DATE_TIME] as? String
        self.paymentType.text = jsonObject[PGW.Constants.JSON_NAME_CHANNEL_CODE] as? String
        self.maskedPan.text = jsonObject[PGW.Constants.JSON_NAME_PAN] as? String
        self.message.text = jsonObject[PGW.Constants.JSON_NAME_RESP_DESC] as? String
        
        self.responseMessageExpand.text = ToolKit.prettyPrintJsonString(jsonStr: decodedRes) 
        
        extraLength = (self.responseMessageExpand.text?.height(withConstrainedWidth: UIScreen.main.bounds.size.width - 2 * 16, font: UIFont.systemFont(ofSize: 16)))!
    }
    
    @IBAction func onClickPaymentCompleted(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onClickResponseExpand(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            self.responseMessageExpand.isHidden = false
            
            var containerViewSize:CGSize = self.containerView.contentSize
            containerViewSize.height += extraLength
            self.containerView.contentSize = containerViewSize
        } else {
            self.responseMessageExpand.isHidden = true
            
            var containerViewSize:CGSize = self.containerView.contentSize
            containerViewSize.height -= extraLength
            self.containerView.contentSize = containerViewSize
        }
    }
}

//MARK: Figure out size of UILabel based on String
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

