//
//  FeatureTableViewCell.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit

protocol FeatureTableViewCellDelegate:NSObjectProtocol {
    func didSelectFeatureTableViewCell(cell:FeatureTableViewCell)
}

class FeatureTableViewCell: UITableViewCell {
    
    weak var delegate:FeatureTableViewCellDelegate?

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClick(_ sender: UIButton) {
        self.delegate?.didSelectFeatureTableViewCell(cell: self)
    }

}
