//
//  CardDetails.swift
//  VMart
//
//  Created by ANTONY on 09/11/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit


class CardDetails: ZAYOKBaseViewController {
    
    //    var sdkBuildType:SDKBuildType = SDKBuildType.CORE_SDK
    //    private var featureList:[Feature] = [Feature]()
    //    private var featureCategoryHelper:FeatureCategoryHelper!
    
    
    @IBOutlet var monthTextField: UITextField!{
        didSet {
            self.monthTextField.text = self.monthTextField.text?.localized
            self.monthTextField.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var yearTextField: UITextField!{
        didSet {
            self.yearTextField.text = self.yearTextField.text?.localized
            self.yearTextField.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var CVVTextField: UITextField!{
        didSet {
            self.CVVTextField.text = self.CVVTextField.text?.localized
            self.CVVTextField.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var cardHolderNameTextField: UITextField!{
        didSet {
            self.cardHolderNameTextField.text = self.cardHolderNameTextField.text?.localized
            self.cardHolderNameTextField.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var cardHolderNumberTextField: UITextField!{
        didSet {
            self.cardHolderNumberTextField.text = self.cardHolderNumberTextField.text?.localized
            self.cardHolderNumberTextField.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var PayDetailLabel: UILabel!{
        didSet {
            self.PayDetailLabel.text = self.PayDetailLabel.text?.localized
            self.PayDetailLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ProdAmtLabel: UILabel!{
        didSet {
            self.ProdAmtLabel.text = self.ProdAmtLabel.text?.localized
            self.ProdAmtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var shipChargeLabel: UILabel!{
        didSet {
            self.shipChargeLabel.text = self.shipChargeLabel.text?.localized
            self.shipChargeLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var TotPayLabel: UILabel!{
        didSet {
            self.TotPayLabel.text = self.TotPayLabel.text?.localized
            self.TotPayLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ExpiryDateLabel: UILabel!{
        didSet {
            self.ExpiryDateLabel.text = self.ExpiryDateLabel.text?.localized
            self.ExpiryDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var payBtn: UIButton!{
        didSet {
            payBtn.setTitle((payBtn.titleLabel?.text ?? "").localized, for: .normal)
            payBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        // Do any additional setup after loading the view.
        
        self.title = "Credit Card".localized
        
        self.cardHolderNumberTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        
        //        self.featureCategoryHelper = FeatureCategoryHelper()
        
        //        self.initData()
    }
    
    //    private func initData() {
    //        
    //        self.featureList.append(contentsOf: self.featureCategoryHelper.getList(sdkBuildType: self.sdkBuildType))
    //    }
    //    
    @objc func didChangeText(textField:UITextField) {
        textField.text = modifyCreditCardString(creditCardString: textField.text!)
    }
    
    
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        
        //        let arrOfCharacters = Array(trimmedString.characters)
        
        let arrOfCharacters = Array(trimmedString.replacePunctuationCharacters())
        
        var modifiedCreditCardString = ""
        
        if(arrOfCharacters.count > 0)
        {
            for i in 0...arrOfCharacters.count-1
            {
                modifiedCreditCardString.append(arrOfCharacters[i])
                
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count)
                {
                    
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    
    @IBAction func stateTextFieldDidBegin(_ sender: UITextField) {
        let pickerView:UIPickerView = UIPickerView()
        sender.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    @IBAction func creditCardRequestResponse(){
        //        self.featureCategoryHelper.redirect(viewController: self, feature: self.featureList[1])
    }
    
}
extension CardDetails : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "ssssss"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        monthTextField.text = "kkkkkk"
        yearTextField.text = "kkkkkk"
    }
    
    
    func checkTextField(){
        if self.CVVTextField.text == nil || self.cardHolderNameTextField.text == nil || self.cardHolderNumberTextField.text == nil || self.monthTextField.text == nil || self.yearTextField.text == nil{
            
        }
    }
}


extension CardDetails: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == CVVTextField{
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == cardHolderNumberTextField{
            //            let newLength = textField.text!.characters.count + string.characters.count - range.length
            let newLength = textField.text!.count + string.count - range.length
            return newLength <= 19
        }
        return true
    }
}




