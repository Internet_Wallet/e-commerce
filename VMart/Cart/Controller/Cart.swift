//
//  Cart.swift
//  VMart
//
//  Created by ANTONY on 26/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Cart: ZAYOKBaseViewController {
    
    var dictionaryArray =               NSMutableArray ()
    var aPIManager = APIManager()
    var shoppingCartInfoArray =         [ShoppingCartInfo]()
    var checkOutGuestflag = false
    var flag = Bool ()
    var textFieldDic =                  [Int:AnyObject]()
    var selectedIndex = IndexPath()
    var allBillingAddresses = [GenericBillingAddress]()
    var newBillingAddress: GenericBillingAddress?
    var defaultSelectedAddress: GenericBillingAddress?
    static var PickupAddress: Store?
    var warningCheckArr = [Bool]()
    
    @IBOutlet var cartTbl: UITableView!
    @IBOutlet weak var CartDataView: UIView!
    @IBOutlet weak var CartDataNotFoundView: UIView!
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    @IBOutlet var infoLbl: UILabel!{
        didSet {
            self.infoLbl.text = self.infoLbl.text?.localized
            self.infoLbl.textColor = UIColor.black
            self.infoLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var moveToPaymentBtn: UIButton! {
        didSet {
            self.moveToPaymentBtn.setTitle("Continue".localized, for: .normal)
            moveToPaymentBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var continueBtn: UIButton! {
        didSet {
            continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176.0, blueValue: 255.0, alpha: 1.0), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.continueBtn.setTitle("Continue Shopping".localized, for: .normal)
            self.continueBtn.layer.cornerRadius = 4
            self.continueBtn.layer.masksToBounds = true
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func custom(){
        
        let HomeCatStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        if let HomeCatController = HomeCatStoryboard.instantiateViewController(withIdentifier: "HomeCategories_ID") as? HomeCategories {
            let mainViewController   = self
            let drawerController     = KYDrawerController(drawerDirection: .left, drawerWidth: 280)
            drawerController.mainViewController = UINavigationController(rootViewController: mainViewController)
            
            if let Nav = drawerController.mainViewController as? UINavigationController{
                Nav.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
                
            }
            
            drawerController.drawerViewController = HomeCatController
            //UIApplication.shared.keyWindow?.rootViewController = drawerController
            
            
            self.setUPNav(controller: drawerController)
            
        }
        
    }
    
    
    func setUPNav(controller: UIViewController){
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        
        // Set the new rootViewController of the window.
        // Calling "UIView.transition" below will animate the swap.
        window.rootViewController = controller
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 1.0
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(0, forKey: "shippingaddress")
        UserDefaults.standard.synchronize()
        flag = false
        if AppUtility.isConnectedToNetwork() {
            self.dictionaryArray = []
            self.getShoppingCartDetails()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        println_debug(UserDefaults.standard.bool(forKey: "LoginStatus"))
        getBillForm()
        // self.custom()
        
        //        appDelegate.openCart()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "My Cart".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: appFont, size: 17)!]
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.bool(forKey: "LoginStatus") {
            println_debug("Login")
        } else {
            println_debug("Logout")
        }
    }
    
    private func getBillForm() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadBillingAddressForm( onSuccess: {[weak self] billingAddress in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if let newBillingAddress = billingAddress.newBillingAddress {
                self?.newBillingAddress = newBillingAddress //this line will crash the app
                self?.allBillingAddresses.append(contentsOf: billingAddress.existingBillingAddresses)
                self?.allBillingAddresses.append(newBillingAddress) //this line will crash the app
            }
            if let allBillAddr = self?.allBillingAddresses {
                for billingArrdess in allBillAddr {
                    if billingArrdess.id == 0 {
                        //                        self.dropDown.dataSource.append(NSLocalizedString("New Address".localized, comment: ""))
                    } else {
                        let title = "\(billingArrdess.firstName ?? "") \(billingArrdess.lastName ?? ""), \(billingArrdess.address1 ?? ""), \(billingArrdess.stateProvinceName ?? "") \(billingArrdess.zipPostalCode ?? ""), \(billingArrdess.cityName ?? ""), \(billingArrdess.countryName ?? "")"
                        println_debug(title)
                        //                        self.dropDown.dataSource.append(title)
                    }
                }
                if allBillAddr.count > 0 {
                    VMLoginModel.shared.addressAdded = true
                }
            }
            self?.defaultSelectedAddress = self?.allBillingAddresses.first ?? nil
            //                self.populateNewAddressForm()
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        //        DispatchQueue.main.async(execute: {
        //            let HomeCatStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        //            if let HomeCatController = HomeCatStoryboard.instantiateViewController(withIdentifier: "KYDrawerController_id") as? KYDrawerController {
        //
        //                self.setUPNav(controller: HomeCatController)
        //
        //                //                UIApplication.shared.keyWindow?.rootViewController = HomeCatController
        //            }
        //            //            self.navigationController?.popToRootViewController(animated: false)
        //        })
        self.dismiss(animated: true, completion: nil)
        
        //        if let drawerController = navigationController?.parent as? KYDrawerController {
        //            drawerController.setDrawerState(.opened, animated: true)
        //        }
    }
    
    @objc func proceedToLogin(_ notification: Notification) {
        //        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let signInViewC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        //        centerControllersObject.pushNewController(controller: signInViewC)
    }
    
    @objc func proceedToRegister(_ notification: Notification) {
        //        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let registerViewC = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        //        centerControllersObject.pushNewController(controller: registerViewC)
    }
    
    @objc func proceedToCheckOut(_ notification: Notification) {
        AppUtility.showLoading(self.view)
        let finalDicArray = NSMutableArray ()
        for key in self.textFieldDic.keys {
            let parameters:Dictionary<String,AnyObject> = ["value": (self.textFieldDic[key] ?? "" as AnyObject), //this line will crash the app
                "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[key].Id) as AnyObject]
            finalDicArray.add(parameters)
        }
        finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
        aPIManager.loadProceedToCheckout(parameters: finalDicArray, onSuccess:{ [weak self] in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let checkOutView = mainStoryBoard.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
            //            centerControllersObject.pushNewController(controller: checkOutView)
            }, onError: { [weak self]
                message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func getShoppingCartDetails() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadShoppingCart(onSuccess:{ [weak self] getProducts in
            self?.shoppingCartInfoArray = getProducts
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                self?.CartDataNotFoundView.isHidden = false
                self?.CartDataView.isHidden = true
                return
            } else {
                self?.CartDataNotFoundView.isHidden = true
                self?.CartDataView.isHidden = false
            }
            //                self.customNavBarView.updateShoppingCartCount()
            if !(self?.shoppingCartInfoArray[0].GiftCardBox_Display ??  false) {
                //                    self.hideGiftCardBox()
            }
            self?.flag = true
            //                self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
            //                self.totalAttributsViewHeight = 0
            for i in 0  ..< (self?.shoppingCartInfoArray[0].checkoutAttributes.count)! {
                if let productAttributes = self?.shoppingCartInfoArray[0].checkoutAttributes[i] {
                    if productAttributes.AttributeControlType == 1 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    } else if productAttributes.AttributeControlType == 2 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 85
                    } else if productAttributes.AttributeControlType == 3 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 41 + 44 * productAttributes.Values.count
                    } else if productAttributes.AttributeControlType == 4 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                        //                        self.textFieldStringRowIndex = i
                        //self.textFieldDic[i] = self.shoppingCartInfoArray[0].checkoutAttributes[i].DefaultValue as AnyObject
                    }
                }
            }
            //                self.attributsTableViewHeightConstant.constant = CGFloat(self.totalAttributsViewHeight)
            if self?.shoppingCartInfoArray[0].checkoutAttributes.count == 0 {
                //                    self.attributsTableViewHeightConstant.constant = 0
                //                    self.attributeTableContainerBottomConstraint.constant = 0
                //                    self.attributeTableTopConstraint.constant = 0
                //                    self.attributeTableBottomConstraint.constant = 0
                //                    self.attributeTableContainer.isHidden = true
            }
            //            UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
            if self?.shoppingCartInfoArray[0].Count == 0{
                UserDefaults.standard.set("", forKey: "badge")
            }
            else{
                UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
            }
            UserDefaults.standard.synchronize()
            
            DispatchQueue.main.async {
                
                
                UIView.animate(withDuration: 0.4, animations: {
                    self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                        UIView.animate(withDuration: 0.4, animations: {
                            self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                        }, completion: ({finished in
                            //                            self?.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                            self?.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                            self?.navigationItem.rightBarButtonItem  = self?.cartBarButton
                        }))
                })
            }
            self?.checkOutForGuest()
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func checkOutForGuest() {
        self.aPIManager.checkOutForGuest( onSuccess:{ [weak self] checkOutGuest in
            self?.checkOutGuestflag = checkOutGuest
            DispatchQueue.main.async {
                self?.cartTbl.delegate = self
                self?.cartTbl.dataSource = self
                self?.cartTbl.reloadData()
                //                self?.continueButtonState()
                self?.warningCheckArr = Array(repeating: false, count: self!.shoppingCartInfoArray[0].Items.count)
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
                if let viewLoc = self?.view {
                    AppUtility.showToast("Cart is Empty".localized,view: viewLoc)
                }
                //                    centerControllersObject.popTopViewController()
            } else {
                //                    self.containerScrollView.isHidden = false;
            }
            
            //                self.contentTableView.reloadData()
            //                self.attributesTableView.reloadData()
            //                self.roundRectToAllView(self.attributeTableContainer)
            //                self.roundRectToAllView(self.discountView)
            //                self.roundRectToAllView(self.giftCardView)
            //                self.roundRectToAllView(self.costView)
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func continueButtonState(index: IndexPath){
        //        if ((self.shoppingCartInfoArray[0].OrderTotalResponse?.value(forKey: "MinOrderTotalAmount").safelyWrappingString())!.replacingOccurrences(of: "MMK", with: "")) > (self.shoppingCartInfoArray[0].orderTotal.safelyWrappingString().replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: ",", with: "")){
        //            self.moveToPaymentBtn.isEnabled = false
        //            self.moveToPaymentBtn.alpha = 0.5
        //            AppUtility.showToastlocal(message: "Increase Your Order in this location".localized, view: self.view)
        //        }
        if let dataValue =  self.shoppingCartInfoArray[0].Items[index.row] as? CheckOutItems{
            if (dataValue.Warnings?.count)! > 0{
                self.moveToPaymentBtn.isEnabled = false
                self.moveToPaymentBtn.backgroundColor = UIColor.darkGray
                self.moveToPaymentBtn.alpha = 0.5
            }
            else{
                self.moveToPaymentBtn.isEnabled = true
                self.moveToPaymentBtn.backgroundColor = UIColor(red: 39.0/255.0, green: 115.0/255.0, blue: 240.0/255.0, alpha: 1.0)
                self.moveToPaymentBtn.alpha = 1.0
            }
        }
        
    }
    
    @objc func deleteBtnAction(_ sender:UIButton!) {
        AppUtility.showLoading(self.view)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject,"key":"removefromcart" as AnyObject]
        let dicArray = NSMutableArray ()
        dicArray.add(parameters)
        self.removeProduct(dicArray: dicArray)
    }
    
    func removeProduct(dicArray: NSMutableArray) {
        aPIManager.DeleteAndUpdate(dicArray, onSuccess: { [weak self] updateCart in
            self?.shoppingCartInfoArray = updateCart
            
            if (self?.shoppingCartInfoArray.count)! > 0{
                //                UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
                
                if self?.shoppingCartInfoArray[0].Count == 0{
                    UserDefaults.standard.set("", forKey: "badge")
                }
                else{
                    UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
                }
                UserDefaults.standard.synchronize()
                
                DispatchQueue.main.async {
                    
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                            UIView.animate(withDuration: 0.4, animations: {
                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                            }, completion: ({finished in
                                //                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                            }))
                    })
                    
                }
                
            }
            
            //            self.customNavBarView.updateShoppingCartCount()
            
            //            let pointInTable: CGPoint = sender.convert(sender.bounds.origin, to: self.contentTableView)
            //            let cellIndexPath = self.contentTableView.indexPathForRow(at: pointInTable)
            //            self.contentTableView.deleteRows(at: [cellIndexPath!], with: UITableViewRowAnimation.automatic)
            //            self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
            //            self.contentTableView.perform(#selector(UITableView.reloadData), with: nil, afterDelay: 0.5)
            
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                self?.CartDataNotFoundView.isHidden = false
                self?.CartDataView.isHidden = true
                return
            } else {
                self?.CartDataNotFoundView.isHidden = true
                self?.CartDataView.isHidden = false
            }
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                //                centerControllersObject.popTopViewController()
            }
            self?.cartTbl.reloadData()
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    @objc func plusBtnAction(_ sender:UIButton!) {
        let indexBtn   = IndexPath(row: sender.tag, section: 0)
        //        let cell        = self.contentTableView.cellForRow(at: indexPath) as! ItemsTableViewCell
        self.shoppingCartInfoArray[0].Items[sender.tag].Quantity += 1
        //        cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity as AnyObject,
                                                       "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id ) as AnyObject]
        let dicArray = NSMutableArray ()
        dicArray.add(parameters)
        AppUtility.showLoading(self.view)
        aPIManager.DeleteAndUpdate(dicArray, onSuccess: { [weak self] updateCart in
            self?.shoppingCartInfoArray = updateCart
            //            UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
            
            if self?.shoppingCartInfoArray[0].Count == 0{
                UserDefaults.standard.set("", forKey: "badge")
            }
            else{
                UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
            }
            
            UserDefaults.standard.synchronize()
            DispatchQueue.main.async {
                
                UIView.animate(withDuration: 0.4, animations: {
                    self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                        UIView.animate(withDuration: 0.4, animations: {
                            self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                        }, completion: ({finished in
                            //                            self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                            self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                        }))
                })
                self?.cartTbl.reloadData()
                self?.continueButtonState(index: indexBtn)
            }
            //            self.customNavBarView.updateShoppingCartCount()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: {[weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    //                    AppUtility.showToast(message,view: viewLoc)
                    AppUtility.showToastlocal(message: message!.localized, view: viewLoc)
                }
        })
    }
    
    @objc func minusBtnAction(sender:UIButton) {
        if self.shoppingCartInfoArray[0].Items[sender.tag].Quantity > 0 {
            let indexBtn       = IndexPath(row: sender.tag, section: 0)
            //            let cell            = self.contentTableView.cellForRow(at: indexPath) as! ItemsTableViewCell
            self.shoppingCartInfoArray[0].Items[sender.tag].Quantity -= 1
            //            cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity as AnyObject,
                                                           "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id ) as AnyObject]
            let dicArray = NSMutableArray ()
            dicArray.add(parameters)
            AppUtility.showLoading(self.view)
            aPIManager.DeleteAndUpdate(dicArray, onSuccess: { [weak self] updateCart in
                self?.shoppingCartInfoArray = updateCart
                //                UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
                
                if self?.shoppingCartInfoArray[0].Count == 0{
                    UserDefaults.standard.set("", forKey: "badge")
                }
                else{
                    UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
                }
                UserDefaults.standard.synchronize()
                
                DispatchQueue.main.async {
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                            UIView.animate(withDuration: 0.4, animations: {
                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                            }, completion: ({finished in
                                //                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                            }))
                    })
                    
                }
                if self?.shoppingCartInfoArray[0].Items.count == 0 {
                    self?.CartDataNotFoundView.isHidden = false
                    self?.CartDataView.isHidden = true
                    //                    return
                } else {
                    self?.CartDataNotFoundView.isHidden = true
                    self?.CartDataView.isHidden = false
                    self?.cartTbl.reloadData()
                    self?.continueButtonState(index: indexBtn)
                }
                
                
                //                self.customNavBarView.updateShoppingCartCount()
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                        //                        AppUtility.showToast(message,view: viewLoc)
                        AppUtility.showToastlocal(message: message!.localized, view: viewLoc)
                    }
            })
        }
    }
    
    func addToWishListBtnAct(_ sender: AnyObject) {
        let finalDicArray = NSMutableArray ()
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject, "key":"addtocart_1.EnteredQuantity" as AnyObject]
        finalDicArray.add(parameters)
        finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
        AppUtility.showLoading(self.view)
        self.aPIManager.loadAddProductToCart(2, productId: self.shoppingCartInfoArray[0].Items[sender.tag].ProductId, parameters: finalDicArray, onSuccess:{ [weak self] priceValue in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                let parameters:Dictionary<String,AnyObject> = ["value": self?.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject,
                                                               "key":"removefromcart" as AnyObject]
                let dicArray = NSMutableArray ()
                dicArray.add(parameters)
                self?.removeProduct(dicArray: dicArray)
                
                AppUtility.showToastlocal(message: "Product moved to wishlist".localized, view: viewLoc)
                //AppUtility.showToastCustomBlackInView(message: "Successfully added to wish list".localized,controller: controller)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToastlocal(message: message ?? "", view: viewLoc)
                }
        })
    }
    
    @IBAction func continueToPaymentAction(_ sender: UIButton) {
        
        print("wwwwwww -\n",self.warningCheckArr)
        var checkWarning = false
        for obj in warningCheckArr {
            if obj {
                checkWarning = true
            }
        }
        
        if let totalOrderalue = self.shoppingCartInfoArray[0].orderTotal, let minimumOrder = self.shoppingCartInfoArray[0].MinOrderTotalAmount {
            if Int(totalOrderalue.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " MMK", with: ""))! <= Int(minimumOrder.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " MMK", with: ""))!
            {
                DispatchQueue.main.async {
                    let alertVC = SAlertController()
                    alertVC.ShowSAlert(title: "", withDescription: "Minimum order value must be \(minimumOrder) MMK".localized, onController: self)
                    let tryAgain = SAlertAction()
                    tryAgain.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        
                    })
                    alertVC.addAction(action: [tryAgain])
                }
                return
            }
            
        }
        if checkWarning {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Buying is disabled for some product".localized, onController: self)
                let tryAgain = SAlertAction()
                tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                    DispatchQueue.main.async {
                        AppUtility.showLoading(self.view)
                        let dicArray = NSMutableArray ()
                        
                        for (index,obj) in self.warningCheckArr.enumerated() {
                            if obj {
                                print("iiiiiiiiiiiiii - ",index)
                                self.warningCheckArr.remove(at: index)
                                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                dicArray.add(parameters)
                            }
                        }
                        print(dicArray)
                        self.removeProduct(dicArray: dicArray)
                    }
                })
                alertVC.addAction(action: [tryAgain])
            }
            return
        }
        
        if Cart.PickupAddress != nil {
            if UserDefaults.standard.bool(forKey: "GuestLogin") {
                if let _ = self.defaultSelectedAddress {
                    self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                }else {
                    DispatchQueue.main.async {
                        if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                            addAddressVC.screenFrom = "Cart"
                            self.navigationController?.pushViewController(addAddressVC, animated: true)
                        }
                    }
                }
            }else {
                if UserDefaults.standard.bool(forKey: "LoginStatus") {
                    if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                        self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                    } else {
                        DispatchQueue.main.async {
                            if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                                addAddressVC.screenFrom = "Cart"
                                self.navigationController?.pushViewController(addAddressVC, animated: true)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.navigateLoginAndRegistration()
                        //                        if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                        //                            loginVC.screenFrm = "Cart"
                        //                            loginVC.asGuest = true
                        //                            self.navigationController?.pushViewController(loginVC, animated: true)
                        //                        }
                    }
                }
            }
        }else {
            if UserDefaults.standard.bool(forKey: "GuestLogin") {
                if let _ = self.defaultSelectedAddress {
                    self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                } else {
                    DispatchQueue.main.async {
                        if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                            addAddressVC.screenFrom = "Cart"
                            self.navigationController?.pushViewController(addAddressVC, animated: true)
                        }
                    }
                }
            }else {
                if UserDefaults.standard.bool(forKey: "LoginStatus") {
                    if UserDefaults.standard.integer(forKey: "shippingaddress") == 0{
                        
                        AppUtility.showLoading(self.view)
                        self.aPIManager.saveBillingOrShippingAddress(1, from: (self.defaultSelectedAddress?.id)!,
                                                                     onSuccess: {
                                                                        
                                                                        DispatchQueue.main.async {
                                                                            UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                                                                            self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                                                                            AppUtility.hideLoading(self.view)
                                                                        }
                        },                    onError: { message in
                            AppUtility.hideLoading(self.view)
                            
                        })
                        /*
                         AppUtility.showLoading(self.view)
                         self.aPIManager.saveStoreAddress((self.defaultSelectedAddress?.id)!, onSuccess: {
                         UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                         self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                         AppUtility.hideLoading(self.view)
                         }, onError: { message in
                         AppUtility.hideLoading(self.view)
                         AppUtility.showToast(message,view: self.view)
                         AppUtility.showToastlocal(message: message!.localized, view: self.view)
                         })
                         */
                    }
                }else {
                    
                    DispatchQueue.main.async {
                        self.navigateLoginAndRegistration()
                        //                        if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                        //                            loginVC.screenFrm = "Cart"
                        //                            loginVC.asGuest = true
                        //                            self.navigationController?.pushViewController(loginVC, animated: true)
                        //                        }
                    }
                }
            }
            //            AppUtility.showToastlocal(message: "Choose Pickup Address".localized, view: self.view)
        }
    }
    
    private func navigateLoginAndRegistration() {
        let regitration_Opened = UserDefaults.standard.value(forKey: "REGITRATION_OPENED") as? Bool ?? false
        if regitration_Opened {
            DispatchQueue.main.async {
                if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                    registrationVC.screenFrom = "Cart"
                    registrationVC.modalPresentationStyle = .fullScreen
                    RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
                    registrationVC.continueAsGuest = true
                    registrationVC.delegate = self
                    self.navigationController?.pushViewController(registrationVC, animated: true)
                }
            }
        }else {
            if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                loginVC.screenFrm = "Cart"
                loginVC.asGuest = true
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    
    
    @IBAction func continueShopAction(_ sender: UIButton) {
        //        self.dismiss(animated: true, completion: nil)
        //        if let viewController = (UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController")) as? ZAYOKBaseViewController{
        //
        //            UIApplication.shared.keyWindow?.rootViewController =  UINavigationController.init(rootViewController: viewController)
        //        }
        
        //        DispatchQueue.main.async(execute: {
        //            let HomeCatStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        //            if let HomeCatController = HomeCatStoryboard.instantiateViewController(withIdentifier: "KYDrawerController_id") as? KYDrawerController {
        //
        //                self.setUPNav(controller: HomeCatController)
        //
        //                //                UIApplication.shared.keyWindow?.rootViewController = HomeCatController
        //            }
        //        })
        
        
        
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func editAddressAction(_ sender: UIButton) {
        //defaultAddress
        if let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
            editAddressVC.screenFrom = "Edit Address"
            editAddressVC.delegate = self
            editAddressVC.defaultAddress = defaultSelectedAddress
            self.navigationController?.pushViewController(editAddressVC, animated: true)
        }
    }
    
}

extension Cart: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.subCategoriesArray.count
        if section == 1 {
            if flag {
                return self.shoppingCartInfoArray[0].Items.count
            }
        } else {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CustomTableViewCell?
        switch indexPath.section {
        case 0:  cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? CustomTableViewCell
        cell?.wrapData(addressModel: self.defaultSelectedAddress)
            break
        case 1:  cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as? CustomTableViewCell
        cell?.wrapCellData(Data: self.shoppingCartInfoArray[0].Items[indexPath.row], indexPath: indexPath)
        
        if self.shoppingCartInfoArray[0].Items[indexPath.row].AvailableQuantity == 0 {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Out of stock".localized, onController: self)
                let tryAgain = SAlertAction()
                tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                    DispatchQueue.main.async {
                        AppUtility.showLoading(self.view)
                        let dicArray = NSMutableArray ()
                        
                        for (index,obj) in self.warningCheckArr.enumerated() {
                            if obj {
                                print("iiiiiiiiiiiiii - ",index)
                                self.warningCheckArr.remove(at: index)
                                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                dicArray.add(parameters)
                            }
                        }
                        print(dicArray)
                        self.removeProduct(dicArray: dicArray)
                    }
                })
                alertVC.addAction(action: [tryAgain])
            }
        }
        
        
        if (self.shoppingCartInfoArray[0].Items[indexPath.row].Warnings?.count)! > 0{
            self.warningCheckArr[indexPath.row] = true
        } else {
            self.warningCheckArr[indexPath.row] = false
        }
            break
        case 2:  cell = tableView.dequeueReusableCell(withIdentifier: "chooseCell", for: indexPath) as? CustomTableViewCell
        //        cell?.pickPointlocBtn.tag = indexPath.section
        cell?.buttonStateManage(locationModel: Cart.PickupAddress,indexpath: indexPath, selectedIndPath: self.selectedIndex)
        
        //        selectedIndex = IndexPath(row: 0, section: indexPath.row)
        //        cell?.defaultSetDelivery(sender: (cell?.deliveryBtn)!)
            break
        case 3:  cell = tableView.dequeueReusableCell(withIdentifier: "selectedaddressCell", for: indexPath) as? CustomTableViewCell
        cell?.wraplocationData(locationModel: Cart.PickupAddress)
        //cell?.pickPointlocBtn.tag = indexPath.section
            break
        default: cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as? CustomTableViewCell
        cell?.paymentCellUpdate(paymentData: self.shoppingCartInfoArray[0])
            break
        }
        cell?.Delegate = self
        return cell!
    }
    
    func TapSubcategoary(indexPath: IndexPath){
        
        guard let productDetailvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails else { return }
        productDetailvc.productId = Int(self.shoppingCartInfoArray[0].Items[indexPath.item].ProductId)
        productDetailvc.productName = self.shoppingCartInfoArray[0].Items[indexPath.item].ProductName as String
        self.navigationController?.pushViewController(productDetailvc, animated: true)
        
        
    }
    
    
    //    {
    
    //        self.categoryId = self.subCategoriesArray[indexPath.row].idd
    //        self.categoryName = self.subCategoriesArray[indexPath.row].name
    //        if AppUtility.isConnectedToNetwork() {
    //            self.loadSubCategories()
    //        } else {
    //            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
    //        }
    
    //    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if indexPath.section == 1{
        //            self.TapSubcategoary(indexPath: indexPath)
        //        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        //            return (defaultSelectedAddress != nil) ? 150 : 35
        case 1:
            if let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell {
                cell.productImgHeightConstraint.constant = cell.contentView.bounds.height-65
            }
            return UITableView.automaticDimension
        //            return 200so
        case 2:
            return self.returnsize(index: 1, indexPath: indexPath)
        case 3:
            return  (Cart.PickupAddress != nil) ? UITableView.automaticDimension : 0
        default:
            return UITableView.automaticDimension
        }
    }
    
    func returnsize(index: Int,indexPath: IndexPath) -> CGFloat {
        if indexPath == selectedIndex && Cart.PickupAddress == nil{
            //            if let cell = cartTbl.dequeueReusableCell(withIdentifier: "chooseCell", for: indexPath) as? CustomTableViewCell {
            //                cell.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
            //                cell.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            //            }
            return 165 //Size you want to increase to
        } else {
            //            if let cell = cartTbl.dequeueReusableCell(withIdentifier: "chooseCell", for: indexPath) as? CustomTableViewCell {
            //                cell.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            //                cell.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
            //            }
            return 122 // Default Size
        }
    }
    
}

extension Cart: CustomCellDelegate {
    func btnEditAddressClick(sender: UIButton) {
        self.performSegue(withIdentifier: "selectAddressSeque", sender: self)
    }
    
    func deliveryBtnActionHandler(sender: UIButton) {
        DispatchQueue.main.async {
            if sender.tag == 2 || self.isObjectNotNil(object: Cart.PickupAddress) {
                Cart.PickupAddress = nil
                let indexSection = NSIndexSet(index: 3)
                self.cartTbl.reloadSections(indexSection as IndexSet, with: .none)
                self.selectedIndex = IndexPath(row: 0, section: sender.tag)
                self.cartTbl.reloadRows(at: [self.selectedIndex], with: .none)
                //        if sender.tag != 2 && isObjectNotNil(object: self.PickupAddress){
                //            self.selectedIndex = IndexPath(row: 0, section: sender.tag)
                //            self.cartTbl.reloadRows(at: [self.selectedIndex], with: .automatic)
                //        }
            } else {
                self.selectedIndex = IndexPath(row: 0, section: sender.tag)
                self.cartTbl.reloadRows(at: [self.selectedIndex], with: .none)
            }
        }
    }
    
    func AddWishListBtnActionHandler(sender: UIButton){
        self.addToWishListBtnAct(sender)
    }
    
    func plusBtnActionHandler(sender: UIButton) {
        self.plusBtnAction(sender)
    }
    
    func minusBtnActionHandler(sender: UIButton) {
        self.minusBtnAction(sender: sender)
    }
    
    func deleteCartItemAction(sender: UIButton) {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "".localized, withDescription: "Are you sure you want to remove item?".localized, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "Yes".localized, AlertType: .defualt, withComplition: {
                self.deleteBtnAction(sender)
            })
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {  })
            alertVC.addAction(action: [YesAction,cancelAction])
        }
        
    }
    
    func btnChangeAddressClick(sender: UIButton) {
        self.performSegue(withIdentifier: "CartToAddAddrerssSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CartToAddAddrerssSegue" {
            if let rootViewController = segue.destination as? addressCollection {
                rootViewController.delegate = self
                rootViewController.allBillingAddresses = self.allBillingAddresses
            }
        } else if segue.identifier == "CartToAddBillingAddressSegue" {
            if let addAddressVC = segue.destination.children[0] as? AddAddress {
                addAddressVC.delegate = self
                
                self.editAddressAction(UIButton())
                
                
            }
        } else if segue.identifier == "selectAddressSeque"{
            if let pickupVC = segue.destination as? Location {
                pickupVC.Delegate = self
            }
        }
    }
}

extension Cart: LocationDelegate {
    func SelectedPickupLocationClick(object: Store) {
        
        
        
        //        DispatchQueue.main.async {
        //            if self.isObjectNotNil(object: object) {
        //                Cart.PickupAddress = object
        //            } else {
        //                Cart.PickupAddress = nil
        //            }
        //            println_debug(object)
        //            let indexSection = NSIndexSet(index: 3)
        //            self.cartTbl.reloadSections(indexSection as IndexSet, with: .automatic)
        //        }
        
        
        
        
        AppUtility.showLoading(self.view)
        
        self.aPIManager.saveStoreAddress(object.storeId.integerValue, onSuccess: {
            AppUtility.hideLoading(self.view)
            DispatchQueue.main.async {
                if self.isObjectNotNil(object: object) {
                    Cart.PickupAddress = object
                } else {
                    Cart.PickupAddress = nil
                }
                println_debug(object)
                let indexSection = NSIndexSet(index: 3)
                self.cartTbl.reloadSections(indexSection as IndexSet, with: .automatic)
            }
            
        }, onError: { message in
            AppUtility.hideLoading(self.view)
            AppUtility.showToast(message,view: self.view)
            AppUtility.showToastlocal(message: message!.localized, view: self.view)
        })
        
        
        
        
        
        //        AppUtility.showLoading(self.view)
        //        self.aPIManager.saveBillingOrShippingAddress(2, from: (object.storeId.integerValue),
        //                                                     onSuccess: {
        //                                                        AppUtility.hideLoading(self.view)
        //                                                        DispatchQueue.main.async {
        //                                                            if self.isObjectNotNil(object: object) {
        //                                                                Cart.PickupAddress = object
        //                                                            } else {
        //                                                                Cart.PickupAddress = nil
        //                                                            }
        //                                                            println_debug(object)
        //                                                            let indexSection = NSIndexSet(index: 3)
        //                                                            self.cartTbl.reloadSections(indexSection as IndexSet, with: .automatic)
        //                                                        }
        //
        //        },                    onError: { message in
        //            AppUtility.hideLoading(self.view)
        ////            AppUtility.showToast(message,view: self.view)
        //             AppUtility.showToastlocal(message: message!.localized, view: self.view)
        //        })
        
        
        
        
        //        self.aPIManager.UserDelete("0095\(VMLoginModel.shared.mobileNumber!.suffix(from: VMLoginModel.shared.mobileNumber!.index(VMLoginModel.shared.mobileNumber!.endIndex, offsetBy: -10)))", onSuccess: {
        //            AppUtility.hideLoading(self.view)
        //        }) { message in
        //            AppUtility.hideLoading(self.view)
        //            AppUtility.showToastlocal(message: message!.localized, view: self.view)
        //        }
        
        
    }
    
    func isObjectNotNil(object:AnyObject!) -> Bool {
        if let _:AnyObject = object {
            return true
        }
        return false
    }
}

extension Cart : AddBillingAddressDelegate {
    func AddressTapped(indexPath: IndexPath) {
        println_debug(indexPath)
        
        AppUtility.showLoading(self.view)
        self.aPIManager.saveBillingOrShippingAddress(1, from: self.allBillingAddresses[indexPath.row].id!,
                                                     onSuccess: {
                                                        AppUtility.hideLoading(self.view)
                                                        DispatchQueue.main.async {
                                                            self.defaultSelectedAddress = self.allBillingAddresses[indexPath.row]
                                                            let indexSection = NSIndexSet(index: 0)
                                                            self.cartTbl.reloadSections(indexSection as IndexSet, with: .none)
                                                        }
        },                    onError: { message in
            AppUtility.hideLoading(self.view)
            //                                                        AppUtility.showToast(message,view: self.view)
            //            AppUtility.showToastlocal(message: message!.localized, view: viewLoc)
        })
        
        //        let addressId = self.allBillingAddresses[indexPath.row].id
        //        let urlStr = String.init(format: "%@/checkout/checkoutsaveadressid/1    ", APIManagerClient.sharedInstance.base_url)
        //        if let url = URL(string: urlStr) {
        //            var dict = [String: Any]()
        //            dict["value"] = addressId ?? 0
        //            let params = AppUtility.JSONStringFromAnyObject(value: dict as AnyObject)
        //            AppUtility.showLoading(self.view)
        //            aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self] (response, success, _) in
        //                if let viewLoc = self?.view {
        //                    AppUtility.hideLoading(viewLoc)
        //                }
        //                if success {
        //                    if let json = response as? Dictionary<String,Any> {
        //                        if let code = json["StatusCode"] as? Int, code == 200 {
        //                            DispatchQueue.main.async {
        //                                self?.defaultSelectedAddress = self?.allBillingAddresses[indexPath.row]
        //                                let indexSection = NSIndexSet(index: 0)
        //                                self?.cartTbl.reloadSections(indexSection as IndexSet, with: .none)
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        println_debug(indexPath)
    }
    
    func addBillingAddressTapped(indexPath: IndexPath,count:Int) {
        println_debug(indexPath)
        if count >= 5{
            AppUtility.showToastlocal(message: "You can not add more address.Maximum add address limit are 5. You can update previous address.".localized, view: self.view);  return
        }
        self.performSegue(withIdentifier: "CartToAddBillingAddressSegue", sender: self)
    }
    
    func pickupLocationTapped(sender: UIButton) {
        self.performSegue(withIdentifier: "selectAddressSeque", sender: self)
    }
}

extension Cart : RegistrationDelegate {
    
    func backToMainPage() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: step 1 Add Protocol here.
protocol CustomCellDelegate: class {
    
    func btnEditAddressClick(sender: UIButton)
    func btnChangeAddressClick(sender: UIButton)
    func deleteCartItemAction(sender: UIButton)
    func plusBtnActionHandler(sender: UIButton)
    func minusBtnActionHandler(sender: UIButton)
    func AddWishListBtnActionHandler(sender: UIButton)
    func deliveryBtnActionHandler(sender: UIButton)
    func pickupLocationTapped(sender: UIButton)
    
}

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet var viewOne: UIView!
    @IBOutlet var viewTwo: UIView!
    @IBOutlet var productDiscountPercentageLbl: UILabel!
    @IBOutlet var oldPriceLbl: UILabel!
    @IBOutlet var productImgHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var colorLbl: UILabel! {
        didSet {
            self.colorLbl.text = self.colorLbl.text?.localized
            self.colorLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var warningLbl: UILabel! {
        didSet {
            self.warningLbl.text = self.warningLbl.text?.localized
            self.warningLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var taxLbl: UILabel! {
        didSet {
            self.taxLbl.text = self.taxLbl.text?.localized
            self.taxLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var taxValueLbl: UILabel! {
        didSet {
            self.taxValueLbl.text = self.taxValueLbl.text?.localized
            self.taxValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var deliveryDetailLbl: UILabel! {
        didSet {
            self.deliveryDetailLbl.text = self.deliveryDetailLbl.text?.localized
            self.deliveryDetailLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var pickupPointLocationLbl: UILabel! {
        didSet {
            self.pickupPointLocationLbl.text = self.pickupPointLocationLbl.text?.localized
            self.pickupPointLocationLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var paymentDetailLbl: UILabel! {
        didSet {
            self.paymentDetailLbl.text = self.paymentDetailLbl.text?.localized
            self.paymentDetailLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productAmountLbl: UILabel! {
        didSet {
            self.productAmountLbl.text = self.productAmountLbl.text?.localized
            self.productAmountLbl.font = UIFont(name: appFont, size: 15.0)
            
        }
    }
    @IBOutlet var shippingChargeLbl: UILabel! {
        didSet {
            self.shippingChargeLbl.text = self.shippingChargeLbl.text?.localized
            self.shippingChargeLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var totalPayLbl: UILabel! {
        didSet {
            self.totalPayLbl.text = self.totalPayLbl.text?.localized
            self.totalPayLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var addressNameLbl: UILabel! {
        didSet {
            self.addressNameLbl.text = self.addressNameLbl.text?.localized
            self.addressNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var addressLabel: UILabel! {
        didSet {
            self.addressLabel.text = self.addressLabel.text?.localized
            self.addressLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var addressPhLabel: UILabel! {
        didSet {
            self.addressPhLabel.text = self.addressPhLabel.text?.localized
            self.addressPhLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var LocaddressNameLbl: UILabel! {
        didSet {
            self.LocaddressNameLbl.text = self.LocaddressNameLbl.text?.localized
            self.LocaddressNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var LocaddressLabel: UILabel! {
        didSet {
            self.LocaddressLabel.text = self.LocaddressLabel.text?.localized
            self.LocaddressLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var LocaddressPhLabel: UILabel! {
        didSet {
            self.LocaddressPhLabel.text = self.LocaddressPhLabel.text?.localized
            self.LocaddressPhLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ProductNameLbl: UILabel! {
        didSet {
            self.ProductNameLbl.text = self.ProductNameLbl.text?.localized
            self.ProductNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ProductNameLbl1: UILabel! {
        didSet {
            self.ProductNameLbl1.text = self.ProductNameLbl1.text?.localized
            self.ProductNameLbl1.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var UnitPriceLbl: UILabel! {
        didSet {
            self.UnitPriceLbl.text = self.UnitPriceLbl.text?.localized
            self.UnitPriceLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var UnitPriceLbl1: UILabel! {
        didSet {
            self.UnitPriceLbl1.text = self.UnitPriceLbl1.text?.localized
            self.UnitPriceLbl1.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var subCategoriesLbl: UILabel! {
        didSet {
            self.subCategoriesLbl.text = self.subCategoriesLbl.text?.localized
            self.subCategoriesLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var incrementLbl: UILabel! {
        didSet {
            self.incrementLbl.text = self.incrementLbl.text?.localized
            self.incrementLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var subTotalLbl: UILabel! {
        didSet {
            self.subTotalLbl.text = self.subTotalLbl.text?.localized
            self.subTotalLbl.font = UIFont(name: appFont, size: 15.0)
            //            self.subTotalLbl.amountAttributedString()
        }
    }
    @IBOutlet var shippingLbl: UILabel! {
        didSet {
            self.shippingLbl.text = self.shippingLbl.text?.localized
            self.shippingLbl.font = UIFont(name: appFont, size: 15.0)
            //            self.shippingLbl.amountAttributedString()
        }
    }
    
    //    @IBOutlet var taxLbl: UILabel! {
    //        didSet {
    //            self.taxLbl.text = self.taxLbl.text?.localized
    //        }
    //    }
    
    @IBOutlet var totalPriceLabel: UILabel! {
        didSet {
            self.totalPriceLabel.text = self.totalPriceLabel.text?.localized
            self.totalPriceLabel.font = UIFont(name: appFont, size: 15.0)
            //            self.totalPriceLabel.amountAttributedString()
            
        }
    }
    @IBOutlet var deliveryDateLabel: UILabel! {
        didSet {
            self.deliveryDateLabel.text = self.deliveryDateLabel.text?.localized
            self.deliveryDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var btnChangeAddress: UIButton! {
        didSet {
            btnChangeAddress.setTitle((btnChangeAddress.titleLabel?.text ?? "").localized, for: .normal)
            btnChangeAddress.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var plusBtn: UIButton!
    @IBOutlet var minusBtn: UIButton!
    @IBOutlet var deleteBtn: UIButton! {
        didSet {
            deleteBtn.setTitle((deleteBtn.titleLabel?.text ?? "").localized, for: .normal)
            deleteBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var AddWishListBtn: UIButton! {
        didSet {
            AddWishListBtn.setTitle((AddWishListBtn.titleLabel?.text ?? "").localized, for: .normal)
            AddWishListBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var deliveryBtn: UIButton! {
        didSet {
            deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            deliveryBtn.setTitle((deliveryBtn.titleLabel?.text ?? "").localized, for: .normal)
            deliveryBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var pickupBtn: UIButton! {
        didSet {
            pickupBtn.setTitle((pickupBtn.titleLabel?.text ?? "").localized, for: .normal)
            pickupBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var pickPointlocBtn: UIButton! {
        didSet {
            pickPointlocBtn.setTitle((pickPointlocBtn.titleLabel?.text ?? "").localized, for: .normal)
            pickPointlocBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    weak var Delegate: CustomCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func btnEditAddressClick(sender: UIButton) {
        self.Delegate?.btnEditAddressClick(sender: sender)
    }
    
    @IBAction func btnChangeAddressClick(sender: UIButton) {
        self.Delegate?.btnChangeAddressClick(sender: sender)
    }
    
    @IBAction func deleteBtnActionClick(sender: UIButton) {
        self.Delegate?.deleteCartItemAction(sender: sender)
    }
    
    @IBAction func plusBtnActionClick(sender: UIButton) {
        self.Delegate?.plusBtnActionHandler(sender: sender)
    }
    
    @IBAction func minusBtnActionClick(sender: UIButton) {
        self.Delegate?.minusBtnActionHandler(sender: sender)
    }
    
    @IBAction func AddWishListActionClick(sender: UIButton) {
        self.Delegate?.AddWishListBtnActionHandler(sender: sender)
    }
    
    @IBAction func deliveryActionClick(sender: UIButton) {
        if sender == deliveryBtn {
            deliveryBtn.isSelected = true
            pickupBtn.isSelected = false
            pickPointlocBtn.isHidden = true
        } else if sender == pickupBtn {
            deliveryBtn.isSelected = false
            pickupBtn.isSelected = true
            pickPointlocBtn.isHidden = false
        }
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func deliveryBtnAction(sender: UIButton) {
        self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
        UserDefaults.standard.set(0, forKey: "shippingaddress")
        UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
        UserDefaults.standard.synchronize()
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func pickupBtnAction(sender: UIButton) {
        DispatchQueue.main.async {
            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        }
        UserDefaults.standard.set(1, forKey: "shippingaddress")
        UserDefaults.standard.set("Pickup Address", forKey: "shipadd")
        UserDefaults.standard.synchronize()
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func pickupBtnClick(sender: UIButton) {
        self.Delegate?.pickupLocationTapped(sender: sender)
    }
    
    //    func defaultSetDelivery(sender: UIButton) {
    //        deliveryBtn.isSelected = true
    //        pickupBtn.isSelected = false
    //        pickPointlocBtn.isHidden = true
    //    }
    
    func wrapCellData(Data: CheckOutItems, indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.productImgHeightConstraint.constant = self.contentView.bounds.height-65
        }
        if let imageUrl = Data.ImageUrl {
            self.itemImageView.sd_setImage(with: URL(string: imageUrl))
            //            AppUtility.getData(from: URL(string: Data.ImageUrl ?? "")!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.itemImageView.image = UIImage(data: data)
            //                }
            //            }
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: Data.ImageUrl ?? ""), imageView: self.itemImageView, imgUrl: Data.ImageUrl ?? "") { (image) in }
            
            
            
            
        }
        if Data.Quantity as NSInteger == 1{
            self.minusBtn.isEnabled = false
        }
        else{
            self.minusBtn.isEnabled = true
        }
        self.ProductNameLbl.text = Data.ProductName as String
        self.incrementLbl.text = String(format: "%d",Data.Quantity as NSInteger)
        self.UnitPriceLbl.text = Data.UnitPrice as String
        self.colorLbl.text = (Data.AttributeInfo as String).replacingOccurrences(of: "<br />", with: "\n")
        if (Data.Warnings?.count)! > 0{
            if let warning = Data.Warnings![0] as? String{
                self.warningLbl.text =  warning
            }
        }
        else{
            self.warningLbl.text =  ""
        }
        self.deliveryDateLabel.text = String.init(format: "%@: %@", "Delivery Time".localized, Data.DeliveryDays ?? "24 Hours")
        self.plusBtn.tag = indexPath.row
        self.minusBtn.tag = indexPath.row
        self.deleteBtn.tag = indexPath.row
        self.AddWishListBtn.tag = indexPath.row
        println_debug(Data.ImageUrl ?? "")
        println_debug(Data.ProductName)
        println_debug(Data.UnitPrice)
        println_debug(Data.Quantity)
        self.ProductNameLbl1.text = Data.ProductName as String
        //        self.UnitPriceLbl1.text = Data.UnitPrice as String
        //        self.UnitPriceLbl1.amountAttributedString()
        self.updatePrice(Data: Data)
        
        
    }
    
    func updatePrice(Data: CheckOutItems) {
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: Data.UnitPrice as String, oldPrice: Data.OldPrice as String, priceWithDiscount: "")
        
        var productPrice = Data.UnitPrice.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice + mmkText
        let attrString = NSMutableAttributedString(string: productPrice)
        let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.UnitPriceLbl1.attributedText = attrString
        
        var oldPrice = Data.OldPrice.replacingOccurrences(of: "MMK", with: "")
        oldPrice = oldPrice + mmkText
        
        if Price == "" {
            var productPrice2 = Data.UnitPrice.replacingOccurrences(of: "MMK", with: "")
            productPrice2 = productPrice2 + mmkText
            let attrStr = NSMutableAttributedString(string: productPrice2)
            let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            self.UnitPriceLbl1.attributedText = attrStr
            
            let attrString = NSMutableAttributedString(string: oldPrice, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            let nsRange2 = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
            self.oldPriceLbl.attributedText = attrString
        }
        
        if Data.DiscountPercentage != 0 {
            self.productDiscountPercentageLbl.text = "\((Data.DiscountPercentage) ?? 0)% off - \(Data.DiscountOfferName ?? "")"
        }
    }
    
    func wrapData(addressModel: GenericBillingAddress?) {
        if let model = addressModel {
            self.addressNameLbl.text = model.firstName ?? ""
            
            var houseNo = "\(model.houseNo?.replacingOccurrences(of: "House No ", with: "") ?? "")"
            var roomNo = "\(model.roomNo?.replacingOccurrences(of: "Room No ", with: "") ?? "")"
            var floorNo = "\(model.floorNo?.replacingOccurrences(of: "Floor No ", with: "") ?? "")"
            var address1 = "\(model.address1 ?? "")"
            var address2 = "\(model.address2 ?? "")"
            var city = "\(model.cityName ?? "")"
            var state = "\(model.stateProvinceName ?? "")"
            
            if (model.houseNo != "" && model.houseNo != nil) && (model.roomNo != "" && model.roomNo != nil) && (model.floorNo != "" && model.floorNo != nil){
                if houseNo != "" {
                    houseNo = ""
                    houseNo.append("House No: \(model.houseNo?.replacingOccurrences(of: "House No ", with: "") ?? "")")
                    houseNo.append(", ")
                }
                if roomNo != "" {
                    roomNo = ""
                    roomNo.append("Room No: \(model.roomNo?.replacingOccurrences(of: "Room No ", with: "") ?? "")")
                    roomNo.append(", ")
                }
                if floorNo != "" {
                    floorNo = ""
                    floorNo.append("Floor No: \(model.floorNo?.replacingOccurrences(of: "Floor No ", with: "") ?? "")")
                    floorNo.append(", ")
                }
                if address1 != "" {
                    address1 = ""
                    address1.append("\(model.address1 ?? "")")
                    address1.append(", ")
                }
                if address2 != "" {
                    address2 = ""
                    address2.append("\(model.address2 ?? "")")
                    address2.append(", ")
                }
                if city != "" {
                    city = ""
                    city.append("\(model.cityName ?? "")")
                    city.append(", ")
                }
                if state != "" {
                    state = ""
                    state.append("\(model.stateProvinceName ?? "")")
                }
                
                self.addressLabel.text = "\(houseNo)\(roomNo)\(floorNo)\(address1)\(address2)\(city)\(state)"
                
                //                self.addressLabel.text = "House No: \(model.houseNo?.replacingOccurrences(of: "House No ", with: "") ?? ""), Room No: \(model.roomNo?.replacingOccurrences(of: "Room No ", with: "") ?? ""), Floor No: \(model.floorNo?.replacingOccurrences(of: "Floor No ", with: "") ?? ""), \(model.address1 ?? ""), \(model.address2 ?? ""), \(model.cityName ?? ""), \(model.stateProvinceName ?? "")"
                
                //                self.addressLabel.text = "\(model.houseNo ?? ""), \(model.roomNo ?? ""), \(model.floorNo ?? ""),\(model.address1 ?? ""), \(model.address2 ?? ""), \(model.cityName ?? ""), \(model.stateProvinceName ?? "")"
            }else{
                self.addressLabel.text = "\(address1)\(address2)\(city)\(state)"
                
                //                self.addressLabel.text = "\(model.address1 ??  ""), \(model.address2 ??  ""), \(model.cityName ??  ""), \(model.stateProvinceName ?? "")"
            }
            
            if let number = model.phoneNumber, number.hasPrefix("0095") {
                self.addressPhLabel.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95 ")
            } else {
                self.addressPhLabel.text = model.phoneNumber ?? ""
            }
        }
    }
    
    func buttonStateManage(locationModel: Store?, indexpath:IndexPath, selectedIndPath: IndexPath){
        self.deliveryBtn.tag = indexpath.row
        self.pickupBtn.tag = indexpath.section
        if indexpath == selectedIndPath && Cart.PickupAddress == nil {
            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        }
        else {
            if (locationModel != nil) {
                self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
                self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            } else {
                self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
            }
        }
        
        //        if (locationModel != nil){
        //            self.pickPointlocBtn.isHidden = true
        //            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        //            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
        //        }
        //        else{
        //            self.pickPointlocBtn.isHidden = false
        //            //            self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
        //            //            self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        //        }
    }
    
    func wraplocationData(locationModel: Store?) {
        if let model = locationModel {
            self.LocaddressNameLbl.text = model.name as String
            self.LocaddressLabel.text = "\(model.storeDescription)"
            //            self.LocaddressPhLabel.text = "\(model.city as String) Township"
            println_debug(model)
            
            
        }
    }
    
    func paymentCellUpdate(paymentData: ShoppingCartInfo) {
        self.subTotalLbl.text = paymentData.subTotal ?? ""
        self.subTotalLbl.amountAttributedString()
        if paymentData.shipping == "0 MMK"{
            self.shippingLbl.text = "Free"
        }
        else{
            self.shippingLbl.text = paymentData.shipping  ?? "0"
            self.shippingLbl.amountAttributedString()
        }
        
        var ta1xStr = paymentData.tax?.replacingOccurrences(of: " MMK", with: "")
        if ta1xStr == "0" {
            self.taxValueLbl.text = ""
            self.taxLbl.text = ""
        } else {
            self.taxValueLbl.text = paymentData.tax ?? ""
            self.taxValueLbl.amountAttributedString()
        }
        
        //        self.totalPriceLabel.text    = paymentData.orderTotal  ?? ""
        
        var productPrice = paymentData.orderTotal?.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        
        let attrString = NSMutableAttributedString(string: productPrice!)
        let nsRange = NSString(string: productPrice!).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.totalPriceLabel.attributedText = attrString
        
        //        self.totalPriceLabel.amountAttributedString()
    }
}

extension Cart: VMAddNewAddressProtocol {
    func addNewAddress() {
        self.newBillingAddress = nil
        self.allBillingAddresses.removeAll()
        self.getBillForm()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let indexpath = IndexPath(item: 0, section: 0)
            self.cartTbl.reloadRows(at: [indexpath], with: .none)
        }
    }
}
