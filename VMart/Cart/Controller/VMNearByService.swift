//
//  VMNearByService.swift
//  VMart
//
//  Created by Mohit on 1/30/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class VMNearByService: UIViewController {
    
    var aPIManager = APIManager()
    var alllocationArray = [NearByServiceModel]()
    @IBOutlet var nearByTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = " "
        self.title = "Nearby OK$ Service".localized
        let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : 16.8171, "Longitude": 96.1310, "PhoneNumber" : "959975307429", "UserIdNotToConsider": ""] as [String : Any]
        self.NearByService(dict: jsonDic)
        // Do any additional setup after loading the view.
    }
    
    
    func NearByService(dict: [String:Any]) {
        AppUtility.showLoading(self.view)
        
        self.aPIManager.NearByOkServiceAPI(dict, onSuccess:{ [weak self] locationArray in
            self?.alllocationArray = locationArray
            self?.nearByTbl.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                //self?.navigationController?.popToRootViewController(animated: true)
            }
            
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    //                    AppUtility.showToast(message,view: viewLoc)
                    AppUtility.showToastCustomBlackInView(message: message!, controller: self!)
                }
        })
    }
    
    
}


class NearByServiceCell: UITableViewCell {
    
    @IBOutlet var LblOne: UILabel?{
        didSet{
            LblOne?.text = LblOne?.text?.localized
            LblOne?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var LblTwo: UILabel?{
        didSet{
            LblTwo?.text = LblTwo?.text?.localized
            LblTwo?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Lblthree: UILabel?{
        didSet{
            Lblthree?.text = Lblthree?.text?.localized
            Lblthree?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Lblfour: UILabel?{
        didSet{
            Lblfour?.text = Lblfour?.text?.localized
            Lblfour?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBAction func btnNearByClick(sender: UIButton) {
        
    }
    
    @IBAction func btnCallClick(sender: UIButton) {
        guard let number = URL(string: "tel://" + (self.LblTwo?.text)!) else { return }
        UIApplication.shared.open(number)
    }
    
    func wrapData(object:NearByServiceModel){
        if let val1 = object.Category,let val2 = object.PhoneNumber?.replacingOccurrences(of: "00", with: "+"),let val3 = object.TownShipName,let coun = object.Country,let val4 = object.SubCategory {
            
            self.LblOne?.text = val1
            self.LblTwo?.text = val2
            self.Lblthree?.text =   "\(val3), \(coun)"
            self.Lblfour?.text = "\(val4)"
            
        }
        
    }
    
}



extension VMNearByService: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.alllocationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : NearByServiceCell?
        cell = tableView.dequeueReusableCell(withIdentifier: "NearByServiceCell", for: indexPath) as? NearByServiceCell
        cell?.wrapData(object: self.alllocationArray[indexPath.row])
        //        cell?.Delegate = self
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
