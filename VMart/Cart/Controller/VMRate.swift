//
//  VMRate.swift
//  VMart
//
//  Created by Mohit on 1/29/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class VMRate: UIViewController {
    
    var aPIManager = APIManager()
    var dictionaryArray =               NSMutableArray ()
    var orderNumber:NSNumber?
    
    @IBOutlet var txtTitle:UITextField!{
        didSet{
            self.txtTitle.text = self.txtTitle?.text?.localized
            self.txtTitle.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var txtComment: UITextView!{ 
        didSet{
            self.txtComment.text = self.txtComment.text.localized
            self.txtComment.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    @IBOutlet weak var okayBtn: UIButton! {
        didSet {
            okayBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            //okayBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            okayBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.okayBtn.setTitle(self.okayBtn.titleLabel?.text?.localized, for: .normal)
            
        }
    }
    
    @IBOutlet weak var submitBtn: UIButton! {
        didSet {
            //            submitBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            submitBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.submitBtn.setTitle("Submit".localized, for: .normal)
            submitBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var Btn0: UIButton!
    @IBOutlet weak var Btn1: UIButton!
    @IBOutlet weak var Btn2: UIButton!
    @IBOutlet weak var Btn3: UIButton!
    @IBOutlet weak var Btn4: UIButton!
    
    @IBOutlet var lblheader: UILabel!{
        didSet {
            self.lblheader.text = self.lblheader.text?.localized
            self.lblheader.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var lblheader1: UILabel!{
        didSet {
            self.lblheader1.text = self.lblheader1.text?.localized
            self.lblheader1.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderNumber=5
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    
    func getShoppingCartDetails(dict: [String:Any]) {
        AppUtility.showLoading(self.view)
        
        self.aPIManager.RateAndReview(dict, onSuccess:{ getProducts in
            println_debug(getProducts)
            if let viewLoc = self.view {
                AppUtility.hideLoading(viewLoc)
                //                self.navigationController?.popToRootViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }, onError: { [weak self] message in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                //                    AppUtility.showToast(message,view: viewLoc)
                AppUtility.showToastCustomBlackInView(message: message!, controller: self!)
            }
        })
    }
    
    
    @IBAction func myButtonTapped(sender: UIButton){
        
        switch sender.tag {
        case 0: self.okayBtn.setTitle("Terrible".localized,for: .normal); self.IconChange(sender: sender);  break
        case 1:  self.okayBtn.setTitle("Bad".localized,for: .normal); self.IconChange(sender: sender);  break
        case 2: self.okayBtn.setTitle("Okay".localized,for: .normal); self.IconChange(sender: sender);  break
        case 3: self.okayBtn.setTitle("Good".localized,for: .normal);  self.IconChange(sender: sender); break
        case 4: self.okayBtn.setTitle("Great".localized,for: .normal);  self.IconChange(sender: sender); break
        default: break
        }
    }
    
    func IconChange(sender: UIButton){
        switch sender.tag {
            
        case 0: self.Btn0.isSelected = true ; self.Btn0.removeContentInset();   self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 1: self.Btn1.isSelected = true ; self.Btn1.removeContentInset(); self.Btn0.isSelected = false ;self.Btn0.addContentInset();  self.Btn2.isSelected = false; self.Btn2.addContentInset();  self.Btn3.isSelected = false; self.Btn3.addContentInset();  self.Btn4.isSelected = false; self.Btn4.addContentInset();  break
        case 2: self.Btn2.isSelected = true ;  self.Btn2.removeContentInset();  self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn0.isSelected = false; self.Btn0.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 3: self.Btn3.isSelected = true ; self.Btn3.removeContentInset();   self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn0.isSelected = false; self.Btn0.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 4: self.Btn4.isSelected = true ;  self.Btn4.removeContentInset();  self.Btn0.isSelected = false ; self.Btn0.addContentInset(); self.Btn1.isSelected = false; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); break
        default:break
        }
    }
    
    
    
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        
        if AppUtility.isConnectedToNetwork() {
            self.dictionaryArray = []
            let dictData : [String:Any] = ["ReviewText":(self.okayBtn.titleLabel?.text)!, "OrderNo":self.orderNumber!,"Rating":sender.tag+1, "ReviewType":1]
            self.getShoppingCartDetails(dict: dictData)
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
}

extension UIButton{
    func addContentInset(){
        self.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 15.0, bottom: 8.0, right: 15.0)
    }
    func removeContentInset()  {
        self.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
    }
}
