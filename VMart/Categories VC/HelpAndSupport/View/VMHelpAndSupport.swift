//
//  VMHelpAndSupport.swift
//  VMart
//
//  Created by Mohit on 2/11/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit
import MessageUI

class VMHelpAndSupport: ZAYOKBaseViewController {
    
    @IBOutlet weak var tbHelpSupport: UITableView!{
        didSet {
            tbHelpSupport.tableFooterView = UIView(frame: .zero)
        }
    }
    @IBOutlet weak var btncallDoctorConstraint: NSLayoutConstraint!
    @IBOutlet weak var btncallDoctor: UIButton!{
        didSet {
            btncallDoctor.setTitle((btncallDoctor.titleLabel?.text ?? "").localized, for: .normal)
            btncallDoctor.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var list : HelpSupportData?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Help & Support".localized
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        HelpSupportDataClass.generateDataList(handler: { (isBool,model)  in
            if isBool {
                self.list = model
                DispatchQueue.main.async {
                    self.tbHelpSupport.delegate = self
                    self.tbHelpSupport.dataSource = self
                    self.tbHelpSupport.reloadData()
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.bool(forKey: "LoginStatus") {
            self.btncallDoctor.isHidden = true
            self.btncallDoctorConstraint.constant = 40.0
        }
        else {
            self.btncallDoctor.isHidden = true
            self.btncallDoctorConstraint.constant = 0.0
        }
    }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnNavToAudioVideo(_ sender: UIButton) {
        //        if let viewController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "AuthController") as? LoginTableViewController{
        //
        //            self.present(viewController, animated: true, completion: nil)
        //        }
        
        
        
        if let viewController = UIStoryboard(name: "Authorization", bundle: nil).instantiateViewController(withIdentifier: "AuthController") as? UINavigationController{ 
            self.present(viewController, animated: true, completion: nil)
        }
        
        
        
        
        
        //        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        //        
        //        let newViewController2 = storyBoard.instantiateViewController(withIdentifier: "AuthController")
        //         
        //        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginTableViewController") as? LoginTableViewController
        //        let navigationController = UINavigationController(rootViewController: newViewController)
        //        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        //        appdelegate.window!.rootViewController = navigationController
        
        
    }
}

extension VMHelpAndSupport : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = list?.data[indexPath.row]
        if indexPath.row == 0 {
            let cell = tbHelpSupport.dequeueReusableCell(withIdentifier: "HeadOffice") as! helpSupportCell
            cell.selectionStyle = .none
            cell.lblHeadOffice.text = item?.value
            return cell
        }else {
            let cell = tbHelpSupport.dequeueReusableCell(withIdentifier: "CallCell") as! helpSupportCell
            return wrapCell(index: indexPath, cell: cell)
        }
    }
    
    func wrapCell(index: IndexPath, cell: helpSupportCell) -> helpSupportCell {
        cell.selectionStyle = .none
        let item = list?.data[index.row]
        cell.btnCall.tag = index.row
        switch item?.title {
        case "CALL US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("Call".localized, for: .normal)
            cell.btnCall.addTarget(self, action: #selector(callOnNumber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "hcall")
            break
        case "SMS US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("Send".localized, for: .normal)
            cell.btnCall.addTarget(self, action: #selector(smsOnNumber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "hmessage")
            break
        case "VIBER":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("Open".localized, for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openViber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "hviber")
            break
        case "WhatsApp":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("Open".localized, for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openWhatsApp(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "hwhatsapp")
            break
        case "EMAIL US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("Open".localized, for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openMail(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "hmail")
            break
        default:
            break
        }
        
        return cell
    }
    
    @objc func onClickCall(_ sender: UIButton) {
        
    }
    
    @objc func callOnNumber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        guard let number = URL(string: "telprompt://" + cleanNumber) else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    @objc  func smsOnNumber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        if !MFMessageComposeViewController.canSendText() {
        }else{
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            composeVC.recipients = [item?.value] as? [String]
            composeVC.body = "Welcome To ZAY OK!"
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @objc  func openViber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let urlWhats = "viber://chat:<\(cleanNumber)>"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    }else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }else {
                    showAlert(alertTitle: "Warning!".localized, description: "Viber app is not install in your Phone, Please install and try agian".localized)
                }
            }
        }
    }
    
    @objc  func openWhatsApp(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let urlWhats = "whatsapp://send?phone=\(cleanNumber)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    }else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }else {
                    showAlert(alertTitle: "Warning!".localized, description: "Whats app is not install in your Phone, Please install and try agian".localized)
                }
            }
        }
    }
    
    
    @objc  func openMail(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let email = item?.value
        if let url = URL(string: "mailto:\(email ?? "INFO@OKZAY.COM")") {
            UIApplication.shared.open(url)
        }else{
            showAlert(alertTitle: "Warning!".localized, description: "Mail is not configured".localized)
        }
        
    }
}


extension VMHelpAndSupport : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


class helpSupportCell : UITableViewCell {
    
    @IBOutlet weak var imgHeadOffice: UIImageView!
    @IBOutlet weak var lblHeadOffice: UILabel!{
        didSet {
            self.lblHeadOffice.text = self.lblHeadOffice.text?.localized
            self.lblHeadOffice.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var lblCall: UILabel!{
        didSet {
            self.lblCall.text = self.lblCall.text?.localized
            self.lblCall.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnCall: UIButton!{
        didSet {
            btnCall.setTitle((btnCall.titleLabel?.text ?? "").localized, for: .normal)
            btnCall.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
        
    }
    
}


//    @IBAction func cartAction(_ sender: UIBarButtonItem) {
//        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
//        self.present(viewController, animated: true, completion: nil)


// self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
// self.searchButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.searchAction(_:)))
//  self.navigationItem.rightBarButtonItems = [ self.cartBarButton, self.searchButton]
// Do any additional setup after loading the view.


//    }
//
//    @IBAction func searchAction(_ sender: UIBarButtonItem) {
//
//        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
//        self.navigationController?.pushViewController(viewController, animated: true)
//
//    }

