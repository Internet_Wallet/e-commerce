//
//  HomeCategories.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class HomeCategories: ZAYOKBaseViewController {
    
    var ListArray: [String]?
    var aPIManager = APIManager()
    var apiManagerClient: APIManagerClient!
    var headerCategoryArray     = [GetAllCategories]()
    var categoryId                  : NSInteger!
    var categoryName: String = ""
    
    fileprivate let cellOne = "UserViewCell"
    fileprivate let cellTwo = "CategoaryViewCell"
    fileprivate let cellThree = "SettingViewCell"
    fileprivate let cellFour = "SegmentViewCell"
    fileprivate let cellFive = "VersionCell"
    
    
    @IBOutlet weak var CategoaryTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        //        statusBarView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        statusBarView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        
        //        statusBarView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 105.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        //        let statusBarColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 245, alpha: 1)
        //        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        CategoaryTbl.estimatedRowHeight = 50
        CategoaryTbl.rowHeight = UITableView.automaticDimension
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.loadInitCategoary()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name("CategoryReload"), object: nil)
        //        self.animate()
        
        if let DrawerViewController = navigationController?.parent as? ViewController{
            DrawerViewController.KYDCDelegate = self
            print(DrawerViewController)
        }
        
    }
    
    @objc func reloadTableView() {
        loadInitCategoary()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.CategoaryTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func animate () {
        self.CategoaryTbl.reloadData()
        let cells = self.CategoaryTbl.visibleCells
        let tableHeight: CGFloat = self.CategoaryTbl.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.3, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    func loadInitCategoary(){
        if UserDefaults.standard.bool(forKey: "LoginStatus") {
            ListArray = ["MY ACCOUNT","MY CART","MY WISHLIST","MY ORDER","HELP & SUPPORT","MEDICINE REQUEST", "LOGOUT"]
        }
        else{
            ListArray =  ["MY ACCOUNT","MY CART","MY WISHLIST","MY ORDER","HELP & SUPPORT","MEDICINE REQUEST"]
        }
        
        self.headerCategoryArray = []
        for category in self.apiManagerClient.leftMenuCategoriesArray {
            if category.isLowestItem == false {
                println_debug(category.name)
                self.headerCategoryArray.append(category)
            }
        }
        DispatchQueue.main.async {
            self.CategoaryTbl.delegate = self
            self.CategoaryTbl.dataSource = self
            self.CategoaryTbl.reloadData()
        }
    }
    
    private func showLogin(screen: String) {
        let regitration_Opened = UserDefaults.standard.value(forKey: "REGITRATION_OPENED") as? Bool ?? false
        if regitration_Opened {
            DispatchQueue.main.async {
                if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                    let nav = UINavigationController()
                    nav.viewControllers = [registrationVC]
                    registrationVC.screenFrom = "Other"
                    RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
                    registrationVC.continueAsGuest = false
                    registrationVC.delegate = self
                    nav.modalPresentationStyle = .fullScreen
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }else {
            if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                loginVC.modalPresentationStyle = .fullScreen
                let nav = UINavigationController()
                nav.viewControllers = [loginVC]
                
                loginVC.screenFrm = "DashBoard / \(screen)"
                loginVC.asGuest = false
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    private func showCart() {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    private func showRegistration() {
        if  let viewController = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController{
            RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
            let nav = UINavigationController()
            nav.viewControllers = [viewController]
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func showLoginVC(){
        
        if UserDefaults.standard.bool(forKey: "GuestLogin") ||  UserDefaults.standard.bool(forKey: "VerifyOnly") {
            //            showCart()
            self.showRegistration()
        }
        else {
            if UserDefaults.standard.bool(forKey: "LoginStatus") {
                if let myAccountVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMMyAccountViewController_ID") as? VMMyAccountViewController {
                    let nav = UINavigationController()
                    myAccountVC.screenFrom = "DashBoard"
                    //                    RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
                    nav.modalPresentationStyle = .fullScreen
                    nav.viewControllers = [myAccountVC]
                    self.present(nav, animated: true, completion: nil)
                }
            } else {
                self.showLogin(screen: "")
            }
        }
    }
    
    private func showMyWishListVC() {
        //        if UserDefaults.standard.bool(forKey: "GuestLogin") || UserDefaults.standard.bool(forKey: "LoginStatus") {
        //            self.performSegue(withIdentifier: "HomeToMyWishlistSegue", sender: self)
        //        } else {
        //            self.showLogin(screen: "WishList")
        //        }
        self.performSegue(withIdentifier: "HomeToMyWishlistSegue", sender: self)
    }
    
    private func showMyOrderVC() {
        if UserDefaults.standard.bool(forKey: "GuestLogin") || UserDefaults.standard.bool(forKey: "LoginStatus") {
            self.performSegue(withIdentifier: "HomeToMyOrderSegue", sender: self)
        }
        else if UserDefaults.standard.bool(forKey: "VerifyOnly"){
            self.performSegue(withIdentifier: "HomeToMyOrderSegue", sender: self)
        }
        else {
            self.showLogin(screen: "MyOrders")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeCategoryToProductCollectionSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductCollections {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                }
            }
        }
        else if segue.identifier == "HomeCategoryToSubCategoriesSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductSubCategories {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                }
            }
        }
    }
    
    private func showAlert(message: String) {
        if message == "Success" {
            DispatchQueue.main.async {
                let alert  = UIAlertController(title: message.localized, message: "Updated successfully".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (alert) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert  = UIAlertController(title: "Warning".localized, message: "Try again".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
     
     
     
     func changeAppLanguageApiCall(index: Int) {
     let type = (index == 0) ? 1 : 2
     let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
     guard let url = URL(string: urlString) else { return }
     let params = Dictionary<String, Any>()
     aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
     if success {
     if let json = response as? Dictionary<String,Any> {
     if let code = json["StatusCode"] as? Int, code == 200 {
     if type == 1 {
     UserDefaults.standard.set("en", forKey: "currentLanguage")
     //                            UserDefaults.standard.set("Century Gothic", forKey: "appFont")
     //                            UserDefaults.standard.synchronize()
     //                                 appFont = "Century Gothic"
     
     appDelegate.setSeletedlocaLizationLanguage(language: "en")
     } else {
     UserDefaults.standard.set("my", forKey: "currentLanguage")
     //                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
     //                            UserDefaults.standard.synchronize()
     //                                  appFont = "Zawgyi-One"
     appDelegate.setSeletedlocaLizationLanguage(language: "my")
     }
     DispatchQueue.main.async {
     self?.CategoaryTbl.reloadData()
     }
     NotificationCenter.default.post(name: NSNotification.Name("AppLanguageChange"), object: nil, userInfo: nil)
     } else {
     self?.showAlert(message: "Failure")
     }
     }
     }
     }
     }
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("CategoryReload"), object: nil)
    }
    
    //MARK: UIGuesture Method
    @objc func tappedOnIconImage() {
        if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
            DrawerTableViewController.setDrawerState(.closed, animated: true)
        }
        performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
    }
    
}

extension HomeCategories: KYDrawerCustomDelegate{
    func resetAllData() {
        self.loadInitCategoary()
    }
    
    
}

/*
 
 
 extension HomeCategories: ChangeLanguageProtocol {
 func changeAppLanguage(index: Int) {
 self.changeAppLanguageApiCall(index: index)
 }
 }
 */

//MARK: - UITableViewDataSource, UITableViewDelegate
extension HomeCategories: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        //        if productList.count == 0 {
        //            let noDataLabel = UILabel(frame: tableView.bounds)
        //            if let myFont = UIFont(name: "Zawgyi-One", size: 17) {
        //                noDataLabel.font = myFont
        //            }
        //            if let _ = promotionProductDetail {
        //                noDataLabel.text = "No Records Found".localized
        //            } else {
        //                noDataLabel.text = "Loading Data...".localized
        //            }
        //            noDataLabel.textAlignment = .center
        //            tableView.backgroundView = noDataLabel
        //            self.lblNumberofProduct.isHidden = true
        //        } else {
        //            self.lblNumberofProduct.isHidden = false
        //            self.lblNumberofProduct.text = "Number of Promotion(s)".localized + " - \(self.productList.count)"
        //            tableView.backgroundView = nil
        //        }
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return self.headerCategoryArray.count
        case 4:
            return self.ListArray!.count
        case 5:
            return 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 || section == 4{
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: CategoaryViewCell!
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: cellOne, for: indexPath) as? CategoaryViewCell
            let tap = UITapGestureRecognizer(target: self, action: #selector(HomeCategories.tappedOnIconImage))
            cell.imgPersonIcon.addGestureRecognizer(tap)
            cell.imgPersonIcon.isUserInteractionEnabled = true
            let firstName = VMLoginModel.shared.firstName
            cell.lblheader.font = UIFont(name: appFont, size: 15.0)
            if firstName != nil {
                cell.lblheader.text = firstName?.localized
            }else {
                cell.lblheader.text = "1 Stop Mart".localized
            }
            //            cell.contentView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            cell.contentView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = "Choose Language English\n(အဂၤလိပ္ဘာသာ)".localized
            cell.wrapCategoary(string: name)
            
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = "Home".localized
            cell.wrapCategoary(string: name)
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = self.headerCategoryArray[indexPath.row].name
            cell.wrapCategoary(string: name)
        case 4:
            cell = tableView.dequeueReusableCell(withIdentifier: cellThree, for: indexPath) as? CategoaryViewCell
            if let name = self.ListArray?[indexPath.row]{
                cell.wrapCategoary(string: name)
            }
            
        case 5:
            cell = tableView.dequeueReusableCell(withIdentifier: cellFour, for: indexPath) as? CategoaryViewCell
            //cell.delegate = self
            cell.lblEnlishLanguage.font = UIFont(name: appFont, size: 14.0)
            cell.lblBurmeseLanguage.font = UIFont(name: appFont, size: 14.0)
            cell.lblEnlishLanguage.text = "English\n(အဂၤလိပ္ဘာသာ)"
            cell.lblBurmeseLanguage.text = "Myanmar\n(ျမန္မာဘာသာ)"
        //cell.configureUI()
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: cellFive, for: indexPath) as? CategoaryViewCell
            //            cell.viewBGVersion.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            cell.viewBGVersion.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            cell.lblVersion.font = UIFont(name: appFont, size: 15.0)
            cell.lblVersion.text = "Version: 1.0".localized
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        case 3:
            return UITableView.automaticDimension
        case 4:
            return UITableView.automaticDimension
        case 5:
            return 100
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 2))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 220, greenValue: 220, blueValue: 220, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch indexPath.section {
            
        case 0:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
            break
            
            
        case 1:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "LanguageSetUpSeque", sender: self)
            break
            
            
        case 2:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)            
            break
        case 3:
            if AppUtility.isConnectedToNetwork() {
                if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                    DrawerTableViewController.setDrawerState(.closed, animated: true)
                }else if let DrawerTableViewController = self.parent as? KYDrawerController{
                    DrawerTableViewController.setDrawerState(.closed, animated: true)
                }
                categoryId = self.headerCategoryArray[indexPath.row].idd
                categoryName = self.headerCategoryArray[indexPath.row].name
                if self.headerCategoryArray[indexPath.row].subCategoriesCount > 0{
                    self.performSegue(withIdentifier: "HomeCategoryToSubCategoriesSegue", sender: self)
                } else {
                    self.performSegue(withIdentifier: "HomeCategoryToProductCollectionSegue", sender: self)
                }
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
            println_debug(self.headerCategoryArray[indexPath.row].idd)
            //print(self.headerCategoryArray[indexPath.row].subCategories[indexPath.row].name)`
            break
        case 4:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }else if let DrawerTableViewController = self.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            if indexPath.row == 0 {
                self.showLoginVC()
            } else if indexPath.row == 1 {
                let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
                self.present(viewController, animated: true, completion: nil)
            } else if indexPath.row == 2 {
                self.showMyWishListVC()
            } else if indexPath.row == 3 {
                self.showMyOrderVC()
            } else if indexPath.row == 4 {
                self.performSegue(withIdentifier: "HometoHelpAndSupport", sender: self)
                
            }else if indexPath.row == 5 {
                
                self.performSegue(withIdentifier: "RequesToHomeUnwindSegue", sender: self)
            }
            else  if indexPath.row == 6{
                if UserDefaults.standard.bool(forKey: "LoginStatus") {
                    
                    if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                        UserDefaults.standard.set(true, forKey: "logout")
                        DrawerTableViewController.setDrawerState(.closed, animated: true)
                    }else if let DrawerTableViewController = self.parent as? KYDrawerController{
                        DrawerTableViewController.setDrawerState(.closed, animated: true)
                    }
                    UserDefaults.standard.set(true, forKey: "logout")
                    performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
                }
            }
            break
        default:
            break
        }
    }
}

/*
 
 protocol ChangeLanguageProtocol: class {
 func changeAppLanguage(index: Int)
 }
 */

//MARK: - PromotionalViewCell
class CategoaryViewCell: UITableViewCell {
    
    @IBOutlet var lblheader: UILabel!{
        didSet {
            self.lblheader.text = self.lblheader.text?.localized
            self.lblheader.font = UIFont(name: appFont, size: 15.0)
        }
    }
    //MARK: - Outlet
    @IBOutlet var lblCatName: UILabel!{
        didSet {
            self.lblCatName.text = self.lblCatName.text?.localized
            self.lblCatName.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var lblCompanyName: UILabel!{
        didSet {
            self.lblCompanyName.text = self.lblCompanyName.text?.localized
            self.lblCompanyName.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var lblDesc: UILabel!{
        didSet {
            self.lblDesc.text = self.lblDesc.text?.localized
            self.lblDesc.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var lblDate: UILabel!{
        didSet {
            self.lblDate.text = self.lblDate.text?.localized
            self.lblDate.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var imgPersonIcon: UIImageView!
    @IBOutlet weak var englishLangView: UIView! {
        didSet {
            englishLangView.roundCorners([.topRight, .bottomRight], radius: 20)
        }
    }
    
    @IBOutlet weak var stackLanguage: UIStackView!
    
    
    @IBOutlet weak var burmeseLangView: UIView! {
        didSet {
            burmeseLangView.roundCorners([.topLeft, .bottomLeft], radius: 20)
        }
    }
    
    @IBOutlet weak var lblEnlishLanguage: UILabel!{
        didSet {
            self.lblEnlishLanguage.text = self.lblEnlishLanguage.text?.localized
            self.lblEnlishLanguage.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblBurmeseLanguage: UILabel!{
        didSet {
            self.lblBurmeseLanguage.text = self.lblBurmeseLanguage.text?.localized
            self.lblBurmeseLanguage.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var viewBGVersion: UIView!
    @IBOutlet weak var lblVersion: UILabel!{
        didSet {
            self.lblVersion.text = self.lblVersion.text?.localized
            self.lblVersion.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    weak var delegate: ChangeLanguageProtocol?
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.imgPersonIcon?.layer.cornerRadius = (self.imgPersonIcon?.frame.width ?? 0) / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapCategoary(string: String) {
        lblCatName.font = UIFont(name: appFont, size: 15.0)
        println_debug("item list : \(string.localized)")
        lblCatName.text = string.localized
    }
    
    /*
     func configureUI() {
     if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "en" {
     //            UserDefaults.standard.set("Century Gothic", forKey: "appFont")
     //            UserDefaults.standard.synchronize()
     englishLangView.backgroundColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 245, alpha: 1)
     burmeseLangView.backgroundColor = UIColor.lightGray
     } else {
     //            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
     //            UserDefaults.standard.synchronize()
     englishLangView.backgroundColor = UIColor.lightGray
     burmeseLangView.backgroundColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 245, alpha: 1)
     }
     }
     
     
     
     
     
     @IBAction func englishLangAction(_ sender: UIButton) {
     if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "my" {
     //            UserDefaults.standard.set("Century Gothic", forKey: "appFont")
     //            UserDefaults.standard.synchronize()
     //            appFont = "Century Gothic"
     self.delegate?.changeAppLanguage(index: 0)
     }
     englishLangView.backgroundColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 245, alpha: 1)
     burmeseLangView.backgroundColor = UIColor.lightGray
     }
     
     @IBAction func burmeseLangAction(_ sender: UIButton) {
     if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "en" {
     //            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
     //            UserDefaults.standard.synchronize()
     //            appFont = "Zawgyi-One"
     self.delegate?.changeAppLanguage(index: 1)
     }
     burmeseLangView.backgroundColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 245, alpha: 1)
     englishLangView.backgroundColor = UIColor.lightGray
     }
     */
    
}

extension HomeCategories : RegistrationDelegate {
    
    func backToMainPage() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
