//
//  CustomMenuforSubCategoary.swift
//  VMart
//
//  Created by ANTONY on 26/11/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

protocol CustomMenuforSubCategoaryDelegate:class {
    func SubTapSubcategoary(indexPath: IndexPath)
    func TapSubcategoary(indexPath: IndexPath,CategoaryId:NSInteger)
    func TapSubcategoary(indexPath: IndexPath,CategoaryId:NSInteger,CatgoaryName:String)
    func backAction(_ sender: UIButton)
}

class CustomMenuforSubCategoary: UIViewController {
    weak var CMSubCatDelegate: CustomMenuforSubCategoaryDelegate?
    var subCategoriesArray          : [FeaturedProductsAndCategory] = []
    var categoryId                  : NSInteger!
    
    @IBOutlet weak var subCategoriesTableView: UITableView!
    @IBOutlet weak var backShadedView: UIView!
    @IBOutlet weak var subCategoriesContainerView: UIView!
    @IBOutlet weak var dismissRightView: UIView!
    @IBOutlet weak var backBottomBtn: UIButton!{
        didSet {
            //            backBottomBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            backBottomBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            backBottomBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            self.backBottomBtn.setTitle(self.backBottomBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    //    }
    //
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug(subCategoriesArray)
        self.initConfigure()
        
    }
    
    func initConfigure(){
        
        subCategoriesTableView.delegate = self
        subCategoriesTableView.dataSource = self
        subCategoriesTableView.estimatedRowHeight = 44
        subCategoriesTableView.rowHeight = UITableView.automaticDimension
        
        self.backShadedView.layer.cornerRadius = 0
        self.backShadedView.layer.shadowOpacity = 0.40
        self.backShadedView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.backShadedView.layer.shadowRadius = 6
        self.backShadedView.layer.shadowColor = UIColor.black.cgColor
        self.backShadedView.layer.masksToBounds = false
        self.backBottomBtn.layer.cornerRadius = 0
    }
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.left && self.subCategoriesContainerView.frame.origin.x == 0{
            println_debug("Swipe Left")
            //            self.subCategoriesContainerView.animateTo(frame: CGRect(x: (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7)
            
            self.subCategoriesContainerView.animateTo(frame: CGRect(x: (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7, completion:{(finished : Bool)  in
                if (finished) {
                    self.willMove(toParent: nil)
                    self.view.removeFromSuperview()
                    self.removeFromParent()
                }
            })
        }
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.right && self.subCategoriesContainerView.frame.origin.x != 0 {
            println_debug("Swipe Right")
            self.subCategoriesContainerView.animateTo(frame: CGRect(x: 0, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7)
        }
    }
    
    @IBAction func dismissSubCategoryListViewTapAction(_ sender: Any) {
        //        self.dismissRightView.isHidden = true
        self.subCategoriesContainerView.animateTo(frame: CGRect(x:  (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7,completion:{(finished : Bool)  in
            if (finished) {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
        //        UIView.animate(withDuration: 0.20, animations: {
        //            self.subCategoriesContainerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        //            self.subCategoriesContainerView.alpha = 0.0;
        //        }, completion:{(finished : Bool)  in
        //            if (finished) {
        //                self.dismiss(animated: true, completion: nil)
        //            }
        //        })
        
        
        
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.CMSubCatDelegate?.backAction(sender)
    }
}

extension CustomMenuforSubCategoary: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subCategoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoriesTableViewCell
        cell.wrapSubCategoary(name: self.subCategoriesArray[indexPath.row].name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.CMSubCatDelegate?.SubTapSubcategoary(indexPath: indexPath)
        //self.CMSubCatDelegate?.TapSubcategoary(indexPath: indexPath, CategoaryId: self.subCategoriesArray[indexPath.row].idd)
        self.CMSubCatDelegate?.TapSubcategoary(indexPath: indexPath, CategoaryId: self.subCategoriesArray[indexPath.row].idd, CatgoaryName: self.subCategoriesArray[indexPath.row].name)
        self.dismissSubCategoryListViewTapAction(indexPath)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
