//
//  String+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit

fileprivate let allowedCharset = CharacterSet
    .decimalDigits
    .union(CharacterSet(charactersIn: "+"))

fileprivate let allowedCharsetDigits = CharacterSet
    .decimalDigits

fileprivate let allowedCharactersAmount = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: ".").union(CharacterSet(charactersIn: ",")))

extension String {
    
    //    func localizedWithComment(_ comment:String) -> String {
    //        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    //    }
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html,
                      .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil) else { return nil }
        return html
    }
    
    func getDigits() -> String?{
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    func toDouble() -> Double {
        return atof(self)
    }
    
    subscript(integerIndex: Int) -> Character {
        let index = self.index(startIndex, offsetBy: integerIndex)
        return self[index]
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
}
