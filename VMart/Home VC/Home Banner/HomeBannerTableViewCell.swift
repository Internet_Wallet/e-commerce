//
//  HomeBannerTableViewCell.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

protocol BannerCollectionCellDelegate:class {
    func collectionView(collectioncell:HomeBannerCollectionViewCell?, didTappedInTableviewTableCell:HomeBannerTableViewCell)
    func pushCustomNavigation(categoryId: String,categoryName: String)
    //other delegate methods that you can define to perform action in viewcontroller
}

class HomeBannerTableViewCell: UITableViewCell {
    
    var pagehomeBanner          = [HomeBanner]()
    weak var cellDelegate:BannerCollectionCellDelegate? //define delegate
    var bannerData = [NSString]()
    var categoryId : String?
    var categoryName : String?
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerCollectionView.register(UINib(nibName: "HomeBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionCell")
        self.bringSubviewToFront(self.pageControl)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateCellWith(bannerData:[NSString],data: [HomeBanner]) {
        self.pagehomeBanner = data
        pageControl.numberOfPages = bannerData.count
        self.bannerData = bannerData
        self.bannerCollectionView.reloadData()
    }
    
}

extension HomeBannerTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! HomeBannerCollectionViewCell
        cell.ProdOrCatId = self.pagehomeBanner[indexPath.row].ProdOrCatId
        cell.CategoryName = self.pagehomeBanner[indexPath.row].CategoryName
        cell.updateData(object: self.pagehomeBanner[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? HomeBannerCollectionViewCell
        self.categoryId = self.pagehomeBanner[indexPath.row].ProdOrCatId
        self.categoryName = self.pagehomeBanner[indexPath.row].CategoryName
        self.cellDelegate?.collectionView(collectioncell: cell, didTappedInTableviewTableCell: self)
        println_debug(bannerData[indexPath.row])
    }
}


extension HomeBannerTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: SCREEN_HEIGHT/3)
    }
}

extension HomeBannerTableViewCell: UIScrollViewDelegate{
    
    //MARK: Private Methods
    
    func reversePhotoArray(photoArray:[HomeBanner], startIndex:Int, endIndex:Int){
        if startIndex >= endIndex{
            return
        }
        //swap(&pagehomeBanner[startIndex], &pagehomeBanner[endIndex])
        
        reversePhotoArray(photoArray: pagehomeBanner, startIndex: startIndex + 1, endIndex: endIndex - 1)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Calculate where the collection view should be at the right-hand end item
        let fullyScrolledContentOffset:CGFloat = bannerCollectionView.frame.size.width * CGFloat(pagehomeBanner.count - 1)
        if (scrollView.contentOffset.x >= fullyScrolledContentOffset) {
            
            // user is scrolling to the right from the last item to the 'fake' item 1.
            // reposition offset to show the 'real' item 1 at the left-hand end of the collection view
            if pagehomeBanner.count>2{
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: 0, endIndex: pagehomeBanner.count - 1)
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: 0, endIndex: 1)
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: 2, endIndex: pagehomeBanner.count - 1)
                let indexPath : NSIndexPath = NSIndexPath(row: 0, section: 0)
                bannerCollectionView.scrollToItem(at: indexPath as IndexPath, at: .left, animated: false)
            }
        }
        else if (scrollView.contentOffset.x == 0){
            
            if pagehomeBanner.count>2{
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: 0, endIndex: pagehomeBanner.count - 1)
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: 0, endIndex: pagehomeBanner.count - 3)
                reversePhotoArray(photoArray: pagehomeBanner, startIndex: pagehomeBanner.count - 2, endIndex: pagehomeBanner.count - 1)
                let indexPath : NSIndexPath = NSIndexPath(row: pagehomeBanner.count - 1, section: 0)
                bannerCollectionView.scrollToItem(at: indexPath as IndexPath, at: .left, animated: false)
            }
        }
    }
}






