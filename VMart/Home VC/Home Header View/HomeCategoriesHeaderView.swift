//
//  HomeCategoriesHeaderView.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/11/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class HomeCategoriesHeaderView: UIView {
    @IBOutlet weak var headerNameLabel: UILabel!{
        didSet {
            self.headerNameLabel.text = self.headerNameLabel.text?.localized
            self.headerNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var viewAllButton: UIButton!{
        didSet {
            //            viewAllButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            viewAllButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            viewAllButton.setTitle((viewAllButton.titleLabel?.text ?? "").localized, for: .normal)
            viewAllButton.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var viewAllButtonWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let lang = UserDefaults.standard.value(forKey: "currentLanguage") as? String, lang == "en" {
            viewAllButtonWidth.constant = 80.0
        } else {
            viewAllButtonWidth.constant = 105.0
        }
        viewAllButton.layer.cornerRadius = 4
    }
}


class CustomDetailView: UIView {
    
    
    @IBOutlet weak var leftNameLabel: UILabel!{
        didSet {
            self.leftNameLabel.text = self.leftNameLabel.text?.localized
            self.leftNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var rightNameLabel: UILabel!{
        didSet {
            self.rightNameLabel.text = self.rightNameLabel.text?.localized
            self.rightNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    func UpdatedLabelData(leftStr:String,rightStr:String)  {
        self.leftNameLabel.text = leftStr
        self.rightNameLabel.text = rightStr
    }
    
    
    //    @IBOutlet weak var viewAllButtonHeight: NSLayoutConstraint!
    
    //    override func awakeFromNib() {
    //        super.awakeFromNib()
    //
    //
    //
    //    }
    
}
