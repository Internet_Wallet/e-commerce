//
//  ProductsCollectionViewCell.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import SDWebImage
import Material

class ProductsCollectionViewCell: UICollectionViewCell {
    
    var productId = 0
    var productName:String = ""
    var categoryName:String = ""
    
    @IBOutlet var productsImageView: UIImageView!
    
    @IBOutlet var gifImageView: UIImageView!{
        didSet{
            gifImageView.isHidden = true
            gifImageView.setGifImage(UIImage(gifName: "new.gif"), manager: SwiftyGifManager(memoryLimit:6))
        }
    }
    
    @IBOutlet var discountPercentageWidthConstraint: NSLayoutConstraint!
    @IBOutlet var discountPercentageLbl: UILabel!{
        didSet {
            self.discountPercentageLbl.text = self.discountPercentageLbl.text?.localized
            self.discountPercentageLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productsNameLbl: UILabel!{
        didSet {
            self.productsNameLbl.text = self.productsNameLbl.text?.localized
            self.productsNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var oldAmountLbl: UILabel!{
        didSet {
            self.oldAmountLbl.text = self.oldAmountLbl.text?.localized
            self.oldAmountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var discountAmountLbl: UILabel!{
        didSet {
            self.discountAmountLbl.text = self.discountAmountLbl.text?.localized
            self.discountAmountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var amountLbl: UILabel!{
        didSet {
            self.amountLbl.text = self.amountLbl.text?.localized
            self.amountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var mainCollectionView: UIView!
    
    
    func fontApply(){
        self.discountPercentageLbl.font = UIFont(name: appFont, size: 15.0)
        self.productsNameLbl.font = UIFont(name: appFont, size: 15.0)
        //        self.oldAmountLbl.font = UIFont(name: appFont, size: 15.0)
        //        self.discountAmountLbl.font = UIFont(name: appFont, size: 15.0)
        //self.amountLbl.font = UIFont(name: appFont, size: 15.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        discountPercentageWidthConstraint.constant = 0
        self.mainCollectionView.shadowToColleectionView()
    }
    //Comment :  error is there in matching section and index 
    func updateCellWithData(homeCollectionData: [GetHomePageProducts], index : Int) {
        if homeCollectionData.count > 0 && index < 2 && index < 1{
            if homeCollectionData[index].MarkAsNew{
                self.gifImageView.isHidden = false
            }
            else{
                self.gifImageView.isHidden = true
            }
            
            productId = homeCollectionData[index].Id
            productName = homeCollectionData[index].Name
            productsImageView.sd_setImage(with: URL(string: homeCollectionData[index].ImageUrl))
            
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: homeCollectionData[index].ImageUrl), imageView: self.productsImageView, imgUrl: homeCollectionData[index].ImageUrl) { (image) in }
            
            
            //            AppUtility.getData(from: URL(string: homeCollectionData[index].ImageUrl)!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.productsImageView.image = UIImage(data: data)
            //                }
            //            }
            
            productsNameLbl.text = homeCollectionData[index].Name
            oldAmountLbl.isHidden = true
            discountAmountLbl.isHidden = true
            
            let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: homeCollectionData[index].Price, oldPrice: homeCollectionData[index].OldPrice, priceWithDiscount: homeCollectionData[index].PriceWithDiscount)
            
            let mmkText = "MMK"
            let mmkFont = UIFont.init(name: appFont, size: 9)
            var productPrice = homeCollectionData[index].Price.replacingOccurrences(of: "MMK", with: "")
            productPrice = productPrice + mmkText
            let attrString = NSMutableAttributedString(string: productPrice)
            let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            amountLbl.attributedText = attrString
            discountAmountLbl.attributedText = attrString
            
            if Price == "" {
                if homeCollectionData[index].DiscountPercentage != 0 {
                    discountPercentageLbl.text = String(format: "%d%@", homeCollectionData[index].DiscountPercentage ?? "", "%")
                    self.discountPercentageWidthConstraint.constant = discountPercentageLbl.intrinsicContentSize.width + 10
                } else {
                    discountPercentageLbl.text = ""
                    self.discountPercentageWidthConstraint.constant = 0
                }
                amountLbl.isHidden = true
                oldAmountLbl.isHidden = false
                discountAmountLbl.isHidden = false
                if homeCollectionData[index].PriceWithDiscount == "" {
                    
                    var productPrice2 = homeCollectionData[index].Price.replacingOccurrences(of: "MMK", with: "")
                    productPrice2 = productPrice2 + mmkText
                    let attrStr = NSMutableAttributedString(string: productPrice2)
                    let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                    discountAmountLbl.attributedText = attrStr
                    
                    
                    let attrString = NSMutableAttributedString(string: "\(homeCollectionData[index].OldPrice)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    let nsRange2 = NSString(string: "\(homeCollectionData[index].OldPrice)" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                    self.oldAmountLbl.attributedText = attrString
                    
                    
                    
                    //                let attrString = NSAttributedString(string: "\(homeCollectionData[index].OldPrice)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    //                self.oldAmountLbl.attributedText = attrString
                } else {
                    var productPrice2 = homeCollectionData[index].PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                    productPrice2 = productPrice2 + mmkText
                    let attrStr = NSMutableAttributedString(string: productPrice2)
                    let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                    discountAmountLbl.attributedText = attrStr
                    
                    
                    
                    
                    let attrString = NSMutableAttributedString(string: "\(homeCollectionData[index].Price)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    let nsRange2 = NSString(string: "\(homeCollectionData[index].Price)" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                    self.oldAmountLbl.attributedText = attrString
                    
                    
                    
                    //                let attrString = NSAttributedString(string: "\(homeCollectionData[index].Price)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    //                self.oldAmountLbl.attributedText = attrString
                }
            }
            else {
                discountPercentageLbl.text = ""
                self.discountPercentageWidthConstraint.constant = 0
                amountLbl.isHidden = false
                oldAmountLbl.isHidden = true
                discountAmountLbl.isHidden = true
            }
        }
    }
    
    func updateCellWithData(homeCollectionData: [HomePageMenuFacture], index : Int) {
        productId = homeCollectionData[index].idd
        productName = homeCollectionData[index].name
        categoryName = homeCollectionData[index].name
        productsImageView.sd_setImage(with: URL(string: homeCollectionData[index].imageUrl))
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: homeCollectionData[index].imageUrl), imageView: productsImageView, imgUrl: homeCollectionData[index].imageUrl) { (image) in }
        
        
        productsNameLbl.text = homeCollectionData[index].name
        amountLbl.text = ""
        oldAmountLbl.text = ""
        discountAmountLbl.text = ""
    }
    
    func updateCellWithData(homeCollectionData: [HomePageCategoryModel], index : Int) {
        if homeCollectionData[0].products[index].MarkAsNew{
            self.gifImageView.isHidden = false
        }
        else{
            self.gifImageView.isHidden = true
        }
        productId = homeCollectionData[0].products[index].Id
        productName = homeCollectionData[0].products[index].Name
        //        if let img = homeCollectionData[0].products[index].ImageUrl as? String{
        //            print(img)
        //        }
        productsImageView.sd_setImage(with: URL(string: homeCollectionData[0].products[index].ImageUrl))
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: homeCollectionData[0].products[index].ImageUrl), imageView: self.productsImageView, imgUrl: homeCollectionData[0].products[index].ImageUrl) { (image) in }
        
        
        
        //        AppUtility.getData(from: URL(string: homeCollectionData[0].products[index].ImageUrl)!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productsImageView.image = UIImage(data: data)
        //            }
        //        }
        
        
        
        productsNameLbl.text = homeCollectionData[0].products[index].Name
        oldAmountLbl.isHidden = true
        discountAmountLbl.isHidden = true
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: homeCollectionData[0].products[index].Price, oldPrice: homeCollectionData[0].products[index].OldPrice, priceWithDiscount: homeCollectionData[0].products[index].PriceWithDiscount)
        
        let mmkText = "MMK"
        let mmkFont = UIFont.init(name: appFont, size: 9)
        var productPrice = homeCollectionData[0].products[index].Price.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice + mmkText
        let attrString = NSMutableAttributedString(string: productPrice)
        let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        amountLbl.attributedText = attrString
        discountAmountLbl.attributedText = attrString
        
        if Price == "" {
            if homeCollectionData[0].products[index].DiscountPercentage != 0 {
                discountPercentageLbl.text = String(format: "%d%@", homeCollectionData[0].products[index].DiscountPercentage ?? "", "%")
                self.discountPercentageWidthConstraint.constant = discountPercentageLbl.intrinsicContentSize.width + 10
            } else {
                discountPercentageLbl.text = ""
                self.discountPercentageWidthConstraint.constant = 0
            }
            amountLbl.isHidden = true
            oldAmountLbl.isHidden = false
            discountAmountLbl.isHidden = false
            if homeCollectionData[0].products[index].PriceWithDiscount == "" {
                var productPrice2 = homeCollectionData[0].products[index].Price.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                discountAmountLbl.attributedText = attrStr
                
                
                
                
                let attrString = NSMutableAttributedString(string: "\(homeCollectionData[0].products[index].OldPrice)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: "\("\(homeCollectionData[0].products[index].OldPrice)")" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                self.oldAmountLbl.attributedText = attrString
                
                
                //                let attrString = NSAttributedString(string: "\(homeCollectionData[0].products[index].OldPrice)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                self.oldAmountLbl.attributedText = attrString
            } else {
                var productPrice2 = homeCollectionData[0].products[index].PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                discountAmountLbl.attributedText = attrStr
                
                
                let attrString = NSMutableAttributedString(string: "\(homeCollectionData[0].products[index].Price)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: "\(homeCollectionData[0].products[index].Price)" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                self.oldAmountLbl.attributedText = attrString
                
                
                
                
                
                //                let attrString = NSAttributedString(string: "\(homeCollectionData[0].products[index].Price)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                self.oldAmountLbl.attributedText = attrString
            }
        }
        else {
            discountPercentageLbl.text = ""
            self.discountPercentageWidthConstraint.constant = 0
            amountLbl.isHidden = false
            oldAmountLbl.isHidden = true
            discountAmountLbl.isHidden = true
        }
        self.fontApply()
    }
}
