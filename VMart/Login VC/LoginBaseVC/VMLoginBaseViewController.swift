//
//  VMLoginBaseViewController.swift
//  VMart
//
//  Created by Kethan on 11/3/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit


class VMLoginBaseViewController: ZAYOKBaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func setNavigation() {
        //        self.navigationController?.navigationBar.barStyle = .default
        //        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 32.0/255.0, green: 89.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        //        self.navigationController?.navigationBar.tintColor = UIColor.white
        //        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func imageTobase64(image: UIImage) -> String {
        var imgStr = ""
        //        var base64String = ""
        //        let  cim = CIImage(image: image)
        //        if (cim != nil) {
        //            let imageData:Data = image.jpegData(compressionQuality: 0.05)!
        //            base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
        //        }
        
        let jpegCompressionQuality: CGFloat = 0.5
        if let base64String = image.jpegData(compressionQuality: jpegCompressionQuality)?.base64EncodedString() {
            imgStr = base64String
        }
        return imgStr
    }
    
    class  func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
            
        }
        return nil
    }
    
    func revertCountryArray() -> NSArray {
        var arrCountry = NSArray.init()
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    func MobileNumberWithCountryCode(num : String) -> (code: String,mob: String) {
        if num != "" {
            var number = num
            let fourDigits = number.prefix(4)
            let countryCode = fourDigits.suffix(2)
            number.removeFirst()
            number.removeFirst()
            number.removeFirst()
            number.removeFirst()
            return (String(countryCode),"0" + number )
        }else {
            return ("","")
        }
    }
    
    func identifyCountryByCode(withPhoneNumber prefix: String) -> (countryCode: String, countryFlag: String) {
        let countArray = revertCountryArray() as! Array<NSDictionary>
        var finalCodeString = ""
        var flagCountry = ""
        for dic in countArray {
            if let code = dic.object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String {
                if prefix == code {
                    finalCodeString = code
                    flagCountry  = flag
                }
            }
        }
        return (finalCodeString,flagCountry)
    }
    
    func convertMobileFormate(countryCode: String, phoneNumber: String)-> (String) {
        var code = countryCode
        var number = phoneNumber
        if code.first == "+" {
            code.removeFirst()
        }
        if number.first == "0" {
            number.removeFirst()
        }
        let finalNumber = "00" + code + number
        return finalNumber
    }
    
    
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension Dictionary {
    func jsonString() -> String {
        var tempDic = self as! Dictionary<String,Any>
        var keys = Array<String>()
        for key in tempDic.keys {
            keys.append(key)
        }
        keys.sort { $0 < $1 }
        var signString = "{"
        var arr: Array<String> = []
        for key in keys {
            let value = tempDic[key]
            if let value = value as? Dictionary<String,Any> {
                //arr.append("\"\(key)\":\"\(value.zx_sortJsonString())\"")不需要引号
                arr.append("\"\(key)\":\(value.jsonString())")
            }else if let value = value as? Array<Any> {
                //arr.append("\"\(key)\":\"\(value.zx_sortJsonString())\"")
                arr.append("\"\(key)\":\(value.jsonString())")
            }else{
                arr.append("\"\(key)\":\"\(tempDic[key]!)\"")
            }
        }
        signString += arr.joined(separator: ",")
        signString += "}"
        return signString
    }
}

extension Array {
    func  jsonString() -> String {
        let array = self
        var arr: Array<String> = []
        var signString = "["
        for value in array {
            if let value = value as? Dictionary<String,Any> {
                arr.append(value.jsonString())
            } else if let value = value as? Array<Any> {
                arr.append(value.jsonString())
            } else{
                arr.append("\"\(value)\"")
            }
        }
        arr.sort { $0 < $1 }
        signString += arr.joined(separator: ",")
        signString += "]"
        return signString
    }
}


class MobileLeftView: UIView {
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var countryNum: UILabel!{
        didSet{
            self.countryNum.font = UIFont.init(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnCountry: UIButton!
    
    class func updateView() -> MobileLeftView {
        let nib   =  UINib(nibName: "MobileLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! MobileLeftView
        
        nib.frame = .init(x: 0.00, y: 0.00, width: 100.00, height: 30.00)
        nib.contentMode = .scaleAspectFill
        nib.layoutUpdates()
        return nib
    }
    
    func setCountryData(countryCode: String, countryImage: UIImage) {
        self.countryNum.text = countryCode
        self.imgCountry.image = countryImage //UIImage(named: countryImage)
        self.imgCountry.contentMode = .scaleAspectFit
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
}

class RegistrationRightView: UIView {
    @IBOutlet weak var lblRequired: UILabel!
    @IBOutlet weak var imgRight: UIImageView!
    
    class func updateView() -> RegistrationRightView {
        let nib   =  UINib(nibName: "RegistrationRightView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! RegistrationRightView
        nib.frame = .init(x: 0.00, y: 0.00, width: 60.00, height: 60.00)
        nib.layoutUpdates()
        return nib
    }
    
    func setRightViewImage(img: String, isImgHidden: Bool, isRequiredHidden: Bool) {
        imgRight.image = UIImage(named: img)
        imgRight.isHidden = isImgHidden
        lblRequired.isHidden = isRequiredHidden
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
}
