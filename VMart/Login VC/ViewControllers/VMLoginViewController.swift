//
//  VMLoginViewController.swift
//  VMart
//
//  Created by Kethan on 11/3/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MessageUI
import SVPinView

enum CountryName {
    case Myanmar
    case Other
}


class VMLoginViewController: VMLoginBaseViewController {
    
    var timer          = Timer()
    var seconds        = 20
    var smsOtpTxt = ""
    var aPIManager = APIManager()
    var txtTag = 0
    var randomOTP = ""
    var screenFrm: String? = "Other"
    var asGuest: Bool?
    private let mobileNumberAcceptedChars = "0123456789"
    var leftView = MobileLeftView.updateView()
    var countryName: CountryName = .Myanmar
    private let EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_"
    
    @IBOutlet var pinView:SVPinView!
    
    lazy var countryPicker: FPNCountryPicker = FPNCountryPicker()
    var countryObject: FPNCountry?
    
    @IBOutlet weak var mobClearBtn: UIButton! {
        didSet {
            self.mobClearBtn.isHidden = true
            mobClearBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var logoTitleLbl: UILabel! {
        didSet {
            self.logoTitleLbl.text = self.logoTitleLbl.text?.localized
            self.logoTitleLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var otpStackView: UIStackView!
    @IBOutlet weak var otpView: UIView! {
        didSet {
            self.otpView.isHidden = true
            self.otpView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    @IBOutlet weak var MobHeaderTitleLbl: UILabel! {
        didSet {
            self.MobHeaderTitleLbl.text = self.MobHeaderTitleLbl.text?.localized
            self.MobHeaderTitleLbl.font = UIFont(name: appFont, size: 14.0)
        }
    }
    
    @IBOutlet weak var mobileNumberTxt: UneditableTextField! {
        didSet {
            self.mobileNumberTxt.text = "09"
            self.mobileNumberTxt.becomeFirstResponder()
        }
    }
    
    @IBOutlet weak var verifyBtn: UIButton! {
        didSet {
            
            //            verifyBtn.layer.cornerRadius = 20
            //            verifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            //            self.verifyBtn.setTitle((self.verifyBtn.titleLabel?.text ?? "").localized, for: .normal)
            
            verifyBtn.layer.cornerRadius = 20
            verifyBtn.layer.masksToBounds = true
            //            verifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            verifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            verifyBtn.setTitle((self.verifyBtn.titleLabel?.text ?? "").localized, for: .normal)
            verifyBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            
            
        }
    }
    
    @IBOutlet weak var otpVerifyBtn: UIButton! {
        didSet {
            //            otpVerifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            otpVerifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.otpVerifyBtn.setTitle((self.otpVerifyBtn.titleLabel?.text ?? "").localized, for: .normal)
            otpVerifyBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        self.navigationController?.modalPresentationStyle = .fullScreen
        VMGeoLocationManager.shared.setUpLocationManager()
        VMGeoLocationManager.shared.startUpdateLocation()
        mobileNumberTxt.setLeftPaddingPoints(0)
        mobileNumberTxt.tintColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
        initialSetUp()
        setupCountryPicker()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Setup background gradient
        let valenciaColor = UIColor.clear
        let discoColor = UIColor.clear
        setGradientBackground(view: pinView, colorTop: valenciaColor, colorBottom: discoColor)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Login".localized
        if let screenFrom = screenFrm, screenFrom.contains("DashBoard") {
            let button = UIButton()
            button.setImage(UIImage(named: "back"), for: .normal)
            button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
            button.imageEdgeInsets.left = -35
            let item = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = item
            //setNavigation()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        let alert = UIAlertController(title: countryObject?.name, message: countryObject?.phoneCode, preferredStyle: .alert)
        //
        //        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        //        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        //
        //        present(alert, animated: true)
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showMyAccount() {
        DispatchQueue.main.async {
            if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMMyAccountViewController_ID") as? VMMyAccountViewController {
                registrationVC.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(registrationVC, animated: true)
            }
        }
    }
    
    @objc func showCountryViewController() {
        println_debug("showCountryViewController")
        
        //
        //                let countryVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        //                countryVC.delegate = self
        //                present(countryVC, animated: true, completion: nil)
        
        
        showSearchController()
        
        
    }
    
    private func initialSetUp() {
        countryName = .Myanmar
        
        leftView.setCountryData(countryCode: "+95", countryImage: UIImage(named: "myanmar")!)
        leftView.btnCountry.addTarget(self, action: #selector(VMLoginViewController.showCountryViewController), for: .touchUpInside)
        self.mobileNumberTxt.leftView  = leftView
        self.mobileNumberTxt.leftViewMode = .always
        
        
        for subview in otpStackView.subviews {
            if let btn = subview as? UIButton {
                btn.setTitle((btn.titleLabel?.text ?? "").localized, for: .normal)
            } else if let lbl = subview as? UILabel {
                lbl.text = lbl.text?.localized
            }
        }
    }
    
    
    private func showRegistrationVC() {
        DispatchQueue.main.async {
            if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                registrationVC.modalPresentationStyle = .fullScreen
                registrationVC.screenFrom = self.screenFrm
                registrationVC.continueAsGuest = self.asGuest
                registrationVC.delegate = self
                self.navigationController?.pushViewController(registrationVC, animated: true)
            }
        }
    }
    
    private func showCartScreen() {
        DispatchQueue.main.async {
            if let cartViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot") as? UINavigationController {
                if let roovc = cartViewController.topViewController {
                    self.navigationController?.pushViewController(roovc, animated: true)
                }
            }
        }
    }
    
    private func showMyWishListScreen() {
        DispatchQueue.main.async {
            if let myWishListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyWishList_ID") as? MyWishList {
                self.navigationController?.pushViewController(myWishListVC, animated: true)
            }
        }
    }
    
    private func showMyOrdersScreen() {
        DispatchQueue.main.async {
            if let myWishListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrder_ID") as? MyOrder {
                self.navigationController?.pushViewController(myWishListVC, animated: true)
            }
        }
    }
    
    private func showAddAddressScreen() {
        DispatchQueue.main.async {
            if let addAddressViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                addAddressViewController.screenFrom = self.screenFrm
                self.navigationController?.pushViewController(addAddressViewController, animated: true)
            }
        }
    }
    
    //MARK:- OTP Generation
    var sixDigitNumber: String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(1000000) )
        } while result.count < 6
        return result
    }
    
    private func showOtpScreen() {
        
        DispatchQueue.main.async {
            self.otpView.isHidden = false
            self.configurePinView()
            self.smsOtpTxt = ""
            
            //            for subview in self.otpStackView.subviews {
            //                if let txtField = subview as? UITextField, txtField.tag == 0 {
            //                    txtField.becomeFirstResponder()
            //                    break
            //                }
            //            }
            
        }
    }
    
    
    //MARK:- Button actions
    @IBAction func verifyButtonAction(_ sender: UIButton) {
        
        if self.mobileNumberTxt.text!.count > 2{
            DispatchQueue.main.async { self.openSMS() }
        }
        else{
            AppUtility.showToastlocal(message: "Please enter mobile number", view: self.view)
        }
        
        
        //        DispatchQueue.main.async {
        //            self.callLoginApi(addressDict: ["street":"37 pansodon street","township":"Botahtaung","region":"yangon","city":"Yangon","Country":"Myanmar"])
        //        }
        
        //self.checkUserLogin()
        //  self.showRegistrationVC()
    }
    
    @IBAction func mobileNumClearAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.countryName == .Myanmar {
                self.mobileNumberTxt.text = "09"
            } else {
                self.mobileNumberTxt.text = ""
            }
            self.mobClearBtn.isHidden = true
            self.disableButtonOnHighlight(isHighlight:true)
            self.mobileNumberTxt.becomeFirstResponder()
        }
    }
    
    @IBAction func otpHideAction(_ tapGesture: UITapGestureRecognizer) {
        otpView.isHidden = true
        for subView in self.otpStackView.subviews {
            if let otpTxt = subView as? UITextField {
                otpTxt.text = ""
            }
        }
        self.txtTag = 0
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }
    
    @IBAction func otpVerifyAction(_sender: UIButton) {
        //        self.otpView.isHidden = true
        
        //        for subview in otpStackView.subviews {
        //            if let txtField = subview as? UITextField {
        //                smsOtpTxt = smsOtpTxt + (txtField.text ?? "")
        //            }
        //        }
        
        if randomOTP == smsOtpTxt {
            self.otpView.isHidden = true
            
            
            //            DispatchQueue.main.async {
            //                self.callLoginApi(addressDict: ["street":"37 pansodon street","township":"Botahtaung","region":"yangon","city":"Yangon","Country":"Myanmar"])
            //            }
            
            
            
            if VMGeoLocationManager.shared.currentLatitude != "0.0" {
                VMGeoLocationManager.shared.getAddressFrom(lattitude: VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en") { (status, data) in
                    if let addressDic = data as? Dictionary<String, String> {
                        DispatchQueue.main.async {
                            self.callLoginApi(addressDict: addressDic)
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.callLoginApi(addressDict: nil)
                }
            }
            
            
        } else {
            //otp doesn't match\
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Warning!".localized, withDescription: "Please enter valid OTP".localized, onController: self)
            let ok = SAlertAction()
            ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                for subView in self.otpStackView.subviews {
                    if let otpTxt = subView as? UITextField {
                        otpTxt.text = ""
                    }
                }
                for subView in self.otpStackView.subviews {
                    if let otpTxt = subView as? UITextField {
                        otpTxt.becomeFirstResponder()
                        break
                    }
                }
                self.txtTag = 0
            })
            alertVC.addAction(action: [ok])
        }
        for subview in otpStackView.subviews {
            if let txtField = subview as? UITextField {
                txtField.text = ""
            }
        }
    }
    
    @IBAction func editingChanged(_ sender: Any) {
        
        if let textField = sender as? UITextField {
            if textField.tag == txtTag {
                if textField.text == "" {
                    txtTag = textField.tag - 1
                    if txtTag == -1 {
                        txtTag = 0
                    }
                    for subView in otpStackView.subviews {
                        if let otpTxt = subView as? UITextField, otpTxt.tag == txtTag {
                            otpTxt.becomeFirstResponder()
                        }
                    }
                } else {
                    txtTag = textField.tag + 1
                    for subView in otpStackView.subviews {
                        if let otpTxt = subView as? UITextField, otpTxt.tag == txtTag {
                            otpTxt.becomeFirstResponder()
                        }
                    }
                }
            }
            if textField.tag == 5 {
                textField.resignFirstResponder()
                self.otpVerifyBtn.sendActions(for: .touchUpInside)
            }
        }
    }
    
    private func disableButtonOnHighlight(isHighlight: Bool) {
        if isHighlight {
            verifyBtn.isHighlighted = true
            verifyBtn.isEnabled = false
        }else {
            verifyBtn.isHighlighted = false
            verifyBtn.isEnabled = true
        }
    }
    
}

/*
 //MARK:- Textfield delegates
 extension VMLoginViewController: UITextFieldDelegate {
 
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
 if textField.tag == -1 {
 guard let tfText   = textField.text else { return false }
 let text       = (tfText as NSString).replacingCharacters(in: range, with: string)
 
 let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptedChars).inverted
 let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
 if string != filteredSet { return false }
 let textCount  = text.count
 
 guard let encoded = string.cString(using: String.Encoding.utf8) else { return false }
 let isBackSpace = strcmp(encoded, "\\b")
 
 if countryName == .Myanmar {
 if range.location <= 1 {
 return false
 }
 }
 
 if (isBackSpace == -92) {
 if countryName == .Myanmar {
 if range.location <= 1 {
 return false
 }
 if textField.text == "09" {
 self.mobClearBtn.isHidden = true
 return false
 }
 }else {
 if textField.text == "" {
 self.mobClearBtn.isHidden = true
 return false
 }
 }
 }
 
 
 
 if text.count < 4 {
 self.mobClearBtn.isHidden = true
 } else {
 self.mobClearBtn.isHidden = false
 }
 
 //Number Validation for Myanmar
 if countryName == .Myanmar {
 
 let chars = textField.text! + string;
 let number = checkValidNumber(number: chars)
 
 if number.isRejected {
 println_debug("Rejected number")
 return false
 }else if number.max == textCount {
 if textField.text!.count <= number.max  {
 textField.text = text
 //self.checkUserLogin()
 self.disableButtonOnHighlight(isHighlight: false)
 self.openSMS()
 textField.resignFirstResponder()
 }
 return false
 }else if textCount > number.max {
 return false
 }else if textCount > number.min {
 self.disableButtonOnHighlight(isHighlight: false)
 }else if textCount < number.min {
 self.disableButtonOnHighlight(isHighlight:true)
 }
 }else {
 if textCount > 13 {
 textField.resignFirstResponder()
 return false
 }else if textCount > 3 {
 self.disableButtonOnHighlight(isHighlight:false)
 }else if textCount < 4 {
 self.disableButtonOnHighlight(isHighlight:true)
 }
 }
 
 return true
 } else {
 if range.location == 1 {
 return false
 }
 }
 return true
 }
 
 
 
 func textFieldDidBeginEditing(_ textField: UITextField) {
 txtTag = textField.tag
 }
 }
 */

//MARK:- Api functions
extension VMLoginViewController {
    private func checkUserLogin() {
        self.view.endEditing(true)
        let urlString = String(format: "%@", APIManagerClient.sharedInstance.base_url_sendSMS)
        guard let url = URL(string: urlString) else { return }
        let params = AppUtility.JSONStringFromAnyObject(value: self.getSendSMSParams() as AnyObject)
        println_debug(params)
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                if let json = response as? Dictionary<String,Any> {
                    println_debug(json)
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        RegistrationModel.share.MobileNumber = json["DestinationNumber"] as? String ?? ""
                        self?.randomOTP = json["OTP"] as? String ?? ""
                        UserDefaults.standard.set(json["IsUserRegistered"] as? Bool, forKey: "IsUserRegistered")
                        UserDefaults.standard.synchronize()
                        self?.showOtpScreen()
                    } else {
                        DispatchQueue.main.async {
                            self?.disableButtonOnHighlight(isHighlight: false)
                            self?.showAlert(alertTitle: "Login".localized, description: "Login failed. Please try again".localized)
                        }
                    }
                }
            }else {
                self?.disableButtonOnHighlight(isHighlight: false)
                self?.showAlert(alertTitle: "Login".localized, description: "Login failed. Please try again".localized)
            }
        }
    }
    
    func getSendSMSParams() -> [String:Any] {
        var paramDic            = [String:Any]()
        paramDic["Application"]  = "1 Stop Mart"
        var mobileNo = mobileNumberTxt.text ?? ""
        if mobileNo.hasPrefix("09") {
            mobileNo = "0095" + mobileNo.dropFirst()
        }
        paramDic["DestinationNumber"]  = mobileNo
        paramDic["Operator"]  = "mpt"//appDelegate.getNetworkCode().1
        paramDic["AppVersionName"] = "1.0"
        paramDic["DeviceTypeId"] =  "5"//APIManagerClient.sharedInstance.DeviceTypeId
        paramDic["BuildType"] = "release"
        return paramDic
    }
    
    private func handleLogin() {
        if let screenFrom = self.screenFrm, screenFrom == "Cart" {
            if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                self.showCartScreen()
            } else {
                self.showAddAddressScreen()
            }
        } else {
            if let screenFrm = self.screenFrm {
                if screenFrm.contains("WishList") {
                    self.showMyWishListScreen()
                } else if screenFrm.contains("MyOrders") {
                    self.showMyOrdersScreen()
                } else {
                    self.showCartScreen()
                }
            } else {
                self.showCartScreen()
            }
        }
    }
    
    private func callLoginApi(addressDict: Dictionary<String, String>?) {
        
        /*
         let urlString = String(format: "%@/customer/registerorlogin", APIManagerClient.sharedInstance.base_url)
         guard let url = URL(string: urlString) else { return }
         // let params = AppUtility.JSONStringFromAnyObject(value: self.getLoginParams(addressDict: addressDict) as AnyObject)
         AppUtility.showLoading(self.view)
         aPIManager.genericClass(url: url, param: self.getLoginParams(addressDict: addressDict) as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
         
         DispatchQueue.main.async{
         if let viewLoc = self?.view {
         AppUtility.hideLoading(viewLoc)
         }
         
         if success {
         let json = JSON(response)
         println_debug("Profile Response: \(json)")
         if let code = json["StatusCode"].int, code == 200 {
         if let loginInfoDic  = json["Data"].dictionaryObject {
         let vmTemoModel = VMTempModel()
         vmTemoModel.wrapDataModel(JSON: loginInfoDic)
         UserDefaults.standard.set(false, forKey: "VerifyOnly")
         }
         self?.handleLogin()
         } else {
         UserDefaults.standard.set(RegistrationModel.share.MobileNumber, forKey: "MOBILE_NUMBER")
         if self?.screenFrm == "DashBoard / MyOrders"{
         UserDefaults.standard.set(true, forKey: "VerifyOnly")
         UserDefaults.standard.synchronize()
         
         self?.showMyOrdersScreen()
         }
         else{
         self?.showRegistrationVC()
         }
         }
         }else {
         self?.showAlert(alertTitle: "Login".localized, description: "Login failed. Please try again".localized)
         }
         }
         }
         
         */
        
        
        ////////////////////////////////////////////////////////
        
        
        
        /////// working code
        
        
        AppUtility.showLoading(self.view)
        aPIManager.LoginInUser(self.getLoginParams(addressDict: addressDict) as [String : AnyObject], onSuccess: { [weak self] response in
            
            
            DispatchQueue.main.async{
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                
                
                let json = JSON(response)
                println_debug("Profile Response: \(json)")
                // &&  UserDefaults.standard.bool(forKey: "IsUserRegistered")
                if let code = json["StatusCode"].int, code == 200 {
                    if let loginInfoDic  = json["Data"].dictionaryObject {
                        let vmTemoModel = VMTempModel()
                        vmTemoModel.wrapDataModel(JSON: loginInfoDic)
                        UserDefaults.standard.set(false, forKey: "VerifyOnly")
                    }
                    self?.handleLogin()
                } else {
                    UserDefaults.standard.set(RegistrationModel.share.MobileNumber, forKey: "MOBILE_NUMBER")
                    if self?.screenFrm == "DashBoard / MyOrders"{
                        UserDefaults.standard.set(true, forKey: "VerifyOnly")
                        UserDefaults.standard.synchronize()
                        
                        self?.showMyOrdersScreen()
                    }
                    else{
                        self?.showRegistrationVC()
                    }
                }
                
            }
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
        
        
        
        
        
        //        if UserDefaults.standard.bool(forKey: "IsUserRegistered"){
        //            UserDefaults.standard.set(false, forKey: "VerifyOnly")
        //            self.handleLogin()
        //        }else{
        //            UserDefaults.standard.set(RegistrationModel.share.MobileNumber, forKey: "MOBILE_NUMBER")
        //            if self.screenFrm == "DashBoard / MyOrders"{
        //                UserDefaults.standard.set(true, forKey: "VerifyOnly")
        //                UserDefaults.standard.synchronize()
        //                self.showMyOrdersScreen()
        //            }
        //            else{
        //                self.showRegistrationVC()
        //            }
        //        }
    }
    
    func getLoginParams(addressDict: Dictionary<String, String>?) -> [String: Any] {
        
        var paramDic            = [String: Any]()
        var deviceInfoDic = [String: Any]()
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        var mobileNo = mobileNumberTxt.text ?? ""
        if mobileNo.hasPrefix("09"){
            mobileNo = "0095" + mobileNo.dropFirst()
        }
        let token = ""
        let mode = "Register"
        let password = uuid.suffix(6)
        var ip = ""
        if let ipAddress = AppUtility.getIPAddress() {
            ip = ipAddress
        }
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        let stringFloat =  String(describing: floatVersion)
        
        paramDic["Address1"] = addressDict?["township"] ?? "Bogyoke Road"
        paramDic["Address2"] = addressDict?["region"] ?? "Kyauktada Township"
        paramDic["City"] = addressDict?["city"] ?? "Kyauktada"
        paramDic["Country"] = addressDict?["Country"] ?? "Myanmar"
        paramDic["State"] = addressDict?["State"] ?? "Yangon Division"
        paramDic["DateOfBirthDay"] = "29"
        paramDic["DateOfBirthYear"] = "1991"
        paramDic["DateofBirthMonth"] = "03"
        paramDic["DeviceID"] = uuid
        
        // paramDic["ProfilePictureUrl"] = ""
        
        deviceInfoDic["ViberNumber"] = ""
        
        // paramDic["MaritalStatus"] = 0
        // paramDic["DisplayAvatar"] = true
        
        deviceInfoDic["BSSID"] = "fc4203d1fb2"
        deviceInfoDic["BluetoothAddress"] = "y"
        deviceInfoDic["BluetoothName"] = "Xperia XZ1 Compact"
        deviceInfoDic["Cellid"] = "0"
        deviceInfoDic["ConnectedNetworkType"] = "WIFI"
        deviceInfoDic["DeviceSoftwareVersion"] = stringFloat
        deviceInfoDic["HiddenSSID"] = false
        deviceInfoDic["IPAddress"] = ip
        deviceInfoDic["LinkSpeed"] = 0
        deviceInfoDic["MACAddress"] = "02:00:00:00:00:00"
        deviceInfoDic["Msid1"] = "42342234"
        deviceInfoDic["Msid2"] = "543543342234"
        deviceInfoDic["NetworkCountryIso"] = "mm"
        deviceInfoDic["NetworkID"] = 0
        deviceInfoDic["NetworkOperator"] = "41406"
        deviceInfoDic["NetworkOperatorName"] = "Telenor"
        deviceInfoDic["NetworkSignal"] = 0
        deviceInfoDic["NetworkType"] = "4G"
        deviceInfoDic["PhoneType"] = "GSM"
        deviceInfoDic["SIMCountryIso"] = "mm"
        deviceInfoDic["SIMOperator"] = "41406"
        deviceInfoDic["SIMOperatorName"] = "Telenor"
        deviceInfoDic["SSID"] = "aman"
        deviceInfoDic["Simid1"] = "42342234"
        deviceInfoDic["Simid2"] = "324342234"
        deviceInfoDic["VoiceMailNo"] = "200"
        deviceInfoDic["isNetworkRoaming"] = false
        deviceInfoDic["Latitude"] = VMGeoLocationManager.shared.currentLatitude
        deviceInfoDic["Longitude"] = VMGeoLocationManager.shared.currentLongitude
        paramDic["DeviceInfo"] = deviceInfoDic
        paramDic["Email"] = "uytt@okdollar.com"
        paramDic["Firstname"] = "ghgffdd"
        paramDic["Gender"] = "M"
        paramDic["Lastname"] = "ghgh"
        paramDic["MobileNumber"] = mobileNo
        paramDic["Mode"] = mode
        paramDic["OtherEmail"] = "venkat@okdollar.com"
        paramDic["Password"] = password
        paramDic["Simid"] = password
        //paramDic["State"] = nil
        paramDic["Token"] = token
        paramDic["Username"] = mobileNo
        paramDic["VersionCode"] = APIManagerClient.sharedInstance.versioncode
        return paramDic
        
    }
    
    
}

extension VMLoginViewController : CountryViewControllerDelegate {
    func countryViewController(_ list: CountryViewController, country: Country) {
        DispatchQueue.main.async {
            if country.dialCode == "+95" {
                self.countryName = .Myanmar
                self.mobileNumberTxt.text = "09"
            }else {
                self.mobileNumberTxt.text = ""
                self.countryName = .Other
            }
            self.leftView.setCountryData(countryCode: country.dialCode, countryImage: UIImage(named: country.code)!)
            self.disableButtonOnHighlight(isHighlight:true)
            
            self.mobClearBtn.isHidden = true
            list.dismiss(animated: true, completion: nil)
        }
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}

extension VMLoginViewController: MFMessageComposeViewControllerDelegate {
    func openSMS() {
        seconds = 20
        var defaultNumber = [String]()
        //defaultNumber.append("+959954672485") // Oreedo
        defaultNumber.append("+959772615693") // Telenor
        //defaultNumber.append("+959891496195") // MPT, Mytel,Mectel
        let manager  =  UUID2//EncryptionUUID.encryptionmanager() as! EncryptionUUID
        // let encryptMsg =  ""//manager.entryptNumber(uuid)
        let msg = String.init(format: "1 Stop Mart-#%@", manager)
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = msg
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            controller.modalPresentationStyle = .fullScreen
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        switch result {
            
        case .sent:
            controller.dismiss(animated: true, completion: nil)
            let time : DispatchTime = DispatchTime.now() + 30.0
            //PTLoader.shared.show()
            AppUtility.showLoading(self.view)
            DispatchQueue.main.async {
                //                let vc = self.storyboard!.instantiateViewController(withIdentifier: String.init(describing: LoginTimerViewController.self)) as! LoginTimerViewController
                //                if let keyWindow = UIApplication.shared.keyWindow {
                //                    vc.view.frame = .init(x: self.view.frame.width/2 - 64, y:  self.view.frame.height - 148, width: 128.00, height: 128.00)
                //                    vc.view.tag = 1473
                //                    keyWindow.addSubview(vc.view)
                //                    keyWindow.makeKeyAndVisible()
                //                    keyWindow.bringSubviewToFront(vc.view)
                //                }
                self.timer.fire()
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            }
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                AppUtility.hideLoading(self.view)
                self.checkUserLogin()
            })
            break
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
            println_debug("remove window")
            break
        case .failed:
            controller.dismiss(animated: true, completion: nil)
            println_debug("popup remove, dismiss")
            break
        }
    }
}

extension VMLoginViewController {
    
    @objc func updateTimer() {
        seconds -= 1
        verifyBtn.setTitle(("Verifying \(seconds) Sec").localized, for: .normal)
        if seconds == 0{
            self.timer.invalidate()
        }
        //        verifyBtn.titleLabel?.text = "Verifying \(seconds) Sec"
    }
    
}

extension VMLoginViewController : RegistrationDelegate {
    
    func backToMainPage() {
        self.dismissScreen()
    }
}

extension VMLoginViewController : PhValidationProtocol {
    
    private func checkValidNumber(number: String) ->(min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        let tuple = myanmarValidation(number)
        return tuple
    }
    
}

extension VMLoginViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == -1 &&  self.countryName == .Myanmar{
            if range.location == 0, string == " " {
                return false
            }
            //            guard let tfText   = textField.text else { return false }
            //            let text = (tfText as NSString).replacingCharacters(in: range, with: string)
            
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let rangeCheck = PayToValidations().getNumberRangeValidation(text)
            let textCount  = text.count
            if !text.hasPrefix("09") {
                return false
            }
            if rangeCheck.isRejected {
                textField.text = "09"
                AppUtility.showToastlocal(message: "Please enter valid other mobile number", view: self.view!)
                return false
            }
            
            if countryName == .Myanmar {
                if range.location <= 1 {
                    return false
                }
            }
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    
                    if textField.tag == 6 {
                        if textField.text == "09" {
                            return false
                        }
                    }
                }
            }
            
            if text.count < 4 {
                self.mobClearBtn.isHidden = true
            } else {
                self.mobClearBtn.isHidden = false
            }
            
            //Number Validation for Myanmar
            if countryName == .Myanmar {
                
                let chars = textField.text! + string;
                let number = checkValidNumber(number: chars)
                
                if number.isRejected {
                    println_debug("Rejected number")
                    return false
                }else if number.max == textCount {
                    if textField.text!.count <= number.max  {
                        textField.text = text
                        //self.checkUserLogin()
                        self.disableButtonOnHighlight(isHighlight: false)
                        DispatchQueue.main.async {
                            
                            
                            //                            DispatchQueue.main.async {
                            //                                self.callLoginApi(addressDict: ["street":"37 pansodon street","township":"Botahtaung","region":"yangon","city":"Yangon","Country":"Myanmar"])
                            //                            }
                            
                            
                            self.openSMS()
                            
                            
                            
                        }
                        textField.resignFirstResponder()
                    }
                    return false
                }else if textCount > number.max {
                    return false
                }else if textCount >= number.min {
                    self.disableButtonOnHighlight(isHighlight: false)
                }else if textCount < number.min {
                    self.disableButtonOnHighlight(isHighlight:true)
                }
            }else {
                if textCount > 13 {
                    textField.resignFirstResponder()
                    return false
                }else if textCount > 3 {
                    self.disableButtonOnHighlight(isHighlight:false)
                }else if textCount < 4 {
                    self.disableButtonOnHighlight(isHighlight:true)
                }
            }
            
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptedChars).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            if string != filteredSet { return false }
            
            if text.count >= rangeCheck.min {
                
            }
            
            if text.count == rangeCheck.max {
                
                textField.text = text
                
                return false
                
            } else if text.count > rangeCheck.max {
                
                return false
            }
            
        }
        
        if textField.tag == -1 &&  self.countryName == .Other{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            
            if textCount > 3{
                self.disableButtonOnHighlight(isHighlight:false)
            }
            if textCount > 13{
                return false
            }
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    
                    if textCount <= 3{
                        self.disableButtonOnHighlight(isHighlight:true)
                    }
                }
            }
            
            
            if text.count <= 2 {
                self.mobClearBtn.isHidden = true
            } else {
                self.mobClearBtn.isHidden = false
            }
            
            
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(self.countryName)
        if textField.tag == -1 &&  self.countryName == .Myanmar  {
            
            if let text = textField.text, text.count < 3 {
                textField.text = "09"
            }
        }
        
        return true
    }
    
}



extension VMLoginViewController: FPNDelegate{
    
    // - FPNDelegate
    
    internal func fpnDidSelect(country: FPNCountry) {
        //        setFlag(for: country.code)
        print(country)
        //        self.showToastlocal(message:  country.name + country.phoneCode, view: super.view)
        countryObject = country
        
        
        DispatchQueue.main.async {
            self.mobileNumberTxt.becomeFirstResponder()
            if self.countryObject?.phoneCode == "+95" {
                self.countryName = .Myanmar
                self.mobileNumberTxt.text = "09"
            }else {
                self.mobileNumberTxt.text = ""
                self.countryName = .Other
            }
            self.leftView.setCountryData(countryCode: self.countryObject!.phoneCode, countryImage: self.countryObject!.flag!)
            self.disableButtonOnHighlight(isHighlight:true)
            
            //self.mobClearBtn.isHidden = true
            
        }
    }
    
    func setupCountryPicker() {
        
        countryPicker.showPhoneNumbers = true
        countryPicker.backgroundColor = .white
        if let regionCode = Locale.current.regionCode, let countryCode = FPNCountryCode(rawValue: regionCode) {
            countryPicker.setCountry(countryCode)
        }
        (mobileNumberTxt.delegate as? FPNTextFieldDelegate)?.fpnDidValidatePhoneNumber(textField: mobileNumberTxt as! FPNTextField, isValid: true)
    }
    
    
    private func showSearchController() {
        if let countries = countryPicker.countries {
            let searchCountryViewController = FPNSearchCountryViewController(countries: countries)
            let navigationViewController = UINavigationController(rootViewController: searchCountryViewController)
            //            navigationViewController.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            navigationViewController.modalPresentationStyle = .fullScreen
            searchCountryViewController.delegate = self
            self.present(navigationViewController, animated: true, completion: nil)
        }
    }
}


extension VMLoginViewController{
    func configurePinView() {
        
        pinView.pinLength = 6
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 10
        pinView.textColor = UIColor.white
        pinView.borderLineColor = UIColor.white
        pinView.activeBorderLineColor = UIColor.white
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = true
        pinView.allowsWhitespaces = false
        pinView.style = .none
        pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
        pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 15
        pinView.activeFieldCornerRadius = 15
        pinView.placeholder = "******"
        pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
        }
    }
    
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers! {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func didFinishEnteringPin(pin:String) {
        smsOtpTxt = pin
        self.otpVerifyAction(_sender: self.verifyBtn)
    }
    
}
