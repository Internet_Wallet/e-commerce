//
//  VMRegistrationViewController.swift
//  VMart
//
//  Created by Kethan on 11/3/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON
import Foundation
import AVFoundation
import SkyFloatingLabelTextField

enum genderType {
    case nothing
    case male
    case female
}

enum prifilePic {
    case taken
    case notTaken
}

@objc protocol RegistrationDelegate: class {
    @objc optional func backToMainPage()
}


class VMRegistrationViewController: VMLoginBaseViewController {
    
    weak var delegate: RegistrationDelegate?
    var aPIManager = APIManager()
    let group = DispatchGroup()
    var continueAsGuest: Bool?
    var datePicker: UIDatePicker?
    let formatter  = DateFormatter()
    var gender: genderType = .nothing
    var profileImage : prifilePic = .notTaken
    var screenFrom: String? = "Other"
    var imagePicker: UIImagePickerController!
    var userImage: UIImage?
    var leftView = MobileLeftView.updateView()
    var localData = ["","","",""]
    
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var base64ImageData = ""
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var cameraContainer: UIView!
    
    
    @IBOutlet weak var registationTV: UITableView!
    @IBOutlet weak var logoTitleLbl: UILabel! {
        didSet {
            self.logoTitleLbl.text = self.logoTitleLbl.text?.localized
            self.logoTitleLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    //var rightView = RegistrationRightView.updateView()
    
    private let nameAcceptedChars  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY .,"
    private let nameMyAcceptedChars = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ .,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY .,"
    private let EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cameraConfigure()
        UserDefaults.standard.set(true, forKey: "REGITRATION_OPENED")
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        //        self.view.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.view.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        self.title = "Registration".localized
        
        RegistrationModel.share.SIMOperatorName = preNetInfo?.networkName ?? ""
        let number = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as? String
        RegistrationModel.share.Username = number ?? ""
        RegistrationModel.share.MobileNumber = number ?? ""
        //        UserDefaults.standard.set(RegistrationModel.share.MobileNumber, forKey: "MOBILE_NUMBER")
        RegistrationModel.share.clearData()
        
        backButton()
        // initialSetup()
    }
    
    private func backButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
    }
    
    @objc private func popScreen() {
        self.navigationController?.popViewController(animated: false)
        if let del = delegate {
            del.backToMainPage!()
        }
        if UserDefaults.standard.bool(forKey: "VerifyOnly"){
            self.performSegue(withIdentifier: "unwindbacktohome", sender: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.checkOKDollar()
        
    }
    
    
    func checkOKDollar(){
        let urlTaxi = "OKDollar://"
        
        let params = String.init(format: urlTaxi)
        
        if let urlString = params.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let urlOk = URL(string: urlString) {
                print("Contains\(urlOk)")
                //To open OKDollar app
                UserDefaults.standard.set(true, forKey: "OKDollarStatus")
                UserDefaults.standard.synchronize()
                
            } else {
                print("Not")
                UserDefaults.standard.set(false, forKey: "OKDollarStatus")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    
    @IBAction func backAction(){
        self.performSegue(withIdentifier: "unwindbacktohome", sender: self)
        
    }
    
    
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        self.navigationController?.navigationBar.shadowImage = nil
    //        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    //        self.navigationController?.navigationBar.barTintColor =  UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1)
    //        self.navigationController?.view.backgroundColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1)
    //        self.navigationController?.setNavigationBarHidden(false, animated: true)
    //    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer!.frame = previewView.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.session!.stopRunning()
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    private func initialSetup() {
        formatter.dateFormat = "dd/MM/yyyy"
        VMGeoLocationManager.shared.startUpdateLocation()
        
        // Do any additional setup after loading the view.
        if let screenFrm = screenFrom, screenFrm == "DashBoard" {
            self.navigationController?.navigationBar.tintColor = UIColor.blue
            self.navigationController?.isNavigationBarHidden = false
            let button = UIButton()
            button.setImage(UIImage(named: "back"), for: .normal)
            button.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            button.imageEdgeInsets.left = -35
            button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
            let item = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItems = [item]
        }
    }
    
    //MARK:- Calendar setup
    func pickUpDate(txtField : UITextField) {
        
        datePicker = UIDatePicker(frame: CGRect(x: 0.0, y: self.view.frame.height - 216.0, width: 0.0, height: 216.0))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker?.datePickerMode = .date
        if let dateStr = txtField.text, dateStr.count > 0, dateStr != "dd/MM/yyyy" {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if let selDate = formatter.date(from: dateStr) {
                datePicker?.date = selDate
            }
        }
        
        let calendar = Calendar.init(identifier: .gregorian)
        var dateComponents = DateComponents()
        dateComponents.year = -12
        let maxDate = calendar.date(byAdding: dateComponents, to: Date())
        dateComponents.year = -80
        let minDate = calendar.date(byAdding: dateComponents, to: Date())
        
        datePicker?.maximumDate = maxDate
        datePicker?.minimumDate = minDate
        datePicker?.calendar = Calendar(identifier: .gregorian)
        txtField.inputView = datePicker
        
        self.dateChanged(datePicker!)
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 128/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtField.inputAccessoryView = toolBar
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let date = sender.date
        let cell = getCell(row: 3, sec: 1)
        let frmt = DateFormatter()
        frmt.dateFormat = "dd-MM-yyyy"
        cell.tfName.text = frmt.string(from: date)
    }
    
    private func showMyWishListScreen() {
        DispatchQueue.main.async {
            if let myWishListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyWishList_ID") as? MyWishList {
                self.navigationController?.pushViewController(myWishListVC, animated: true)
            }
        }
    }
    
    private func showMyOrdersScreen() {
        DispatchQueue.main.async {
            if let myWishListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrder_ID") as? MyOrder {
                self.navigationController?.pushViewController(myWishListVC, animated: true)
            }
        }
    }
    
    private func showAddAddressScreen() {
        DispatchQueue.main.async {
            if let addAddressViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                addAddressViewController.screenFrom = self.screenFrom
                self.navigationController?.pushViewController(addAddressViewController, animated: true)
            }
        }
    }
    
    private func showCartScreen() {
        DispatchQueue.main.async {
            if let cartViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot") as? UINavigationController {
                if let roovc = cartViewController.topViewController {
                    self.navigationController?.pushViewController(roovc, animated: true)
                }
            }
        }
    }
    
    private func getCell(row : Int, sec: Int) -> NameCell {
        let indexPath = IndexPath(row: row, section: sec)
        return registationTV.cellForRow(at: indexPath) as! NameCell
    }
    
    
    //MARK:- calendar button actions
    @objc func doneClick() {
        let cell = getCell(row: 3, sec: 1)
        cell.tfName.resignFirstResponder()
        localData[3] = cell.tfName.text ?? ""
        
    }
    
    @objc func cancelClick()  {
        let cell = getCell(row: 3, sec: 1)
        cell.tfName.resignFirstResponder()
    }
    
    
    private func checkAllRequiredField(handler: (_ success: Bool ,_ message: String) -> Void) {
        
        if localData[1] == "" {
            //            handler(false, "Please enter user name".localized)
            AppUtility.showToastlocal(message: "Please enter user name".localized, view: self.view)
            return
        }else if localData[1].count < 3 {
            //            handler(false, "Name should be minimum 3 characters".localized)
            AppUtility.showToastlocal(message: "Name should be minimum 3 characters".localized, view: self.view)
            return
        }
        
        
        if !AppUtility.isValidEmail(localData[2]) && localData[2] != "" {
            //            handler(false, "Please enter valid Email Id".localized)
            AppUtility.showToastlocal(message: "Please enter valid Email Id".localized, view: self.view)
            return
        }
        
        if localData[3] == "" {
            //            handler(false, "Please select date of birth".localized)
            AppUtility.showToastlocal(message: "Please select date of birth".localized, view: self.view)
            return
        }
        //        if localData[0] == "" {
        //            handler(false, "Please take selfie".localized)
        //        }
        
        if gender == .nothing {
            //            handler(false, "Please select gender".localized)
            AppUtility.showToastlocal(message: "Please select gender".localized, view: self.view)
            return
        }
        handler(true, "success")
        return
    }
    
    private func uploadProfilePic(handler: @escaping (_ success: Bool) -> Void) {
        
        if localData[0] != ""  {
            let paramString: [String : Any] = [
                "MobileNumber" : RegistrationModel.share.MobileNumber,
                "Base64String" : [base64ImageData,"","","",""],
            ]
            AppUtility.showLoading(self.view)
            aPIManager.UploadImage(params: paramString , url: "https://www.okdollar.co/RestService.svc/GetMultiImageUrlByBase64String", successBlock: {(success, response) in
                DispatchQueue.main.async { AppUtility.hideLoading(self.view) }
                if success { 
                    if let firstUrl = response.first as? String {
                        RegistrationModel.share.ProfilePictureUrl = firstUrl.replacingOccurrences(of: " ", with: "%20")
                    }
                    handler(true)
                }else {
                    handler(false)
                }
            })
        }else {
            handler(true)
        }
    }
}

extension VMRegistrationViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            if profileImage == .taken {
                return 4
            }else {
                return 5
            }
        }else if section == 2{
            return 1
        }else {
            //            if self.continueAsGuest!{
            //                return 4
            //            }else{
            //                return 2
            //            }
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePicCell") as! ProfilePicCell
            
            if let img = userImage {
                cell.imgProfilePic.image = img
                cell.imgProfilePic.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(VMRegistrationViewController.tappedMe))
                cell.imgProfilePic.addGestureRecognizer(tap)
            }
            return cell
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MobilNumberCell") as! MobilNumberCell
                cell.selectionStyle = .none
                cell.imgCountry.image = UIImage(named:"myanmar")
                cell.lblTitle.text = "Mobile Number".localized
                var number = RegistrationModel.share.MobileNumber
                let fourDigits = number.prefix(4)
                let countryCode = fourDigits.suffix(2)
                number.removeFirst()
                number.removeFirst()
                number.removeFirst()
                number.removeFirst()
                cell.lblNumber.text = "(+" + countryCode + ")" + " 0" + number
                return cell
            }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as! NameCell
                return nameCell(index: indexPath, cell: cell)
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageCell") as! ProfileImageCell
                cell.selectionStyle = .none
                cell.btnProfileImage.setTitle("Take Your Photo".localized, for: .normal)
                cell.btnProfileImage.addTarget(self, action: #selector(self.onSelectCamera), for: .touchUpInside)
                return cell
            }
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell") as! GenderCell
            if gender == .male {
                cell.imgMaleRadio.image = UIImage(named: "act_radio")
                cell.imgFemaleRadio.image = UIImage(named: "radio")
            }else if gender == .female{
                cell.imgMaleRadio.image = UIImage(named: "radio")
                cell.imgFemaleRadio.image = UIImage(named: "act_radio")
            }else {
                cell.imgMaleRadio.image = UIImage(named: "radio")
                cell.imgFemaleRadio.image = UIImage(named: "radio")
            }
            cell.btnMale.addTarget(self, action: #selector(self.onSelectGender(_:)), for: .touchUpInside)
            cell.btnFemale.addTarget(self, action: #selector(self.onSelectGender(_:)), for: .touchUpInside)
            return cell
        }else {
            
            if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DesignCell")
                return cell!
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RegistrationCell") as! RegistrationCell
                return registerCell(index: indexPath, cell: cell)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if profileImage == .taken {
                return 140
            }else {
                return 0
            }
        }else if indexPath.section == 3{
            if indexPath.row == 1{
                if continueAsGuest ?? false {
                    return 60
                }
                else{
                    return 0
                }
            }
            else if indexPath.row == 2{
                return 60
            }else{
                return 90
            }
        }else{
            return 60
        }
    }
    
    private func nameCell(index: IndexPath, cell: NameCell) -> NameCell {
        cell.selectionStyle = .none
        cell.tfName.delegate = self
        cell.tfName.tag = index.row
        if index.row == 1 {
            //Name
            let rightView = RegistrationRightView.updateView()
            rightView.setRightViewImage(img: "NoImage", isImgHidden: true, isRequiredHidden: false)
            cell.tfName.rightView = nil
            cell.tfName.rightView = rightView
            cell.tfName.rightViewMode = .always
            cell.tfName.text = localData[1].trim()//RegistrationModel.share.Firstname
            cell.tfName.placeholder = "Enter Name".localized
            cell.lblTitle.text = "Name".localized
        }else if index.row == 2 {
            //email
            let rightView = RegistrationRightView.updateView()
            rightView.setRightViewImage(img: "NoImage", isImgHidden: true, isRequiredHidden: true)
            cell.tfName.rightView = nil
            cell.tfName.rightView = rightView
            cell.tfName.rightViewMode = .always
            cell.tfName.text = localData[2]//RegistrationModel.share.Email
            cell.tfName.placeholder = "Enter Email ID".localized
            cell.tfName.keyboardType = .emailAddress
            cell.lblTitle.text = "Email ID".localized
        }else {
            //DOB
            let rightView = RegistrationRightView.updateView()
            rightView.setRightViewImage(img: "r_dob", isImgHidden: false, isRequiredHidden: false)
            cell.tfName.rightView = nil
            cell.tfName.rightView = rightView
            cell.tfName.rightViewMode = .always
            if localData[3] == "" {
                cell.tfName.text = ""
            }else {
                cell.tfName.text = localData[3]
            }
            cell.tfName.placeholder = " Enter Date Of Birth".localized
            cell.lblTitle.text = "Date of birth".localized
        }
        return cell
        
    }
    
    private func registerCell(index: IndexPath, cell: RegistrationCell) -> RegistrationCell {
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        if index.row == 0 {
            cell.btnRegistration.setTitle("Register".localized, for: .normal)
            cell.btnRegistration.setTitleColor(UIColor.white, for: .normal)
            cell.btnRegistration.layer.cornerRadius = 20
            cell.btnRegistration.layer.masksToBounds = true
            //            cell.btnRegistration.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            cell.btnRegistration.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            cell.btnRegistration.setTitle((cell.btnRegistration.titleLabel?.text ?? "").localized, for: .normal)
            cell.btnRegistration.addTarget(self, action: #selector(VMRegistrationViewController.regitraionAction), for: .touchUpInside)
        }
        else if index.row == 1 {
            if continueAsGuest ?? false {
                cell.viewBG.isHidden = false
            }else {
                cell.viewBG.isHidden = true
            }
            cell.viewBG.backgroundColor = UIColor.clear
            cell.btnRegistration.setTitleColor(UIColor.black, for: .normal)
            cell.btnRegistration.layer.cornerRadius = 20
            cell.btnRegistration.layer.masksToBounds = true
            cell.btnRegistration.borderColor = UIColor.blue
            cell.btnRegistration.layer.borderWidth = 1.0
            cell.btnRegistration.setTitle("Continue as Guest".localized, for: .normal)
            cell.btnRegistration.addTarget(self, action: #selector(VMRegistrationViewController.continueGuestAction), for: .touchUpInside)
        }
        else{
            cell.viewBG.backgroundColor = UIColor.clear
            cell.btnRegistration.setTitleColor(UIColor.black, for: .normal)
            cell.btnRegistration.layer.cornerRadius = 20
            cell.btnRegistration.layer.masksToBounds = true
            cell.btnRegistration.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 235.0/255.0, green: 196.0/255.0, blue: 70.0/255.0, alpha: 1.0)])
            cell.btnRegistration.setTitle("Login with OKDollar".localized, for: .normal)
            cell.btnRegistration.addTarget(self, action: #selector(VMRegistrationViewController.LoginWithOkDollar), for: .touchUpInside)
        }
        return cell
    }
    
    @objc func LoginWithOkDollar(){
        self.performSegue(withIdentifier: "OKDollarRegistrationSegue", sender: self)
    }
    
    
    @objc func tappedMe(){
        self.onSelectCamera()
    }
    
    @objc func onSelectCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                self.openCamera()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        self.openCamera()
                    } else {
                        self.showAlert()
                    }
                })
            }
        }
    }
    private func showAlert() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alertVC = SAlertController()
        alertVC.ShowSAlert(title: "Camera".localized, withDescription: "Camera access required to take image".localized, onController: self)
        let cancel = SAlertAction()
        cancel.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {})
        let allow = SAlertAction()
        allow.action(name: "Allow".localized, AlertType: .defualt, withComplition: {
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        })
        alertVC.addAction(action: [cancel,allow])
    }
    
    private func openCamera() {
        
        
        self.cameraContainer.isHidden = false
        self.view.bringSubviewToFront(self.cameraContainer)
        
        DispatchQueue.main.async {
            
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "", withDescription: "Please take a clear selfie. We will use this photo for 1 Stop Mart account security.".localized, onController: self)
            let ok = SAlertAction()
            ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                
            })
            alertVC.addAction(action: [ok])
        }
        
        
        /*
         imagePicker =  UIImagePickerController()
         imagePicker.delegate = self
         imagePicker.sourceType = .camera
         imagePicker.allowsEditing = false
         imagePicker.cameraDevice = .front
         present(imagePicker, animated: true, completion: nil)
         
         */
    }
    
    @objc func onSelectGender(_ sender: UIButton) {
        let index = IndexPath(row: 0, section: 2)
        let cell = registationTV.cellForRow(at: index) as! GenderCell
        
        if sender.tag == 1 {
            cell.imgMaleRadio.image = UIImage(named: "act_radio")
            cell.imgFemaleRadio.image = UIImage(named: "radio")
            gender = .male
        }else {
            cell.imgMaleRadio.image = UIImage(named: "radio")
            cell.imgFemaleRadio.image = UIImage(named: "act_radio")
            gender = .female
        }
    }
    
    @objc func regitraionAction() {
        println_debug("regitraionAction")
        self.checkAllRequiredField(handler: {(response, message) in
            if response {
                self.uploadProfilePic(handler: {(success) in
                    if success {
                        
                        self.checkRegistraion(addressDict: ["street":"37 pansodon street","township":"Botahtaung","region":"yangon","city":"Yangon","Country":"Myanmar"])
                        
                        /*
                         if VMGeoLocationManager.shared.currentLatitude != "0.0" {
                         VMGeoLocationManager.shared.getAddressFrom(lattitude: VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en") { (status, data) in
                         if let addressDic = data as? Dictionary<String, String> {
                         self.checkRegistraion(addressDict: addressDic)
                         }
                         }
                         } else {
                         self.checkRegistraion(addressDict: nil)
                         }
                         */
                    }else {
                        self.showAlert(alertTitle: "Registration".localized, description: "Profile Image uploading failed. Please try again".localized)
                    }
                })
            }
                
                
            else {
                self.showAlert(alertTitle: "Information".localized, description: message)
            }
        })
        
    }
    @objc func continueGuestAction() {
        
        println_debug("continueGuestAction")
        UserDefaults.standard.set(true, forKey: "GuestLogin")
        if let screenFrm = screenFrom, screenFrm.contains("DashBoard") {
            if screenFrm.contains("WishList") {
                self.showMyWishListScreen()
            } else if screenFrm.contains("MyOrders") {
                self.showMyOrdersScreen()
            } else {
                if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                    self.showCartScreen()
                } else {
                    self.showAddAddressScreen()
                }
            }
        } else {
            if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                DispatchQueue.main.async {
                    if let addAddressViewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "Payment_ID") as? Payment {
                        self.navigationController?.pushViewController(addAddressViewController, animated: true)
                    }
                }
            } else {
                self.showAddAddressScreen()
            }
        }
        
    }
}

extension VMRegistrationViewController: UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: selectedImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        let resizedeImg = AppUtility.resize(selectedImage)
        if let faces = faces {
            if faces.count > 1 {
                println_debug("More than 1 faces")
                self.showAlert(alertTitle: "Registration".localized, description: "More than one faces".localized)
                self.profileImage = .notTaken
            }else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    localData[0] = self.imageTobase64(image: selectedImage)
                    base64ImageData = self.imageTobase64(image: resizedeImg)
                    self.userImage = nil
                    self.userImage = selectedImage
                    self.profileImage = .taken
                    self.registationTV.reloadData()
                }else {
                    self.showAlert(alertTitle: "Registration".localized, description: "Face did not detected".localized)
                    self.profileImage = .notTaken
                }
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//MARK:_ Textfield delegates
extension VMRegistrationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 3 {
            textField.placeholder = "Date of birth".localized
            textField.tintColor = UIColor.white
            self.pickUpDate(txtField: textField)
        }else if textField.tag == 1 {
            textField.autocorrectionType = .no
            localData[1] = textField.text?.trim() ?? ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        if textField.tag == 1 {
            if text != "" {
                localData[1] = text.trim()
            }
        }else if textField.tag == 2 {
            localData[2] = text.trim()
        }else {
            localData[3] =  text.trim()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                
                return true
                
            }
        }
        if range.location == 0, string == " " {
            return false
        }
        if textField.tag == 1 {
            
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: nameMyAcceptedChars).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            if string != filteredSet { return false }
            
            if text.count > 40 {
                return false
            }
            
            let isNeedToUpdate = Validations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            if isNeedToUpdate {
                return false
            }
        } else if textField.tag == 2 {
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: EMAILCHARSET).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            if string != filteredSet { return false }
            
            let isNeedToUpdate = Validations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            if isNeedToUpdate {
                return false
            }
        }
        return true
    }
}

//MARK:- Api functions
extension VMRegistrationViewController {
    
    private func checkRegistraion(addressDict: Dictionary<String, String>?) {
        DispatchQueue.main.async(){
            let urlString = String(format: "%@/customer/registerorlogin", APIManagerClient.sharedInstance.base_url)
            guard let url = URL(string: urlString) else { return }
            let params = AppUtility.JSONStringFromAnyObject(value: self.getRegParams(addressDict: addressDict) as AnyObject)
            AppUtility.showLoading(self.view)
            self.aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
                
                DispatchQueue.main.async(){
                    
                    
                    
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    if success {
                        let json = JSON(response)
                        println_debug(json)
                        if let code = json["StatusCode"].int, code == 200 {
                            if let loginInfoDic  = json["Data"].dictionaryObject {
                                let vmTemoModel = VMTempModel()
                                vmTemoModel.wrapDataModel(JSON: loginInfoDic)
                                UserDefaults.standard.set(false, forKey: "VerifyOnly")
                            }
                            if let screenFrm = self?.screenFrom, screenFrm == "Cart" {
                                if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                                    self?.showCartScreen()
                                } else {
                                    self?.showAddAddressScreen()
                                }
                            } else {
                                if let screenFrm = self?.screenFrom {
                                    if screenFrm.contains("WishList") {
                                        self?.showMyWishListScreen()
                                    } else if screenFrm.contains("MyOrders") {
                                        self?.showMyOrdersScreen()
                                    } else {
                                        
                                        if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                                            self?.showCartScreen()
                                        } else {
                                            self?.showAddAddressScreen()
                                        }
                                        //                                self?.showCartScreen()
                                    }
                                } else {
                                    self?.showCartScreen()
                                }
                            }
                        } else {
                            
                        }
                    }else {
                        println_debug("Registration Failed")
                    }
                }
            }
        }
    }
    
    private func setDeviceInfoData() {
        let (n_Code, n_name) = appDelegate.getNetworkCode()
        
        RegistrationModel.share.Msid1 = n_Code
        RegistrationModel.share.NetworkID = n_Code
        RegistrationModel.share.NetworkOperator = n_Code
        RegistrationModel.share.NetworkOperatorName = n_name
        RegistrationModel.share.NetworkSignal = n_Code
        RegistrationModel.share.SIMOperator = n_Code
        
        if let ipAddress = AppUtility.getIPAddress() {
            RegistrationModel.share.IPAddress = ipAddress
        }
        RegistrationModel.share.Latitude = VMGeoLocationManager.shared.currentLatitude
        RegistrationModel.share.Longitude = VMGeoLocationManager.shared.currentLongitude
        
        RegistrationModel.share.DeviceInfo = RegistrationModel.share.wrapDeviceInfoData()
    }
    
    
    func getRegParams(addressDict: Dictionary<String, String>?) -> [String: Any] {
        
        RegistrationModel.share.Address1 = addressDict?["township"] ?? ""
        RegistrationModel.share.Address2 = addressDict?["region"] ?? ""
        RegistrationModel.share.City = addressDict?["city"] ?? ""
        RegistrationModel.share.State = addressDict?["region"] ?? ""
        RegistrationModel.share.Country = addressDict?["Country"] ?? ""
        
        if localData[3] != "" {
            let date = localData[3].components(separatedBy: "-")
            RegistrationModel.share.DateOfBirthDay = date[0]
            RegistrationModel.share.DateofBirthMonth = date[1]
            RegistrationModel.share.DateOfBirthYear = date[2]
        }
        
        RegistrationModel.share.Firstname = localData[1]
        RegistrationModel.share.Lastname = localData[1]
        RegistrationModel.share.Email = localData[2]
        
        if gender == .male {
            RegistrationModel.share.Gender = "M"
        }else if gender == .female {
            RegistrationModel.share.Gender = "F"
        }else {
            RegistrationModel.share.Gender = ""
        }
        
        self.setDeviceInfoData()
        
        var paramDic = [String: Any]()
        paramDic = RegistrationModel.share.wrapData()
        return paramDic
    }
    
}


/*
 extension VMRegistrationViewController{
 
 func navigateToOKDollar() {
 println_debug("navigateToOKDollar")
 DispatchQueue.main.async(){
 self.openUrlForOKDollar()
 }
 }
 
 func openUrlForOKDollar() {
 let urlTaxi = "okdollar://?action=login&name=%@&email=%@&cuncode=%@&phnum=%@&gen=%@&dob=%@"
 
 var gender = RegistrationModel.share.Gender
 if gender == "1" {
 gender = "male"
 } else {
 gender = "female"
 }
 
 let mobile = PTManagerClass.decodeMobileNumber(phoneNumber: RegistrationModel.share.MobileNumber)
 
 let params = String.init(format: urlTaxi, RegistrationModel.share.Username, RegistrationModel.share.Email, mobile.country.dialCode,RegistrationModel.share.MobileNumber, gender, RegistrationModel.share.DateOfBirthDay)
 
 if let urlString = params.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
 if let okTaxiUrl = URL(string: urlString) {
 if UIApplication.shared.canOpenURL(okTaxiUrl) {
 UIApplication.shared.open(okTaxiUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
 , completionHandler: nil)
 } else {
 self.navigateToTaxiAppStoreLink()
 }
 }
 }
 }
 
 func navigateToTaxiAppStoreLink(){
 
 let urlStr = "https://itunes.apple.com/us/app/ok-%24/id1067828611?mt=8"
 if #available(iOS 10.0, *) {
 UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
 
 } else {
 UIApplication.shared.openURL(URL(string: urlStr)!)
 }
 
 }
 
 }
 */

//Custom TextField

class CustomMobUITextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(paste(_:)) ||  action == #selector(cut(_:)) || action == #selector(select(_:)){
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - 40, y: 0, width: 40 , height: bounds.height)
    }
}



class ProfilePicCell: UITableViewCell {
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.height / 2
        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.width / 2
        imgProfilePic.layer.masksToBounds = true
        self.imgProfilePic.layer.borderWidth = 1.0;
        self.imgProfilePic.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.layoutIfNeeded()
        
    }
}

class NameCell: UITableViewCell {
    @IBOutlet weak var tfName: CustomMobUITextField!{
        didSet{
            self.tfName.text = self.tfName.text?.localized
            self.tfName.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            self.lblTitle.text = self.lblTitle.text?.localized
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
        }
    }
}
//CustomMobUITextField
class GenderCell: UITableViewCell {
    @IBOutlet weak var imgMaleRadio: UIImageView!
    @IBOutlet weak var imgFemaleRadio: UIImageView!
    @IBOutlet weak var lblGender: UILabel!{
        didSet {
            self.lblGender.text = "Gender".localized
            self.lblGender.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblMale: UILabel!{
        didSet {
            self.lblMale.text = "Male".localized
            self.lblMale.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblFemale: UILabel!{
        didSet {
            self.lblFemale.text = "Female".localized
            self.lblFemale.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnMale: UIButton!{
        didSet {
            self.btnMale.setTitle((btnMale.titleLabel?.text ?? "").localized, for: .normal)
            btnMale.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnFemale: UIButton!{
        didSet {
            self.btnFemale.setTitle((btnFemale.titleLabel?.text ?? "").localized, for: .normal)
            btnFemale.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
}

class RegistrationCell: UITableViewCell {
    @IBOutlet weak var btnRegistration: UIButton!{
        didSet {
            self.btnRegistration.setTitle(("Register" ?? "").localized, for: .normal)
            btnRegistration.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var viewBG: UIView!
    
}

class MobilNumberCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            self.lblTitle.text = self.lblTitle.text?.localized
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblNumber: UILabel!{
        didSet {
            self.lblNumber.text = self.lblNumber.text?.localized
            self.lblNumber.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var imgCountry: UIImageView!
    
}

class ProfileImageCell: UITableViewCell {
    @IBOutlet weak var btnProfileImage: UIButton!{
        didSet {
            self.btnProfileImage.setTitle((btnProfileImage.titleLabel?.text ?? "").localized, for: .normal)
            btnProfileImage.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
}



extension VMRegistrationViewController{
    
    func cameraConfigure(){
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let frontCamera =  AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: frontCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey:  AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity =    AVLayerVideoGravity.resizeAspect
                videoPreviewLayer!.connection?.videoOrientation =   AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    
    @IBAction func closeCamera(){
        self.cameraContainer.isHidden = true
    }
    
    @IBAction func captureImage(){
        
        if let videoConnection = stillImageOutput!.connection(with: AVMediaType.video) {
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!){
                    //                    self.previewView.image = UIImage(data: imageData)
                    if  self.faceDetection(selectedImage: (UIImage(data: imageData)!)){
                        let indexpath = NSIndexPath(row: 0, section: 0)
                        let cell = self.registationTV.cellForRow(at: indexpath as IndexPath) as? UpdateProfilePicCell
                        cell?.imgProfileIamge.image = UIImage(data: imageData)
                        self.cameraContainer.isHidden = true
                    }
                }
            }
        }
    }
    
    
    func faceDetection(selectedImage: UIImage) -> Bool{
        var FaceDetectFlag = false
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: selectedImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        
        let resizedeImg = AppUtility.resize(selectedImage)
        
        if let faces = faces {
            if faces.count > 1 {
                //                self.showAlert(alertTitle: "Error!".localized, description: "More than one faces".localized)
                AppUtility.showToastlocal(message: "More than one faces".localized, view: self.view)
                FaceDetectFlag = false
                self.profileImage = .notTaken
            }else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    base64ImageData = self.imageTobase64(image: resizedeImg)
                    localData[0] = self.imageTobase64(image: selectedImage)
                    self.userImage = UIImage(named: "")
                    self.userImage = selectedImage
                    self.profileImage = .taken
                    FaceDetectFlag = true
                    self.registationTV.reloadData()
                }else {
                    FaceDetectFlag = false
                    AppUtility.showToastlocal(message: "No Face detected,Required Face.Please press capture button one more time to click your picture".localized, view: self.view)
                    //                    self.showAlert(alertTitle: "Error!".localized, description: "Face did not detected".localized)
                    self.profileImage = .notTaken
                }
            }
        }
        return FaceDetectFlag
    }
    
}
