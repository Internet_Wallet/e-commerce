    //
    //  MedicineRequest.swift
    //  VMart
    //
    //  Created by Mohit on 7/13/19.
    //  Copyright © 2019 Shobhit. All rights reserved.
    //
    
    import UIKit
    import QuartzCore
    
    class MedicineRequest: ZAYOKBaseViewController {
        
        var aPIManager = APIManager()
        var selectedIndex = 0
        var imgArr = [String]()
        
        //
        @IBOutlet var ViewHeightConstraint: NSLayoutConstraint!
        @IBOutlet var lblHeightConstraint: NSLayoutConstraint!
        @IBOutlet weak var productCollectionView2: UICollectionView!
        
        @IBOutlet var InfoLbl: UILabel!{
            didSet {
                self.InfoLbl.text = self.InfoLbl.text?.localized
                self.InfoLbl.font = UIFont(name: appFont, size: 12.0)
                self.InfoLbl.isHidden = true
                self.InfoLbl?.layer.cornerRadius = 2.0
                self.InfoLbl?.layer.masksToBounds = true
            }
        }
        
        
        @IBOutlet var PatientLbl: UILabel!{
            didSet {
                self.PatientLbl.text = self.PatientLbl.text?.localized
                self.PatientLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        @IBOutlet var DoctorLbl: UILabel!{
            didSet {
                self.DoctorLbl.text = self.DoctorLbl.text?.localized
                self.DoctorLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        @IBOutlet var HospitalLbl: UILabel!{
            didSet {
                self.HospitalLbl.text = self.HospitalLbl.text?.localized
                self.HospitalLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet var CommentLbl: UILabel!{
            didSet {
                self.CommentLbl.text = self.CommentLbl.text?.localized
                self.CommentLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        
        @IBOutlet weak var Btn1: UIButton! {
            didSet {
                self.Btn1.setTitle("".localized, for: .normal)
                Btn1.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet weak var Btn2: UIButton! {
            didSet {
                self.Btn2.setTitle("".localized, for: .normal)
                Btn2.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet weak var Btn3: UIButton! {
            didSet {
                self.Btn3.setTitle("".localized, for: .normal)
                Btn3.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet weak var PatientTxt: UITextField! {
            didSet {
                self.PatientTxt.text = "".localized
                self.PatientTxt.font = UIFont(name: appFont, size: 15.0)
                //self.Txt.becomeFirstResponder()
            }
        }
        @IBOutlet weak var DoctorTxt: UITextField! {
            didSet {
                self.DoctorTxt.text = "".localized
                self.DoctorTxt.font = UIFont(name: appFont, size: 15.0)
                //self.Txt.becomeFirstResponder()
            }
        }
        @IBOutlet weak var HospitalTxt: UITextField! {
            didSet {
                self.HospitalTxt.text = "".localized
                self.HospitalTxt.font = UIFont(name: appFont, size: 15.0)
                //self.Txt.becomeFirstResponder()
            }
        }
        @IBOutlet weak var CommentTxtView: UITextView! {
            didSet {
                self.CommentTxtView.text = "".localized
                self.CommentTxtView.font = UIFont(name: appFont, size: 15.0)
                
            }
        }
        @IBOutlet weak var SendBtn: UIButton! {
            didSet {
                SendBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
                self.SendBtn.setTitle("Send".localized, for: .normal)
                self.SendBtn.layer.cornerRadius = 4
                self.SendBtn.layer.masksToBounds = true
                SendBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        @IBAction func RequestListAction(_ sender: UIBarButtonItem) {
            let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyRequestList")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            //self.imgArr.append("cross-dark.png")
            
            self.title = "Medicine Request".localized
            
            self.collectionViewConstraintManage()
            
            
            
        }
        
        
        func collectionViewConstraintManage(){
            if self.imgArr.count > 0 {
                view.layoutIfNeeded() // force any pending operations to finish
                
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.ViewHeightConstraint.constant = 100
                    self.lblHeightConstraint.constant = 115
                    self.view.layoutIfNeeded()
                })
                // Do any additional setup after loading the view.
                productCollectionView2.reloadData()
            }
            else{
                self.ViewHeightConstraint.constant = 0
                self.lblHeightConstraint.constant = 15
            }
        }
        
        
        
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            // productCollectionView2.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .right, animated: true)
        }
        
        @IBAction func SendRequestClick(_ sender: UIButton) {
            
            if self.checkTextField(){
                if self.imgArr.count == 0{
                    AppUtility.showToastlocal(message: "Please take medicine image by camera", view: self.view)
                    return
                }
                if AppUtility.isConnectedToNetwork() {
                    //
                    self.uploadProfilePic(handler: {(success,AWSResponse) in
                        DispatchQueue.main.async {
                            if success {
                                
                                self.MedicineRequest(AWSImgArray: AWSResponse)
                                
                                print(success)
                            }else {
                                self.clearTextFiled()
                                AppUtility.showToastlocal(message: AWSResponse[0] as? String ?? "", view: self.view)
                            }
                        }})
                } else {
                    AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
                }
            }
                
            else{
                AppUtility.showToastlocal(message: "Please fill all the data in the form", view: self.view)
                //            AppUtility.showToast("Please fill all the data in the form",view: self.view)
            }
            
            
            
        }
        
        func checkTextField() -> Bool {
            if self.DoctorTxt.text == "" || self.PatientTxt.text == "" || self.HospitalTxt.text == "" || self.CommentTxtView.text == ""{
                return false
                
            }else{
                return true
            }
            
        }
        
        func MedicineRequest(AWSImgArray: [AnyObject]) {
            AppUtility.showLoading(self.view)
            let dict: [String:Any] = self.getSendParams(imageArrayURl: AWSImgArray)
            aPIManager.medicineRequest(params: dict, onSuccess: { [weak self]  responseData in
                DispatchQueue.main.async() {
                    self?.clearTextFiled()
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    
                }}, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                        AppUtility.showToast(message,view: viewLoc)
                    }})
            
        }
        
        func clearTextFiled(){
            DispatchQueue.main.async {
                self.imgArr.removeAll()
                self.collectionViewConstraintManage()
                self.productCollectionView2.reloadData()
                self.DoctorTxt.text = ""
                self.PatientTxt.text = ""
                self.HospitalTxt.text = ""
                self.CommentTxtView.text = ""
            }
        }
        
        
        private func uploadProfilePic(handler: @escaping (_ success: Bool, _ AWSResponseArray: [AnyObject]) -> Void) {
            
            let paramString: [String : Any] = [
                "MobileNumber" : "09955329342",
                "Base64String" : self.imgArr,
            ]
            AppUtility.showLoading(self.view)
            aPIManager.UploadImage(params: paramString , url: "https://www.okdollar.co/RestService.svc/GetMultiImageUrlByBase64String", successBlock: {(success, response) in
                DispatchQueue.main.async() {  AppUtility.hideLoading(self.view)
                    if success {
                        for object in response{
                            print(object.replacingOccurrences(of: " ", with: "%20"))
                        }
                        handler(true, response)
                    }else {
                        handler(false, response)
                    }
                }
            })
            
        }
        
        
        func imageTobase64(image: UIImage) -> String {
            var str = ""
            if let imageData = image.pngData(){
                str = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
            }
            return str
        }
        
        
        
        func getSendParams(imageArrayURl: [AnyObject]) -> [String:Any] {
            var paramDic            = [String:Any]()
            var imgStrArr = [String]()
            
            for obj in imageArrayURl{
                imgStrArr.append(obj as? String ?? "")
            }
            paramDic["Id"]  = APIManagerClient.sharedInstance.medicineRequestId // 0 for create new request
            var mobileNo = RegistrationModel.share.MobileNumber
            if mobileNo.hasPrefix("09") {
                mobileNo = "0095" + mobileNo.dropFirst()
            }
            paramDic["PatientName"]  = self.PatientTxt.text
            paramDic["DoctorName"]  = self.DoctorTxt.text
            paramDic["HospitalName"]  = self.HospitalTxt.text
            paramDic["MobileNumber"]  = "09955329342"
            paramDic["Remarks"] = self.CommentTxtView.text
            paramDic["PrescriptionImageUrl"] =  imgStrArr
            
            return paramDic
        }
        
        @IBAction func click_AudioRecord(_ sender: AnyObject) {
            self.performSegue(withIdentifier: "CustomAudioSegue", sender: self)
        }
        
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "CustomAudioSegue" {
                if let vc = segue.destination as? CustomAudio {
                    vc.delegateAudio = self
                }
            }
        }
        
        
        @IBAction func infoClick(_ sender: UIButton) {
            if sender.isSelected == true{
                sender.isSelected = false
                //            self.InfoLbl.isHidden = true
                self.InfoLbl.fadeIn()
            }else{
                sender.isSelected = true
                //            self.InfoLbl.isHidden = false
                self.InfoLbl.fadeOut()
            }
        }
        
    }
    extension MedicineRequest: AudioProcess{
        func updateTheAudioFile(audioURL: URL) {
            print(audioURL)
        }
        
    }
    
    extension MedicineRequest: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.imgArr.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = productCollectionView2.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! CustomCollectionViewCell
            //        cell.roundCell(cell)
            cell.wrapData(index: indexPath.row, image: self.imgArr[indexPath.row])
            cell.Delegate = self
            //        cell.contentView.layer.borderWidth = 1
            return cell
        }
        
        func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            print(String(format: "%i", scrollView.subviews.count))
            let vv = UIView()
            for v: UIView in scrollView.subviews {
                if (v is UIImageView) {
                    return v
                }
            }
            return vv
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            //        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductImagesCollection") as? ProductImagesCollection
            //        viewController?.fullImageUrlArray = self.imgArr
            //        viewController?.selectedIndex = self.selectedIndex
            //        viewController?.wishListSelected = false
            //        self.present(viewController!, animated: true, completion: nil)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            return CGSize(width: 105, height: productCollectionView2.bounds.height)
            
        }
    }
    
    extension MedicineRequest: CustomCollectionViewCellDelegate{
        func DeleteCall(cell: CustomCollectionViewCell) {
            if self.imgArr.count > 0{
                self.imgArr.remove(at: cell.deleteBtn.tag)
                self.productCollectionView2.reloadData()
                if self.imgArr.count == 0 {
                    view.layoutIfNeeded() // force any pending operations to finish
                    UIView.animate(withDuration: 0.2, animations: { () -> Void in
                        self.ViewHeightConstraint.constant = 0
                        self.lblHeightConstraint.constant = 15
                        self.view.layoutIfNeeded()
                    })
                }
            }
            //        self.showAlert(alertTitle: "Seleted Image", description: "Image number \(cell.deleteBtn.tag) clicked")
        }
    }
    
    
    protocol CustomCollectionViewCellDelegate: class {
        func DeleteCall(cell: CustomCollectionViewCell)
    }
    
    class CustomCollectionViewCell: UICollectionViewCell {
        
        weak var Delegate: CustomCollectionViewCellDelegate?
        @IBOutlet var productImageView: UIImageView!
        @IBOutlet var deleteBtn: UIButton!
        
        @IBAction func deletePhotoClick(_ sender: UIButton) {
            print(sender.tag)
            self.Delegate?.DeleteCall(cell: self)
        }
        
        func wrapData(index:Int, image: String) {
            
            if let img = self.convertBase64ToImage(imageString: image) as? UIImage{
                self.productImageView.image = img
            }
            self.deleteBtn.tag = index
        }
        
        func convertBase64ToImage(imageString: String) -> UIImage {
            let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
            return UIImage(data: imageData)!
        }
    }
    
    
    extension MedicineRequest: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
        @IBAction func TakePhotoClick(_ sender: UIButton) {
            let vc = UIImagePickerController()
            vc.sourceType = .camera
            vc.allowsEditing = true
            vc.delegate = self
            present(vc, animated: true)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true)
            
            guard let image = info[.editedImage] as? UIImage else {
                print("No image found")
                return
            }
            // print out the image size as a test
            print(image.size)
            
            if self.imgArr.count < 10{
                self.view.layoutIfNeeded() // force any pending operations to finish
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.ViewHeightConstraint.constant = 100
                    self.lblHeightConstraint.constant = 115
                    self.view.layoutIfNeeded()
                })
                self.imgArr.append(self.imageTobase64(image: image))
                self.productCollectionView2.reloadData()
            }
            else{
                self.showAlert(alertTitle: "Information", description: "Maximum 10 image can upload")
            }
            //        self.Btn1.setBackgroundImage(image, for: UIControl.State.normal)
            //        self.Btn2.setBackgroundImage(image, for: UIControl.State.normal)
        }
        
        
        @IBAction func backAction(_ sender: UIBarButtonItem) {
            self.dismiss(animated: true, completion: nil)
        }
        
        func createDirectory(){
            let fileManager = FileManager.default
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
            if !fileManager.fileExists(atPath: paths){
                try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
            }else{
                print("Already dictionary created.")
            }
        }
        
        func saveImageDocumentDirectory(){
            let fileManager = FileManager.default
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.jpg")
            let image = UIImage(named: "apple.jpg")
            print(paths)
            let imageData = UIImage.jpegData(image!) as? Data
            fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        }
        
    }
    
    class MedicineRequestTableViewCell: UITableViewCell {
        
        @IBOutlet weak var imageOne: UIImageView!
        @IBOutlet weak var imageTwo: UIImageView!
        @IBOutlet weak var imageThree: UIImageView!
        @IBOutlet weak var imageFour: UIImageView!
        @IBOutlet weak var countImageLbl: UILabel!{
            didSet {
                self.countImageLbl.text = self.countImageLbl.text?.localized
                self.countImageLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        @IBOutlet weak var titleLbl: UILabel!{
            didSet {
                self.titleLbl.text = self.titleLbl.text?.localized
                self.titleLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet weak var statusLbl: UILabel!{
            didSet {
                self.statusLbl.text = self.statusLbl.text?.localized
                self.statusLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        @IBOutlet weak var remarksDetailsLbl: UILabel!{
            didSet {
                self.remarksDetailsLbl.text = self.remarksDetailsLbl.text?.localized
                self.remarksDetailsLbl.font = UIFont(name: appFont, size: 15.0)
            }
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            // Configure the view for the selected state
        }
        
        func updateCellData(object: medicineRequest ,indexPath: IndexPath) {
            
            if let imgArr = object.prescriptionImageUrl {
                
                if imgArr.count <= 4 {
                    countImageLbl.isHidden = true
                    countImageLbl.superview?.isHidden = true
                }
                else {
                    countImageLbl.isHidden = false
                    countImageLbl.superview?.isHidden = false
                    countImageLbl.text = "+\(imgArr.count - 4)"
                }
                for index in 1...4 {
                    var urlFinal : String = ""
                    // changes as check for array count then moove next
                    if imgArr.count > index - 1 {
                        guard let imgBlur  = imgArr[index - 1] as? String else {
                            return
                        }
                        if imgBlur.isEmpty {
                            urlFinal = imgArr.last!
                        }else {
                            urlFinal = imgBlur
                            //                    self.imageOne.sd_setImage(with: URL(string: imgBlur))
                        }
                    }
                    else {
                        urlFinal = imgArr.last!
                    }
                    switch index {
                    case 1:
                        self.imageOne.sd_setImage(with: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20")))
                        
                        //                    AppUtility.NKPlaceholderImage(image: UIImage(named: urlFinal.replacingOccurrences(of: " ", with: "%20")), imageView: self.imageOne, imgUrl: urlFinal.replacingOccurrences(of: " ", with: "%20")) { (image) in }
                        
                        
                        
                        //                    AppUtility.getData(from: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20"))!) { data, response, error in
                        //                        guard let data = data, error == nil else { return }
                        //                        print("Download Finished")
                        //                        DispatchQueue.main.async() {
                        //                            self.imageOne.image = UIImage(data: data)
                        //                        }
                        //                    }
                        
                    case 2:
                        self.imageTwo.sd_setImage(with: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20")))
                        
                        
                        //                    AppUtility.NKPlaceholderImage(image: UIImage(named: urlFinal.replacingOccurrences(of: " ", with: "%20")), imageView: self.imageTwo, imgUrl: urlFinal.replacingOccurrences(of: " ", with: "%20")) { (image) in }
                        
                        
                        //                    AppUtility.getData(from: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20"))!) { data, response, error in
                        //                        guard let data = data, error == nil else { return }
                        //                        print("Download Finished")
                        //                        DispatchQueue.main.async() {
                        //                            self.imageTwo.image = UIImage(data: data)
                        //                        }
                    //                    }
                    case 3:
                        self.imageThree.sd_setImage(with: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20")))
                        
                        
                        //                    AppUtility.NKPlaceholderImage(image: UIImage(named: urlFinal.replacingOccurrences(of: " ", with: "%20")), imageView: self.imageThree, imgUrl: urlFinal.replacingOccurrences(of: " ", with: "%20")) { (image) in }
                        
                        
                        //                    AppUtility.getData(from: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20"))!) { data, response, error in
                        //                        guard let data = data, error == nil else { return }
                        //                        print("Download Finished")
                        //                        DispatchQueue.main.async() {
                        //                            self.imageThree.image = UIImage(data: data)
                        //                        }
                        //                    }
                        
                        
                    case 4:
                        self.imageFour.sd_setImage(with: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20")))
                        
                        //                    AppUtility.NKPlaceholderImage(image: UIImage(named: urlFinal.replacingOccurrences(of: " ", with: "%20")), imageView: self.imageFour, imgUrl: urlFinal.replacingOccurrences(of: " ", with: "%20")) { (image) in }
                        
                        
                        //                    AppUtility.getData(from: URL(string: urlFinal.replacingOccurrences(of: " ", with: "%20"))!) { data, response, error in
                        //                        guard let data = data, error == nil else { return }
                        //                        print("Download Finished")
                        //                        DispatchQueue.main.async() {
                        //                            self.imageFour.image = UIImage(data: data)
                        //                        }
                        //                    }
                        
                        
                    default:
                        break
                    }
                }
            }
            titleLbl.text = object.patientName
            remarksDetailsLbl.text = object.remarks
            let status = object.requestStatusMessage
            statusLbl.text = status
            switch status {
            case "Pending":
                statusLbl.backgroundColor = UIColor.init(red: (253.0/255.0), green: (193.0/255.0), blue: (10.0/255.0), alpha: 1.0)
            case "Rejected":
                statusLbl.backgroundColor = UIColor.init(red: (220.0/255.0), green: (53.0/255.0), blue: (69.0/255.0), alpha: 1.0)
            default:
                statusLbl.backgroundColor = UIColor.init(red: (40.0/255.0), green: (166.0/255.0), blue: (69.0/255.0), alpha: 1.0)
            }
        }
        
        //    func convertBase64ToImage(imageString: String) -> UIImage {
        //        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        //        return UIImage(data: imageData)!
        //    }
        
    }
