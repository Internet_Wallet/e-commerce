//
//  CheckOutOrderInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/22/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

open class CheckOutOrderInfo: NSObject {
    
    var ShippingMethod       :                NSString?
    var PaymentMethod :                       NSString?
    var SubTotal                         :    String?
    var SubTotalDiscount                 :    String?
    var Shipping                         :    String?
    var Tax                              :    String?
    var RequiresShipping                 :    Bool?
    var OrderTotal                       :    String?
    var ShippingAddress_Address1        :     NSString?
    var ShippingAddress_Address2        :     NSString?
    var ShippingAddress_City            :     NSString?
    var ShippingAddress_Email           :     NSString?
    var ShippingAddress_LastName :            NSString?
    var ShippingAddress_CountryName     :     NSString?
    var ShippingAddress_StateProvinceName :   NSString?
    var ShippingAddress_ZipPostalCode :       NSString?
    var ShippingAddress_PhoneNumber :         NSString?
    var ShippingAddress_FirstName :           NSString?
    var BillingAddress_Address1        :      NSString?
    var BillingAddress_Address2        :      NSString?
    var BillingAddress_City :                 NSString?
    var BillingAddress_Email         :        NSString?
    var BillingAddress_LastName :             NSString?
    var BillingAddress_CountryName    :       NSString?
    var BillingAddress_StateProvinceName :    NSString?
    var BillingAddress_ZipPostalCode :        NSString?
    var BillingAddress_PhoneNumber :          NSString?
    var BillingAddress_FirstName :            NSString?
    var Items                         :       [CheckOutItems]
    var checkoutAttributes             :      [CheckoutAttributes]
    init (JSON : AnyObject) {
        ShippingMethod                            =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingMethod") as? NSString)
        PaymentMethod                             =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.PaymentMethod") as? NSString)
        SubTotal                                  = (JSON.value(forKeyPath: "OrderTotalModel.SubTotal") as? String)
        SubTotalDiscount                          = (JSON.value(forKeyPath: "OrderTotalModel.SubTotalDiscount") as? String)
        Shipping                                  = (JSON.value(forKeyPath: "OrderTotalModel.Shipping") as? String)
        OrderTotal                                = (JSON.value(forKeyPath: "OrderTotalModel.OrderTotal") as? String)
        Tax                                       = (JSON.value(forKeyPath: "OrderTotalModel.Tax") as? String)
        RequiresShipping                          = (JSON.value(forKeyPath: "OrderTotalModel.RequiresShipping") as? Bool)
        ShippingAddress_Address1                  =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.Address1") as? NSString)
        ShippingAddress_Address2                  = (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.Address2") as? NSString)
        ShippingAddress_City                      =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.City") as? NSString)
        ShippingAddress_Email                     =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.Email") as? NSString)
        ShippingAddress_LastName                  =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.LastName") as? NSString)
        ShippingAddress_CountryName               =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.CountryName") as? NSString)
        ShippingAddress_StateProvinceName         =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.StateProvinceName") as? NSString)
        ShippingAddress_ZipPostalCode             =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.ZipPostalCode") as? NSString)
        ShippingAddress_PhoneNumber               =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.PhoneNumber") as? NSString)
        ShippingAddress_FirstName                 =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.ShippingAddress.FirstName") as? NSString)
        BillingAddress_Address1                    =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.Address1") as? NSString)
        BillingAddress_Address2                    = (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.Address2") as? NSString)
        BillingAddress_City                        =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.City") as? NSString)
        BillingAddress_Email                       =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.Email") as? NSString)
        BillingAddress_LastName                    =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.LastName") as? NSString)
        BillingAddress_CountryName                 =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.CountryName") as? NSString)
        BillingAddress_StateProvinceName           =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.StateProvinceName") as? NSString)
        BillingAddress_ZipPostalCode               =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.ZipPostalCode") as? NSString)
        BillingAddress_PhoneNumber                 =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.PhoneNumber") as? NSString)
        BillingAddress_FirstName                   =  (JSON.value(forKeyPath: "ShoppingCartModel.OrderReviewData.BillingAddress.FirstName") as? NSString)
        Items                                      = [CheckOutItems] ()
        checkoutAttributes                         = [CheckoutAttributes] ()
        
        for data in (JSON.value(forKeyPath: "ShoppingCartModel.Items") as! NSArray) {
            let checkOut = CheckOutItems(JSON: data as AnyObject)
            self.Items.append(checkOut)
        }
        
        for data in (JSON.value(forKeyPath: "ShoppingCartModel.CheckoutAttributes") as! NSArray) {
            let attributs = CheckoutAttributes(JSON: data as AnyObject)
            self.checkoutAttributes.append(attributs)
        }
    }
}
