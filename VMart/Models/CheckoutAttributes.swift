//
//  CheckoutAttributes.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

open class CheckoutAttributes: NSObject {
    var AllowedFileExtensions     : NSArray?
    var AttributeControlType      : NSInteger
    var CustomProperties          : NSDictionary?
    var DefaultValue              : String?
    var Id                        : NSInteger
    var IsRequired                : Bool
    var Name                      : NSString
    var SelectedDay               : NSString?
    var SelectedMonth             : NSString?
    var SelectedYear              : NSString?
    var TextPrompt                : NSString?
    var Values                    : [CheckOutValusInfo]
    
    init(JSON : AnyObject) {
        
        self.AllowedFileExtensions     = (JSON.value(forKeyPath: "AllowedFileExtensions") as? NSArray)
        self.AttributeControlType      = (JSON.value(forKeyPath: "AttributeControlType") as! NSInteger)
        self.CustomProperties          = (JSON.value(forKeyPath: "CustomProperties") as? NSDictionary)
        self.DefaultValue              = (JSON.value(forKeyPath: "DefaultValue") as? String)
        self.Id                        = (JSON.value(forKeyPath: "Id") as! NSInteger)
        self.IsRequired                = (JSON.value(forKeyPath: "IsRequired") as! Bool)
        self.Name                      = (JSON.value(forKeyPath: "Name") as! NSString)
        self.SelectedDay               = (JSON.value(forKeyPath: "SelectedDay") as? NSString)
        self.SelectedMonth             = (JSON.value(forKeyPath: "SelectedMonth") as? NSString)
        self.SelectedYear              = (JSON.value(forKeyPath: "SelectedYear") as? NSString)
        self.TextPrompt                = (JSON.value(forKeyPath: "TextPrompt") as? NSString)
        self.Values                    = [CheckOutValusInfo]()
        
        for value in (JSON.value(forKeyPath: "Values") as! NSArray){
            
            let valueData = CheckOutValusInfo(JSON: value as AnyObject)
            self.Values.append(valueData)
            
        }
    }
}


open class CheckOutValusInfo: NSObject {
    
    var ColorSquaresRgb            : NSString?
    var CustomProperties           : NSString?
    var Id                         : NSInteger
    var IsPreSelected              : Bool
    var Name                       : NSString
    var PriceAdjustment            : NSString?
    
    init(JSON : AnyObject) {
        
        self.ColorSquaresRgb       = (JSON.value(forKeyPath: "ColorSquaresRgb") as? NSString)
        self.CustomProperties      = (JSON.value(forKeyPath: "CustomProperties") as? NSString)
        self.Id                    = (JSON.value(forKeyPath: "Id") as! NSInteger)
        self.IsPreSelected         = (JSON.value(forKeyPath: "IsPreSelected") as! Bool)
        self.Name                  = (JSON.value(forKeyPath: "Name") as! NSString)
        self.PriceAdjustment       = (JSON.value(forKeyPath: "PriceAdjustment") as? NSString)
        
    }
}
