//
//  CustomerAttributs.swift
//  NopCommerce
//
//  Created by BS-125 on 5/29/17.
//  Copyright © 2017 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class CustomerAttributs: Mappable {
    
    
    //    var ErrorList: [String]?
    //
    //    var SuccessMessage: String?
    //
    //    var StatusCode: Int = 0
    //
    //    var data: [Data]?
    //    
    //}
    //class Data: Mappable {
    
    var values: [Values]?
    
    var DefaultValue: String?
    
    var Id: Int = 0
    
    var IsRequired: Bool = false
    
    var CustomProperties: Customproperties?
    
    var type: String?
    
    var Name: String?
    
    var AttributeControlType: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        
        values   <- map["Values"]
        
        DefaultValue <- map["DefaultValue"]
        
        Id      <- map["Id"]
        
        IsRequired <- map["IsRequired"]
        
        CustomProperties <- map["CustomProperties"]
        
        type <- map["Type"]
        
        Name <- map["Name"]
        
        AttributeControlType <- map["AttributeControlType"]
    }
    
}

class Values: Mappable {
    
    var CustomProperties: Customproperties?
    
    var IsPreSelected: Bool = false
    
    var Name: String?
    
    var Id: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        
        CustomProperties   <- map["CustomProperties"]
        
        IsPreSelected <- map["IsPreSelected"]
        
        Name      <- map["Name"]
        
        Id <- map["Id"]
    }
    
}

class Customproperties: NSObject {
    
}

