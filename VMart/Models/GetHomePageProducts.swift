//
//  GetHomePageProducts.swift
//  NopCommerce
//
//  Created by BS-125 on 11/3/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
open class GetHomePageProducts : NSObject {
    
    var CustomProperties :  String
    var ImageUrl :          String
    var Id :                NSInteger
    var Name :              String
    var OldPrice :          String
    var Price :             String
    var AllowCustomerReviews : Bool
    var MarkAsNew : Bool
    var ProductId :         NSInteger
    var RatingSum :         NSInteger
    var TotalReviews :      NSInteger
    var ShortDescription :  String
    var PriceWithDiscount : String = ""
    var DiscountPercentage : Int?
    var PriceValue : Float
    var isWishList : Bool = false
    var DisableWishlistButton : Bool = false
    
    init(CustomProperties : String, ImageUrl : String , Id : NSInteger , Name : String , OldPrice : String , Price : String , AllowCustomerReviews : Bool, MarkAsNew : Bool , ProductId : NSInteger , RatingSum : NSInteger , TotalReviews : NSInteger, ShortDescription : String, PriceWithDiscount : String, DiscountPercentage : Int? , PriceValue : Float, IsWishList : Bool? = false, DisableWishlistButton : Bool) {
        self.CustomProperties = CustomProperties
        self.ImageUrl = ImageUrl
        self.Id = Id
        self.Name = Name
        self.OldPrice = OldPrice
        self.Price =  Price
        self.AllowCustomerReviews = AllowCustomerReviews
        self.MarkAsNew = MarkAsNew
        self.ProductId = ProductId
        self.RatingSum = RatingSum
        self.TotalReviews = TotalReviews
        self.ShortDescription = ShortDescription
        self.PriceWithDiscount = PriceWithDiscount
        self.DiscountPercentage = DiscountPercentage
        self.PriceValue = PriceValue
        self.isWishList = IsWishList ?? false
        self.DisableWishlistButton = DisableWishlistButton ?? false
    }
}
