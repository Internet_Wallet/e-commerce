//
//  GetLanguageModel.swift
//  NopCommerce
//
//  Created by BS-136 on 7/9/18.
//  Copyright © 2018 Jahid Hassan. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class GetLanguageModel: Mappable {
    var CurrentLanguageId: Int = -100
    var AvailableLanguages: [AvailableLanguages]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.CurrentLanguageId  <- map["CurrentLanguageId"]
        self.AvailableLanguages   <- map["AvailableLanguages"]
    }
}


class AvailableLanguages: Mappable{
    var Name: String = ""
    var Id: Int = -100
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.Name  <- map["Name"]
        self.Id   <- map["Id"]
    }
}
