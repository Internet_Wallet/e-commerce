//
//  HomeBanner.swift
//  NopCommerce
//
//  Created by BS-116 on 7/31/17.
//  Copyright © 2017 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

open class HomeBanner: Mappable {
    
    var ImageUrl:          String?
    var IsProduct:         Bool = false
    var Text:              String?
    var Link:              String?
    var ProdOrCatId:       String?
    var CategoryName:      String?
    
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.ImageUrl       <- map["ImageUrl"]
        self.IsProduct      <- map["IsProduct"]
        self.ProdOrCatId    <- map["ProdOrCatId"]
        self.Text           <- map["Text"]
        self.Link           <- map["Link"]
        self.CategoryName           <- map["CategoryName"]
    }
}
