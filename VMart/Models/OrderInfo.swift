//
//  OrderInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/28/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper
open class OrderInfo: Mappable {
    
    
    var OrderTotal       : NSString = ""
    var OrderStatus      : NSString = ""
    var PaymentStatus    : NSString = ""
    var ShippingStatus   : NSString = ""
    var CreatedOn        : NSString = ""
    var Id               : NSInteger = 10000
    
    required public init?(map: Map) {
    }
    public func mapping(map: Map) {
        self.OrderTotal     <- map["OrderTotal"]
        self.OrderStatus    <- map["OrderStatus"]
        self.PaymentStatus  <- map["PaymentStatus"]
        self.ShippingStatus <- map["ShippingStatus"]
        self.CreatedOn      <- map["CreatedOn"]
        self.Id             <- map["Id"]
    }
    
    //    init (JSON: [String: AnyObject]) {
    //
    //        self.OrderTotal     = JSON["OrderTotal"] as! NSString
    //        self.OrderStatus    = JSON["OrderStatus"] as! NSString
    //        self.PaymentStatus  = JSON["PaymentStatus"] as! NSString
    //        self.ShippingStatus = JSON["ShippingStatus"] as! NSString
    //        self.CreatedOn      = JSON["CreatedOn"] as! NSString
    //        self.Id             = JSON["Id"] as! NSInteger
    //
    //
    //    }
    
}
