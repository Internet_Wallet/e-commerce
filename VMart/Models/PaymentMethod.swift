//
//  PaymentMethod.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

open class PaymentMethod: Mappable {
    
    var Name                    : NSString = ""
    var Selected                : Bool = false
    var LogoUrl                 : NSString = ""
    var PaymentMethodSystemName : NSString = ""
    
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.Name                       <- map["Name"]
        self.Selected                   <- map["Selected"]
        self.LogoUrl                    <- map["LogoUrl"]
        self.PaymentMethodSystemName    <- map["PaymentMethodSystemName"]
    }
}

