//
//  ProductAttributesInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 11/25/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductAttributesInfo: Mappable {
    
    var AllowedFileExtensions     : NSArray?
    var AttributeControlType      : Int = 0
    var CustomProperties          : NSDictionary?
    var DefaultValue              : String?
    var Description               : NSString?
    var Id                        : Int = 0
    var IsRequired                : Bool = false
    var Name                      : NSString?
    var ProductAttributeId        : Int = 0
    var ProductId                 : Int = 0
    var SelectedDay               : NSString?
    var SelectedMonth             : NSString?
    var SelectedYear              : NSString?
    var TextPrompt                : NSString?
    var Values                    : [ValusInfo]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.AllowedFileExtensions    <- map["AllowedFileExtensions"]
        self.AttributeControlType     <- map["AttributeControlType"]
        self.CustomProperties         <- map["CustomProperties"]
        self.DefaultValue             <- map["DefaultValue"]
        self.Description              <- map["Description"]
        self.Id                       <- map["Id"]
        self.IsRequired               <- map["IsRequired"]
        self.Name                     <- map["Name"]
        self.ProductAttributeId       <- map["ProductAttributeId"]
        self.ProductId                <- map["ProductId"]
        self.SelectedDay              <- map["SelectedDay"]
        self.SelectedMonth            <- map["SelectedMonth"]
        self.SelectedYear             <- map["SelectedYear"]
        self.TextPrompt               <- map["TextPrompt"]
        self.Values                   <- map["Values"]
    }
}

class ValusInfo: Mappable {
    
    
    var ColorSquaresRgb            : String?
    var CustomProperties           : NSString?
    var Id                         : Int = 0
    var IsPreSelected              : Bool = false
    var SelectedAttribute          : Bool = false
    var Name                       : String?
    var pictureModel               : PictureModel?
    var PriceAdjustment            : String?
    var PriceAdjustmentValue       : Int = 0
    
    required init? (map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.ColorSquaresRgb        <- map["ColorSquaresRgb"]
        self.CustomProperties       <- map["CustomProperties"]
        self.Id                     <- map["Id"]
        self.IsPreSelected          <- map["IsPreSelected"]
        self.Name                   <- map["Name"]
        self.pictureModel           <- map["PictureModel"]
        self.PriceAdjustment        <- map["PriceAdjustment"]
        self.PriceAdjustmentValue   <- map["PriceAdjustmentValue"]
    }
}

class PictureModel: Mappable {
    var ImageUrl     : NSString?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.ImageUrl <- map["ImageUrl"]
    }
}
