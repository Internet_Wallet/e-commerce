//
//  ProductSpecifications.swift
//  NopCommerce
//
//  Created by BS-125 on 12/22/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductSpecifications: Mappable {
    
    
    var SpecificationAttributeId  : NSInteger = 0
    var SpecificationAttributeName: String = ""
    var ValueRaw                  : String = ""
    var ColorSquaresRgb           : String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.SpecificationAttributeId           <- map["SpecificationAttributeId"]
        self.SpecificationAttributeName         <- map["SpecificationAttributeName"]
        self.ValueRaw                           <- map["ValueRaw"]
        self.ColorSquaresRgb                    <- map["ColorSquaresRgb"]
    }
}
