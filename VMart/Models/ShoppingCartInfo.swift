//
//  ShoppingCartInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

open class ShoppingCartInfo: NSObject {
    
    
    
    var ShowSku :                             Bool
    var ShippingMethod       :                NSString?
    var OrderReviewData_CustomProperties     :NSDictionary?
    var OrderTotalResponse: NSDictionary?
    var ShippingAddress_Company              :                NSString?
    var ShippingAddress_FaxRequired    :                      Bool
    var ShippingAddress_FaxEnabled :                          Bool
    var ShippingAddress_AvailableCountries    :               NSArray?
    
    var ShippingAddress_Address1        :                     NSString?
    var ShippingAddress_City :                                NSString?
    var ShippingAddress_Email         :                       NSString?
    var ShippingAddress_StreetAddress2Enabled :               Bool
    var ShippingAddress_CountryEnabled            :           Bool
    
    var ShippingAddress_LastName :                            NSString?
    var ShippingAddress_CountryId  :                          NSInteger?
    
    var ShippingAddress_CountryName    :                      NSString?
    var ShippingAddress_CityRequired :                        Bool
    var ShippingAddress_FaxNumber :                           NSInteger?
    var ShippingAddress_CompanyRequired :                     Bool
    var ShippingAddress_AvailableStates :                     NSArray
    
    var ShippingAddress_StateProvinceName :                   NSString?
    var ShippingAddress_FormattedCustomAddressAttributes :    NSString?
    var ShippingAddress_CustomProperties :                    NSDictionary?
    var ShippingAddress_ZipPostalCode :                       NSString?
    var ShippingAddress_StateProvinceId :                     NSInteger?
    var ShippingAddress_CompanyEnabled :                      Bool
    var ShippingAddress_CustomAddressAttributes :             NSArray?
    
    
    var ShippingAddress_PhoneRequired :                       Bool
    var ShippingAddress_StreetAddressEnabled :                Bool
    
    var ShippingAddress_CityEnabled :                         Bool
    var ShippingAddress_StateProvinceEnable :                 Bool
    var ShippingAddress_Address2 :                            NSString?
    var ShippingAddress_ZipPostalCodeEnabled :                Bool
    var ShippingAddress_PhoneNumber :                         NSString?
    var ShippingAddress_StreetAddress2Required :              Bool
    var ShippingAddress_FirstName :                           NSString?
    var ShippingAddress_PhoneEnabled :                        Bool
    var ShippingAddress_ZipPostalCodeRequired :               Bool
    var ShippingAddress_Id :                                  NSInteger?
    var ShippingAddress_StreetAddressRequired :               Bool
    
    
    var CustomValues :                        NSDictionary?
    var PaymentMethod :                       NSString?
    
    var SelectedPickUpInStore :               Bool
    var Display :                             Bool
    var IsShipEnabled :                       Bool
    
    
    var BillingAddress_Company :              NSString?
    var BillingAddress_FaxRequired :          Bool
    var BillingAddress_FaxEnabled :           Bool
    var BillingAddress_AvailableCountries :   NSArray?
    
    var BillingAddress_Address1        :                     NSString?
    var BillingAddress_City :                                NSString?
    var BillingAddress_Email         :                       NSString?
    var BillingAddress_StreetAddress2Enabled :               Bool
    var BillingAddress_CountryEnabled            :           Bool
    
    var BillingAddress_LastName :                            NSString?
    var BillingAddress_CountryId  :                          NSInteger?
    
    var BillingAddress_CountryName    :                      NSString?
    var BillingAddress_CityRequired :                        Bool
    var BillingAddress_FaxNumber :                           NSInteger?
    var BillingAddress_CompanyRequired :                     Bool
    var BillingAddress_AvailableStates :                     NSArray?
    
    
    var BillingAddress_StateProvinceName :                   NSString?
    var BillingAddress_FormattedCustomAddressAttributes :    NSString?
    var BillingAddress_CustomProperties :                    NSDictionary?
    var BillingAddress_ZipPostalCode :                       NSString?
    var BillingAddress_StateProvinceId :                     NSInteger?
    var BillingAddress_CompanyEnabled :                      Bool
    var BillingAddress_CustomAddressAttributes :             NSArray?
    
    
    var BillingAddress_PhoneRequired :                       Bool
    var BillingAddress_StreetAddressEnabled :                Bool
    
    var BillingAddress_CityEnabled :                         Bool
    var BillingAddress_StateProvinceEnable :                 Bool?
    var BillingAddress_Address2 :                            NSString?
    var BillingAddress_ZipPostalCodeEnabled :                Bool
    var BillingAddress_PhoneNumber :                         NSString?
    var BillingAddress_StreetAddress2Required :              Bool
    var BillingAddress_FirstName :                           NSString?
    var BillingAddress_PhoneEnabled :                        Bool
    var BillingAddress_ZipPostalCodeRequired :               Bool
    var BillingAddress_Id :                                  NSInteger?
    var BillingAddress_StreetAddressRequired :               Bool
    
    
    
    var Count :                                              NSInteger
    var Warnings :                                           NSArray?
    var OnePageCheckoutEnabled  :                            Bool
    var ShowProductImages  :                                 Bool
    var DiscountBox_CustomProperties  :                      NSDictionary?
    var DiscountBox_Message :                                NSString?
    var DiscountBox_Display  :                               Bool
    var DiscountBox_CurrentCode :                            NSString?
    var DiscountBox_IsApplied   :                            Bool
    var TermsOfServiceOnOrderConfirmPage   :                 Bool
    var CheckoutAttributeInfo   :                            NSString?
    var GiftCardBox_CustomProperties  :                      NSDictionary?
    var GiftCardBox_Display           :                      Bool
    var GiftCardBox_IsApplied         :                      Bool
    var GiftCardBox_Message           :                      NSString?
    
    
    
    var subTotal    : String?
    var tax         : String?
    var orderTotal  : String?
    var MinOrderTotalAmount  : String?
    var shipping    : String?
    var SubTotalDiscount    : String?
    
    
    
    var Items                         :                      [CheckOutItems]
    var checkoutAttributes            :                      [CheckoutAttributes]
    
    
    
    
    
    
    init (JSON : AnyObject) {
        
        
        ShowSku                                   =  (JSON.value(forKeyPath: "ShowSku") as! Bool)
        ShippingMethod                            =  (JSON.value(forKeyPath: "OrderReviewData.ShippingMethod") as? NSString)
        OrderReviewData_CustomProperties          =  (JSON.value(forKeyPath: "OrderReviewData.CustomProperties") as! NSDictionary)
        OrderTotalResponse                                     = (JSON.value(forKeyPath: "OrderTotalResponseModel") as! NSDictionary)
        CustomValues                              =  (JSON.value(forKeyPath: "OrderReviewData.CustomValues") as? NSDictionary)
        PaymentMethod                             =  (JSON.value(forKeyPath: "OrderReviewData.PaymentMethod") as? NSString)
        
        SelectedPickUpInStore                     =  (JSON.value(forKeyPath: "OrderReviewData.SelectedPickUpInStore") as! Bool)
        Display                                   =  (JSON.value(forKeyPath: "OrderReviewData.Display") as! Bool)
        IsShipEnabled                             =  (JSON.value(forKeyPath: "OrderReviewData.IsShippable") as! Bool)
        
        
        ShippingAddress_Company                   =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.Company") as? NSString)
        ShippingAddress_FaxRequired               =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.FaxRequired") as! Bool)
        ShippingAddress_FaxEnabled                =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.FaxEnabled") as! Bool)
        ShippingAddress_AvailableCountries        =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.AvailableCountries") as? NSArray)
        
        ShippingAddress_Address1                  =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.Address1") as? NSString)
        ShippingAddress_City                      =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.City") as? NSString)
        ShippingAddress_Email                     =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.Email") as? NSString)
        ShippingAddress_StreetAddress2Enabled     =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StreetAddress2Enabled") as! Bool)
        ShippingAddress_CountryEnabled            =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CountryEnabled") as! Bool)
        
        ShippingAddress_LastName                  =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.LastName") as? NSString)
        ShippingAddress_CountryId                 =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CountryId") as? NSInteger)
        
        ShippingAddress_CountryName               =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CountryName") as? NSString)
        ShippingAddress_CityRequired              =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CityRequired") as! Bool)
        ShippingAddress_FaxNumber                 =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.FaxNumber") as? NSInteger)
        ShippingAddress_CompanyRequired           =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CompanyRequired") as! Bool)
        ShippingAddress_AvailableStates           =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.AvailableStates") as! NSArray)
        
        ShippingAddress_StateProvinceName         =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StateProvinceName") as? NSString)
        ShippingAddress_FormattedCustomAddressAttributes = (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.FormattedCustomAddressAttributes") as? NSString)
        ShippingAddress_CustomProperties          =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CustomProperties") as? NSDictionary)
        ShippingAddress_ZipPostalCode             =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.ZipPostalCode") as? NSString)
        ShippingAddress_StateProvinceId           =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StateProvinceId") as? NSInteger)
        ShippingAddress_CompanyEnabled            =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CompanyEnabled") as! Bool)
        ShippingAddress_CustomAddressAttributes   =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CustomAddressAttributes") as? NSArray)
        
        
        ShippingAddress_PhoneRequired             =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.PhoneRequired") as! Bool)
        ShippingAddress_StreetAddressEnabled      =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StreetAddressEnabled") as! Bool)
        
        ShippingAddress_CityEnabled               =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.CityEnabled") as! Bool)
        ShippingAddress_StateProvinceEnable       =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StateProvinceEnabled") as! Bool)
        ShippingAddress_Address2                  =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.Address2") as? NSString)
        ShippingAddress_ZipPostalCodeEnabled      =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.ZipPostalCodeEnabled") as! Bool)
        ShippingAddress_PhoneNumber               =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.PhoneNumber") as? NSString)
        ShippingAddress_StreetAddress2Required    =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StreetAddress2Required") as! Bool)
        ShippingAddress_FirstName                 =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.FirstName") as? NSString)
        ShippingAddress_PhoneEnabled              =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.PhoneEnabled") as! Bool)
        ShippingAddress_ZipPostalCodeRequired     =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.ZipPostalCodeRequired") as! Bool)
        ShippingAddress_Id                        =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.Id") as? NSInteger)
        ShippingAddress_StreetAddressRequired     =  (JSON.value(forKeyPath: "OrderReviewData.ShippingAddress.StreetAddressRequired") as! Bool)
        
        
        
        
        BillingAddress_Company                     =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.Company") as? NSString)
        BillingAddress_FaxRequired                 =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.FaxRequired") as! Bool)
        BillingAddress_FaxEnabled                  =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.FaxEnabled") as! Bool)
        BillingAddress_AvailableCountries          =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.AvailableCountries") as? NSArray)
        
        BillingAddress_Address1                    =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.Address1") as? NSString)
        BillingAddress_City                        =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.City") as? NSString)
        BillingAddress_Email                       =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.Email") as? NSString)
        BillingAddress_StreetAddress2Enabled       =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StreetAddress2Enabled") as! Bool)
        BillingAddress_CountryEnabled              =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CountryEnabled") as! Bool)
        
        BillingAddress_LastName                    =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.LastName") as? NSString)
        BillingAddress_CountryId                   =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CountryId") as? NSInteger)
        
        BillingAddress_CountryName                 =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CountryName") as? NSString)
        BillingAddress_CityRequired                =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CityRequired") as! Bool)
        BillingAddress_FaxNumber                   =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.FaxNumber") as? NSInteger)
        BillingAddress_CompanyRequired             =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CompanyRequired") as! Bool)
        BillingAddress_AvailableStates             =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.AvailableStates") as? NSArray)
        
        
        BillingAddress_StateProvinceName           =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StateProvinceName") as? NSString)
        BillingAddress_FormattedCustomAddressAttributes =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.FormattedCustomAddressAttributes") as? NSString)
        BillingAddress_CustomProperties            =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CustomProperties") as? NSDictionary)
        BillingAddress_ZipPostalCode               =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.ZipPostalCode") as? NSString)
        BillingAddress_StateProvinceId             =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StateProvinceId") as? NSInteger)
        BillingAddress_CompanyEnabled              =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CompanyEnabled") as! Bool)
        BillingAddress_CustomAddressAttributes     =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CustomAddressAttributes") as? NSArray)
        
        
        BillingAddress_PhoneRequired               =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.PhoneRequired") as! Bool)
        BillingAddress_StreetAddressEnabled        =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StreetAddressEnabled") as! Bool)
        
        BillingAddress_CityEnabled                 =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.CityEnabled") as! Bool)
        BillingAddress_StateProvinceEnable         =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StateProvinceEnable") as? Bool)
        BillingAddress_Address2                    =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.Address2") as? NSString)
        BillingAddress_ZipPostalCodeEnabled        =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.ZipPostalCodeEnabled") as! Bool)
        BillingAddress_PhoneNumber                 =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.PhoneNumber") as? NSString)
        BillingAddress_StreetAddress2Required      =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StreetAddress2Required") as! Bool)
        BillingAddress_FirstName                   =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.FirstName") as? NSString)
        BillingAddress_PhoneEnabled                =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.PhoneEnabled") as! Bool)
        BillingAddress_ZipPostalCodeRequired       =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.ZipPostalCodeRequired") as! Bool)
        BillingAddress_Id                          =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.Id") as? NSInteger)
        BillingAddress_StreetAddressRequired       =  (JSON.value(forKeyPath: "OrderReviewData.BillingAddress.StreetAddressRequired") as! Bool)
        
        
        
        Count                                      =  (JSON.value(forKeyPath: "Count") as! NSInteger)
        Warnings                                   =  (JSON.value(forKeyPath: "Warnings") as? NSArray)
        OnePageCheckoutEnabled                     =  (JSON.value(forKeyPath: "ShowProductImages") as! Bool)
        ShowProductImages                          =  (JSON.value(forKeyPath: "ShowProductImages") as! Bool)
        DiscountBox_CustomProperties               =  (JSON.value(forKeyPath: "DiscountBox.CustomProperties") as? NSDictionary)
        DiscountBox_Message                        =  (JSON.value(forKeyPath: "DiscountBox.Message") as? NSString)
        DiscountBox_Display                        =  (JSON.value(forKeyPath: "DiscountBox.Display") as! Bool)
        DiscountBox_CurrentCode                                =  (JSON.value(forKeyPath: "DiscountBox.CurrentCode") as? NSString)
        DiscountBox_IsApplied                                  =  (JSON.value(forKeyPath: "DiscountBox.IsApplied") as! Bool)
        TermsOfServiceOnOrderConfirmPage           =  (JSON.value(forKeyPath: "TermsOfServiceOnOrderConfirmPage") as! Bool)
        CheckoutAttributeInfo                      =  (JSON.value(forKeyPath: "CheckoutAttributeInfo") as? NSString)
        GiftCardBox_CustomProperties               =  (JSON.value(forKeyPath: "GiftCardBox.GiftCardBox.CustomProperties") as? NSDictionary)
        GiftCardBox_Display                        =  (JSON.value(forKeyPath: "GiftCardBox.Display") as! Bool)
        GiftCardBox_IsApplied                      =  (JSON.value(forKeyPath: "GiftCardBox.IsApplied") as! Bool)
        GiftCardBox_Message                        =  (JSON.value(forKeyPath: "GiftCardBox.Message") as? NSString)
        
        
        
        
        
        subTotal                                   =  (JSON.value(forKeyPath: "OrderTotalResponseModel.SubTotal") as? String)
        tax                                        =  (JSON.value(forKeyPath: "OrderTotalResponseModel.Tax") as? String)
        orderTotal                                 =  (JSON.value(forKeyPath: "OrderTotalResponseModel.OrderTotal") as? String)
        MinOrderTotalAmount                                 =  (JSON.value(forKeyPath: "OrderTotalResponseModel.MinOrderTotalAmount") as? String)
        SubTotalDiscount                           =  (JSON.value(forKeyPath: "OrderTotalResponseModel.SubTotalDiscount") as? String)
        shipping                                   =   (JSON.value(forKeyPath: "OrderTotalResponseModel.Shipping") as? String)
        
        
        
        
        Items                                      = [CheckOutItems] ()
        checkoutAttributes                         = [CheckoutAttributes] ()
        
        
        for data in (JSON.value(forKeyPath: "Items") as! NSArray){
            let checkOut = CheckOutItems(JSON: data as AnyObject)
            self.Items.append(checkOut)
            
        }
        for data in (JSON.value(forKeyPath: "CheckoutAttributes") as! NSArray){
            let attributs = CheckoutAttributes(JSON: data as AnyObject)
            self.checkoutAttributes.append(attributs)
            
        }
    }
}



