//
//  Store.swift
//  NopCommerce
//
//  Created by BS-116 on 7/27/17.
//  Copyright © 2017 Jahid Hassan. All rights reserved.
//

import UIKit

open class Store: NSObject {
    var countryName : NSString
    var storeDescription : NSString
    var zipPostalCode: NSString
    var storeId: NSString
    var name: NSString
    var latitude: NSString
    var openingHours: NSString
    var address: NSString
    var longitude: NSString
    var providerSystemName: NSString
    var city: NSString
    var pickupFee: NSString
    var Distance: NSString
    
    init (JSON: [String: AnyObject]) {
        
        self.countryName = JSON["CountryName"] as? NSString ?? ""
        self.storeDescription = JSON["Description"] as? NSString ?? ""
        self.zipPostalCode = JSON["ZipPostalCode"] as? NSString ?? ""
        self.storeId = JSON["Id"] as? NSString ?? ""
        self.name = JSON["Name"] as? NSString ?? ""
        self.latitude = JSON["Latitude"] as? NSString ?? ""
        self.openingHours = JSON["OpeningHours"] as? NSString ?? ""
        self.address = JSON["Address"] as? NSString ?? ""
        self.longitude = JSON["Longitude"] as? NSString ?? ""
        self.providerSystemName = JSON["ProviderSystemName"] as? NSString ?? ""
        self.city = JSON["City"] as? NSString ?? ""
        self.pickupFee = JSON["PickupFee"] as? NSString ?? ""
        self.Distance = JSON["Distance"] as? NSString ?? ""
    }
} 
