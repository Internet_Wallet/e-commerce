//
//  TotalOrderGift.swift
//  NopCommerce
//
//  Created by BS-136 on 7/24/18.
//  Copyright © 2018 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper
open class TotalOrderGift: Mappable {
    
    var subTotal            : String?
    var tax                 : String?
    var orderTotal          : String?
    var shipping            : String?
    var discount            : String?
    var giftCardModel       : [GiftCardShoppingCartModel]?
    
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.subTotal               <- map["SubTotal"]
        self.tax                    <- map["Tax"]
        self.orderTotal             <- map["OrderTotal"]
        self.shipping               <- map["Shipping"]
        self.discount               <- map["OrderTotalDiscount"]
        self.giftCardModel          <- map["GiftCards"]
    }
}
open class GiftCardShoppingCartModel: Mappable {
    
    var Remaining    : String?
    var CouponCode         : String?
    var Amount  : String?
    var Id    : Int?
    var CustomProperties    : NSDictionary?
    
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.Remaining           <- map["Remaining"]
        self.CouponCode          <- map["CouponCode"]
        self.Amount              <- map["Amount"]
        self.Id                  <- map["Id"]
        self.CustomProperties    <- map["CustomProperties"]
    }
}

