//
//  UpdationModel.swift
//  VMart
//
//  Created by Avaneesh Kumar on 20/01/2020.
//  Copyright © 2020 Shobhit. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON

class UpdationModel: Mappable {
    
    var appID: Int = -100
    var appLatestVersionName : String?
    var appLatestVersionCode : Int = -110
    var appURL : String?
    var isUpdateRequired : Bool?
    var isBackgroundDownload : Bool?
    var message : String?
    var isProduction : Bool?
    var playStoreUrl : String?
    var apkName : String?
    var isUpdateForceRequired : Bool?
    var successMessage : String?
    var statusCode : Int = -120
    var errorList: [String]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        self.appID                  <- map["AppID"]
        self.appLatestVersionName   <- map["AppLatestVersionName"]
        self.appLatestVersionCode   <- map["AppLatestVersionCode"]
        self.appURL                 <- map["AppURL"]
        self.isUpdateRequired       <- map["IsUpdateRequired"]
        self.isBackgroundDownload   <- map["IsBackgroundDownload"]
        self.message                <- map["message"]
        self.isProduction           <- map["IsProduction"]
        self.playStoreUrl           <- map["playStoreUrl"]
        self.apkName                <- map["apkName"]
        self.isUpdateForceRequired  <- map["IsUpdateForceRequired"]
        self.successMessage         <- map["SuccessMessage"]
        self.statusCode             <- map["StatusCode"]
        self.errorList              <- map["ErrorList"]
    }
}

