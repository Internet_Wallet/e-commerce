//
//  WishListItems.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/12/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

open class WishListItems: Mappable {
    var id             : NSInteger = -1
    var ProductId      : NSInteger = -1
    var ProductName    : NSString = ""
    var ImageUrl       : String?
    var UnitPrice      : NSString = ""
    var perOff         : NSString = ""
    var OldPrice       : String = ""
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.id         <- map["Id"]
        self.ImageUrl    <- map["Picture.ImageUrl"]
        self.ProductId         <- map["ProductId"]
        self.ProductName      <- map["ProductName"]
        self.UnitPrice       <- map["UnitPrice"]
        self.perOff         <- map["Discount"]
        self.OldPrice         <- map["OldPrice"]
    }
}
