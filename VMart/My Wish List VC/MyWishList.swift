//
//  MyWishList.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/19/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class MyWishList: ZAYOKBaseViewController {
    
    var aPIManager = APIManager()
    var wishListArray : [WishListItems] = []
    
    
    @IBOutlet weak var wishlistTableView: UITableView!
    @IBOutlet weak var noWishFoundlbl: UILabel!{
        didSet {
            self.noWishFoundlbl.text = self.noWishFoundlbl.text?.localized
            self.noWishFoundlbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var noRecordView: UIView! {
        didSet {
            self.noRecordView.isHidden = true
        }
    }
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    @IBOutlet var infoLbl: UILabel!{
        didSet {
            self.infoLbl.text = self.infoLbl.text?.localized
            self.infoLbl.textColor = UIColor.black
            self.infoLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var continueShopingBtn: UIButton! {
        didSet {
            //            continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            continueShopingBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.continueShopingBtn.setTitle("Continue Shopping".localized, for: .normal)
            self.continueShopingBtn.layer.cornerRadius = 4
            self.continueShopingBtn.layer.masksToBounds = true
            continueShopingBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
        
        
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        if AppUtility.isConnectedToNetwork() {
            self.loadWishList()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My WishList".localized
        wishlistTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        //        if AppUtility.isConnectedToNetwork() {
        //            self.loadWishList()
        //        } else {
        //            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        //        }
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        //        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        //        self.present(viewController, animated: true, completion: nil)
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismissController()
    }
    
    @IBAction func continueShopAction(_ sender: UIButton) {
        //        self.dismissController()
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    func dismissController(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func loadWishList() {
        AppUtility.showLoading(self.view)
        aPIManager.loadWishList(onSuccess: { [weak self] wishListItems in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            self?.wishListArray = wishListItems.reversed()
            
            if let count = self?.wishListArray.count, count == 0 {
                DispatchQueue.main.async {
                    self?.noRecordView.isHidden = false
                }
            } else {
                //self.containerView.isHidden = false
                //self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count * 150)
                self?.wishlistTableView.dataSource = self
                self?.wishlistTableView.delegate = self
                self?.wishlistTableView.reloadData()
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func deleteBtnAction(_ sender: IndexPath, handle :@escaping (_ success: Bool) -> Void)  {
        let row = sender.row
        let parameters : Dictionary <String, AnyObject> = ["value": self.wishListArray[row].id  as AnyObject, "key": "removefromcart" as AnyObject]
        let dicArray = NSMutableArray()
        dicArray.add(parameters)
        AppUtility.showLoading(self.view)
        aPIManager.deleteAndUpdateWishList(dicArray, onSuccess: { [weak self] in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                AppUtility.showToastlocal(message: "Product removed".localized, view: viewLoc)
            }
            self?.wishListArray.remove(at: row)
            self?.wishlistTableView.deleteRows(at: [sender], with: UITableView.RowAnimation.automatic)
            if let count = self?.wishListArray.count, count == 0 {
                DispatchQueue.main.async {
                    self?.noRecordView.isHidden = false
                }
            }
            self?.wishlistTableView.reloadData()
            handle(true)
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToastlocal(message: message!, view: viewLoc)
                    //                    AppUtility.showToast(message,view: viewLoc)
                    handle(false)
                }
        })
    }
    
    private func showProductDetailVC(index: Int) {
        let productId = self.wishListArray[index].ProductId
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails {
            vc.productId = productId
            vc.productName = self.wishListArray[index].ProductName as String
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension MyWishList: MyWishListDelegate{
    func deleteWishListItemAction(sender: IndexPath) {
        
        self.deleteBtnAction(sender) { (_) in
        }
        
        
        //        let alertVC = SAlertController()
        //        alertVC.ShowSAlert(title: "Alert".localized, withDescription: "Do you want to remove from WishList?".localized, onController: self)
        //        let ok = SAlertAction()
        //        let Cancel = SAlertAction()
        //        Cancel.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {})
        //        ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
        //            self.deleteBtnAction(sender) { (_) in
        //            }
        //        })
        //        alertVC.addAction(action: [Cancel,ok])
        
        //        let alertView = UIAlertController(title: "Alert".localized, message: "Do you want to remove from WishList?".localized, preferredStyle: .alert)
        //        alertView.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        //            self.deleteBtnAction(sender) { (_) in
        //
        //            }
        //        }))
        //        self.present(alertView, animated: true, completion: nil)
    }
}

extension MyWishList: UITableViewDataSource, UITableViewDelegate {
    // MARK: - TableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wishListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyWishListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyWishListCell") as! MyWishListTableViewCell
        cell.deleteBtn.tag = indexPath.row
        cell.Delegate = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.updateCell(wishListArray: wishListArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREEN_HEIGHT/5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showProductDetailVC(index: indexPath.row)
    }
    
    //    @available(iOS 11.0, *)
    //    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    //        let deleteAction = UIContextualAction(style: .normal, title:  "Delete".localized, handler: { (ac:UIContextualAction, view:UIView, success:@escaping (Bool) -> Void) in
    //            self.deleteBtnAction(indexPath, handle: { (status) in
    //                success(status)
    //            })
    //        })
    //        deleteAction.backgroundColor = .red
    //        return UISwipeActionsConfiguration(actions: [deleteAction])
    //    }
}
