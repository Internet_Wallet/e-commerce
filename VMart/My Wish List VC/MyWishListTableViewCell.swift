//
//  MyWishListTableViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/19/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol MyWishListDelegate: class {
    func deleteWishListItemAction(sender: IndexPath)
}

class MyWishListTableViewCell: UITableViewCell {
    
    weak var Delegate: MyWishListDelegate?
    
    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productNameLbl: UILabel!{
        didSet {
            self.productNameLbl.text = self.productNameLbl.text?.localized
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var oldAmountLbl: UILabel! {
        didSet {
            self.oldAmountLbl.text = self.oldAmountLbl.text?.localized
            self.oldAmountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var amountLbl: UILabel! {
        didSet {
            self.amountLbl.text = self.amountLbl.text?.localized
            self.amountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var percentLbl: UILabel! {
        didSet {
            self.percentLbl.text = self.percentLbl.text?.localized
            self.percentLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var perOffView : UIView! {
        didSet {
            self.perOffView.isHidden = true
        }
    }
    @IBOutlet var deleteBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateCellData(cell:MyWishListTableViewCell, wishListArray:[WishListItems], indexPath: IndexPath) {
        cell.deleteBtn.tag = indexPath.row
        cell.productNameLbl.text = wishListArray[indexPath.row].ProductName as String
        cell.amountLbl.text = wishListArray[indexPath.row].UnitPrice as String
        cell.productImageView.layer.borderWidth = 1.0
        cell.productImageView.layer.masksToBounds = false
        cell.productImageView.layer.borderColor = UIColor.white.cgColor
        cell.productImageView.layer.cornerRadius = 13
        cell.productImageView.layer.cornerRadius = cell.productImageView.frame.size.height/2
        cell.productImageView.clipsToBounds = true
        
        cell.productImageView.image = nil
        if let imageUrl = wishListArray[indexPath.row].ImageUrl {
            cell.productImageView.sd_setImage(with: URL(string: imageUrl))
            
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: imageUrl), imageView: self.productImageView, imgUrl: imageUrl) { (image) in }
            
            
            
            
            //            AppUtility.getData(from: URL(string: imageUrl)!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.productImageView.image = UIImage(data: data)
            //                }
            //            }
            
            
        }
        //        cell.containerView.layer.cornerRadius = 8
        //        cell.containerView.layer.masksToBounds = true
        //        cell.containerView.layer.shadowColor = UIColor.darkGray.cgColor
        //        cell.containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        //        cell.containerView.layer.shadowOpacity = 0.5
        //        cell.containerView.layer.shadowRadius = 1
        //        cell.containerView.layer.masksToBounds = true
        //        cell.containerView.clipsToBounds = false
    }
    
    func updateCell(wishListArray: WishListItems) {
        self.productNameLbl.text = wishListArray.ProductName as String
        if let imageUrl = wishListArray.ImageUrl {
            self.productImageView.sd_setImage(with: URL(string: imageUrl))
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: imageUrl), imageView: self.productImageView, imgUrl: imageUrl) { (image) in }
            
            
            
            
            
            //            AppUtility.getData(from: URL(string: imageUrl)!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.productImageView.image = UIImage(data: data)
            //                }
            //            }
            
            
        }
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: wishListArray.UnitPrice as String, oldPrice: wishListArray.OldPrice, priceWithDiscount: "")
        
        var productPrice = wishListArray.UnitPrice.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice + mmkText
        let attrString = NSMutableAttributedString(string: productPrice)
        let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.amountLbl.attributedText = attrString
        
        var oldPrice = wishListArray.OldPrice.replacingOccurrences(of: "MMK", with: "")
        oldPrice = oldPrice + mmkText
        
        if Price == "" {
            var productPrice2 = wishListArray.UnitPrice.replacingOccurrences(of: "MMK", with: "")
            productPrice2 = productPrice2 + mmkText
            let attrStr = NSMutableAttributedString(string: productPrice2)
            let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            self.amountLbl.attributedText = attrStr
            
            let attrString = NSMutableAttributedString(string: oldPrice, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            let nsRange2 = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
            self.oldAmountLbl.attributedText = attrString
        }
        
        self.amountLbl.text = wishListArray.UnitPrice as String
        if (wishListArray.perOff as String) != "" {
            let amountStr = Int((wishListArray.UnitPrice as String).replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " MMK", with: "")) ?? 0
            let perOffStr = Int((wishListArray.perOff as String).replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " MMK", with: "")) ?? 0
            let totalAmount = amountStr + perOffStr
            let discount = (perOffStr * 100) / totalAmount
            self.perOffView.isHidden = false
            self.percentLbl.text = "\(discount)% off "
        }
    }
    
    @IBAction func deleteBtnActionClick(sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        self.Delegate?.deleteWishListItemAction(sender: indexPath)
    }
}
