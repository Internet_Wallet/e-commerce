//
//  MyRequestList.swift
//  VMart
//
//  Created by Mohit on 7/13/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class MyRequestList: ZAYOKBaseViewController {
    
    @IBOutlet var filterBarButton: UIBarButtonItem!
    @IBOutlet weak var requestTableView: UITableView!
    @IBOutlet weak var filterViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var btnApproved: UIButton!
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnRejected: UIButton!
    
    var isSelectedFilter = false
    var filter : Bool = false
    
    @IBOutlet weak var approvedLbl : UILabel!{
        didSet {
            self.approvedLbl.text = self.approvedLbl.text?.localized
            self.approvedLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var rejectedLbl: UILabel!{
        didSet {
            self.rejectedLbl.text = self.rejectedLbl.text?.localized
            self.rejectedLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var pendingLbl: UILabel!{
        didSet {
            self.pendingLbl.text = self.pendingLbl.text?.localized
            self.pendingLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var aPIManager = APIManager()
    var ServerResponse : GetMedicineModel?
    var responseObject :[medicineRequest] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Medicine Request List".localized
        
        self.requestTableView.delegate = self
        self.requestTableView.dataSource = self
        self.addBack()
        
        self.filterView.isHidden = true
        self.filterViewHeightConstraint.constant = 0
        
        self.btnApproved.addTarget(self, action:#selector(applyFilterAction(sender:)) , for:.touchUpInside)
        self.btnPending.addTarget(self, action:#selector(applyFilterAction(sender:)) , for:.touchUpInside)
        self.btnRejected.addTarget(self, action:#selector(applyFilterAction(sender:)) , for:.touchUpInside)
        
        // Do any additional setup after loading the view.
        
        self.filterBarButton = UIBarButtonItem.init(image: UIImage.init(named: "filter_medicine"), style: .plain, target: self , action: #selector(self.openAndCloseFilterAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.filterBarButton
        self.navigationItem.rightBarButtonItem?.tintColor = .white
    }
    
    @IBAction func openAndCloseFilterAction(_ sender: UIBarButtonItem) {
        
        if filter {
            filter = false
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.filterView.isHidden = true
                self.filterViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        }
        else {
            filter = true
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.filterView.isHidden = false
                self.filterViewHeightConstraint.constant = 65
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func applyFilterAction(sender:UIButton) {
        
        let selected : Bool = sender.isSelected
        let imageStringSelected = "act_radio"
        let imageStringUnSelected = "nonact_radio"
        let index = sender.tag
        switch index {
        case 1:
            if isSelectedFilter {
                if selected {
                    self.btnApproved.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                    sender.isSelected = false
                    isSelectedFilter = false
                }
                else {
                    self.btnApproved.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                    sender.isSelected = true
                    isSelectedFilter = true
                }
                self.btnPending.isSelected = false
                self.btnRejected.isSelected = false
                self.btnPending.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnRejected.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
            }
            else {
                self.btnApproved.isSelected = true
                self.btnPending.isSelected = false
                self.btnRejected.isSelected = false
                self.btnApproved.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                self.btnPending.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnRejected.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                isSelectedFilter = true
                
            }
            
        case 2:
            if isSelectedFilter {
                if selected {
                    self.btnPending.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                    isSelectedFilter = false
                    sender.isSelected = false
                }
                else {
                    self.btnPending.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                    isSelectedFilter = true
                    sender.isSelected = true
                }
                self.btnApproved.isSelected = false
                self.btnRejected.isSelected = false
                self.btnApproved.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnRejected.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
            }
            else {
                self.btnApproved.isSelected = false
                self.btnPending.isSelected = true
                self.btnRejected.isSelected = false
                self.btnApproved.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnPending.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                self.btnRejected.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                isSelectedFilter = true
            }
        case 3:
            if isSelectedFilter {
                if selected {
                    self.btnRejected.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                    isSelectedFilter = false
                    sender.isSelected = false
                    
                }
                else {
                    self.btnRejected.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                    isSelectedFilter = true
                    sender.isSelected = true
                }
                self.btnApproved.isSelected = false
                self.btnPending.isSelected = false
                self.btnApproved.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnPending.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
            }
            else {
                self.btnApproved.isSelected = false
                self.btnPending.isSelected = false
                self.btnRejected.isSelected = true
                self.btnApproved.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnPending.setImage(UIImage.init(named: imageStringUnSelected), for: .normal)
                self.btnRejected.setImage(UIImage.init(named: imageStringSelected), for: .normal)
                isSelectedFilter = true
            }
        default : break
        }
        self.filtertheDetails(senderType: sender)
    }
    
    func filtertheDetails(senderType : UIButton) {
        
        
        if self.responseObject.count == 0{
            AppUtility.showToastlocal(message: "Record not found".localized, view: self.view)
            return
        }
        
        if isSelectedFilter {
            if senderType.isSelected {
                let sender = senderType.tag
                let allItems :[medicineRequest] = self.ServerResponse!.medicineData!
                switch (sender) {
                case 1:
                    self.responseObject = allItems.filter({ (medicineRequest) -> Bool in
                        return (medicineRequest.requestStatusMessage == "Approved")
                    })
                case 2:
                    self.responseObject = allItems.filter({ (medicineRequest) -> Bool in
                        return (medicineRequest.requestStatusMessage == "Pending")
                    })
                case 3:
                    self.responseObject = allItems.filter({ (medicineRequest) -> Bool in
                        return (medicineRequest.requestStatusMessage == "Rejected")
                    })
                default:
                    break
                }
            }
            else {
                self.responseObject = self.ServerResponse?.medicineData ?? []
            }
        }
        else {
            self.responseObject = self.ServerResponse?.medicineData ?? []
        }
        self.requestTableView.reloadData()
    }
    
    func addBack(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
    }
    
    
    @objc func dismissScreen(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        
        if AppUtility.isConnectedToNetwork() {
            self.getMedicineRequest()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        
    }
    
    func getMedicineRequest() {
        AppUtility.showLoading(self.view)
        aPIManager.getmyMedicineRequest("", onSuccess: { [weak self] responseData in
            
            if let dataModel = responseData  as? GetMedicineModel {
                
                //print(dataDict["Data"])
                self?.ServerResponse = dataModel
                if let array = dataModel.medicineData  {
                    self?.responseObject = array 
                    self?.requestTableView.reloadData()
                }
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }})
    }
    
}

extension MyRequestList: UITableViewDataSource, UITableViewDelegate {
    // MARK: - TableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseObject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MedicineRequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RequestListCell") as! MedicineRequestTableViewCell
        // cell.deleteBtn.tag = indexPath.row
        //        cell.Delegate = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.updateCellData(object: self.responseObject[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestDetails") as? RequestDetails{
            viewController.responseObject = self.responseObject[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

