//
//  NoInternetConnectionViewController.swift
//  VMart
//
//  Created by Ashish on 12/17/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import Reachability

protocol NoInternetDelegate : class {
    func tryAgainButtonTapped(_ vc: NoInternetConnectionViewController)
    func isNetworkReachable(_ network: Bool, vc: NoInternetConnectionViewController)
}

class NoInternetConnectionViewController : UIViewController {
    
    @IBOutlet weak var noInternetLabel: UILabel! {
        didSet {
            self.noInternetLabel.text = "No Internet Connection".localized
            self.noInternetLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var detailInfoLabel: UILabel! {
        didSet {
            self.detailInfoLabel.text = "Slow or No Internet Connection. Please check your Internet Settings.".localized
            self.detailInfoLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var tryAgainBtn: UIButton! {
        didSet {
            self.tryAgainBtn.setTitle("Try Again".localized, for: .normal)
            tryAgainBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    weak var delegate : NoInternetDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        do{
            let reachability = try Reachability()
            let name = Notification.Name.reachabilityChanged
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: name, object: reachability)
            
            try reachability.startNotifier()
        }catch{
            println_debug("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .none:
            self.delegate?.isNetworkReachable(false, vc: self)
        case .wifi, .cellular:
            self.delegate?.isNetworkReachable(true, vc: self)
        default :
            self.delegate?.isNetworkReachable(false, vc: self)
        }
        
    }
    
    //MARK:- Try again Button
    @IBAction func tryAgainAction(_ sender: UIButton) {
        if AppUtility.isConnectedToNetwork() {
            self.delegate?.tryAgainButtonTapped(self)
        }
    }
}
