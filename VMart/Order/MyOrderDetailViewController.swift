//
//  MyOrderDetailViewController.swift
//  VMart
//
//  Created by Kethan on 12/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class MyOrderDetailViewController: ZAYOKBaseViewController {
    
    var responseModel: Dictionary<String, Any>?
    var itemsArray = [Dictionary<String, Any>]()
    var noteDictionary: [Dictionary<String,Any>] = []
    var PaymentMethodStatus : String = ""
    var aPIManager = APIManager()
    var selectedProductId = Int()
    var orderDetail: VMMyOrderDetail? = nil
    var shareBarButton: UIBarButtonItem!
    
    @IBOutlet var PDFView:UIView?
    @IBOutlet weak var myOrderDetailTbl: UITableView!
    @IBOutlet weak var PdfTbl: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        myOrderDetailTbl.estimatedRowHeight = 50
        myOrderDetailTbl.rowHeight = UITableView.automaticDimension
        getProductDetails()
        myOrderDetailTbl.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.shareBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action:  #selector(self.ShareAction(_:)))
        self.shareBarButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem  = self.shareBarButton
    }
    
    
    @IBAction func ShareAction(_ sender: UIBarButtonItem) {
        self.PDFView?.isHidden = false
        DispatchQueue.main.async {
            self.PdfTbl.delegate = self
            self.PdfTbl.dataSource = self
            self.PdfTbl.reloadData()
            let pdfUrl = self.createPdfFromView(aView: self.PDFView!, saveToDocumentsWithFileName: "Transaction Receipt".localized)
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: {
                self.PDFView?.isHidden = true
            })
        }
    }
    
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:PdfTbl.frame.width, height:self.view.frame.height)
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    private func parseItems(dict: [Any], completionHandler: @escaping(Bool) -> Void) {
        if dict.count > 0 {
            for item in dict {
                if let itm = item as? Dictionary<String, Any> {
                    itemsArray.append(itm)
                }
            }
        }
        completionHandler(true)
    }
    
    private func getProductDetails() {
        let urlString = String(format: "%@/order/details/%d", APIManagerClient.sharedInstance.base_url, selectedProductId)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self](response, success, data) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                do {
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        self?.responseModel = dict
                    }
                    if let dict = self?.responseModel, let orders = dict["Items"] as? [Any] {
                        let d = dict["OrderNotes"] as? [Dictionary<String,Any>]
                        let paystatus = dict["PaymentMethodStatus"] as? String
                        self?.PaymentMethodStatus = paystatus ?? ""
                        if let nt = d {
                            self?.noteDictionary = nt
                            self?.noteDictionary.reverse()
                        }
                        
                        self?.parseItems(dict: orders) { (status) in
                            DispatchQueue.main.async {
                                self?.myOrderDetailTbl.delegate = self
                                self?.myOrderDetailTbl.dataSource = self
                                self?.myOrderDetailTbl.reloadData()
                            }
                        }
                        
                    }
                } catch _ {
                    println_debug("Parse error")
                }
            }
        }
    }
}


extension MyOrderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == PdfTbl{
            return 6
        }else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == PdfTbl {
            if section == 4 {
                let TotalObject = responseModel!["Items"] as? [Dictionary<String,Any>]
                return (TotalObject?.count)!
            }else {
                return 1
            }
        }else {
            if section == 0 {
                return itemsArray.count
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == PdfTbl{
            println_debug(indexPath)
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFHeaderCell", for: indexPath) as! PDFTableCell
                //                cell.wrapData(responseModel: itemsArray[indexPath.row])
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFinvoiceCell", for: indexPath) as! PDFTableCell
                cell.PDFInvoice(responseModel: responseModel!)
                return cell
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFAddressCell", for: indexPath) as! PDFTableCell
                cell.PDFAddress(responseModel: responseModel!)
                return cell
            }else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderHeaderCell", for: indexPath) as! PDFTableCell
                return cell
            }else if indexPath.section == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderDetailCell", for: indexPath) as! PDFTableCell
                let currentObject = responseModel!["Items"] as? [Dictionary<String,Any>]
                cell.PDFOrderDetail(responseModel: currentObject![indexPath.row],completeDict: responseModel)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFTotalCell", for: indexPath) as! PDFTableCell
                cell.PDFwrapTotalData(responseModel: responseModel!)
                return cell
            }
        }
        else{
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myOrderItemCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapData(responseModel: itemsArray[indexPath.row])
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "shipStatusCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapNotecell(notes: noteDictionary, completeDict: responseModel)
                return cell
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderAddressCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapAddressData(responseModel: responseModel)
                return cell
            }
            else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentStatusCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wraPaymentStatusCell(paymentStatus: PaymentMethodStatus)
                return cell
            }
                
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderPaymentCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapPaymentData(orders: responseModel)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == PdfTbl {
            if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 1{
                return 100
            }else if indexPath.section == 5{
                return 80
            }else {
                return 50
            }
        }else {
            if indexPath.section == 0 {
                return 175
            }else if indexPath.section == 1 {
                if noteDictionary.count == 0 {
                    return 0
                }else if noteDictionary.count == 1  {
                    //                    return 100
                    return 180
                }
                else if noteDictionary.count == 2  {
                    return 180
                }
                    
                else if noteDictionary.count == 4  {
                    return 225
                }else  {
                    return 0
                }
            }else if indexPath.section == 2 {
                return  205
            }
                
            else if indexPath.section == 3  {
                return 80
            }
            else {
                return  320
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    private func showProductDetailVC(index: Int) {
        let productId =  self.itemsArray[index]["ProductId"] as? Int
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails {
            vc.productId = productId ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}


class MyOrderDetailTableCell: UITableViewCell {
    
    @IBOutlet weak var orderNumLable: UILabel!{
        didSet {
            self.orderNumLable.text = self.orderNumLable.text?.localized
            self.orderNumLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var orderNumValueLable: UILabel!{
        didSet {
            self.orderNumValueLable.text = self.orderNumValueLable.text?.localized
            self.orderNumValueLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var RefNumLable: UILabel!{
        didSet {
            self.RefNumLable.text = self.RefNumLable.text?.localized
            self.RefNumLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var RefNumValueLable: UILabel!{
        didSet {
            self.RefNumValueLable.text = self.RefNumValueLable.text?.localized
            self.RefNumValueLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var TaxLable: UILabel!{
        didSet {
            self.TaxLable.text = self.TaxLable.text?.localized
            self.TaxLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var TaxValueLable: UILabel!{
        didSet {
            self.TaxValueLable.text = self.TaxValueLable.text?.localized
            self.TaxValueLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet weak var colorLable: UILabel!{
        didSet {
            self.colorLable.text = self.colorLable.text?.localized
            self.colorLable.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var ratingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
            self.nameLabel.text = self.nameLabel.text?.localized
            self.nameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var starLabel: UILabel!{
        didSet {
            self.starLabel.text = self.starLabel.text?.localized
            self.starLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var ratRevLabel: UILabel!{
        didSet {
            self.ratRevLabel.text = self.ratRevLabel.text?.localized
            self.ratRevLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var priceAfterDisc: UILabel!{
        didSet {
            self.priceAfterDisc.text = self.priceAfterDisc.text?.localized
            self.priceAfterDisc.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var priceBefDisc: UILabel!{
        didSet {
            self.priceBefDisc.text = self.priceBefDisc.text?.localized
            self.priceBefDisc.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var perOffLabel: UILabel!{
        didSet {
            self.perOffLabel.text = self.perOffLabel.text?.localized
            self.perOffLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var qtyLabel: UILabel!{
        didSet {
            self.qtyLabel.text = self.qtyLabel.text?.localized
            self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var blueLable1: UILabel!{
        didSet {
            self.blueLable1.text = self.blueLable1.text?.localized
            self.blueLable1.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var blueLable2: UILabel!{
        didSet {
            self.blueLable2.text = self.blueLable2.text?.localized
            self.blueLable2.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var blueLable3: UILabel!{
        didSet {
            self.blueLable3.text = self.blueLable3.text?.localized
            self.blueLable3.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var blueLable4: UILabel!{
        didSet {
            self.blueLable4.text = self.blueLable4.text?.localized
            self.blueLable4.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var blueLable5: UILabel!{
        didSet {
            self.blueLable5.text = self.blueLable5.text?.localized
            self.blueLable5.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var approveStatusLabel: UILabel!{
        didSet {
            self.approveStatusLabel.text = self.approveStatusLabel.text?.localized
            self.approveStatusLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var approveStatusDateLabel: UILabel!{
        didSet {
            self.approveStatusDateLabel.text = self.approveStatusDateLabel.text?.localized
            self.approveStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var shippingStatusLabel: UILabel!{
        didSet {
            self.shippingStatusLabel.text = self.shippingStatusLabel.text?.localized
            self.shippingStatusLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var shippingStatusDateLabel: UILabel!{
        didSet {
            self.shippingStatusDateLabel.text = self.shippingStatusDateLabel.text?.localized
            self.shippingStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var deliverStatusLabel: UILabel!{
        didSet {
            self.deliverStatusLabel.text = self.deliverStatusLabel.text?.localized
            self.deliverStatusLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var deliverStatusDateLabel: UILabel!{
        didSet {
            self.deliverStatusDateLabel.text = self.deliverStatusDateLabel.text?.localized
            self.deliverStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var addressNameLabel: UILabel!{
        didSet {
            self.addressNameLabel.text = self.addressNameLabel.text?.localized
            self.addressNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var addressLabel: UILabel!{
        didSet {
            self.addressLabel.text = self.addressLabel.text?.localized
            self.addressLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var addressPhoneLabel: UILabel!{
        didSet {
            self.addressPhoneLabel.text = self.addressPhoneLabel.text?.localized
            self.addressPhoneLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var productAmtLabel: UILabel!{
        didSet {
            self.productAmtLabel.text = self.productAmtLabel.text?.localized
            self.productAmtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var shippingChargesLabel: UILabel!{
        didSet {
            self.shippingChargesLabel.text = self.shippingChargesLabel.text?.localized
            self.shippingChargesLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var totalPayableAmtLabel: UILabel!{
        didSet {
            self.totalPayableAmtLabel.text = self.totalPayableAmtLabel.text?.localized
            self.totalPayableAmtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var paymentModeLabel: UILabel!{
        didSet {
            self.paymentModeLabel.text = self.paymentModeLabel.text?.localized
            self.paymentModeLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var paymentModeLbl: UILabel!{
        didSet {
            self.paymentModeLbl.text = self.paymentModeLbl.text?.localized
            self.paymentModeLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var totalPayAmtLbl: UILabel!{
        didSet {
            self.totalPayAmtLbl.text = self.totalPayAmtLbl.text?.localized
            self.totalPayAmtLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var shippingChrgLbl: UILabel!{
        didSet {
            self.shippingChrgLbl.text = self.shippingChrgLbl.text?.localized
            self.shippingChrgLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var prdtAmtLbl: UILabel!{
        didSet {
            self.prdtAmtLbl.text = self.prdtAmtLbl.text?.localized
            self.prdtAmtLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var paymntDetailLbl: UILabel!{
        didSet {
            self.paymntDetailLbl.text = self.paymntDetailLbl.text?.localized
            self.paymntDetailLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var addDetailLbl: UILabel!{
        didSet {
            self.addDetailLbl.text = self.addDetailLbl.text?.localized
            self.addDetailLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var paymentStatusLbl: UILabel!{
        didSet {
            self.paymentStatusLbl.text = self.paymentStatusLbl.text?.localized
            self.paymentStatusLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    func wrapData(responseModel: Dictionary<String, Any>) {
        //        if let item = items {
        //            if let imageUrl = item.picture?.imageURL {
        //                self.itemImageView.setImageUsingUrl(imageUrl)
        //            }
        //            self.nameLabel.text = item.productName ?? ""
        //            self.starLabel.text = "\(Float(item.productReviewOverview?.ratingSum ?? 0))"
        //            self.ratRevLabel.text = "Ratings \(item.productReviewOverview?.ratingSum ?? 0) and Reviews \(item.productReviewOverview?.totalReviews ?? 0)"
        //            self.priceAfterDisc.text = item.priceAfterDiscount ?? ""
        //            self.priceBefDisc.text = item.unitPrice ?? ""
        //            if let disc = item.discountPercentage, disc > 0 {
        //                self.perOffLabel.text = "\(disc)% off"
        //            } else {
        //                self.perOffLabel.isHidden = true
        //            }
        //            if let qty = item.quantity, qty > 1 {
        //                self.qtyLabel.text = "Quantity: \(qty)"
        //            } else {
        //                self.qtyLabel.isHidden = true
        //            }
        //        }
        
        if let imageDict = responseModel["Picture"] as? Dictionary<String, Any> {
            itemImageView.setImageUsingUrl(imageDict["ImageUrl"] as? String ?? "")
        }
        self.nameLabel.text = responseModel["ProductName"] as? String ?? ""
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: responseModel["OldPrice"] as? String ?? "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        if responseModel["OldPrice"] as? String == "0 MMK"{
            self.priceBefDisc.attributedText = nil
        }else{
            self.priceBefDisc.attributedText = attributeString
        }
        self.colorLable.text = (responseModel["AttributeInfo"] as? String)?.replacingOccurrences(of: "<br />", with: "\n") ?? ""
        qtyLabel.text = "Quantity \(responseModel["Quantity"] as? Int ?? 0)"
        if let disc = responseModel["DiscountPercentage"] as? Int, disc > 0 {
            //self.priceBefDisc.amountAttributedString()
            
            //             self.priceBefDisc.isHidden = false
            self.priceAfterDisc.text = responseModel["PriceAfterDiscount"] as? String ?? ""
            self.priceAfterDisc.amountAttributedString()
            self.perOffLabel.text = "\(disc)%"
        } else {
            //            self.priceBefDisc.isHidden = true
            self.priceAfterDisc.text = responseModel["UnitPrice"] as? String ?? ""
            self.priceAfterDisc.amountAttributedString()
            self.perOffLabel.isHidden = true
            
        }
        
        if let qty = responseModel["Quantity"] as? Int{ //, qty > 1 {
            self.qtyLabel.text = "Quantity: \(qty)"
        } else {
            self.qtyLabel.isHidden = true
        }
        
        if let productReviewDict = responseModel["ProductReviewOverview"] as? Dictionary<String, Any> {
            if productReviewDict["RatingSum"] as? Double == 0.0 {
                ratingView.isHidden = true
                ratingWidthConstraint.constant = 0
                ratingHeightConstraint.constant = 0
                self.ratRevLabel.text = "Reviews \(productReviewDict["TotalReviews"] as? Int ?? 0)"
            } else {
                ratingWidthConstraint.constant = 70
                ratingHeightConstraint.constant = 25
                ratingView.isHidden = false
                self.starLabel.text = "\(Double(productReviewDict["RatingSum"] as? Double ?? 0))"
                self.ratRevLabel.text = "\(productReviewDict["TotalReviews"] as? Int ?? 0) Reviews"
            }
        }
    }
    
    func wrapAddressData(responseModel: Dictionary<String, Any>?) {
        
        if let model = responseModel {
            var addressDic: Dictionary<String, Any>?
            // println_debug( model["PickUpInStore"])
            if let pickuplocation = model["PickUpInStore"] as? Bool {
                if pickuplocation {
                    addressDic =  model["PickupAddress"] as? Dictionary<String, Any>
                }else {
                    addressDic =  model["BillingAddress"] as? Dictionary<String, Any>
                }
            }
            
            if let address = addressDic {
                self.addressNameLabel.text = address["FirstName"] as? String ?? ""
                
                if var number = address["PhoneNumber"] as? String, number.hasPrefix("0095") {
                    number = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95 ")
                    self.addressPhoneLabel.text = number
                } else {
                    self.addressPhoneLabel.text = address["PhoneNumber"] as? String ?? ""
                }
                let houseNo = address["HouseNo"] as? String ?? ""
                let floorNo = address["FloorNo"] as? String ?? ""
                let roomNo = address["RoomNo"] as? String ?? ""
                let address1 = address["Address1"] as? String ?? ""
                let address2 = address["Address2"] as? String ?? ""
                let city = address["City"] as? String ?? ""
                let state = address["StateProvinceName"] as? String ?? ""
                let country = address["CountryName"] as? String ?? ""
                
                var fullAddres = ""
                if houseNo != "" {
                    fullAddres = houseNo + ", "
                }
                if floorNo != "" {
                    fullAddres += floorNo + ", "
                }
                if roomNo != "" {
                    fullAddres += roomNo + ", "
                }
                if address1 != "" {
                    fullAddres += address1 + ", "
                }
                if address2 != "" {
                    fullAddres += address2 + ", "
                }
                if city != "" {
                    fullAddres += city + ", "
                }
                if state != "" {
                    fullAddres += state + ", "
                }
                if country != "" {
                    fullAddres += country
                }
                
                self.addressLabel.text = fullAddres
            }
        }
    }
    
    func wraPaymentStatusCell(paymentStatus :String) {
        
        if paymentStatus == "Paid"{
            self.paymentStatusLbl.textColor = UIColor(red: 53.0/255.0, green: 165.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        }
        else{
            self.paymentStatusLbl.textColor = UIColor.red
        }
        self.paymentStatusLbl.text = "\("Payment Status".localized) : \(paymentStatus)"
    }
    
    
    
    func wrapNotecell(notes : [Dictionary<String, Any>],completeDict : Dictionary<String, Any>?) {
        
        blueLable1.isHidden = true
        blueLable2.isHidden = true
        blueLable3.isHidden = true
        blueLable4.isHidden = true
        blueLable5.isHidden = true
        println_debug(notes)
        
        approveStatusLabel.text = ""
        approveStatusDateLabel.text = ""
        shippingStatusLabel.text = ""
        shippingStatusDateLabel.text = ""
        deliverStatusLabel.text = ""
        deliverStatusDateLabel.text = ""
        
        if notes.count == 1 {
            
            //            let note = notes[0]
            //            blueLable1.isHidden = false
            //            approveStatusLabel.text = note["Note"] as? String ?? ""
            //            approveStatusDateLabel.text = note["CreatedOn"] as? String ?? ""
            
            
            
            let string =   (completeDict?["CreatedOn"]as? String ?? "") //"2017-01-27T18:36:36Z"
            let str = string.components(separatedBy: ".")
            let dateFormatter = DateFormatter()
            let tempLocale = dateFormatter.locale // save locale temporarily
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let date = dateFormatter.date(from: "\(str[0])Z")!
            dateFormatter.dateFormat = "EEEE, MMM d, yyyy, h:mm a"
            dateFormatter.locale = tempLocale // reset the locale
            let dateString = dateFormatter.string(from: date)
            print("EXACT_DATE : \(dateString)")
            
            
            
            blueLable1.isHidden = false
            blueLable2.isHidden = false
            blueLable3.isHidden = false
            let note = notes[0]
            approveStatusLabel.text = "Delivered"
            //            approveStatusDateLabel.text = "\(completeDict?["ExpectedDeliveryDate"]as? String ?? "") \(dateString)"
            approveStatusDateLabel.text = completeDict?["ExpectedDeliveryDate"]as? String ?? ""
            
            //            let note1 = notes[1]
            shippingStatusLabel.text = note["Note"] as? String ?? ""
            //            shippingStatusDateLabel.text =  "\(completeDict?["ExpectedDeliveryDate"]as? String ?? "") \(note["CreatedOn"] as? String ?? "")"
            shippingStatusDateLabel.text = dateString
            
            
        }else if notes.count == 2 {
            blueLable1.isHidden = false
            blueLable2.isHidden = false
            blueLable3.isHidden = false
            let note = notes[0]
            approveStatusLabel.text = note["Note"] as? String ?? ""
            approveStatusDateLabel.text = note["CreatedOn"] as? String ?? ""
            let note1 = notes[1]
            shippingStatusLabel.text = note1["Note"] as? String ?? ""
            shippingStatusDateLabel.text = note1["CreatedOn"] as? String ?? ""
        }else if notes.count == 3 {
            blueLable1.isHidden = false
            blueLable2.isHidden = false
            blueLable3.isHidden = false
            blueLable4.isHidden = false
            blueLable5.isHidden = false
            let note = notes[0]
            approveStatusLabel.text = note["Note"] as? String ?? ""
            approveStatusDateLabel.text = note["CreatedOn"] as? String ?? ""
            
            let note1 = notes[1]
            shippingStatusLabel.text = note1["Note"] as? String ?? ""
            shippingStatusDateLabel.text = note1["CreatedOn"] as? String ?? ""
            
            let note2 = notes[2]
            deliverStatusLabel.text = note2["Note"] as? String ?? ""
            deliverStatusDateLabel.text = note2["CreatedOn"] as? String ?? ""
        }
        
    }
    
    
    func wrapPaymentData(orders: Dictionary<String, Any>?) {
        if let order = orders {
            self.orderNumValueLable.text = order["CustomOrderNumber"].safelyWrappingString()
            self.RefNumValueLable.text = order["ReferenceNumber"].safelyWrappingString()
            self.TaxValueLable.text = order["Tax"].safelyWrappingString()
            self.TaxValueLable.amountAttributedString()
            self.productAmtLabel.text = order["OrderSubtotal"].safelyWrappingString()
            self.productAmtLabel.amountAttributedString()
            self.shippingChargesLabel.text = order["OrderShipping"].safelyWrappingString()
            self.shippingChargesLabel.amountAttributedString()
            self.totalPayableAmtLabel.text = order["OrderTotal"].safelyWrappingString()
            self.totalPayableAmtLabel.amountAttributedString()
            self.paymentModeLabel.text = order["PaymentMethod"].safelyWrappingString()
        }
    }
}

class PDFTableCell: UITableViewCell{
    // pdf invoice outlet
    
    @IBOutlet var PDFheaderLabel: UILabel!{
        didSet {
            self.PDFheaderLabel.text = self.PDFheaderLabel.text?.localized
            self.PDFheaderLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var PickUpAddheaderLabel: UILabel!{
        didSet {
            self.PickUpAddheaderLabel.text = self.PickUpAddheaderLabel.text?.localized
            self.PickUpAddheaderLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var headerLabel: UILabel!{
        didSet {
            self.headerLabel.text = self.headerLabel.text?.localized
            self.headerLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var invoiceLabel: UILabel!{
        didSet {
            self.invoiceLabel.text = self.invoiceLabel.text?.localized
            self.invoiceLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var orderDateLabel: UILabel!{
        didSet {
            self.orderDateLabel.text = self.orderDateLabel.text?.localized
            self.orderDateLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var InvoiceNumLabel: UILabel!{
        didSet {
            self.InvoiceNumLabel.text = self.InvoiceNumLabel.text?.localized
            self.InvoiceNumLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var SoldByLabel: UILabel!{
        didSet {
            self.SoldByLabel.text = self.SoldByLabel.text?.localized
            self.SoldByLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var PickupAddLabel: UILabel!{
        didSet {
            self.PickupAddLabel.text = self.PickupAddLabel.text?.localized
            self.PickupAddLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    
    @IBOutlet var orderNOLabel: UILabel!{
        didSet {
            self.orderNOLabel.text = self.orderNOLabel.text?.localized
            self.orderNOLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var ProductLabel: UILabel!{
        didSet {
            self.ProductLabel.text = self.ProductLabel.text?.localized
            self.ProductLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var QuantityLabel: UILabel!{
        didSet {
            self.QuantityLabel.text = self.QuantityLabel.text?.localized
            self.QuantityLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var GrossAmtLabel: UILabel!{
        didSet {
            self.GrossAmtLabel.text = self.GrossAmtLabel.text?.localized
            self.GrossAmtLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var DiscountLabel: UILabel!{
        didSet {
            self.DiscountLabel.text = self.DiscountLabel.text?.localized
            self.DiscountLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var TotalLabel: UILabel!{
        didSet {
            self.TotalLabel.text = self.TotalLabel.text?.localized
            self.TotalLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var orderNOValLabel: UILabel!{
        didSet {
            self.orderNOValLabel.text = self.orderNOValLabel.text?.localized
            self.orderNOValLabel.font = UIFont(name: appFont, size: 12.0)
        }
        
    }
    
    @IBOutlet var ProductValLabel: UILabel!{
        didSet {
            self.ProductValLabel.text = self.ProductValLabel.text?.localized
            self.ProductValLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var QuantityValLabel: UILabel!{
        didSet {
            self.QuantityValLabel.text = self.QuantityValLabel.text?.localized
            self.QuantityValLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var GrossAmtValLabel: UILabel!{
        didSet {
            self.GrossAmtValLabel.text = self.GrossAmtValLabel.text?.localized
            self.GrossAmtValLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var DiscounValtLabel: UILabel!{
        didSet {
            self.DiscounValtLabel.text = self.DiscounValtLabel.text?.localized
            self.DiscounValtLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var TotalValLabel: UILabel!{
        didSet {
            self.TotalValLabel.text = self.TotalValLabel.text?.localized
            self.TotalValLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var TotalQuantityLabel: UILabel!{
        didSet {
            self.TotalQuantityLabel.text = self.TotalQuantityLabel.text?.localized
            self.TotalQuantityLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var TaxLabel: UILabel!{
        didSet {
            self.TaxLabel.text = self.TaxLabel.text?.localized
            self.TaxLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var ShippingLabel: UILabel!{
        didSet {
            self.ShippingLabel.text = self.ShippingLabel.text?.localized
            self.ShippingLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var TotalPriceLabel: UILabel!{
        didSet {
            self.TotalPriceLabel.text = self.TotalPriceLabel.text?.localized
            self.TotalPriceLabel.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    @IBOutlet var Add1Label: UILabel!{
        didSet {
            self.Add1Label.text = self.Add1Label.text?.localized
            self.Add1Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add2Label: UILabel!{
        didSet {
            self.Add2Label.text = self.Add2Label.text?.localized
            self.Add2Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add3Label: UILabel!{
        didSet {
            self.Add3Label.text = self.Add3Label.text?.localized
            self.Add3Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add4Label: UILabel!{
        didSet {
            self.Add4Label.text = self.Add4Label.text?.localized
            self.Add4Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add5Label: UILabel!{
        didSet {
            self.Add5Label.text = self.Add5Label.text?.localized
            self.Add5Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add6Label: UILabel!{
        didSet {
            self.Add6Label.text = self.Add6Label.text?.localized
            self.Add6Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add7Label: UILabel!{
        didSet {
            self.Add7Label.text = self.Add7Label.text?.localized
            self.Add7Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet var Add8Label: UILabel!{
        didSet {
            self.Add8Label.text = self.Add8Label.text?.localized
            self.Add8Label.font = UIFont(name: appFont, size: 12.0)
        }
    }
    
    
    func PDFInvoice(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
            self.InvoiceNumLabel.text = "Order NO: \(order["CustomOrderNumber"].safelyWrappingString())" + "\n Invoice NO:\( order["ReferenceNumber"].safelyWrappingString())"
            self.orderDateLabel.text = "Order Date :\(order["ExpectedDeliveryDate"].safelyWrappingString())"
        }
    }
    
    
    func PDFAddress(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
            
            if let dict = order["PickupAddress"] as? Dictionary<String, Any>{
                var pickupadd = ""
                if let house = dict["HouseNo"] as? String{
                    pickupadd = house
                }
                if let floor = dict["FloorNo"] as? String{
                    pickupadd += ",\(floor)"
                }
                if let room = dict["RoomNo"] as? String{
                    pickupadd += ",\(room)"
                }
                if let add1 = dict["Address1"] as? String{
                    pickupadd += ",\(add1)"
                }
                if let add2 = dict["Address2"] as? String{
                    pickupadd += ",\(add2)"
                }
                if let city = dict["City"] as? String{
                    pickupadd += ",\(city)"
                }
                if let state = dict["StateProvinceName"] as? String{
                    pickupadd += ",\(state)"
                }
                if let country = dict["CountryName"] as? String{
                    pickupadd += ",\(country)"
                }
                
                //     self.Add5Label.text =  "\(dict["HouseNo"] as? String ?? ""),\(dict["FloorNo"] as? String ?? ""),\(dict["RoomNo"] as? String ?? ""),\(dict["Address1"] as? String ?? ""),\(dict["Address2"] as? String ?? ""),\(dict["City"] as? String ?? ""),\(dict["StateProvinceName"] as? String ?? ""),\(dict["CountryName"] as? String ?? "")"
                
                self.Add5Label.text = pickupadd
                
            }
            if let dictObj = order["ShippingAddress"] as? Dictionary<String,Any>{
                var shippadd = ""
                if let house = dictObj["HouseNo"] as? String{
                    shippadd = house
                }
                if let floor = dictObj["FloorNo"] as? String{
                    shippadd += ",\(floor)"
                }
                if let room = dictObj["RoomNo"] as? String{
                    shippadd += ",\(room)"
                }
                if let add1 = dictObj["Address1"] as? String{
                    shippadd += ",\(add1)"
                }
                if let add2 = dictObj["Address2"] as? String{
                    shippadd += ",\(add2)"
                }
                if let city = dictObj["City"] as? String{
                    shippadd += ",\(city)"
                }
                if let state = dictObj["StateProvinceName"] as? String{
                    shippadd += ",\(state)"
                }
                if let country = dictObj["CountryName"] as? String{
                    shippadd += ",\(country)"
                }
                
                
                //    self.Add1Label.text =  "\(dictObj["HouseNo"] as? String ?? ""),\(dictObj["FloorNo"] as? String ?? ""),\(dictObj["RoomNo"] as? String ?? ""),\(dictObj["Address1"] as? String ?? ""),\(dictObj["Address2"] as? String ?? ""),\(dictObj["City"] as? String ?? ""),\(dictObj["StateProvinceName"] as? String ?? ""),\(dictObj["CountryName"] as? String ?? "")"
                
                self.Add1Label.text = shippadd
            }
        }
    }
    
    func PDFOrderDetail(responseModel: Dictionary<String,Any>?, completeDict: Dictionary<String,Any>?){
        self.orderNOValLabel.text = "\(completeDict!["CustomOrderNumber"].safelyWrappingString())"
        if let order = responseModel {
            
            var qty: String?
            var grossamount: String?
            //            if let obj = order["Items"] as? [Dictionary<String,Any>]{
            //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
            self.QuantityValLabel.text = order["Quantity"].safelyWrappingString()
            
            self.GrossAmtValLabel.text = order["UnitPrice"].safelyWrappingString()
            qty = order["Quantity"].safelyWrappingString()
            grossamount = self.GrossAmtValLabel.text?.replacingOccurrences(of: " MMK", with: "")
            self.GrossAmtValLabel.amountAttributedString()
            self.DiscounValtLabel.text = "\(order["DiscountAmount"] ?? "0")"
            self.DiscounValtLabel.amountAttributedString()
            self.ProductValLabel.text = order["ProductName"].safelyWrappingString()
            //            }
            let discountamount = ((order["DiscountAmount"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)
            let qtyNum = qty?.numberValue?.intValue
            let grossamt = grossamount?.numberValue?.intValue
            let multi = (qtyNum! * grossamt!) - discountamount
            self.TotalValLabel.text =  "\(String(multi)) MMK"
            self.TotalValLabel.amountAttributedString()
        }
    }
    
    
    func PDFwrapTotalData(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
            self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
            self.TaxLabel.amountAttributedString()
            self.ShippingLabel.text = "Shipping Charges : \(order["OrderShipping"].safelyWrappingString())"
            self.ShippingLabel.amountAttributedString()
            
            if let obj = order["Items"] as? [Dictionary<String,Any>]{
                var totalQuantity = 0
                var discountAmt = 0
                for k in obj{
                    totalQuantity += Int(k["Quantity"].safelyWrappingString())!
                    discountAmt += ((k["DiscountAmount"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)
                }
                //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
                self.TotalQuantityLabel.text = "Total QUANTITY : \((totalQuantity))"
                self.TotalPriceLabel.text = "TOTAL PRICE : \((((order["OrderSubtotal"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["OrderShipping"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["Tax"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue))-discountAmt) MMK"
                self.TotalPriceLabel.amountAttributedString()
            }
        }
    }
    
    
    /*
     func loadPDFView(responseModel: Dictionary<String, Any>?){
     
     if let order = responseModel {
     self.InvoiceNumLabel.text = "Order NO: \(order["CustomOrderNumber"].safelyWrappingString())" + "\n Invoice NO:\( order["ReferenceNumber"].safelyWrappingString())"
     self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
     self.TaxLabel.amountAttributedString()
     self.ShippingLabel.text = "Shipping Charges : \(order["OrderShipping"].safelyWrappingString())"
     self.ShippingLabel.amountAttributedString()
     self.TotalPriceLabel.text = "TOTAL PRICE : \(order["OrderTotal"].safelyWrappingString())"
     self.TotalPriceLabel.amountAttributedString()
     
     self.orderNOValLabel.text = "\(order["CustomOrderNumber"].safelyWrappingString())"
     self.orderDateLabel.text = "Order Date :\(order["ExpectedDeliveryDate"].safelyWrappingString())"
     
     
     var qty: String?
     var grossamount: String?
     if let obj = order["Items"] as? [Dictionary<String,Any>]{
     self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
     self.QuantityValLabel.text = obj[0]["Quantity"].safelyWrappingString()
     self.GrossAmtValLabel.text = obj[0]["UnitPrice"].safelyWrappingString()
     qty = obj[0]["Quantity"].safelyWrappingString()
     grossamount = self.GrossAmtValLabel.text?.replacingOccurrences(of: " MMK", with: "")
     self.DiscounValtLabel.text = "\(obj[0]["DiscountAmount"] ?? "0")"
     self.ProductValLabel.text = obj[0]["ProductName"].safelyWrappingString()
     }
     
     
     let qtyNum = qty?.numberValue?.intValue
     let grossamt = grossamount?.numberValue?.intValue
     let multi = qtyNum! * grossamt!
     
     
     self.TotalValLabel.text =  "\(String(multi))MMK"
     self.TotalValLabel.amountAttributedString()
     
     if let dict = order["PickupAddress"] as? Dictionary<String, Any>{
     self.Add5Label.text =  "\(dict["HouseNo"] as? String ?? ""),\(dict["FloorNo"] as? String ?? ""),\(dict["RoomNo"] as? String ?? ""),\(dict["Address1"] as? String ?? ""),\(dict["Address2"] as? String ?? ""),\(dict["City"] as? String ?? ""),\(dict["StateProvinceName"] as? String ?? ""),\(dict["CountryName"] as? String ?? "")"
     }
     if let dictObj = order["ShippingAddress"] as? Dictionary<String,Any>{
     self.Add1Label.text =  "\(dictObj["HouseNo"] as? String ?? ""),\(dictObj["FloorNo"] as? String ?? ""),\(dictObj["RoomNo"] as? String ?? ""),\(dictObj["Address1"] as? String ?? ""),\(dictObj["Address2"] as? String ?? ""),\(dictObj["City"] as? String ?? ""),\(dictObj["StateProvinceName"] as? String ?? ""),\(dictObj["CountryName"] as? String ?? "")"
     }
     }
     }
     */
    
}
