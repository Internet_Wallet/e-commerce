//
//  OrderConfirmation.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import OKPaymentFramework
import my2c2pSDK
import CoreTelephony

//private var PRIVATE_KEY = "MIIVJwYJKoZIhvcNAQcDoIIVGDCCFRQCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAJhMsuHEqj080kwzEWNXdRG3kxPSnt+25PwJP7kAJro7HgM+6iXVjMneR7/+4qtGYeJfodtuNJuyFvwlVlqrDe5FOkMa7O376dh2aC7l3Uoe5X8Q0MiDBwzdaHMmVSx1tvfb4NItEfQFBAj7QDVXAuENPGtvk+RQ6YV2HRmo0yKaRX7gVIBwE4Vw2AZ19rHDpHH9PQ9gtebKKjYxGpU4Avixqq/V6S7Wa4cn09c+MI2xcu3dEWHovOnOww0G8NwkxcXlIgJzlFltKtiSDwxDFs1IA3vUO5n0yJEhkpDieXLLRqIuUACObWTYwFeVnjxGjtSIKpMPSBFwjJVjOtNvNLwwghNdBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECHNMeHvILroFgIITOPhB9Vdsw6htcXCevKvyiI09I/VGCqobbmFPhY1PfZ9x8P5rrcbTTViOaYnXQVaZnzsI3lkynYdS51PezOeKWuqihOQCdmEWSYaxqUHGMNbdsn1p9i8W2eUR370ii3UIb6I4c55XqtaldeEL3te7L8PzUkvJaNipObu2yze6hqC/D1mY2RybpT5xYDo61iCEvpP2bbiTdN4t8LJumN0hQt0E+f89TApo33ECTdeszAFZlxsCk71pyk+HgGp0Ly4oeHR/g0IbtGxyFKeAjvEyMaScXO4s3fJ514IAxzIwLB3WuFznJwsjUds350wek2ZSz6G6AC6vpR0vyTMW37Q5mETq3bC94W2amuDLsvCALTLpc82+9jS3ufLZkanNBjE7clpBM+wScWXkkbbAGemntMm/d0u9UM9pGdeRbrXc0wTBfqHuGmz5qxKj/HI1t4u3gqGWsZjHqNiwh1UhsxYm/wQFjUSpTRghw1mawOdTcasTHcA8C/DfVWyrC+KBh8aUCAoQr72k9PNP/cYn/oV31qlkwXXM1WCT8kDoqcOYZ1hGQ/pGSa/3n63sr7CNAkY5raPoL6MLOFGh/U7ryLjuCK9ZikShZ3PskD4MM5S2J3B/LJEUm3J1uuCPiLU+5nvTJVwI2meCMiAZc5kp7HlbmI/ykizM9QHrIngdlYQPhojDn2g7024o/D0+S5jczo44bCOKRAYh2B9rTWkLN+01HRA+zSEQRb27Xn5by088BL46VDFho3rD7Pm90vCuVCdtjtFWTBKTC+/+/11Vii/vHYLf3mx7QOJo7lFo/lYIXnfz66kkue6Aqfn4YKeM8NfKT8ayelC5GZo70BNZUdnEKLwFZBsoHKupWwQD9gwOVXCmytyzSAZtBgbtNokw4huYw6DajEZGA7M6tKYPyih9NusAP7+p3SVozn/8pBAEKr9L1itSUnMeJPx07A9zKrrk9eoqEbEatcvnkBC4HOyWeJ7RJtW1YkBqLxiUKoAyCnjYAk4CWcM2Jjuf+B+UIkUttf53hCEmpzflLxK8MPwBxDSvTPKzmt6sk8zG+GHgRCDCiHUFNY0O1YkDVrR7coyf8HfaTTy49x7bxr226kn4hAOFn5POR1bfCeUmwg00rJpc1waPaS0FPZ3R7Gkbsndp/ovo6qtcZ6YCoThv0DfLg3hsUOSeavow76XjznaAIkD27PwDtnXimtVrGfQgXPbW+haAdnAjBDT16eaYmVCWhadCnmz6RcmvznKStbyy4udNOyx3hI6goyy3QMjIVzvhh+EXckCl4SxDU5RhpxDhhPgHoCQyRePjYvmAyn1mP8ZB7BnGi0bJpL+mCAcZF+rDg2mTWdos4EP00taeioXTAGWgQ9t615emJaqzoj5gMvbUrHVUNRRJzBKtplf5Coa5i2NB5l0oSmAcJDAUMHWcyiiwX7QKe5OexFSCpES1Y4WuPKOAi08Nx9rjx8eErP37x2qSIQqP19oV6ogxfofzxyNofL1JuPaqIArw9poagx3qyWkPmK5/XeQ9GAQoCq2YLKesP3VT/hZ0k/NupgqZhmOAhfyaY/aXZZDfl/M2kyDUN2ihRfDEP/MC8QNPQdbu9d+2ZZG1txhbTB6+Au5TjaGWsMLWzxiXiq6N+q781K92FIQjbjlK77t6/bN14uBKMXWPkPA5bcpYQh+tDz9vHlHCLw3Qx9ppH6qpkAcUSbkSfyd6iA70j0he7iw4IO4a/RfN7yfgrqSr9uBWdqUIMq1fYWbEsmOzaUadS2POP5aQeo53X2vHtUszFig4C+1iiMmzNMUuesKVa1stpRwuAwSrBlzxT3nrZhLzNzpMlfeYys+AcQbvSsZ3sq/TaQy0KLGPQ9sb7DnYc4TgKeqx2zeBxi+xQ0s5qtRVc6Ps4MGQKJJZ0yG2j+cH3qOE4bKFrMLznSWQ9CXdz74NRHN2vae7+Bc8HOh/hSad9QoRJVfh4sPikSpALYW61FjbrXaQrQ6tkTZ8VQthRZ9/+UcX5VeX75MuR0shRRTGRbEALGuC9N1ZxzeT/nnyoCXSHj+4a8zwb8E1mQwppKKirnulWn3GYWQhUQlz/kwm5MNKASPoWooVilMAZrusVD1p9KKTO23xPeuoESImKVNuVt4v05xzthlGdVP+K/uft00AJTaIAw5usJNZjLJWy1N0s4lkiamtwUCYY6tO7gcQJwPVkuU3bLHwzaj1k1raiOGuERyXpCKzMgxuoPivS9r88hbqlPYDHioeRg9OMLjrthHtfGzD2zC9vcMmNTTYyHBcQRbOLAh/Rjh52fWIahY15cx95lrTCBu7V3t/pPSURF1cU13Nl9bB/cLRYxFJ8YvKMXo1yM6uQCdgL93DsVaOIoXwp8UkK67SDJnqBY4d5MEwdjdzxsO9kc9TRMCWqrTtEUYLS7+nYpsRRtWEbWNKr5SqYGgmM/+K4wtvMB7n0rJ8oDqUKZnfR7zqhTuO2A4CAtk/me2PHFWDkO6dIgLFKX6Okh3jBZqRpcMnsChVvNMn7D9f8SkDxUf8IJsgm8Hdd18Eq+eFYo2pGPVouZ/pkcDkURxbIXekjf9SvTaM613ZUGPgngHoCoHgQJ4oNcfKEemNQ9/8rOuJn23x5uMgQ3IG5TYifsvKREuOrSH6PA7gdSPDksnIxKgJGM4K3BGUixfNuBi8httJ5ov77ejj1MG0vDA//2F8Dq5BmaDVGxi2lcpnhTNN8cm8XIQmRfjaBRHI5bVSFswrtU2ijS0Zqg3S4KPlWl7jccoUCzpUBT13qGqJwvbhV4e5mzsy2hKgNiqo4DhiQo9DCmKpLjhoP0cfMp92iyFe8iCHKLtY1HAzxivybI9l8yzYslz2mkcEy5Klukkf+Rrkchjd5vtLuVw9phWPTDLT3enCXmgWDx9D49yaURm93YJKki10Ib7kCYPHyE/rkS3XaawoF4srK9fiCRpbhUgQJUYlpahoTOsGkN10Lo8MtOT2S1EpPxSuUih+Dg/a5LbbDU8iQH53tfYn2H7vOMWyFYPrUz7mBO7LnFQdU9nsx5uDK/zolUD1yvAAkegEZXK2CaoTiAMZRjhIbbnbru8E++jTFXpb0I1z/7eNlpRUqoRTYAMMu4UID15Ggtx8eKxZ/EdDUzZ7bHwbVtNgV4ALdKaJpc0JFtq0R/iVEXYEl0T2oDkuB5T6AQWMcU5N+rdb4AnIps5WXsfFVkuFGRg0DNNJoav3hZ7k7lbkgTQP4b5lOMdIUNYypcwzC9y/vKR+iq5ilRAwrSIP99tU1fZDg1pkN9/ZAlv24X628BtW9KOtgys4akxQ1uh34j9WbA43CZuNw5E9KEg14JRF5Mho4bGjM6enVtRwwnIBP57uLv1wMzxY0eiIqDy9PSDhpk06x16opWfUKymkLF4xKdOordDN3i+g0Jcmdcw7EtGXP23aRMjnSsETPidBYx6NVKRW/ZxfK4ZoLPH8FGrUsU0CKpzBuksT67GaB/qPyP6+rOoENQC4Q16zuueqFkALfoPGHIK0ewWMc/6wdbIIHsjXX5xNBLMKGEUkrkLeYtDU83PsX6kHW07GPMANFtd9Bq4AR4OJZWkzewdNvHUxq1vzuRtdltuo/DB4FX9F25tUg9mTahxifBqi8zj6Jl570CH21qaKAN1z5YTt6T7EPpqNOOvIcEDl0DTpRE2Pa/WdU0PoDAxqbqVSaLfyzAvrbM4EhtVtzPc3Rhwbl4aw37iWUKimCEgOnwo4sw41WtlkllbJo9tYZ6f9bGGwCdRDsc6+ka7rE+dQw+RCfNz/YpUmqjfhxjwwsb7OXYbEm+7T66akaBuX7wMTvJUyy7LwvMcmTycUSKcSg3RsgiqSkiv4JLIMlY2m6MmiOQuxu52VSSYs7ezu9VQx5mDZ51FIEAkdxmfDW63BOCKcd0QVxu3WEm77FjF+4e8EooGxBgNjkb5aKTcIt6dDKOwyJIWhTZTMkXUGLCkZ9i8zoB46TcN/HAJqrNpfyjpAliLXTjjQxShvivkgKpnqafF51zAQTe1jWPZxuKN54LRphHqKkyfwYyXXrzoNwRYmCMM7lSaD93KhtiINWvHYfbehUcJK9OYiHJdInRAgbBQle0oQKPv16L3zD1D0zXGBlJEseTu4J9xkq1+Q+h1/CFnuUSzOrkSdOwKXkV0IYCOC7ko0dUPJ5b07hq1IUuuy89aUgs5JI2IoTGPpaobPFrsgc1TAgBisMiXZgvpu0m7uO8KySHym0modg+0uKuFvHY3ks4a9DN3PwXMzkIzH+ruFrIigxpOSHgcra23khsAbqPueLQa2EyxB8SJ8rzmvuEjhAzB4+LcvqOpOnqcrb7hyQeuHoZ9tv+lqG4AhpC9g3KQ6GMtr+sgorTN7yCh0ngPccJU9lhgXc025+FGpU7IdSa/qLOhad9hGfOKJzWmzPlIBwewQccUBrzZu9EVdgqvUTIGGQXktYYEk99SvdVSkdxPpOg2PShxbTKQz9XYeYm55q4B8k1c+2sOgCpFiKY29yMj3QvXuei6buNwYPl27+MNYROZjPkSdEj5K/UsAc4vsXvIaXl/ru12lKKG/yCjgYVWUncwX+xTsP5k08oQ9HOiFfR3tigh1SmXJXwng6W5K1uQ7+1ZzR8bTYntlHgWpeHUnpbd9qz0N2nMafgbobKGds9lkCxCN/Be5w9n3TMjhq2kpI6l1+oDYFd9xDP97TOX5CLF39eM+BtplFyzaPQVm2duy1LR6E4CA2WwzeKbfJapShQSISL8UqAy4DNjhOUKI/U3D71CfcUT6IFVFfynYm6uQ787/ClGtnk+zqgD5Y8BSMo2k34GvbWF1qFP1DHhGeZ7NsIwbrVKQMkj66znWvwncUxKDCkyADKO21kTSKB2rBcKBSo3ebu4cY0HegKo3DhDHHXAq9lKZ5cT4vZjq8NqfTVthIE+uEJqnntqyuhEsxr8VaQZhbkcqQHbR1qa8M25y8/qkdi3gIJYBAoEonqDUgTxYamWne9tHokxwzo+Neuy0BBX7Bya7oZ1GlMYxRU9VSAGIA2RDuXVV+3+2rsr0Ug5fdOSAxdH7jauH9vpEyGRRE0pjlpqFbUvwiK8fmTvCDJBOAyzDPGu3BrDoYjVCvOB2aZNXt+x2YWO4lhHtMvhDN0hYWH8qIaugkR0SgZ+nEuozvEs4/G/IGsyMEZLdmk41hPWXYNzNyviL5Ao79CprzsVp0vYm0CtDt4FLHevhOXmcayD+VeaAyiT7tSaycJMHQCfXdjf12xC4UgO2xzgIlvvt/vjTE2TSp4BIxGvdqsGoFzfrNdE+uLDmExyIDBnFn5arK6iAzk3GrSX2AOGHzpOiUik31W8IB8gMnJ8X572w9B4BWG94+tfgiGhcH8ginovftjgSdYfVzS7nUvnGrWqLegpqGGk0a6fIxAN4E2WKWFgXdFrMk4bWMIlOjE5ZEXWAJYeeLUxkA6eDJXj1RKYLU4qDFvo/15PYREvCVtH473KpBIMRYqwapqCGSY9YP5sZQ5UkT7zo97wxc8Qzhuv1VIut5UTBeBlpL6ggULmWEXPjTWRj6ew0AE/wMOxBGjIieLPt3HmjeKI3sP+kDn6PREC1EP35NGHftGdKtSfEyizwzbXXsCOht0iR9NbEkN69VJ85FANW83zySFOlblVIRS/qIu/gduZDYS8H54XF7v6F/RqMXouqApLBPmafbQ15XMd9xkcXgZkzK7k1jAzvJE03RTPq/EIDLlcWJsSZeYn7olPssIl0DdwWJ/28QjG+cPfLCIO6jMWOyrCsIn70ukW0bDAGQhht3gVg9A3Ri8wugT+8rhQprPgxW3c4Sd0gaAKyj/150rBDcE/f4MuAlvp27phVYp0DR1ZiBJ3oKIamQADAM+d7wMCkB9vynw5wBe7cam2cagMRFUYwg5SbZjxqOgqaiMSlVQZ/Jgly3+HcC+qbnlxd+OMvmIYLxQvPp3PqVW/UfmstOigIdNSbWHETA9EoHdGDKQO172hx1V2+bq6XxDpYzcT2QGYdMwyP474hiYPpRozqUYjIFR7i8sBd2Yc/43X3YtsnQIZW+b8KmPHxXqFdWAmeUNwMOM7vbNwqP5h5yYlyKs8s7bHesdEgwV1nROLZV7rqkZvwwQlDbnKdMrfUGvb/NVjISDQgOzXXxC/d4UvrTF5TfAyhohVspvFs2RfzDnu0TBfPRpSJcjh/hJnayfroxRni476cPoYVLBKB9JFrifzctfLL2c/iWsHtovtt+0BA9plSTj1HBoBsUE3BrHvNczeyo7ezogMY4az65jc+45xQ+2T3ssAvXbmG3fklsFkCLUkv1Q2Jv2OT8DFEH7T46CIhkfItPvgx8DLVFsrlBS6dQ9wK9uW4pJSjtw1f74K6jUytorUO7TiKjwpNYda3Zd8hEeWIyJQ4ueV38FV5drVL7nEDa7TiQEj7uYySK3wMY+H/R36++zeK+VFp1bTeKGGFpxV84rT9P2XwIxvgYkb1h9EhwvitD9upm0hcRzZZiaPMgXMM2BIasXgMH10ApfXRv+Z7qaRMJjWnDQN+CkSdxnY7cmndMNgUiG3bUDbffg=="


//private var AkashStagingPRIVATE_KEY = "MIINHwYJKoZIhvcNAQcDoIINEDCCDQwCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAK5oNQEQSadi/YEQYtC3R1qsl7TUvXb5q02Pp3OqZXJkBzlFoq7jCN+1Cw5zia0PEdNW2wSy+Q+e4NHDpWk8ZIoC/bDzd68XdK8Cm/1ajmZxCfs0i0t0dcRXG0s1ksACaS89VuUjcdTHYMwnkEr8cRIlvCGINoevCFoUhSDcTgFc5Nn0hevDWvJuqsVRS6KiQJw/QTSRoiIx/rxzpXTINOagYbMd51Yo6+o7BwZorTHvFNU4QvecQcKJLokS1AhxMr2C0m1kQIH5Syp27u20X5z5sRQGqLPfFLhOXf35MBTmFjFt/bgPYnD5zHntUyGVTkksGbTdGRgD2muN82w/2pkwggtVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECHLlH8wI8ZiVgIILMFPXWK+POI98ZF8xoRvpKPmhc14juAoCxARCh1k0sW/brtusMygZ9MZccl3yVla7ha3wkOYSXPsh/X1BGpF+FBFE8Qhx84GLjwojlMuhCGPN4etCvBGl1P7j/RZchTxdUcX7LSpbs3dtlrqor1PK/byLrx/fmnNweTQzB8ebJ6rTg9krn+nVCGR4+Nma0j8EwmDfQe2+YSUXle51kDin5lcKcpKGgKhBxc1DfRc6kbN7C/VOv2ndfORF+HHDwKVJY+TSrkccQuC/Bk1EEYuwnHEQd5NTtEyQcDWrxSAEZ/YgyRgwc44yWFHtspFXbRXv3d6agtg6lesXztuiWDP6rcTSuZkC72crEH9okls7drjATxq7zQWl9JDsJR8OhBhMdvW63TfHK2SkUx9uwWnsL6VVJ4v/XHGxyg5JWbeg0EFoPHJK3d46IwTRuIG+1GTpSyS6Y7p0A0FYJ+66sShRSrOEerDfG8Rvjd3jXSyauw4qHqwHdDuLDfOQG2jRtk4vkvo4Vu0V/giUNnhZzqyk2WQGntAzEjH4ib1KuHyQdfsiQX8NEBK048m+yv6TmHoMtwzRkuXjpiUHzFkt5FHGn7CR2rT5t8xrhgy34ijXIjE6nlD7lV0TtblQpK3a4buNsEGF5JJsRhNvd8G3TDLkyQj1M/opLwmPf9u9p4CKixNmqa3IFVfNk41CuSPStmepg0nFRsaC6NkDYNfsIK0vLb8PiEhkgi9+DsjH+RByc80TXKXDTjEKGlTUc4pYhadVROfpV3hr9ZD+yVcMb2SE19LFELUgKLsWPcgywJvuNMK0fCju1ZgiLnJ5oADwizBhHCGQWYaAlsyCTWfKdC9Iy11ATrHwWlO6Q8OXNhOY/c3tUUtr0Leww1FIl1mIExRp5vlqizOVJC9vjqEPcPdzi80I8yZ4AtyZ0wtvHL7gUXn9lHYaeTT+TOPHUVf6DFaugiaQOzB+PGYhZFdIJzwCmn+tkawKLSSZrc9GvARK95wHLHVTS0jE0bHJho9uyt5QeUJtB8bnjtTazSO68nOFw6bcM8GYqO17D+2t+b2hk/d/n1aECF4hQ2KJOHS4KtcAVBrQYm623T1l7eBEhN3eFnm0s3RyKFbAgnAaLrbq/zqPDbZ4HWvZaBC1Iy2Qlkc3eOV40YFrZfQ+1D0JuvEXae3i/yIWq73pxmz0uYYhXGVn+hWh4r2Cx2RKd1I8Js1SFPm8gz6+PaYLMUed1RgSzsycB7MCqWDYeM04T7DDKb7KYwOQWmHQfOiLcq0p9G4itWuh73j0jmIBf0SNH4MNDhnR0bY2yQ9jbnNOFVr9RhmLoubtUbAebcKLVjTtmCcDm78Os4pa74x0TU+phHQY9eA4O+sNhc13gBer28CNh9VOgbKSAIAGusrShshqyfI22fwiPiJnQXhh2OlbG2BZNIxS0jPFwvyxOqwuq+0xln/lOBzm3Tg8WrrxfwHEHoEWzEjp+ppGaRg16RGb2qoEg96luGxv0rIyAx8lrphzPvt9a9G7V755a9Tnwm0VoaPud+x5j2VbbGEaWAULcU8u6q3Msiui8R2BN910PUIJunGrWHcNqodUXIVp1hlK4bi4tAnXc2fabV+zuyBBRktBaLH7f6J1Ico4ZOt9YadgewIvvynPGGzGVrWG3FPNbazoVbXncKiWFEKgScQJITsKy2Wb6sMvINaZUV28vNXb0S4MYkXC1RhuyAsWslslzkTu1po2Y0QdgG06OEVmBALVcNZ0A5k01UU3cwWgPNL6RsDlQvoS0bKBZoFbIQGWsbc3W95eQX3PSVA4Lb0VOyCSpcsLLGSImm59l920A4JxJj4Bd8zu7f8B0p034oK0xsmE9bWzrmu8VDhPqmXyM9ojf0iGkTKvAkjtdz+ePKjmU3ufrW8wdXRl5cjgRXEU0hXpHn+a73hClXpyusahgRE9TyGy6u3ho9s+zELEtIRhWktFKBCbMP62KKiXgFaTRPc3YhCY1poS2xJU3jFcpfw4YzNiX5S2/Bp39jn589jaVfiDA/s4b9Ss8vBAPvSSFO+oCVfsoUzQIRkbaxFnTH7sGtP6ASDg7nWHF67NHtOlyalCrZEh8DYAwoLrpoDZg4W5QzS/ET2/eHaNzfclknlzA9rWjKAGUt9pCi37eWquPb3RCoaUU5+178Z1S3hIYEE2kx+sXFD0+xgZ4Rz5mdagsEgxNuTQXnThn/jPAMXz8zwi0CYgl0IgVoOvUxsvuHiQbZ0zNz5w2DRonBSTO9o1SRSZrxyv5GfXsfdx3CsyKNpheBVFgRPRPpX459QZBZ3/M642BLJRb+oqUqnUNDSN+DxjDtcVdF83RMJqTR5ej7qi/oQb7uMqlDC3fwE3mPJ7BHjkdZkd4tKvj5UFwjlVCuRKtFlu+G1k/faoAtu9qXabdh95Vgv/nFaUBACdy+ZaHMDAJESxdhsksYfzYCEvnBkmX4hH1htd/4k4uG7mdfNxnZ0u/wV6FhvPp+996g9hRMPAhfQvg7Q61eC5FbgPwYzN/TYmzBQA1LqiL63opEYvFX0D/FalS+kpPzVnttT7R1Xg4czAi+bXDH5//yrgK4IJO0KZIHK9RiOYHPyWU0Ag3JTsCQHgdxhrSX1d6S/7/TVuCWKscPN/xiCjb4p5hOyXuaOGMTFARfuRB3x/5/sgfzfxEVQwfb0DDBVsOO9rh6GzIyt5hIh8EFoAzjxeJjylCgyLx0h71ycYiH2jm5IPAQ41+jMC/YTLY0aKdSxDXePR+2f1ATGS9dng+UN0qvLaLb8eIxWDWh83peOgAZCKcmRnBt+WPO3jBLiuxvV7RIjSg0dgyqBceGaVO3WMJZ0lXsMjrknEkBx4NNFRAxtmlQRVXTx8at7nSwhHZf0yHsPRjqHYyxIOx0lRf8JG/YlMNVTarR50XrWXf5Y6EAsY2gm2Q1dgblOAzIRtJz3ZX1ijfKktQKXt9j3K2K+7CQdqHqbFKWl40rAq4EvX/X35wq6rm4AX8sO9qjAVHjtpY50SmMpXLGWoxRreNsh3Xl/Nb2yiN/luiljA/FFZO11++MB9mtS+RVxl0zI9MH99dDhrLmC46azc5aSFXuR6b2twjntsNr04ys+KFi6iJU4vMTLWAOtLQMqtWNRbucNJsoMX+gQqqnqHrn7YaO5rE7GJIz09DxWjAcE2DHqJDFwmB42rmB5d8AH8DQXD7clcZ4hk2f+wF5G7JrwXRHalFsHpb8mgr6lJBJhPkJ5XS4OVR/QAg4nx0qXGadPeaYKx6VlCaHpHiQuy4+lt7gZSgeplK37Os6tPlvdkSmkoXN2aX4XnSlSs/fGfxV/P8jCUVfJVpHJDC/ORcg90AfiuH6a5K2/zpJscadexZADa2rNeYnJ3OGZ0HAJOHLAjt3vTPpHa9DqWwPn8W7jJNkwIiGaritWnrKQc0KIdlfaybAAkghr+mOKiZYH9NLTP0ik1wVtefhdcjR0nY5dn+vivKBMUhW7F2LrG8xIMKE5O0FELGyhGA7AjCxOm3oOq4cPkPax/5nldje/LlT6cM/xdQ5Af0rBASvdZrmWHCV5bIv4c1EeWD1R6aBY7XPJ6FvsJWBJ1rPGPtjHvU/g8+c/s+x86TvAcOstp7p+wLTWGtqE+wo4UHl8JvrzKM7cxSjIszqvM9rTR8IsMnfqk4MQHczytTzgVebrxVZJs6h2iuwIUVCus6EA1uw9wmReoxIsMfFx9yNz9GmAQcPiVScCGKfGJGkMjaDUnQSia8dAJDP+WvITXScEyIUqH2OlzGRrb6SSbh9uYXwwAeC0fPg4zYOpTOrfC8lTW8re30iuMcNGi"


//private var AkashProductionPRIVATE_KEY = "MIINhwYJKoZIhvcNAQcDoIINeDCCDXQCAQAxggGsMIIBqAIBADCBjzCBgTELMAkGA1UEBhMCU0cxEjAQBgNVBAgTCVNpbmdhcG9yZTESMBAGA1UEBxMJU2luZ2Fwb3JlMQ0wCwYDVQQKEwQyYzJwMRQwEgYDVQQLEwtEZXZlbG9wbWVudDElMCMGCSqGSIb3DQEJARYWaHRhaW4ubGluc2h3ZUAyYzJwLmNvbQIJAMtZfvtZLaGXMA0GCSqGSIb3DQEBAQUABIIBAHtW5fAcUK+u6as+ZQkHQtFnUXXRefHY4BiAt8oWocAjqXPB8T+XV+eEloPFc01qiYW3/xRKWlDxFaaK+0Ru8E7tD6GenGJMtIjaa7YMyMnQyWMUkcDq8ykqkfy4ln2ON9Me5/dYUgKIIfteBWsZ02xs02KEnZdNxIagD/qxBHsoACANZDmB18BFY55UP9OF+P/yo1SuPVuyWYIqxpREh+MqF8zGNvvok8iFn0wfgfncC+uccdS+aocWdvMoTcpbz97OcQTThrgfbyJCyLIdifXkYYgK01Oa/ehpncsFrvRohPnrwje8MWjgBPd92tpl75XEPhEjPdJwnnkAgbq2GiIwggu9BgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECGou2maju0VlgIILmOiUJ/a4wRhGaWiVG5OdwsguRJYOvNNJGvdweGFO4RtkNkyXsv5mRRmmLznPTU3V7whn6xC02zW6isv7p5okdmX+AaFKbjN9pA2jJpfRNj3RUx7aIqeLrOTnAxmyyJ4hvRO5Ak83P+OAedMK4BnT2ohJfeYuH8hpBCEoKaLaUnocwO2c6Ifq6hCwel7npSVvqfSPjOlyn8KFSsknSWWnXLJD8ZPSxsz3aIOzbDnmtsQVUgLZoYddDJHirWKaCbq7hSHv7hhkGyyvMsZCQrlRmMuQn9lPQstEBPUR2HacYrxIa8TIu+uNf5QsjamD7APRQBv+PfZn7etx4UlEdGTzYT5xn18LuzRfhIvs4YakNjrSdwjDcdqlVX6I/0KCiUPL/9fxFedrrXti6mKAJPv2eCf+3/kO5Ql6GtmFSFfhHcmj0vnTcnsMqecPwKG9nHB0lEs1DYNeShLZPiZyZJ/xH3kN7PGv7PZO5jR8QxPKbZACDOuggqfKCfkDa08yMO6e6svlOuPrpuOLKj9RvWQ1J21EujiMs8mKdmJJtmoH6dTQb+SnyPM15i+8lYt+ahaQqpsL2emrsK7SsHkjg+7RKcygeSxYkzd3ZK6GV5rE8XaVleZ1Ey4QKXOzpcfOwDwBdpnUiFf/cnirCPBFVzVh9Wpj9SVRjeadipHqGpkUMaeGVIDtYD9LGCpQY+m1wu16g1cJ6ne8yYTBmyX1qiV2Hl17ZlCznbFc3L7Y+2zkTG5e7+qKOKYKxD0kA4q5JlzCE4Rewe2R4ecqz3LcaCAzE6ZN3OoTetJa2MmAdtyrsqhrsMnOK9kVyHbbRdXtnAeqkOVCYQzdUDjaO+05hxA+sxFaQ5svJPLK0kzRQKFbv3X2Dv8kaX6qh7PaoLmPRpQ2/2oE4Kdp7LeN7VFGA5S6nU/7yoEelTgk94vJpgazCZqfAohNv3JxZhTX+WKgi+FyKJY4TyAzGOjOyGqx03M+SPxB8j/lXk2nn/tp4kk5Mq8xsgqrj2SoXuFBrREpzm+8AJhh2JugrUOUCxn/YiQhKnBTwipENoQsunJAQUyU8q7NJsB4DKLszffECRJm2MhNsr0dHXl/ErnmvCDf9DT2eZIhelChoAj6ogDoOzPrcUhP/Yo9+6+ywezIM+apWvepQWmeDyYWovwXRUL5S/z9HBn8+RrUEq/5y/xQpThJJlbfgbdm1JfGqXzQ5Z7OrLYVqf9mvaFgegbOxhhYNepkpfvE6OlaiIGPb01TSNiIA3RHF9vH3p1Chcbs44+2C/evvx53oKlE+jzHFMs1YRwncu/EXPepjq+cCNVcSxXMH71pGVlcsjZ5me3Ba0NAAPcJJwRpVtkigXdYkWKRgB7goJQ1rKW95ktESj/H+FtQgImXJSUE2gT+JzgUB9IO6RG4ZT4hM2UwQ4MsXHLo399OYn7JHbKf5L80TDO/IXFOZ4xtXJCpGSJ4dHMuwWjpuXtSlx3oc6gVdqp+f+MLM08C2s/1uKXgmm21B48kFkDG+yDSd36mGKj5V7R61tKMOt97+Z3aKuPw8DBedtZbYgIf/77UvQjaTqyL3Ws69NeWxQGZ/BLzEgxjIsvQwGX18KcwfGfGST2Zuq4GAb09Zxq8sekmLPyJgpGS/RlbcofBDaOwsARXapGxq6oe4HHV0eE/4gTaMt6uizrN+xlftPU2SBEalGX3SxxhcXLGcLY5h6FDiVsqBeFcUppcIJ0o3iaNqjSe2U5B0zyRPFAe6Fm82SKjbaWNv7fAGsp/y3ovu++KjqD37iyn8w0RK3y3hXnKZZ3BP6BylqkqXYmTI/D877lFqoVW/3UwRHpUfkgX+gZS6gniXjAxz4ws56E1Fg33ypxMKJW8srce1OUQxkgO/VR0+lI/iIOXmznIGm4wzEBwAdizMe3adMiXTJExCoBtd9rIoavOIJUKDD8SADxQHgbqwbrMUZFX7uN+PT1FJFMQi7Ss1zoZCSUtYRWJFjsdHcvIkiZzBNUWaDQHMtyHhUivVziKCZSskZR0M021WriWFBwVGABdPUdqnTmM7yksyy7/eKqhNFSyXgmFxRaRNJrzv1k4QAn/cUka4o7znmHYKamAw/S+9MhXYg+lhO7Jj9Ve+fmbUm9VHOJlSshwrrMQ/Mh0TBrjERwufu6osFvmeQv2y8Gf/FsV+Uj4Hz7QUp6V1sfWQx8IU16n/ad6d9/uoSJJ9yqfKp6PdY6QmUnw/ijqSDXvvOlb9Qby+2GBSN+db55uXvQ6AeMV9L9w7E3xsGz02tk46eqiomBFJsKJbeykq6DvsqzZdTzVVoYKCOsho4VFZ7JdATmifpslDuBl0joTAGKoX4laPK6saKMSLYZCUG4XaPRxLC621gowzJSBOpQApiZWCQnDRfreN+5q9OIHv2SKvNvk1uHVA3YUiLlyQ/qwIp9XIg8ScT/rw2Mr72aO583HqB/jXE9W9o9pYtHAOwViO7iJyGW2uheXTQAzX2dfbpOHoqf0adzzNmJTQpztLibbBa/VOt4HPCipZU4zjurkKm3zyNoALLVNcc91X6UzIUXh6rgkwBP8fiNVIS8YH59S9PInVWFwC27AohCHzW9I5lMba9+Oqe5aSfyDwHxO7miHGCp97ULEpakyessuSQQ0Yvgjd0C+az6lVV9IijqetV9l6sDtjsXs44/Od9keEeA51kL2xnwMh7xT6mQ9vJTesTGP1erYQzprIkazzxaXtPSjETIAnuFmPyq+eGPWl3NaluaSD4WA2GaLPu1hOLDGrSkRWZaa9YgTWE7icyYHyk9jNo8VmIogBuUafewCtIXBrhS94PJM62zWGkuCVRS5N6FyK8+8+Qp4eSf3eA0efLw5hD4vY23IkpQMgYjvTfGI/eHLh/Ain22UWYv3eA4/r3tAx/r4lZCnmjvp18GIXbgnZtaiEiWq3jTUvxNGiOseu16C7fAiQIE6VrFES9bJJJrm6q8DnKdbpO14sHeykSoHV8h2Utp82YeQnj0s+vedkyr86oViVPrGbVlGjc8vqhc2qnPsGAxWXHwZkIcPaqQOIBzub9GzA5I5jlZapHPomPyHLkfHLQUfMk8bGKChmNOOrAjIWK5v088eB7lPRia4D/KZEzho1zExATl6OaLXtBO/c1d/JY+DXQ32UscMvw5VwQBR66I7yCP6s+uErA9nWs/lfIfj3MU4oct9nH3M3D/rZ8bu0SUFZC5J2A4LedofaZnY8puTb0rVvYH0nBsjtPzhToQo67ixipS8EEP44NvhmE0dra63QkUo6I7cX4+e3sty8hbHaUPxyi+BRzXTm7PuGGLkKf4KbXAxwZ4ZNsOVqFtoduyZt74x5xbA5W6+pv5z/9tnhU9NJazPzQ5wnG4WuyVAWWrscvm/F1pmjRzhXUsM41xfRnlyJrpmqPsUJ1ShdTf6v/j6w2VJp2z7X48Qnbd+95TqYfWA2HNXWDmX6h/dftMPOVEsAr27Oirfl4c3goDWpN5DVJWW6MUd6vXClVk4f75d1lECclTZJSFh1hEg1TliQfdPkvoV9r593fXOBEQAigf9PaUSzOf4sqyJj9/IXxICs60q/lp8zjAqqzAG3cySmwPdV6/ibja1QSokB/JUXqJa5jmmM55t7GWc5WXTdSyQBr8Aq5W2wttHL54HY8FrCbkadDEd4iKJysuIJBkjRrCCAZr9hM9YhfvLhNIRmgcvxqKONaHgeRO/BhdO4XDwplj+T5X52GqbfWOd6z0uR5fDInJTRNuHrQ5mh3OxrfevRgA6hK+m5XGfZ0k0naQZSauZMZV3a7M5MTrOyRZ0iciUqTHmD889vTYbtOVDV4uo21onKHeg5iEq5hN3zLhRlmweAiS5fNsESQeKXEvCT67t0aK2wZHKXKz8hdlHiZh9i9NhBGvOPvOIyyq3D9bXet7ZYI4KChSnLctQ7LGGXeyRbJToX3ciK8t5tTCqEpKzJkoDXss="


class OrderConfirmation: ZAYOKBaseViewController {
    
    var aPIManager = APIManager()
    var checkOutOrderInfo : CheckOutOrderInfo? = nil
    var flag = Bool()
    var sectionArr = [String]()
    var successDict = Dictionary<String,Any>() 
    var paymentVC: UIViewController?
    var paymenType = ""
    var my2c2pSDK: My2c2pSDK?
    var paymentForm: my2c2pPaymentFormViewController?
    var warningCheckArr = [Bool]()
    
    
    @IBOutlet weak var orderConfirmTableView: UITableView!
    @IBOutlet weak var ContinueBtn: UIButton! {
        didSet {
            self.ContinueBtn.setTitle((ContinueBtn.titleLabel?.text ?? "").localized, for: .normal)
            //            self.ContinueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            self.ContinueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            ContinueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var ApplyBtn: UIButton! {
        didSet {
            ApplyBtn.setTitle((ApplyBtn.titleLabel?.text ?? "").localized, for: .normal)
            ApplyBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.paymenType)
        my2c2pSDK = My2c2pSDK(privateKey: APIManagerClient.sharedInstance.ProductionAndStagingPrivateKeyfor2c2p)
        self.navigationItem.title = "Order Confirmation".localized
        sectionArr = [UserDefaults.standard.value(forKey: "shipadd") as? String ?? "", "Products", ""]
        let dummyViewHeight = CGFloat(50)
        self.orderConfirmTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.orderConfirmTableView.bounds.size.width, height: dummyViewHeight))
        self.orderConfirmTableView.contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
        self.getCheckoutOrderInfo()
    }
    
    func customize2C2PUI(){
        paymentForm = my2c2pPaymentFormViewController.init(nibName: "myPaymentFormViewController", bundle: nil)
        paymentForm?.modalPresentationStyle = .fullScreen
        paymentForm?.delegateVC = self
        //nav bar color
        paymentForm?.navBarColor = UIColor.red
        //nav button color
        paymentForm?.navButtonTintColor = UIColor.white
        //nav title color
        paymentForm?.navTitleColor = UIColor.white
        //nav title
        paymentForm?.navTitle = "MyPayment"
        //nav title image
        paymentForm?.navLogo = UIImage(named: "iconapp")
    }
    
    
    func randomUniqueTransactionCode() -> String? {
        return "\("\(Int(roundf(Float(Date().timeIntervalSince1970))))")\(RAND_FROM_TO(min: 0, max: 9))"
    }
    
    
    
    func getCheckoutOrderInfo() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadCheckOutOrderInformation(onSuccess: { [weak self] gotcheckOutOrder in
            self?.checkOutOrderInfo = gotcheckOutOrder
            if (self?.checkOutOrderInfo?.Items.count)! > 0{
                self?.warningCheckArr = Array(repeating: false, count: (self?.checkOutOrderInfo?.Items.count)!)
            }
            
            self?.flag = true
            self?.orderConfirmTableView.delegate = self
            self?.orderConfirmTableView.dataSource = self
            self?.orderConfirmTableView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    
    //MARK:- Payment Api Call
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        
        print("wwwwwww -\n",self.warningCheckArr)
        var checkWarning = false
        for obj in warningCheckArr {
            if obj {
                checkWarning = true
            }
        }
        
        
        var availableSIM: Bool {
            return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil ) ? false : true // sim detection
        }
        
        if  !availableSIM{
            AppUtility.showToastlocal(message: "Insert your registered sim", view: self.view)
            self.navigationController?.popToRootViewController(animated: true)
            return
        }
        
        
        if checkWarning {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Buying is disabled for some product".localized, onController: self)
                let tryAgain = SAlertAction()
                tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                    DispatchQueue.main.async {
                        AppUtility.showLoading(self.view)
                        let dicArray = NSMutableArray ()
                        
                        for (index,obj) in self.warningCheckArr.enumerated() {
                            if obj {
                                print("iiiiiiiiiiiiii - ",index)
                                self.warningCheckArr.remove(at: index)
                                let parameters:Dictionary<String,AnyObject> = ["value": self.checkOutOrderInfo?.Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                dicArray.add(parameters)
                            }
                        }
                        print(dicArray)
                        self.removeProduct(dicArray: dicArray)
                    }
                })
                alertVC.addAction(action: [tryAgain])
            }
            return
        }
        
        
        
        if self.paymenType == "OK Dollar"{
            
            var model : OKPayModel?
            
            if APIManagerClient.sharedInstance.serverUrl == .productionUrl{
                model = OKPayModel(code: "+95", destination: "00959975278302", amount: 1.0, key: PaymentKey.zayOkPayKey, paymentTouple: (url: URLType.production, app: ApplicationType.okEcommerce, pay: PaymentType.okWallet), geoTuple: (lat: "0.0", long: "0.0"))
            }else{
                model = OKPayModel(code: "+95", destination: "00959975278302", amount: 1.0, key: PaymentKey.zayOkPayKey, paymentTouple: (url: URLType.testing, app: ApplicationType.okEcommerce, pay: PaymentType.okWallet), geoTuple: (lat: "0.0", long: "0.0"))
            }
            
            
            if let val = model{
                if let vc = OKPayManager.pay.start(val, withDelegate: self) {
                    vc.modalPresentationStyle = .fullScreen
                    paymentVC = vc
                    DispatchQueue.main.async{
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
        else{
            // 2c2p code
            
            //            my2c2pSDK?.merchantID = "104104000000270"
            //            my2c2pSDK?.secretKey = "4C18A036BD22DC37F7412BB648E6D88C554EFBB8C4D9194FE3C19FAF64BAA519"//Production Key"524EE9FC87F9832814708365A49703407879BAFD3490BF099FFAD0D395BA55B4"
            
            my2c2pSDK?.merchantID = APIManagerClient.sharedInstance.merchantID
            my2c2pSDK?.currencyCode = APIManagerClient.sharedInstance.currencyCode
            my2c2pSDK?.secretKey = APIManagerClient.sharedInstance.secretKey
            my2c2pSDK?.productionMode = APIManagerClient.sharedInstance.productionMode
            my2c2pSDK?.uniqueTransactionCode = randomUniqueTransactionCode()
            my2c2pSDK?.desc = "Cart Product"
            my2c2pSDK?.paymentUI = true
            my2c2pSDK?.amount = 1.00
            
            //            self.children[0].modalPresentationStyle = .fullScreen
            //            self.parent?.modalPresentationStyle = .fullScreen
            
            my2c2pSDK?.request(withTarget: self, onResponse: { response in
                if (response?["respCode"] as? String == "00") {
                    print("Payment Success")
                    self.successDict = response as! [String : Any]
                    DispatchQueue.main.async{
                        if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
                            paymentProcessingVC.navController = self.navigationController
                            paymentProcessingVC.paymenType = self.paymenType
                            paymentProcessingVC.paymentDict = self.successDict
                            paymentProcessingVC.navController?.modalPresentationStyle = .fullScreen
                            self.present(paymentProcessingVC, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        AppUtility.showToastlocal(message: response?["failReason"] as! String , view: self.view)
                    }
                    print("Fail reason: \(response?["failReason"] ?? "")")
                }
            }, onFail: { error in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    AppUtility.showToastlocal(message: error?.localizedDescription ?? "" , view: self.view)
                }
                print("Error: \(error?.localizedDescription ?? "")")
            })
        }
        
    }
    
    func RAND_FROM_TO(min: UInt32, max: UInt32) -> Int {
        return Int(min + arc4random_uniform(max - min + 1))
    }
    
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.successDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func removeProduct(dicArray: NSMutableArray) {
        aPIManager.DeleteAndUpdate(dicArray, onSuccess: { [weak self] updateCart in
            //self?.checkOutOrderInfo = updateCart
            self?.navigationController?.popToRootViewController(animated: true)
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                
            }
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    private func showPaymentProcessingVC() {
        if let payVC = paymentVC {
            payVC.dismiss(animated: false) {
                if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
                    paymentProcessingVC.navController = self.navigationController
                    paymentProcessingVC.paymenType = self.paymenType
                    paymentProcessingVC.paymentDict = self.successDict
                    paymentProcessingVC.modalPresentationStyle = .fullScreen
                    self.present(paymentProcessingVC, animated: true, completion: nil)
                }
            }
        }
    }
}

extension OrderConfirmation: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return self.checkOutOrderInfo?.Items.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 20
        }
        return 40
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        return sectionArr[section]
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 2))
            headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 240, greenValue: 240, blueValue: 240, alpha: 1)
            //            headerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            headerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            return headerView
        }
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        //        backView.backgroundColor = UIColor.colorWithRedValue(redValue: 240, greenValue: 240, blueValue: 240, alpha: 1)
        //        backView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        backView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.bounds.width-15, height: 20))
        label.font = UIFont(name: appFont, size: 15.0)
        label.textColor = UIColor.white
        //        label.textColor = UIColor.white
        label.text = sectionArr[section].localized
        headerView.addSubview(backView)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : OrderConfirmationTableViewCell?
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryAddressConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            cell?.wrapAddressCell(orerInfo: self.checkOutOrderInfo)
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "ProductOrdersConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            if (self.checkOutOrderInfo?.Items[indexPath.row].Warnings?.count)! > 0{
                self.warningCheckArr[indexPath.row] = true
            } else {
                self.warningCheckArr[indexPath.row] = false
            }
            cell?.wrapProductDetails(orderInfo: self.checkOutOrderInfo, row: indexPath.row)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "TotalAmountOrderConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            cell?.wrapAmountCell(orderInfo: self.checkOutOrderInfo)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 130
        }
        if indexPath.section == 1 {
            return SCREEN_WIDTH/3.2 + 40
        }
        else if indexPath.section == 2 {
            return 162
        }
        return UITableView.automaticDimension
    }
}

class OrderConfirmationTableViewCell: UITableViewCell {
    
    @IBOutlet var productAmtHeaderLbl: UILabel! {
        didSet {
            productAmtHeaderLbl?.text = productAmtHeaderLbl?.text?.localized
            self.productAmtHeaderLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var userNameLbl: UILabel! {
        didSet {
            userNameLbl?.text = userNameLbl?.text?.localized
            self.userNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var emailLbl: UILabel! {
        didSet {
            emailLbl?.text = emailLbl?.text?.localized
            self.emailLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var phNumberLbl: UILabel! {
        didSet {
            phNumberLbl?.text = phNumberLbl?.text?.localized
            self.phNumberLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var addressAndCityLbl: UILabel! {
        didSet {
            addressAndCityLbl?.text = addressAndCityLbl?.text?.localized
            self.addressAndCityLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var countryLbl: UILabel! {
        didSet {
            countryLbl?.text = countryLbl?.text?.localized
            self.countryLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productImgView: UIImageView!
    @IBOutlet var productNameLbl: UILabel! {
        didSet {
            productNameLbl?.text = productNameLbl?.text?.localized
            productNameLbl?.numberOfLines = 2
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var priceLabel: UILabel! {
        didSet {
            priceLabel?.text = "\("Price".localized):"
            self.priceLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var quantityLabel: UILabel! {
        didSet {
            quantityLabel?.text = "\("Quantity".localized):"
            self.quantityLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var totalLabel: UILabel! {
        didSet {
            totalLabel?.text = "\("Total".localized):"
            self.totalLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var attributeLabel: UILabel!{
        didSet {
            self.attributeLabel.font = UIFont(name: appFont, size: 15.0)
            attributeLabel?.text = attributeLabel?.text?.localized
        }
    }
    
    @IBOutlet var priceValueLbl: UILabel! {
        didSet {
            self.priceValueLbl.font = UIFont(name: appFont, size: 15.0)
            priceValueLbl?.text = priceValueLbl?.text?.localized
        }
    }
    @IBOutlet var quantityValueLbl: UILabel! {
        didSet {
            quantityValueLbl?.text = quantityValueLbl?.text?.localized
            self.quantityValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var totalValueLbl: UILabel! {
        didSet {
            totalValueLbl?.text = totalValueLbl?.text?.localized
            self.totalValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var lineLbl: UILabel! {
        didSet {
            lineLbl?.text = lineLbl?.text?.localized
            self.lineLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var subTotallLabel: UILabel! {
        didSet {
            subTotallLabel?.text = subTotallLabel?.text?.localized
            self.subTotallLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var shippingLabel: UILabel! {
        didSet {
            shippingLabel?.text = shippingLabel?.text?.localized
            self.shippingLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var taxLabel: UILabel! {
        didSet {
            taxLabel?.text = taxLabel?.text?.localized
            self.taxLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var discountLabel: UILabel! {
        didSet {
            discountLabel?.text = discountLabel?.text?.localized
            self.discountLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var grandTotalLabel: UILabel! {
        didSet {
            grandTotalLabel?.text = grandTotalLabel?.text?.localized
            self.grandTotalLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var subTotallValueLbl: UILabel! {
        didSet {
            subTotallValueLbl?.text = subTotallValueLbl?.text?.localized
            self.subTotallValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var shippingValueLbl: UILabel! {
        didSet {
            shippingValueLbl?.text = shippingValueLbl?.text?.localized
            self.shippingValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var taxValueLbl: UILabel! {
        didSet {
            taxValueLbl?.text = taxValueLbl?.text?.localized
            self.taxValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var discountValueLbl: UILabel! {
        didSet {
            discountValueLbl?.text = discountValueLbl?.text?.localized
            self.discountValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var grandTotalValueLbl: UILabel! {
        didSet {
            grandTotalValueLbl?.text = grandTotalValueLbl?.text?.localized
            self.grandTotalValueLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    func wrapAddressCell(orerInfo: CheckOutOrderInfo?) {
        let userName = "\(orerInfo?.ShippingAddress_FirstName ?? "")"
        let address = "\(orerInfo?.ShippingAddress_Address1 ?? ""), \(orerInfo?.ShippingAddress_Address2 ?? ""), \(orerInfo?.ShippingAddress_City ?? "")"
        self.userNameLbl.text = userName
        self.emailLbl.text = orerInfo?.ShippingAddress_Email as String? ?? ""
        self.phNumberLbl.text = orerInfo?.ShippingAddress_PhoneNumber as String? ?? ""
        self.addressAndCityLbl.text = address
        let stateCountry = "\(orerInfo?.ShippingAddress_StateProvinceName as String? ?? ""), \(orerInfo?.ShippingAddress_CountryName as String? ?? "")"
        self.countryLbl.text = stateCountry
    }
    
    func wrapProductDetails(orderInfo: CheckOutOrderInfo?, row: Int) {
        self.productImgView.sd_setImage(with: URL(string: orderInfo?.Items[row].ImageUrl ?? ""))
        
        
        
        //        AppUtility.getData(from: URL(string: orderInfo?.Items[row].ImageUrl ?? "")!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productImgView.image = UIImage(data: data)
        //            }
        //        }
        
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: orderInfo?.Items[row].ImageUrl ?? ""), imageView: self.productImgView, imgUrl: orderInfo?.Items[row].ImageUrl ?? "") { (image) in }
        
        
        
        
        
        self.productNameLbl.text = orderInfo?.Items[row].ProductName as String? ?? ""
        self.priceValueLbl.text = orderInfo?.Items[row].UnitPrice as String? ?? ""
        self.priceValueLbl.amountAttributedString()
        self.quantityValueLbl.text = String(format: "%d", orderInfo?.Items[row].Quantity ?? 0)
        self.attributeLabel.text = (orderInfo?.Items[row].AttributeInfo)!.replacingOccurrences(of: "<br />", with: "\n")
        //        self.totalValueLbl.text = orderInfo?.Items[row].SubTotal as String? ?? ""
        if row == (orderInfo?.Items.count ?? 0) - 1 {
            self.lineLbl.isHidden = true
        } else {
            self.lineLbl.isHidden = false
        }
    }
    
    func wrapAmountCell(orderInfo: CheckOutOrderInfo?) {
        if orderInfo?.SubTotal == nil || orderInfo?.SubTotal == "" {
            self.subTotallLabel.text = ""
        }
        if orderInfo?.Shipping == nil || orderInfo?.Shipping == "" {
            self.shippingLabel.text = ""
        }
        if orderInfo?.Tax == nil || orderInfo?.Tax == "" {
            self.taxLabel.text = ""
        }
        if orderInfo?.SubTotalDiscount == nil || orderInfo?.SubTotalDiscount == "" {
            self.discountLabel.text = ""
        }
        if orderInfo?.OrderTotal == nil || orderInfo?.OrderTotal == "" {
            self.grandTotalLabel.text = ""
        }
        self.subTotallValueLbl.text = orderInfo?.SubTotal ?? ""
        self.subTotallValueLbl.amountAttributedString()
        if orderInfo?.Shipping == "0 MMK"{
            self.shippingValueLbl.text = "Free"
        }
        else{
            self.shippingValueLbl.text = orderInfo?.Shipping ?? ""
            self.shippingValueLbl.amountAttributedString()
        }
        
        self.taxValueLbl.text = orderInfo?.Tax ?? ""
        self.taxValueLbl.amountAttributedString()
        self.discountValueLbl.text = orderInfo?.SubTotalDiscount ?? ""
        
        //        self.grandTotalValueLbl.text = orderInfo?.OrderTotal ?? ""
        //        self.grandTotalValueLbl.amountAttributedString()
        //
        
        
        
        var productPrice = orderInfo?.OrderTotal!.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        
        let attrString = NSMutableAttributedString(string: productPrice!)
        let nsRange = NSString(string: productPrice!).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.grandTotalValueLbl.attributedText = attrString
        
        
    }
}


extension OrderConfirmation: OKPaymentDelegates {
    func didEncounteredErrorWhilePayment(errMsg: NSError) {
        DispatchQueue.main.async {
            if let payVC = self.paymentVC {
                payVC.dismiss(animated: true, completion: nil)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            AppUtility.showToastlocal(message: errMsg.domain.localized , view: self.view)
        }
    }
    
    func paymentSuccessfull(raw: Data?) {
        guard let data = raw else {
            return
        }
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(PaymentSuccessModel.self, from: data)
            guard let xmlString = model.data else { return }
            let xml = SWXMLHash.parse(xmlString)
            self.enumerate(indexer: xml)
            if model.code == 200 {
                DispatchQueue.main.async { self.showPaymentProcessingVC() }
            } else {
                DispatchQueue.main.async {
                    if let payVC = self.paymentVC {
                        payVC.dismiss(animated: true, completion: nil)
                    }
                }
                AppUtility.showToastlocal(message: model.message!.localized, view: self.view)
            }
        } catch _ {
            
        }
    }
}


extension OrderConfirmation: my2c2pPaymentFormViewControllerSourceDelegate{
    func paymentFormViewDidLoad() {
        
        //rounded corner
        paymentForm?.confirmbtn!.layer.cornerRadius = 5
        paymentForm?.storeCardConfirmbtn!.layer.cornerRadius = 5
        
        paymentForm?.useNewCardBtn!.layer.cornerRadius = 5
        
        //add border color for buttons
        paymentForm?.confirmbtn!.layer.borderWidth = 1
        paymentForm?.confirmbtn!.layer.borderColor = UIColor.gray.cgColor
        
        paymentForm?.storeCardConfirmbtn!.layer.borderWidth = 1
        paymentForm?.storeCardConfirmbtn!.layer.borderColor = UIColor.gray.cgColor
        
        paymentForm?.useNewCardBtn!.layer.borderWidth = 1
        paymentForm?.useNewCardBtn!.layer.borderColor = UIColor.gray.cgColor
        
    }
}
