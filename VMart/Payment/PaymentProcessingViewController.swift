//
//  PaymentProcessingViewController.swift
//  VMart
//
//  Created by Kethan on 12/13/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class PaymentProcessingViewController: ZAYOKBaseViewController {
    
    @IBOutlet var PaymentProcessLabel: UILabel!{
        didSet {
            self.PaymentProcessLabel.text = self.PaymentProcessLabel.text?.localized
            self.PaymentProcessLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var paymentDict: Dictionary<String, Any>?
    var paymentResponse: Dictionary<String, Any>?
    var aPIManager = APIManager()
    var navController: UINavigationController?
    var paymenType = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(paymenType)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.savePaymentTransactionHistory()
        }
    }
    
    private func savePaymentTransactionHistory() {
        var payStatusDict = Dictionary<String, Any>()
        if self.paymenType == "OK Dollar"{
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Transaction Failure"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            if let model = paymentDict, let description = model["resultdescription"] as? String {
                if description.lowercased() == "Transaction Successful".lowercased() {
                    payStatusDict["TransactionId"] = model["transid"] as? String ?? ""
                    payStatusDict["TransactionDescription"] = "Transaction Successful"
                    payStatusDict["TransactionStatusCode"] = "000"
                    payStatusDict["PaymentMethod"] =  "Payments.OKDollar"//self.paymenType//"4"
                    //                    payStatusDict["TransactionStatusCode"] =   model["failReason"] as? String ?? ""  //description
                    payStatusDict["TransactionAmount"] = model["amount"] as? String ?? ""
                }
            }
        }
        else{
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Transaction Failure"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            if let model = paymentDict,let description = model["failReason"] as? String {
                if description.lowercased() == "APPROVED".lowercased() {
                    payStatusDict["TransactionId"] = model["tranRef"] as? String ?? ""
                    payStatusDict["TransactionDescription"] = "Transaction Successful"
                    payStatusDict["TransactionStatusCode"] =   "000"//model["failReason"] as? String ?? ""  //description
                    payStatusDict["PaymentMethod"] = "Payments.2C2P" //self.paymenType//"4"
                    payStatusDict["TransactionAmount"] = model["amt"] as? String ?? ""
                }
            }
        }
        
        
        
        
        let urlString = String(format: "%@/checkout/checkoutcomplete", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = AppUtility.JSONStringFromAnyObject(value: payStatusDict as AnyObject)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, data) in
            if success {
                do {
                    let _ = JSONDecoder()
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        //let model = try decoder.decode(VMUpdatePaymentStatusModel.self, from: dataVal)
                        self?.paymentResponse = dict
                        if let statusCode = dict["StatusCode"] as? Int, statusCode == 200 {
                            //                            UserDefaults.standard.setValue("", forKey: "badge")
                            UserDefaults.standard.set("", forKey: "badge")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                self?.showReceiptScreen()
                            }
                        } else {
                            if let viewLoc = self?.view {
                                if let errorList = dict["ErrorList"] as? [String], errorList.count > 0 {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc)
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self?.dismiss(animated: false, completion: nil)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self?.dismiss(animated: false, completion: nil)
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self?.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
        
        
        /*
         let urlString = String(format: "%@/checkout/SavePaymentTransactionHistory", APIManagerClient.sharedInstance.base_url)
         guard let url = URL(string: urlString) else { return }
         let params = AppUtility.JSONStringFromAnyObject(value: payStatusDict as AnyObject)
         aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, data) in
         if success {
         if let json = response as? Dictionary<String,Any> {
         if let code = json["StatusCode"] as? Int, code == 200 {
         self?.checkoutComplete()
         } else {
         if let viewLoc = self?.view {
         if let errorList = json["ErrorList"] as? [String], errorList.count > 0 {
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc)
         }
         }
         }
         DispatchQueue.main.async {
         self?.dismiss(animated: false, completion: nil)
         }
         }
         }
         }
         }
         */
    }
    
    private func checkoutComplete() {
        let urlString = String(format: "%@/checkout/checkoutcomplete", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let dict = Dictionary<String, Any>()
        aPIManager.genericClass(url: url, param: dict as AnyObject, httpMethod: "GET", header: true) { [weak self](response, success, data) in
            if success {
                do {
                    let decoder = JSONDecoder()
                    guard let dataVal = data else { return }
                    let model = try decoder.decode(VMCheckoutCompleteModel.self, from: dataVal)
                    if let statusCode = model.statusCode, statusCode == 200 {
                        self?.UpdatePaymentStatus(model: model)
                    } else {
                        if let viewLoc = self?.view {
                            if let errorList = model.errorList, errorList.count > 0 {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc)
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            self?.dismiss(animated: false, completion: nil)
                        }
                    }
                } catch _ {
                    println_debug("Parse error")
                }
            }
        }
    }
    
    private func updatePayStatusParam(model: VMCheckoutCompleteModel) -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        dict["orderId"] = model.orderID ?? 0
        dict["orderNumber"] = model.customOrderNumber ?? ""
        dict["phoneNumber"] = ""
        dict["productAmount"] = ""
        dict["referenceNumber"] = ""
        dict["shippingAddress"] = ""
        dict["shippingCharges"] = ""
        dict["successMessage"] = self.paymentDict?["resultdescription"] as? String ?? ""
        dict["totalPaidAmount"] = self.paymentDict?["amount"] as? String ?? ""
        dict["transactionId"] = self.paymentDict?["transid"] as? String ?? ""
        dict["transactionStatus"] = "Success"
        dict["StatusCode"] = 0
        return dict
    }
    
    private func UpdatePaymentStatus(model: VMCheckoutCompleteModel) {
        let urlString = String(format: "%@/checkout/UpdatePaymentStatus", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = AppUtility.JSONStringFromAnyObject(value: self.updatePayStatusParam(model: model) as AnyObject)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, data) in
            if success {
                do {
                    let _ = JSONDecoder()
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        //let model = try decoder.decode(VMUpdatePaymentStatusModel.self, from: dataVal)
                        self?.paymentResponse = dict
                        if let statusCode = dict["StatusCode"] as? Int, statusCode == 200 {
                            //                            UserDefaults.standard.setValue("", forKey: "badge")
                            UserDefaults.standard.set("", forKey: "badge")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                self?.showReceiptScreen()
                            }
                        } else {
                            if let viewLoc = self?.view {
                                if let errorList = dict["ErrorList"] as? [String], errorList.count > 0 {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc)
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self?.dismiss(animated: false, completion: nil)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self?.dismiss(animated: false, completion: nil)
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self?.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    private func showReceiptScreen() {
        
        if let paymentSuccessVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccess_ID") as? PaymentSuccess {
            paymentSuccessVC.paymentResponse = self.paymentResponse
            if let nav = navController {
                nav.pushViewController(paymentSuccessVC, animated: true)
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
}
