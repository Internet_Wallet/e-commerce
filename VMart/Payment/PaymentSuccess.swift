//
//  PaymentSuccess.swift
//  VMart
//
//  Created by ANTONY on 06/12/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
//import PGW

class PaymentSuccess: ZAYOKBaseViewController {
    
    
    @IBOutlet var PDFView:UIView?
    @IBOutlet weak var PdfTbl: UITableView!
    
    @IBOutlet weak var PaymentTbl: UITableView!
    
    @IBOutlet var doneBtn: UIButton!{
        didSet {
            doneBtn.setTitle((doneBtn.titleLabel?.text ?? "").localized, for: .normal)
            doneBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var SharePDFBtn: UIButton!{
        didSet {
            SharePDFBtn.setTitle((SharePDFBtn.titleLabel?.text ?? "").localized, for: .normal)
            SharePDFBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    
    var paymentResponse: Dictionary<String, Any>?
    var paymentInquiryResponse:String!
    
    override func viewDidLoad() {
        
        self.title = "Payment Successful".localized
        super.viewDidLoad()
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
        
        if let data = self.paymentResponse{
            println_debug(data)
            self.PaymentTbl.dataSource = self
            self.PaymentTbl.delegate = self
            self.PaymentTbl.reloadData()
            //self.updateUIData(payStatusModel: data)
        }
        
        if self.paymentInquiryResponse != nil{
            //            self.initData()
        }
        // Do any additional setup after loading the view.
    }
    
    //    private func initData() {
    //
    //        guard let decodedRes:String = ToolKit.base64DecodedString(encodedString: self.paymentInquiryResponse) else { return }
    //
    //        guard let jsonObject:[String:Any] = ToolKit.dicFrom(jsonString: decodedRes) else { return }
    //
    //        guard let responseCode:String = jsonObject[PGW.Constants.JSON_NAME_RESP_CODE] as? String else { return }
    //
    //        if(responseCode != APIResponseCode.API_SUCCESS) {
    
    //            self.transactionResultHeader.text = Constants.transaction_result_header_failed
    //            self.transactionResultHeader.textColor = UIColor.red
    //
    //            self.transactionResultMessage.text = Constants.transaction_result_message_failed
    //
    //            self.message.textColor = UIColor.red
    //        }
    //
    //        print(jsonObject)
    //        self.invoiceNo.text = jsonObject[PGW.Constants.JSON_NAME_INVOICE_NO] as? String
    //        self.amount.text = jsonObject[PGW.Constants.JSON_NAME_AMOUNT] as? String
    //        self.dateTime.text = jsonObject[Constants.JSON_NAME_TRANSACTION_DATE_TIME] as? String
    //        self.paymentType.text = jsonObject[PGW.Constants.JSON_NAME_CHANNEL_CODE] as? String
    //        self.maskedPan.text = jsonObject[PGW.Constants.JSON_NAME_PAN] as? String
    //        self.message.text = jsonObject[PGW.Constants.JSON_NAME_RESP_DESC] as? String
    //
    //        self.responseMessageExpand.text = ToolKit.prettyPrintJsonString(jsonStr: decodedRes)
    //
    //        extraLength = (self.responseMessageExpand.text?.height(withConstrainedWidth: UIScreen.main.bounds.size.width - 2 * 16, font: UIFont.systemFont(ofSize: 16)))!
    //    }
    
    
    
    @objc func dismissScreen() {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func loadPDFView(order: Dictionary<String, Any>){
        
        
        
        //        if let order = order["Orders"] as? [Dictionary<String,Any>] {
        //            self.InvoiceNumLabel.text = "Order NO: \(order[0]["CustomOrderNumber"].safelyWrappingString())" + "\n Invoice NO:\( order[0]["ReferenceNumber"].safelyWrappingString())"
        //            self.TaxLabel.text = "Tax: \(order[0]["Tax"].safelyWrappingString())"
        //            self.TaxLabel.amountAttributedString()
        //            self.ShippingLabel.text = "Shipping Charges : \(order[0]["OrderShipping"].safelyWrappingString())"
        //            self.ShippingLabel.amountAttributedString()
        //            self.TotalPriceLabel.text = "TOTAL PRICE : \(order[0]["OrderTotal"].safelyWrappingString())"
        //            self.TotalPriceLabel.amountAttributedString()
        //
        //            self.orderNOValLabel.text = "\(order[0]["CustomOrderNumber"].safelyWrappingString())"
        //            self.orderDateLabel.text = "Order Date :\(order[0]["ExpectedDeliveryDate"].safelyWrappingString())"
        //
        //
        //            var qty: String?
        //            var grossamount: String?
        //            if let obj = order[0]["Items"] as? [Dictionary<String,Any>]{
        //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
        //                self.QuantityValLabel.text = obj[0]["Quantity"].safelyWrappingString()
        //                self.GrossAmtValLabel.text = obj[0]["UnitPrice"].safelyWrappingString()
        //                qty = obj[0]["Quantity"].safelyWrappingString()
        //                grossamount = self.GrossAmtValLabel.text?.replacingOccurrences(of: " MMK", with: "")
        //                self.DiscounValtLabel.text = "\(obj[0]["DiscountAmount"] ?? "0")"
        //                self.ProductValLabel.text = obj[0]["ProductName"].safelyWrappingString()
        //            }
        //
        //
        //            let qtyNum = qty?.numberValue?.intValue
        //            let grossamt = grossamount?.numberValue?.intValue
        //            let multi = qtyNum! * grossamt!
        //
        //
        //            self.TotalValLabel.text =  "\(String(multi))MMK"
        //            self.TotalValLabel.amountAttributedString()
        //
        //            if let dict = order[0]["PickupAddress"] as? Dictionary<String, Any>{
        //                self.Add5Label.text =  "\(dict["HouseNo"] as? String ?? ""),\(dict["FloorNo"] as? String ?? ""),\(dict["RoomNo"] as? String ?? ""),\(dict["Address1"] as? String ?? ""),\(dict["Address2"] as? String ?? ""),\(dict["City"] as? String ?? ""),\(dict["StateProvinceName"] as? String ?? ""),\(dict["CountryName"] as? String ?? "")"
        //            }
        //            if let dictObj = order[0]["ShippingAddress"] as? Dictionary<String,Any>{
        //                self.Add1Label.text =  "\(dictObj["HouseNo"] as? String ?? ""),\(dictObj["FloorNo"] as? String ?? ""),\(dictObj["RoomNo"] as? String ?? ""),\(dictObj["Address1"] as? String ?? ""),\(dictObj["Address2"] as? String ?? ""),\(dictObj["City"] as? String ?? ""),\(dictObj["StateProvinceName"] as? String ?? ""),\(dictObj["CountryName"] as? String ?? "")"
        //            }
        //        }
        
        //        if let ordersArray = payStatusModel["Orders"] as? [Any], ordersArray.count > 0 {
        //            if let order = ordersArray.first as? Dictionary<String, Any> {
        //                self.InvoiceNumLabel.text = "#\(order["CustomOrderNumber"] as? String ?? "")  \(payStatusModel["ReferenceNumber"] as? String ?? "")" //
        //            }
        //        } else {
        //            self.InvoiceNumLabel.text = ""
        //        }
        //        //        self.InvoiceNumLabel.text = payStatusModel["ReferenceNumber"] as? String ?? ""
        //        //        self.lblShippingAddress.text = payStatusModel["ShippingAddress"] as? String ?? ""
        //        //        self.lblPhoneNumber.text = payStatusModel["PhoneNumber"] as? String ?? ""
        //        //        if let number = lblPhoneNumber.text {
        //        //            if number.hasPrefix("0095") {
        //        //                lblPhoneNumber.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95")
        //        //            }
        //        //        }
        //        //        self.lblProductAmount.text = payStatusModel["ProductAmount"] as? String ?? "0.0 MMK"
        //        //        self.lblProductAmount.amountAttributedString()
        //        self.TotalPriceLabel.text = payStatusModel["TotalPaidAmount"] as? String ?? "0.0 MMK" //
        //        self.TotalPriceLabel.amountAttributedString()
        //        self.ShippingLabel.text = payStatusModel["ShippingCharges"] as? String ?? "0.0 MMK" //
        //        self.ShippingLabel.amountAttributedString()
        //
        //        if let ordersArray = payStatusModel["Orders"] as? [Any], ordersArray.count > 0 {
        //            if let order = ordersArray.first as? Dictionary<String, Any> {
        //                if let itemArray = order["Items"] as? [Any], itemArray.count > 0 {
        //                    self.TotalQuantityLabel.text = "Total Order Count: \(itemArray.count)"
        //                }
        //            }
        //        } else {
        //            self.TotalQuantityLabel.text = "Total Order Count: 1"
        //        }
        
        
    }
    
    
    
    @IBAction func DoneButtonAction(_ sender: UIButton) {
        //        self.performSegue(withIdentifier: "InvoiceReceiptSeque", sender: self)
        self.performSegue(withIdentifier: "ratingSeque", sender: self)
        
    }
    
    @IBAction func SharePDFButtonAction(_ sender: UIButton) {
        self.PDFView?.isHidden = false
        DispatchQueue.main.async {
            self.PdfTbl.delegate = self
            self.PdfTbl.dataSource = self
            self.PdfTbl.reloadData()
            
            let pdfUrl = self.createPdfFromView(aView: self.PDFView!, saveToDocumentsWithFileName: "Transaction Receipt".localized)
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: {
                self.PDFView?.isHidden = true
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "InvoiceReceiptSeque" {
            if let vc = segue.destination as? Invoice {
                vc.responseModel = self.paymentResponse
            }
        }
    }
    
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:PdfTbl.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
}




extension PaymentSuccess: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == PaymentTbl{
            return 4
        }
        else{
            if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
                return order.count + 5
            }else{
                return 5
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == PaymentTbl{
            if section == 2{
                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
                    return order.count
                }
            }
            else{
                return 1
            }
        }
        else if tableView == PdfTbl{
            if section == 4{
                var total = 0
                //                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>] {
                //                    let totalSectionCount = order.count + 5
                //                    if section == totalSectionCount-1 {
                //                        return 1ww
                //                    }
                //                    if let TotalObject = order[section-4]["Items"] as? [Dictionary<String,Any>]{
                //                        return TotalObject.count
                //                    }
                //                }
                
                if let order = self.paymentResponse{
                    if let orderObject = order["Orders"] as? [Dictionary<String,Any>] {
                        for _ in (orderObject[section-4]["Items"] as? [Dictionary<String,Any>])!{
                            total += 1
                        }
                    }
                    return total
                }
            }
            else{
                return 1
            }
        }
        
        
        //        if section == 4{
        //            var count = 0
        //            if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>] {
        //
        //                    count = order.count
        //
        //            }
        //            return (count)
        //        }
        //        else{
        //            return 1
        //        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == PaymentTbl{
            if indexPath.section == 0 {
                let cell = PaymentTbl.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath) as! PaymentSuccessTableCell
                return cell
            } else if indexPath.section == 1 {
                let cell = PaymentTbl.dequeueReusableCell(withIdentifier: "cellPayDetail", for: indexPath) as! PaymentSuccessTableCell
                if let data = self.paymentResponse{
                    cell.setOrderData(payStatusModel: data)
                }
                return cell
            } else if indexPath.section == 2 {
                let cell = PaymentTbl.dequeueReusableCell(withIdentifier: "cellOrder", for: indexPath) as! PaymentSuccessTableCell
                if let data = self.paymentResponse{
                    cell.orderData(payStatusModel: data,index:indexPath)
                }
                return cell
            } else{
                let cell = PaymentTbl.dequeueReusableCell(withIdentifier: "cellAddress", for: indexPath) as! PaymentSuccessTableCell
                if let data = self.paymentResponse{
                    cell.updateUIData(payStatusModel: data)
                }
                return cell
            }
        }
        else{
            
            var totalSectionCount = 0
            if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>] {
                totalSectionCount = order.count + 5
            }
            else {
                totalSectionCount = 5
            }
            
            println_debug(indexPath)
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFHeaderCell", for: indexPath) as! PDFTableCell
                //                cell.wrapData(responseModel: itemsArray[indexPath.row])
                return cell
            } else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFinvoiceCell", for: indexPath) as! PDFTableCell
                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
                    cell.PDFInvoice(responseModel: order[0])
                }
                return cell
            } else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFAddressCell", for: indexPath) as! PDFTableCell
                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
                    cell.PDFAddress(responseModel: order[0])
                }
                return cell
            } else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderHeaderCell", for: indexPath) as! PDFTableCell
                //                cell.Address(responseModel: responseModel!)
                return cell
            }
                
            else if indexPath.section == 5 {
                //            else if indexPath.section == totalSectionCount-1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFTotalCell", for: indexPath) as! PDFTableCell
                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
                    cell.PDFwrapTotalData(responseModel: order[0])
                }
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderDetailCell", for: indexPath) as! PDFTableCell
                if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>] {
                    
                    if let TotalObject = order[indexPath.section-4]["Items"] as? [Dictionary<String,Any>]{
                        cell.PDFOrderDetail(responseModel: TotalObject[indexPath.row],completeDict: order[indexPath.section-4])
                    }
                    
                    
                }
                return cell
            }
        }
        
        //        else if indexPath.section > 3 {
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderDetailCell", for: indexPath) as! PDFTableCell
        //            if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>] {
        //                if let TotalObject = order[indexPath.row]["Items"] as? [Dictionary<String,Any>]{
        //                    cell.PDFOrderDetail(responseModel: TotalObject[indexPath.section-4],completeDict: order[indexPath.row])
        //                }
        //            }
        //            return cell
        //        }
        //            else {
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "PDFTotalCell", for: indexPath) as! PDFTableCell
        //            if let order = self.paymentResponse!["Orders"] as? [Dictionary<String,Any>]{
        //                cell.PDFwrapTotalData(responseModel: order[0])
        //            }
        //            return cell
        //        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == PaymentTbl{
            if indexPath.section == 0{
                return 250 ;
            }
            else  if indexPath.section == 1 || indexPath.section == 2 {
                return 40
            }
            else if indexPath.section == 3 {
                return 400
            }
            else{
                return 40 ;
            }
        }
        else{
            if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 1{
                return 100
            }
            else if indexPath.section == 5{
                return 80
            }
            else{
                return 50
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}


class PaymentSuccessTableCell: UITableViewCell{
    @IBOutlet weak var lblOrderNumber: UILabel!{
        didSet {
            self.lblOrderNumber.text = self.lblOrderNumber.text?.localized
            self.lblOrderNumber.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var totalOrderCountLbl: UILabel!{
        didSet {
            self.totalOrderCountLbl.text = self.totalOrderCountLbl.text?.localized
            self.totalOrderCountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblRefNumber: UILabel!{
        didSet {
            self.lblRefNumber.text = self.lblRefNumber.text?.localized
            self.lblRefNumber.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblShippingAddress: UILabel!{
        didSet {
            self.lblShippingAddress.text = self.lblShippingAddress.text?.localized
            self.lblShippingAddress.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblPhoneNumber: UILabel!{
        didSet {
            self.lblPhoneNumber.text = self.lblPhoneNumber.text?.localized
            self.lblPhoneNumber.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblProductAmount: UILabel!{
        didSet {
            self.lblProductAmount.text = self.lblProductAmount.text?.localized
            self.lblProductAmount.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblTotalPay: UILabel!{
        didSet {
            self.lblTotalPay.text = self.lblTotalPay.text?.localized
            self.lblTotalPay.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblShippingCharge: UILabel!{
        didSet {
            self.lblShippingCharge.text = self.lblShippingCharge.text?.localized
            self.lblShippingCharge.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var ThanksLabel: UILabel!{
        didSet {
            self.ThanksLabel.text = self.ThanksLabel.text?.localized
            self.ThanksLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var paySuccessLabel: UILabel!{
        didSet {
            self.paySuccessLabel.text = self.paySuccessLabel.text?.localized
            self.paySuccessLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var payDetailLabel: UILabel!{
        didSet {
            self.payDetailLabel.text = self.payDetailLabel.text?.localized
            self.payDetailLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    //    @IBOutlet var orderNumberLabel: UILabel!{
    //        didSet {
    //            self.orderNumberLabel.text = self.orderNumberLabel.text?.localized
    //        }
    //    }
    
    @IBOutlet var RefNumberLabel: UILabel!{
        didSet {
            self.RefNumberLabel.text = self.RefNumberLabel.text?.localized
            self.RefNumberLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ShipAddressLabel: UILabel!{
        didSet {
            self.ShipAddressLabel.text = self.ShipAddressLabel.text?.localized
            self.ShipAddressLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var PhoNumLabel: UILabel!{
        didSet {
            self.PhoNumLabel.text = self.PhoNumLabel.text?.localized
            self.PhoNumLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var ProdAmtLabel: UILabel!{
        didSet {
            self.ProdAmtLabel.text = self.ProdAmtLabel.text?.localized
            self.ProdAmtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var TotalPayLabel: UILabel!{
        didSet {
            self.TotalPayLabel.text = self.TotalPayLabel.text?.localized
            self.TotalPayLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet var OrderNumberLabel: UILabel!{
        didSet {
            self.OrderNumberLabel.text = self.OrderNumberLabel.text?.localized
            self.OrderNumberLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var ShippingChargesLabel: UILabel!{
        didSet {
            self.ShippingChargesLabel.text = self.ShippingChargesLabel.text?.localized
            self.ShippingChargesLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    // pdf invoice outlet
    
    @IBOutlet var headerLabel: UILabel!{
        didSet {
            self.headerLabel.text = self.headerLabel.text?.localized
            self.headerLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var invoiceLabel: UILabel!{
        didSet {
            self.invoiceLabel.text = self.invoiceLabel.text?.localized
            self.invoiceLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var orderDateLabel: UILabel!{
        didSet {
            self.orderDateLabel.text = self.orderDateLabel.text?.localized
            self.orderDateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var InvoiceNumLabel: UILabel!{
        didSet {
            self.InvoiceNumLabel.text = self.InvoiceNumLabel.text?.localized
            self.InvoiceNumLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var SoldByLabel: UILabel!{
        didSet {
            self.SoldByLabel.text = self.SoldByLabel.text?.localized
            self.SoldByLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var PickupAddLabel: UILabel!{
        didSet {
            self.PickupAddLabel.text = self.PickupAddLabel.text?.localized
            self.PickupAddLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet var orderNOLabel: UILabel!{
        didSet {
            self.orderNOLabel.text = self.orderNOLabel.text?.localized
            self.orderNOLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var ProductLabel: UILabel!{
        didSet {
            self.ProductLabel.text = self.ProductLabel.text?.localized
            self.ProductLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var QuantityLabel: UILabel!{
        didSet {
            self.QuantityLabel.text = self.QuantityLabel.text?.localized
            self.QuantityLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var GrossAmtLabel: UILabel!{
        didSet {
            self.GrossAmtLabel.text = self.GrossAmtLabel.text?.localized
            self.GrossAmtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var DiscountLabel: UILabel!{
        didSet {
            self.DiscountLabel.text = self.DiscountLabel.text?.localized
            self.DiscountLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var TotalLabel: UILabel!{
        didSet {
            self.TotalLabel.text = self.TotalLabel.text?.localized
            self.TotalLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var orderNOValLabel: UILabel!{
        didSet {
            self.orderNOValLabel.text = self.orderNOValLabel.text?.localized
            self.orderNOValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var ProductValLabel: UILabel!{
        didSet {
            self.ProductValLabel.text = self.ProductValLabel.text?.localized
            self.ProductValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var QuantityValLabel: UILabel!{
        didSet {
            self.QuantityValLabel.text = self.QuantityValLabel.text?.localized
            self.QuantityValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var GrossAmtValLabel: UILabel!{
        didSet {
            self.GrossAmtValLabel.text = self.GrossAmtValLabel.text?.localized
            self.GrossAmtValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var DiscounValtLabel: UILabel!{
        didSet {
            self.DiscounValtLabel.text = self.DiscounValtLabel.text?.localized
            self.DiscounValtLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var TotalValLabel: UILabel!{
        didSet {
            self.TotalValLabel.text = self.TotalValLabel.text?.localized
            self.TotalValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet var TotalQuantityLabel: UILabel!{
        didSet {
            self.TotalQuantityLabel.text = self.TotalQuantityLabel.text?.localized
            self.TotalQuantityLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var TaxLabel: UILabel!{
        didSet {
            self.TaxLabel.text = self.TaxLabel.text?.localized
            self.TaxLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var ShippingLabel: UILabel!{
        didSet {
            self.ShippingLabel.text = self.ShippingLabel.text?.localized
            self.ShippingLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var TotalPriceLabel: UILabel!{
        didSet {
            self.TotalPriceLabel.text = self.TotalPriceLabel.text?.localized
            self.TotalPriceLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet var Add1Label: UILabel!{
        didSet {
            self.Add1Label.text = self.Add1Label.text?.localized
            self.Add1Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add2Label: UILabel!{
        didSet {
            self.Add2Label.text = self.Add2Label.text?.localized
            self.Add2Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add3Label: UILabel!{
        didSet {
            self.Add3Label.text = self.Add3Label.text?.localized
            self.Add3Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add4Label: UILabel!{
        didSet {
            self.Add4Label.text = self.Add4Label.text?.localized
            self.Add4Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add5Label: UILabel!{
        didSet {
            self.Add5Label.text = self.Add5Label.text?.localized
            self.Add5Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add6Label: UILabel!{
        didSet {
            self.Add6Label.text = self.Add6Label.text?.localized
            self.Add6Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add7Label: UILabel!{
        didSet {
            self.Add7Label.text = self.Add7Label.text?.localized
            self.Add7Label.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var Add8Label: UILabel!{
        didSet {
            self.Add8Label.text = self.Add8Label.text?.localized
            self.Add8Label.font = UIFont(name: appFont, size: 15.0)
            
        }
    }
    
    
    
    
    @IBOutlet var TaxValLabel: UILabel!{
        didSet {
            self.TaxValLabel.text = self.TaxValLabel.text?.localized
            self.TaxValLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    func orderData(payStatusModel: Dictionary<String, Any>,index: IndexPath){
        if let ordersArray = payStatusModel["Orders"] as? [Any], ordersArray.count > 0 {
            if let order = ordersArray[index.row] as? Dictionary<String, Any> {
                self.lblOrderNumber.text = "#\(order["CustomOrderNumber"] as? String ?? "")"
                self.OrderNumberLabel.text = "Order Number \(index.row + 1)"
            }
        } else {
            self.lblOrderNumber.text = ""
            self.OrderNumberLabel.text = "Order Number "
        }
    }
    
    func setOrderData(payStatusModel: Dictionary<String, Any>) {
        if let ordersArray = payStatusModel["Orders"] as? [Any], ordersArray.count > 0 {
            //            if let order = ordersArray[index.row] as? Dictionary<String, Any> {
            //                if let itemArray = order["Items"] as? [Dictionary<String, Any>], itemArray.count > 0 {
            self.totalOrderCountLbl.text = "Total Order Count: \(ordersArray.count)"
            //                }
            //            }
        } else {
            self.totalOrderCountLbl.text = "Total Order Count: 1"
        }
    }
    
    func updateUIData(payStatusModel: Dictionary<String, Any>) {
        
        self.lblRefNumber.text = payStatusModel["ReferenceNumber"] as? String ?? ""
        
        
        self.lblShippingAddress.text = payStatusModel["ShippingAddress"] as? String ?? ""
        self.lblPhoneNumber.text = payStatusModel["PhoneNumber"] as? String ?? ""
        if let number = lblPhoneNumber.text {
            if number.hasPrefix("0095") {
                lblPhoneNumber.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95")
            }
        }
        self.lblProductAmount.text = payStatusModel["ProductAmount"] as? String ?? "0.0 MMK"
        self.lblProductAmount.amountAttributedString()
        
        self.TaxValLabel.text = payStatusModel["Tax"] as? String ?? "0.0 MMK"
        self.TaxValLabel.amountAttributedString()
        
        //        self.lblTotalPay.text = payStatusModel["TotalPaidAmount"] as? String ?? "0.0 MMK"
        //        self.lblTotalPay.amountAttributedString()
        
        
        
        var productPrice = (payStatusModel["TotalPaidAmount"] as? String)?.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        
        let attrString = NSMutableAttributedString(string: productPrice!)
        let nsRange = NSString(string: productPrice!).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.lblTotalPay.attributedText = attrString
        
        
        
        self.lblShippingCharge.text = payStatusModel["ShippingCharges"] as? String ?? "0.0 MMK"
        self.lblShippingCharge.amountAttributedString()
        
    }
}


