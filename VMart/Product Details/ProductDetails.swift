//
//  ProductDetails.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/19/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import WebKit



class ProductDetails: ZAYOKBaseViewController {
    
    var cartBarButton: UIBarButtonItem!
    var searchButton: UIBarButtonItem!
    
    var textView2: UILabel!{
        didSet{
            textView2.text = textView2.text?.localized
            textView2.font = UIFont(name: appFont, size: 15.0)
        }
    }
    var shortDescriptionLbl: UILabel!{
        didSet{
            shortDescriptionLbl.text = shortDescriptionLbl.text?.localized
            shortDescriptionLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    var allowQtyTF: UITextField!{
        didSet{
            self.allowQtyTF.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    let pickerView = UIPickerView()
    
    var allowQuantity = [String]()
    var wishListSelected = false
    var productDetailsInfo : ProductDetailsInfo?   = nil
    var aPIManager = APIManager()
    var productId = 0
    var imageUrlArray = [String]()
    var fullImageUrlArray = [String]()
    var selectedIndex = 0
    var checked = [Bool]()
    var dictionaryArray = NSMutableArray()
    var tableContentHeightsArr : [CGFloat] = [0.0, 0.0]
    var outerY:CGFloat = 0
    var DyHeight:CGFloat = 50
    var relatedProductDetailsInfoArray = CategoryDetailsInfo()
    var sizeParameters = Dictionary<String, Any>()
    var colorParameters = Dictionary<String, Any>()
    var measurementParameters = Dictionary<String, Any>()
    var numberOfSecondCollection:CGFloat!
    var numberOfSecondCollectionLastIndex:Int = 0
    var moreImagesCount:Int = 0
    var productName = ""
    
    weak var delegate: AddRemoveWishListProtocol?
    
    @IBOutlet var GSMHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var gsmBtn: UIButton!{
        didSet {
            gsmBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var underLineLbl: UILabel!{
        didSet {
            self.underLineLbl.isHidden = true
        }
    }
    
    @IBOutlet weak var productCollectionView1: UICollectionView!
    @IBOutlet weak var productCollectionView2: UICollectionView!
    @IBOutlet var productNameLbl: UILabel!{
        didSet {
            self.productNameLbl.text = self.productNameLbl.text?.localized
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var oldPriceLbl: UILabel!{
        didSet {
            self.oldPriceLbl.text = self.oldPriceLbl.text?.localized
            self.oldPriceLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productPriceLbl: UILabel!{
        didSet {
            self.productPriceLbl.text = self.productPriceLbl.text?.localized
            self.productPriceLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productDeliveryDateLbl: UILabel!{
        didSet {
            self.productDeliveryDateLbl.text = self.productDeliveryDateLbl.text?.localized
            self.productDeliveryDateLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productRatingReviewLbl: UILabel!{
        didSet {
            self.productRatingReviewLbl.text = self.productRatingReviewLbl.text?.localized
            self.productRatingReviewLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productDiscountPercentageLbl: UILabel!{
        didSet {
            self.productDiscountPercentageLbl.text = self.productDiscountPercentageLbl.text?.localized
            self.productDiscountPercentageLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var comingSoonLbl: UILabel!{
        didSet {
            self.comingSoonLbl.text = self.comingSoonLbl.text?.localized
            self.comingSoonLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet var ratingStarsLbl: UILabel!
    @IBOutlet var availabilityStockLbl: UILabel!{
        didSet{
            availabilityStockLbl.text = availabilityStockLbl.text?.localized
            availabilityStockLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var mainView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomView2: UIView!
    @IBOutlet var allowQuantityView: UIView!
    @IBOutlet var sizeMainView: UIView!
    @IBOutlet var measurementsView: UIView!
    @IBOutlet var productAttributesView: UIView!
    @IBOutlet var allowQuantityViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var producAttributesViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var sizeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var measurementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentTableVIewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentTableView: UITableView!
    
    @IBOutlet var AddtoCartBtn: UIButton!{
        didSet {
            AddtoCartBtn.setTitle("ADD TO CART".localized, for: .normal)
            AddtoCartBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var BuyNowBtn: UIButton!{
        didSet {
            BuyNowBtn.setTitle("BUY NOW".localized, for: .normal)
            BuyNowBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var NotifyBtn: UIButton! {
        didSet {
            NotifyBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
            self.NotifyBtn.setTitle("NOTIFY ME".localized, for: .normal)
            self.NotifyBtn.layer.cornerRadius = 4
            self.NotifyBtn.layer.masksToBounds = true
            NotifyBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    @IBOutlet weak var wishListBtn: UIButton!
    @IBOutlet weak var ShareBtn: UIButton!
    
    
    @IBOutlet var webView:WKWebView?
    
    var barButton: UIBarButtonItem?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.searchButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.searchAction(_:)))
        self.navigationItem.rightBarButtonItems = [ self.cartBarButton, self.searchButton]
        //        self.callProductDetailsAPI()
        
        //        self.ShareBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        //        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_cart_outline.png"), style: .plain, target: self, action: #selector(self.cartAction(_:)))
        //        let rightBarButton = self.navigationItem.rightBarButtonItem
        //        rightBarButton?.setBadge(text: "4")
        
    }
    
    @objc func webArenaBackAction(){
        if self.webView?.isHidden == false{
            self.webView?.isHidden = true
            self.webView?.removeFromSuperview()
            self.navigationItem.leftBarButtonItem = nil
            self.viewWillAppear(true)
        }
    }
    
    
    @IBAction func GSMArenaClick(_ sender: UIButton) {
        self.GSMHeightConstraint.constant = 30
        self.configureWebView(UrlStr: self.productDetailsInfo?.productReviewOverviewModel?.RatingUrlFromGSMArena as String? ?? "")
    }
    
    
    @IBAction func chatAndCall(_ sender: UIButton){ 
        if let viewController = UIStoryboard(name: "Authorization", bundle: nil).instantiateViewController(withIdentifier: "AuthController") as? UINavigationController{
            UserDefaults.standard.set(sender.tag, forKey: "KeyAudioOrVideo")  //Integer
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView?.isHidden = true
        mainView.isHidden = true
        bottomView.isHidden = true
        self.bottomView2.isHidden = true
        bottomView.layer.shadowOpacity = 0.20
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomView.layer.shadowRadius = 3
        bottomView.layer.shadowColor = UIColor.black.cgColor
        bottomView.layer.masksToBounds = false
        self.navigationItem.setMarqueLabelInNavigation(TextStr: self.productName, count: self.productName.count)
        self.callProductDetailsAPI()
        
    }
    
    func callProductDetailsAPI() {
        for view in self.sizeMainView.subviews {
            view.removeFromSuperview()
        }
        for view in self.measurementsView.subviews {
            view.removeFromSuperview()
        }
        for view in self.productAttributesView.subviews {
            DispatchQueue.main.async {
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                view.removeFromSuperview()
            }
            
        }
        producAttributesViewHeightConstraint.constant = 5
        sizeViewHeightConstraint.constant = 0
        measurementViewHeightConstraint.constant = 0
        outerY = 0
        imageUrlArray = []
        fullImageUrlArray = []
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.getProductDetails()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailsToImagesCollectionSegue" {
            if let vc = segue.destination as? ProductImagesCollection {
                vc.fullImageUrlArray = self.fullImageUrlArray
                vc.selectedIndex = self.selectedIndex
                vc.wishListSelected = self.wishListSelected
                vc.productId = self.productId
                vc.productName = self.productName
                vc.delegate = self
            }
        }
    }
    
    func getProductDetails() {
        self.aPIManager.loadProductDetails(self.productId, onSuccess: { [weak self] getProductsDetails in
            self?.productDetailsInfo = getProductsDetails
            if (self?.productDetailsInfo?.addToCartModel?.AllowedQuantities!.count)! > 0{
                self?.allowQuantity.removeAll()
                for obj in (self?.productDetailsInfo?.addToCartModel!.AllowedQuantities)!{
                    if let value = obj as? NSDictionary{
                        self?.allowQuantity.append(value["Value"] as? String ?? "")
                    }
                }
                self?.pickerView.reloadAllComponents()
            }
            if self?.productDetailsInfo?.FullDescription == "" {
                if let mainview = self?.mainView, let bottomview = self?.bottomView {
                    mainview.isHidden = false
                    bottomview.isHidden = false
                    self?.bottomView2.isHidden = true
                }
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            self?.mainView.isHidden = false
            self?.bottomView.isHidden = false
            self?.bottomView2.isHidden = true
            self?.updateProductDetails()
            self?.updateDynamicProductAttributes()
            self?.loadRelatedProduct()
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Error!".localized, withDescription: message ?? "", onController: self!)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self?.performSegue(withIdentifier: "ProductDetailToHome", sender: self)
                })
                alertVC.addAction(action: [ok])
        })
    }
    
    func loadRelatedProduct(){
        self.aPIManager.loadRelatedProduct(productId, onSuccess:{ [weak self] getRelatedProductDetails in
            self?.relatedProductDetailsInfoArray = getRelatedProductDetails
            if self?.relatedProductDetailsInfoArray.productsInfo.count == 0 {
                self?.contentTableVIewHeightConstraint.constant = 0
            } else{
                self?.contentTableVIewHeightConstraint.constant = SCREEN_HEIGHT/2.5 + 20
            }
            self?.contentTableView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: {[weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    func updateProductDetails() {
        if let pictureArray = self.productDetailsInfo?.PictureModels {
            for picturs in pictureArray {
                if let imageUrl = (picturs as AnyObject).value(forKeyPath: "ImageUrl") as? String {
                    self.imageUrlArray.append(imageUrl)
                }
                if let fullSizeImageUrl = (picturs as AnyObject).value(forKeyPath: "FullSizeImageUrl") as? String {
                    self.fullImageUrlArray.append(fullSizeImageUrl)
                }
            }
        }
        
        let totalCount = CGFloat(self.imageUrlArray.count)
        self.numberOfSecondCollection = SCREEN_WIDTH/70
        if totalCount > self.numberOfSecondCollection {
            self.numberOfSecondCollectionLastIndex = Int(self.numberOfSecondCollection)
            self.moreImagesCount = self.imageUrlArray.count - self.numberOfSecondCollectionLastIndex
        }
        
        checked = Array(repeating: false, count: imageUrlArray.count)
        if !self.checked.isEmpty {
            checked[0] = true
        }
        
        productCollectionView2.delegate = self
        productCollectionView2.dataSource = self
        productCollectionView1.reloadData()
        productCollectionView2.reloadData()
        
        productNameLbl.text = (self.productDetailsInfo?.Name ?? "") as String
        //        let rating = String(format: "%.0f", self.productDetailsInfo?.productReviewOverviewModel?.RatingSum ?? "")
        let review = String(format: "%d", self.productDetailsInfo?.productReviewOverviewModel?.TotalReviews ?? "")
        let deliveryDate = (self.productDetailsInfo?.DeliveryDate ?? "") as String
        let ratingTxt = "Ratings".localized
        let reviewsTxt = "Reviews".localized
        productRatingReviewLbl.text = review + " \(ratingTxt) & " + review + " \(reviewsTxt)"
        let deliveryTime = "Delivery Time".localized
        productDeliveryDateLbl.text = "\(deliveryTime): " + deliveryDate
        print("ssss",String(format: "%.1f *", self.productDetailsInfo?.productReviewOverviewModel?.RatingSum ?? ""))
        print("ssss",String(format: "%.0f *", self.productDetailsInfo?.productReviewOverviewModel?.RatingSum ?? ""))
        ratingStarsLbl.text = String(format: "%.1f ", self.productDetailsInfo?.productReviewOverviewModel?.RatingSum ?? "")
        
        if self.productDetailsInfo?.productReviewOverviewModel?.RatingUrlFromGSMArena == "" || self.productDetailsInfo?.productReviewOverviewModel?.RatingUrlFromGSMArena == nil{
            self.GSMHeightConstraint.constant = 0
            self.gsmBtn.isHidden = true
            self.underLineLbl.isHidden = true
        }
        else{
            self.gsmBtn.isHidden = false
            self.underLineLbl.isHidden = false
            self.GSMHeightConstraint.constant = 30
        }
        //        self.configureWebView(UrlStr: self.productDetailsInfo?.productReviewOverviewModel?.RatingUrlFromGSMArena as String? ?? "")
        //        AppUtility.showToastlocal(message: self.productDetailsInfo?.productReviewOverviewModel?.RatingUrlFromGSMArena as String? ?? "" , view: self.view)
        
        let stockQuantityLbl = self.productDetailsInfo?.quantityModel?.StockQuantity
        let availOutStock = "Availability : Out-Of-Stock".localized
        let availInStock = "Availability : In-Stock".localized
        let availability = "Availability :".localized
        
        if stockQuantityLbl == 0 {
            availabilityStockLbl.text = availOutStock.localized
        }
        else if stockQuantityLbl! < 6 {
            availabilityStockLbl.text = String(format: "\(availability) : %d", self.productDetailsInfo?.quantityModel?.StockQuantity ?? "")
        }
        else {
            availabilityStockLbl.text = availInStock.localized
        }
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: self.productDetailsInfo?.productPriceModel?.Price ?? "", oldPrice: self.productDetailsInfo?.productPriceModel?.OldPrice ?? "", priceWithDiscount: self.productDetailsInfo?.productPriceModel?.PriceWithDiscount ?? "")
        
        var productPrice = self.productDetailsInfo?.productPriceModel?.Price.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        let attrString = NSMutableAttributedString(string: productPrice ?? "")
        let nsRange = NSString(string: productPrice ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.productPriceLbl.attributedText = attrString
        
        var oldPrice = self.productDetailsInfo?.productPriceModel?.OldPrice.replacingOccurrences(of: "MMK", with: "")
        oldPrice = oldPrice! + mmkText
        
        if Price == "" {
            if self.productDetailsInfo?.productPriceModel?.DiscountPercentage != 0 {
                self.productDiscountPercentageLbl.text = "\((self.productDetailsInfo?.productPriceModel?.DiscountPercentage) ?? 0)% off - \(self.productDetailsInfo?.productPriceModel?.DiscountOfferName ?? "")"
            }
            
            if self.productDetailsInfo?.productPriceModel?.PriceWithDiscount == "" {
                var productPrice2 = self.productDetailsInfo?.productPriceModel?.Price.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2! + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2 ?? "")
                let nsRange = NSString(string: productPrice2 ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                self.productPriceLbl.attributedText = attrStr
                
                
                
                
                var oldPri = oldPrice?.replacingOccurrences(of: "MMK", with: "")
                oldPri = oldPri! + mmkText
                let attrString = NSMutableAttributedString(string: oldPri ?? "", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: oldPri ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                self.oldPriceLbl.attributedText = attrString
                
                
                //                let attrString = NSMutableAttributedString(string: oldPrice ?? "", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: productPrice ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                self.oldPriceLbl.attributedText = attrString
                
            } else {
                var productPrice2 = self.productDetailsInfo?.productPriceModel?.PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2! + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2 ?? "")
                let nsRange = NSString(string: productPrice2 ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                self.productPriceLbl.attributedText = attrStr
                
                //                let attrString = NSMutableAttributedString(string: productPrice ?? "", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: productPrice ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                attrString.addAttributes([NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue], range: nsRange2)
                //                self.oldPriceLbl.attributedText = attrString
                
                
                
                var oldPri = oldPrice?.replacingOccurrences(of: "MMK", with: "")
                oldPri = oldPri! + mmkText
                let attrString = NSMutableAttributedString(string: oldPri ?? "", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: oldPri ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                self.oldPriceLbl.attributedText = attrString
            }
        }
        
        if self.productDetailsInfo?.addToCartModel?.DisableWishlistButton == true {
            self.wishListBtn.isHidden = true
        }
        
        if self.productDetailsInfo?.addToCartModel?.IsWishList == true {
            self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
            wishListSelected = true
        } else {
            self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
            wishListSelected = false
        }
        
        //        if self.productDetailsInfo?.addToCartModel?.DisableBuyButton == true {
        //            self.AddtoCartBtn.isEnabled = false
        //            self.BuyNowBtn.isEnabled = false
        //        }
        
        if self.productDetailsInfo?.quantityModel?.StockQuantity == 0 {
            self.bottomView.isHidden = true
            self.bottomView2.isHidden = false
            //            self.showAlert(alertTitle: "Information".localized, description: "Out of stock")
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Out of stock".localized, onController: self)
                let tryAgain = SAlertAction()
                tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                    self.mainView.alpha = 0.7
                })
                alertVC.addAction(action: [tryAgain])
            }
        }
    }
    
    func updateDynamicProductAttributes() {
        
        //        for i in 0 ..< (self.productDetailsInfo?.productAttributes?.count)! {
        //            if self.productDetailsInfo?.productAttributes?[i].Name == "Size" {
        //                var outerYForSizeView:CGFloat = 5
        //                if (self.productDetailsInfo?.productAttributes?[i].Values?.count)! > 0 {
        //                    let sizeLineLbl = UILabel(frame: CGRect(x: 0, y: outerYForSizeView, width: SCREEN_WIDTH, height: 2))
        //                    sizeLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        //                    sizeMainView.addSubview(sizeLineLbl)
        //                    outerYForSizeView += 5
        //
        //                    let sizeLbl = UILabel(frame: CGRect(x: 10, y: outerYForSizeView, width: SCREEN_WIDTH-20, height: 20))
        //                    sizeLbl.textColor = UIColor.darkGray
        //                    sizeLbl.font = UIFont(name: SEMI_BOLD_FONT, size: 13)
        //                    sizeLbl.text = "Size"
        //                    sizeMainView.addSubview(sizeLbl)
        //                    outerYForSizeView += 30
        //
        //                    let sizeView  = UIView(frame:CGRect(x:0, y:outerYForSizeView, width:SCREEN_WIDTH, height:40))
        //                    sizeView.tag = i
        //                    sizeMainView.addSubview(sizeView)
        //                    let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
        //                    scrollView.showsHorizontalScrollIndicator = false
        //
        //                    var internalX:CGFloat = 20
        //                    for j in 0 ..< (self.productDetailsInfo?.productAttributes?[i].Values?.count)! {
        //                        let sizeNameBtn = UIButton(frame: CGRect(x:internalX, y:0, width:45, height:35))
        //                        sizeNameBtn.setTitle(self.productDetailsInfo?.productAttributes?[i].Values?[j].Name, for: .normal)
        //                        sizeNameBtn.titleLabel?.font = UIFont(name: REGULAR_FONT, size: 12)
        //                        sizeNameBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        //                        sizeNameBtn.layer.borderWidth = 1
        //                        sizeNameBtn.layer.cornerRadius = 4
        //                        sizeNameBtn.setTitleColor(UIColor.black, for: .normal)
        //                        sizeNameBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
        //                        sizeNameBtn.tag = 100 + j
        //                        let btnSize = sizeNameBtn.intrinsicContentSize.width
        //                        if btnSize > 42 {
        //                            sizeNameBtn.frame.size.width = sizeNameBtn.intrinsicContentSize.width + 10
        //                            internalX += sizeNameBtn.intrinsicContentSize.width + 20
        //                        } else {
        //                            internalX += 55
        //                        }
        //                        scrollView.addSubview(sizeNameBtn)
        //                        sizeNameBtn.addTarget(self, action: #selector(self.tapOnSize(sender:)), for: .touchUpInside)
        //                    }
        //                    scrollView.contentSize = CGSize(width: internalX, height: 40)
        //                    sizeView.addSubview(scrollView)
        //                    outerYForSizeView += 40
        //                    sizeViewHeightConstraint.constant = outerYForSizeView
        //                }
        //            }
        //        }
        
        if (self.productDetailsInfo?.productAttributes?.count)! > 0 {
            for i in 0 ..< (self.productDetailsInfo?.productAttributes?.count)! {
                if (self.productDetailsInfo?.productAttributes?[i].Values?.count)! > 0 {
                    if self.productDetailsInfo?.productAttributes?[i].AttributeControlType == 40 {
                        if (self.productDetailsInfo?.productAttributes?[i].Values?.count)! > 0 {
                            let colorLineLbl = UILabel(frame: CGRect(x: 0, y: outerY, width: SCREEN_WIDTH, height: 2))
                            colorLineLbl.font = UIFont(name: appFont, size: 15.0)
                            colorLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
                            productAttributesView.addSubview(colorLineLbl)
                            outerY += 10
                            
                            let colorLbl = UILabel(frame: CGRect(x: 10, y: outerY, width: SCREEN_WIDTH-20, height: 20))
                            colorLbl.textColor = UIColor.darkGray
                            colorLbl.font = UIFont(name: appFont, size: 15.0)
                            if self.productDetailsInfo?.productAttributes?[i].TextPrompt != nil && self.productDetailsInfo?.productAttributes?[i].Name != "" {
                                colorLbl.text = self.productDetailsInfo?.productAttributes?[i].TextPrompt as String?
                            } else {
                                colorLbl.text = self.productDetailsInfo?.productAttributes?[i].Name as String?
                            }
                            productAttributesView.addSubview(colorLbl)
                            outerY += 30
                            
                            let colorView  = UIView(frame:CGRect(x:0, y:outerY, width:SCREEN_WIDTH, height:42))
                            colorView.tag = i
                            productAttributesView.addSubview(colorView)
                            let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 42))
                            scrollView.showsHorizontalScrollIndicator = false
                            
                            var internalX:CGFloat = 20
                            var val = 1
                            for j in 0 ..< (self.productDetailsInfo?.productAttributes?[i].Values?.count)! {
                                let colorBorderView  = UIView(frame:CGRect(x:internalX, y:0, width:42, height:42))
                                colorBorderView.layer.borderWidth = 1.5
                                colorBorderView.layer.cornerRadius = 21
                                colorBorderView.clipsToBounds = true
                                colorBorderView.contentMode = .scaleAspectFill
                                colorBorderView.tag = 100 + j
                                let colorBtn = UILabel(frame: CGRect(x:3, y:3, width:36, height:36))
                                colorBtn.font = UIFont(name: appFont, size: 15.0)
                                colorBtn.backgroundColor = AppUtility.hexStringToUIColor(hex: self.productDetailsInfo?.productAttributes?[i].Values?[j].ColorSquaresRgb ?? "#000000")
                                colorBtn.layer.cornerRadius = 18
                                colorBtn.clipsToBounds = true
                                colorBtn.contentMode = .scaleAspectFill
                                colorBorderView.layer.borderColor = UIColor.clear.cgColor
                                
                                if (self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected)! {
                                    if val == 1 {
                                        if self.productDetailsInfo?.productAttributes?[i].Values?[j].Name == "White" {
                                            colorBorderView.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                                        } else {
                                            colorBorderView.layer.borderColor = AppUtility.hexStringToUIColor(hex: self.productDetailsInfo?.productAttributes?[i].Values?[j].ColorSquaresRgb ?? "#000000").cgColor
                                        }
                                    }
                                    else {
                                        self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected = false
                                    }
                                    val += 1
                                }
                                else {
                                    colorBorderView.layer.borderColor = UIColor.clear.cgColor
                                }
                                
                                if self.productDetailsInfo?.productAttributes?[i].Values?[j].Name == "White" {
                                    colorBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                                    colorBtn.layer.borderWidth = 1.5
                                }
                                internalX += 50
                                colorBorderView.addSubview(colorBtn)
                                scrollView.addSubview(colorBorderView)
                                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnColor))
                                tapGesture.view?.tag = j
                                colorBorderView.isUserInteractionEnabled = true
                                colorBorderView.addGestureRecognizer(tapGesture)
                            }
                            scrollView.contentSize = CGSize(width: internalX, height: 42)
                            colorView.addSubview(scrollView)
                            outerY += 60
                        }
                    }
                    else {
                        let productAttributeLineLbl = UILabel(frame: CGRect(x: 0, y: outerY, width: SCREEN_WIDTH, height: 2))
                        productAttributeLineLbl.font = UIFont(name: appFont, size: 15.0)
                        productAttributeLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
                        productAttributesView.addSubview(productAttributeLineLbl)
                        outerY += 10
                        
                        let productAttributeTitleLbl = UILabel(frame: CGRect(x: 10, y: outerY, width: SCREEN_WIDTH-20, height: 20))
                        productAttributeTitleLbl.font = UIFont(name: appFont, size: 15.0)
                        productAttributeTitleLbl.textColor = UIColor.darkGray
                        productAttributeTitleLbl.font = UIFont(name: appFont, size: 14)
                        if self.productDetailsInfo?.productAttributes?[i].TextPrompt != nil && self.productDetailsInfo?.productAttributes?[i].Name != "" {
                            productAttributeTitleLbl.text = self.productDetailsInfo?.productAttributes?[i].TextPrompt as String?
                        } else {
                            productAttributeTitleLbl.text = self.productDetailsInfo?.productAttributes?[i].Name as String?
                        }
                        productAttributesView.addSubview(productAttributeTitleLbl)
                        outerY += 30
                        
                        let productAttributeView  = UIView(frame:CGRect(x:0, y:outerY, width:SCREEN_WIDTH, height:40))
                        productAttributeView.tag = i
                        productAttributesView.addSubview(productAttributeView)
                        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
                        scrollView.showsHorizontalScrollIndicator = false
                        
                        var internalX:CGFloat = 20
                        var val = 1
                        for j in 0 ..< (self.productDetailsInfo?.productAttributes?[i].Values?.count)! {
                            let attributeNameBtn = UIButton(frame: CGRect(x:internalX, y:0, width:45, height:35))
                            attributeNameBtn.setTitle(self.productDetailsInfo?.productAttributes?[i].Values?[j].Name, for: .normal)
                            attributeNameBtn.titleLabel?.font = UIFont(name: appFont, size: 12)
                            attributeNameBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
                            attributeNameBtn.layer.borderWidth = 1
                            attributeNameBtn.layer.cornerRadius = 4
                            attributeNameBtn.setTitleColor(UIColor.black, for: .normal)
                            attributeNameBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                            
                            if (self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected)! {
                                if val == 1 {
                                    attributeNameBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1), for: .normal)
                                    attributeNameBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 235, alpha: 1).cgColor
                                }
                                else {
                                    self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected = false
                                }
                                val += 1
                            }
                            else {
                                attributeNameBtn.setTitleColor(UIColor.black, for: .normal)
                                attributeNameBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                            }
                            
                            attributeNameBtn.tag = 100 + j
                            let btnSize = attributeNameBtn.intrinsicContentSize.width
                            if btnSize > 42 {
                                attributeNameBtn.frame.size.width = attributeNameBtn.intrinsicContentSize.width + 10
                                internalX += attributeNameBtn.intrinsicContentSize.width + 20
                            } else {
                                internalX += 55
                            }
                            scrollView.addSubview(attributeNameBtn)
                            attributeNameBtn.addTarget(self, action: #selector(self.tapOnSize(sender:)), for: .touchUpInside)
                        }
                        scrollView.contentSize = CGSize(width: internalX, height: 40)
                        productAttributeView.addSubview(scrollView)
                        outerY += 55
                    }
                }
            }
        }
        
        self.loadProductDetailsPagePrice()
        
        if self.productDetailsInfo?.ShortDescription != "" {
            let detailsLineLbl = UILabel(frame: CGRect(x: 0, y: outerY, width: SCREEN_WIDTH, height: 2))
            detailsLineLbl.font = UIFont(name: appFont, size: 15.0)
            detailsLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            productAttributesView.addSubview(detailsLineLbl)
            outerY += 20
            
            let detailsLbl = UILabel(frame: CGRect(x: 10, y: outerY, width: SCREEN_WIDTH-20, height: 20))
            detailsLbl.textColor = UIColor.darkGray
            detailsLbl.font = UIFont(name: appFont, size: 15.0)
            detailsLbl.text = "Short Description".localized
            productAttributesView.addSubview(detailsLbl)
            outerY += 30
            
            shortDescriptionLbl = UILabel(frame: CGRect(x: 20, y: outerY, width: SCREEN_WIDTH-30, height: 20))
            shortDescriptionLbl.lineBreakMode = .byWordWrapping
            shortDescriptionLbl.numberOfLines = 0
            shortDescriptionLbl.font = UIFont(name: appFont, size: 15.0)!
            shortDescriptionLbl.textAlignment = .left
            shortDescriptionLbl.text = self.productDetailsInfo?.ShortDescription as String?
            if let htlm = self.productDetailsInfo?.ShortDescription.html {
                shortDescriptionLbl.attributedText = htlm //loadHTMLString(self.productDetailsInfo?.FullDescription ?? "", baseURL: nil)
                productAttributesView.addSubview(shortDescriptionLbl)
            }
            self.updateShortDescriptionView()
            outerY += 20
        }
        
        if self.productDetailsInfo?.FullDescription != "" {
            let descriptionLineLbl = UILabel(frame: CGRect(x: 0, y: outerY, width: SCREEN_WIDTH, height: 2))
            descriptionLineLbl.font = UIFont(name: appFont, size: 15.0)
            descriptionLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            productAttributesView.addSubview(descriptionLineLbl)
            outerY += 20
            
            let descriptionLbl = UILabel(frame: CGRect(x: 10, y: outerY, width: SCREEN_WIDTH-20, height: 20))
            descriptionLbl.textColor = UIColor.darkGray
            descriptionLbl.font = UIFont(name: appFont, size: 15.0)
            descriptionLbl.text = "Description".localized
            productAttributesView.addSubview(descriptionLbl)
            outerY += 30
            
            textView2 = UILabel(frame: CGRect(x:10, y:outerY, width:SCREEN_WIDTH - 20, height:100))
            textView2.lineBreakMode = .byWordWrapping
            textView2.numberOfLines = 0
            textView2.font = UIFont(name: appFont, size: 15.0)!
            textView2.textAlignment = .left
            if let htlm = self.productDetailsInfo?.FullDescription.html,let strValue = self.productDetailsInfo?.FullDescription.htmlToString {
                textView2.attributedText = htlm //loadHTMLString(self.productDetailsInfo?.FullDescription ?? "", baseURL: nil)
                //                textView2.text = strValue.replacingOccurrences(of: "\n\t•\t", with: "").replacingOccurrences(of: "\t•\t", with: "")
                productAttributesView.addSubview(textView2)
            }
            self.updateTextView()
        }
        
        if (self.productDetailsInfo?.productSpecifications?.count ?? 0) > 0 {
            let detailsLineLbl = UILabel(frame: CGRect(x: 0, y: outerY, width: SCREEN_WIDTH, height: 2))
            detailsLineLbl.font = UIFont(name: appFont, size: 15.0)
            detailsLineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            productAttributesView.addSubview(detailsLineLbl)
            outerY += 10
            
            let detailsLbl = UILabel(frame: CGRect(x: 10, y: outerY, width: SCREEN_WIDTH-20, height: 20))
            detailsLbl.textColor = UIColor.darkGray
            detailsLbl.font = UIFont(name: appFont, size: 15.0)
            detailsLbl.text = "Details".localized
            productAttributesView.addSubview(detailsLbl)
            outerY += 30
            
            for j in 0 ..< (self.productDetailsInfo?.productSpecifications?.count ?? 0) {
                
                //                let myView = Bundle.loadView(fromNib: "MyView", withType: MyView.self)
                
                if let myView = UINib(nibName: "HomeCategoriesHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as? CustomDetailView{
                    myView.frame = CGRect(x:20, y:outerY, width:SCREEN_WIDTH-30, height:DyHeight)
                    myView.UpdatedLabelData(leftStr: (self.productDetailsInfo?.productSpecifications?[j].SpecificationAttributeName ?? ""), rightStr: (self.productDetailsInfo?.productSpecifications?[j].ValueRaw ?? ""))
                    myView.frame = CGRect(x:20, y:outerY, width:SCREEN_WIDTH-30, height:myView.rightNameLabel.frame.height)
                    print(myView)
                    //                    if j%2 == 0{
                    //                        myView.backgroundColor = UIColor.gray
                    //                    }
                    //                    else{
                    //                        myView.backgroundColor = UIColor.yellow
                    //                    }
                    
                    print("Height: \(myView.rightNameLabel.frame.height + 50)")
                    productAttributesView.addSubview(myView)
                    let detailsLabelHeight = AppUtility.heightForView(text: myView.rightNameLabel.text ?? "", font: UIFont(name: appFont, size: 14)!, width: SCREEN_WIDTH-30)
                    print(detailsLabelHeight)
                    if detailsLabelHeight < 20 {
                        outerY += detailsLabelHeight + myView.rightNameLabel.frame.height
                    }
                    //                    else {
                    //                        outerY += detailsLabelHeight + 20
                    //                    }
                }
                
                
                //                let specificationNameLbl  = UILabel(frame:CGRect(x:20, y:outerY, width:SCREEN_WIDTH-30, height:20))
                //                specificationNameLbl.lineBreakMode = .byWordWrapping
                //                specificationNameLbl.numberOfLines = 0
                //                specificationNameLbl.font = UIFont(name: appFont, size: 14.0)
                //                specificationNameLbl.attributedText = ((self.productDetailsInfo?.productSpecifications?[j].SpecificationAttributeName ?? "")  + "          : " + (self.productDetailsInfo?.productSpecifications?[j].ValueRaw ?? "")).html
                //                let detailsLabelHeight = AppUtility.heightForView(text: specificationNameLbl.text ?? "", font: UIFont(name: appFont, size: 14)!, width: SCREEN_WIDTH-30)
                //                specificationNameLbl.sizeToFit()
                //                productAttributesView.addSubview(specificationNameLbl)
                
                
                
            }
        }
        
        if (self.productDetailsInfo?.addToCartModel?.AllowedQuantities?.count)! > 0 {
            self.allowQtyTF = UITextField(frame: CGRect(x: 10, y: 10, width: 100, height: 30))
            self.allowQtyTF.placeholder = "Qty".localized
            self.allowQtyTF.text =  "Qty \(self.allowQuantity[0])"
            self.allowQtyTF.font = UIFont.init(name: appFont, size: 14)
            self.allowQtyTF.borderStyle = UITextField.BorderStyle.roundedRect
            self.allowQtyTF.tintColor = UIColor.white
            self.allowQtyTF.textAlignment = .center
            self.allowQtyTF.addTarget(self, action: #selector(quantityTextFieldBeginEditing(_:)), for: .editingDidBegin)
            self.allowQuantityView.addSubview(allowQtyTF)
            self.allowQuantityViewHeightConstraint.constant = 50
        }
        producAttributesViewHeightConstraint.constant = outerY
        
    }
    
    @objc func quantityTextFieldBeginEditing(_ textField: UITextField) {
        textField.inputView = self.pickerView
        //        self.allowQtyTF.text =  "Qty \(allowQuantity[row])"
        self.pickerView.delegate = self
    }
    
    @objc func tapOnSize(sender:UIButton) {
        measurementParameters.removeAll()
        let superTag = sender.superview?.superview?.tag
        if let subviews = sender.superview?.subviews {
            for (i,view) in subviews.enumerated() {
                if (view.viewWithTag(100 + i) != nil) {
                    if let sizeBtn  = view.viewWithTag(100+i) as? UIButton {
                        sizeBtn.setTitleColor(UIColor.black, for: .normal)
                        sizeBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                    }
                }
            }
        }
        if let sizeBtn  = sender.viewWithTag(sender.tag) as? UIButton {
            sizeBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1), for: .normal)
            sizeBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 235, alpha: 1).cgColor
        }
        
        measurementParameters["value"] = self.productDetailsInfo?.productAttributes?[superTag!].Values?[sender.tag-100].Name as Any
        measurementParameters["key"] = String(format: "product_measurement_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[superTag!].ProductId ?? 0),(self.productDetailsInfo?.productAttributes?[superTag!].ProductAttributeId ?? 0) ,(self.productDetailsInfo?.productAttributes?[superTag!].Id ?? 0)) as AnyObject
        
        for i in 0  ..< (self.productDetailsInfo?.productAttributes?[superTag!].Values?.count)! {
            self.productDetailsInfo?.productAttributes?[superTag!].Values?[i].IsPreSelected = false
        }
        self.productDetailsInfo?.productAttributes?[superTag!].Values?[sender.tag-100].IsPreSelected   = true
        
        for (index,dict) in dictionaryArray.enumerated() {
            let dd = dict as! Dictionary<String, AnyObject>
            if (measurementParameters["key"] as AnyObject as! String) == (dd["key"] as AnyObject as! String) {
                self.dictionaryArray.replaceObject(at: index, with: measurementParameters)
                self.loadProductDetailsPagePrice()
                return
            }
        }
        
        self.dictionaryArray.add(measurementParameters)
        self.loadProductDetailsPagePrice()
    }
    
    @objc func tapOnColor(sender:UITapGestureRecognizer) {
        let superTag = sender.view?.superview?.superview?.tag
        let indexPath = IndexPath(row: sender.view!.tag - 100, section: 0)
        
        if self.imageUrlArray.count >= (self.productDetailsInfo?.productAttributes?[superTag!].Values!.count)!{
            scrollToIndexCollection(indexpath: indexPath)
            productCollectionView1.scrollToItem(at: indexPath, at: .right, animated: true)
        }
        
        
        
        for (i,view) in (sender.view?.superview?.subviews.enumerated())! {
            if (view.viewWithTag(100 + i) != nil) {
                let colorView  = view.viewWithTag(100+i)
                colorView?.layer.borderColor = UIColor.clear.cgColor
            }
        }
        if self.productDetailsInfo?.productAttributes?[superTag!].Values?[(sender.view?.tag)! - 100].Name == "White" {
            let colorView  = sender.view?.viewWithTag(sender.view?.tag ?? 0)
            colorView?.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
        }
        else {
            let colorView  = sender.view?.viewWithTag(sender.view?.tag ?? 0)
            colorView?.layer.borderColor = AppUtility.hexStringToUIColor(hex: self.productDetailsInfo?.productAttributes?[superTag!].Values?[(sender.view?.tag)! - 100].ColorSquaresRgb ?? "#000000").cgColor
        }
        
        for i in 0  ..< (self.productDetailsInfo?.productAttributes?[superTag!].Values?.count)! {
            self.productDetailsInfo?.productAttributes?[superTag!].Values?[i].IsPreSelected = false
        }
        self.productDetailsInfo?.productAttributes?[superTag!].Values?[(sender.view?.tag)! - 100].IsPreSelected   = true
        self.loadProductDetailsPagePrice()
    }
    
    func loadProductDetailsPagePrice() {
        self.dictionaryArray = []
        for i in 0  ..< (self.productDetailsInfo?.productAttributes?.count)!  {
            for j in 0  ..< (self.productDetailsInfo?.productAttributes?[i].Values?.count)!  {
                if (self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected)! {
                    var parameters = Dictionary<String, AnyObject>()
                    
                    parameters["value"] = self.productDetailsInfo?.productAttributes?[i].Values?[j].Id as Any as AnyObject
                    parameters["key"] = String(format: "product_attribute_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[i].ProductId)!,(self.productDetailsInfo?.productAttributes?[i].ProductAttributeId)!,(self.productDetailsInfo?.productAttributes?[i].Id)!) as AnyObject
                    
                    self.dictionaryArray.add(parameters)
                }
            }
        }
        print("dddddd - ",self.dictionaryArray)
        
        self.aPIManager.loadProductDetailsPagePrice(self.productId, parameters: dictionaryArray, onSuccess:{
            data in
            AppUtility.hideLoading(self.view)
            let stockQuantity:Int = data["StockQuantity"].int ?? 0
            let availOutStock = "Availability : Out-Of-Stock".localized
            let availInStock = "Availability : In-Stock".localized
            let availability = "Availability".localized
            
            var productPrice = data["Price"].stringValue.replacingOccurrences(of: "MMK", with: "")
            productPrice = productPrice + mmkText
            let attrString = NSMutableAttributedString(string: productPrice )
            let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            self.productPriceLbl.attributedText = attrString
            
            if stockQuantity == 0 {
                self.availabilityStockLbl.text = availOutStock
            }
            else if stockQuantity < 6 {
                self.availabilityStockLbl.text = String(format: "\(availability) : \(stockQuantity)")
            }
            else {
                self.availabilityStockLbl.text = availInStock
            }
        },onError: {
            message in
            AppUtility.hideLoading(self.view)
        })
    }
    
    @objc func tapOnSize2(sender:UIButton) {
        if let superTag = sender.superview?.superview?.tag {
            sizeParameters.removeAll()
            measurementParameters.removeAll()
            sizeParameters["value"] = self.productDetailsInfo?.productAttributes?[superTag].Values?[sender.tag-100].Id as Any
            sizeParameters["key"] = String(format: "product_attribute_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[superTag].ProductId ?? 0),(self.productDetailsInfo?.productAttributes?[superTag].ProductAttributeId ?? 0) ,(self.productDetailsInfo?.productAttributes?[superTag].Id ?? 0)) as AnyObject
            
            measurementParameters["value"] = self.productDetailsInfo?.productAttributes?[superTag].Values?[sender.tag-100].Name as Any
            measurementParameters["key"] = String(format: "product_measurement_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[superTag].ProductId ?? 0),(self.productDetailsInfo?.productAttributes?[superTag].ProductAttributeId ?? 0) ,(self.productDetailsInfo?.productAttributes?[superTag].Id ?? 0)) as AnyObject
        }
        
        if let subviews = sender.superview?.subviews {
            for (i,view) in subviews.enumerated() {
                if (view.viewWithTag(100 + i) != nil) {
                    if let sizeBtn  = view.viewWithTag(100+i) as? UIButton {
                        sizeBtn.setTitleColor(UIColor.black, for: .normal)
                        sizeBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
                    }
                }
            }
        }
        if let sizeBtn  = sender.viewWithTag(sender.tag) as? UIButton {
            sizeBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1), for: .normal)
            sizeBtn.layer.borderColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 110, blueValue: 235, alpha: 1).cgColor
        }
        for (index,dict) in dictionaryArray.enumerated() {
            if let dd = dict as? Dictionary<String, Any> {
                if (sizeParameters["key"] as? String) == (dd["key"] as? String) {
                    self.dictionaryArray.replaceObject(at: index, with: sizeParameters)
                }
            }
        }
        for (index,dict) in dictionaryArray.enumerated() {
            if let dd = dict as? Dictionary<String, Any> {
                if (measurementParameters["key"] as? String) == (dd["key"] as? String) {
                    self.dictionaryArray.replaceObject(at: index, with: measurementParameters)
                    self.loadProductDetailsPagePrice()
                    return
                }
            }
        }
        self.dictionaryArray.add(sizeParameters)
        self.dictionaryArray.add(measurementParameters)
        self.loadProductDetailsPagePrice()
    }
    
    @objc func tapOnColor2(sender:UITapGestureRecognizer) {
        colorParameters.removeAll()
        let superTag = sender.view?.superview?.superview?.tag
        colorParameters["value"] = self.productDetailsInfo?.productAttributes?[superTag!].Values![(sender.view?.tag)! - 200].Id as AnyObject
        colorParameters["key"] = String(format: "product_attribute_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[superTag!].ProductId)!,(self.productDetailsInfo?.productAttributes?[superTag!].ProductAttributeId)! ,(self.productDetailsInfo?.productAttributes?[superTag!].Id)!) as AnyObject
        
        for (i,view) in (sender.view?.superview?.subviews.enumerated())! {
            if (view.viewWithTag(200 + i) != nil) {
                let colorView  = view.viewWithTag(200+i)
                colorView?.layer.borderColor = UIColor.clear.cgColor
            }
        }
        if self.productDetailsInfo?.productAttributes?[superTag!].Values?[(sender.view?.tag)! - 200].Name == "White" {
            let colorView  = sender.view?.viewWithTag(sender.view?.tag ?? 0)
            colorView?.layer.borderColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1).cgColor
        }
        else {
            let colorView  = sender.view?.viewWithTag(sender.view?.tag ?? 0)
            colorView?.layer.borderColor = AppUtility.hexStringToUIColor(hex: self.productDetailsInfo?.productAttributes?[superTag!].Values?[(sender.view?.tag)! - 200].ColorSquaresRgb ?? "#000000").cgColor
        }
        
        for (index,dict) in dictionaryArray.enumerated() {
            let dd = dict as! Dictionary<String, AnyObject>
            if (colorParameters["key"] as AnyObject as! String) == (dd["key"] as AnyObject as! String) {
                self.dictionaryArray.replaceObject(at: index, with: colorParameters)
                self.loadProductDetailsPagePrice()
                return
            }
        }
        self.dictionaryArray.add(colorParameters)
        self.loadProductDetailsPagePrice()
    }
    
    //    func loadProductDetailsPagePrice2() {
    //        print("dddddd - ",self.dictionaryArray)
    //        self.aPIManager.loadProductDetailsPagePrice(self.productId, parameters: dictionaryArray, onSuccess:{
    //            priceValue in
    //            AppUtility.hideLoading(self.view)
    //            let price = priceValue["Price"]
    //            self.productPriceLbl.text = price as? String
    //            let measurement = priceValue["MeasurementData"] as? String
    //            let measurementData = measurement?.replacingOccurrences(of: "\\n", with: "\n")
    //            let finalMeasurement = measurementData?.replacingOccurrences(of: "-", with: "    -    ")
    //            for view in self.measurementsView.subviews {
    //                view.removeFromSuperview()
    //            }
    //            if measurement == "" || measurement == nil{
    //                self.measurementViewHeightConstraint.constant = 0
    //                return
    //            }
    //            let titleLbl  = UILabel(frame:CGRect(x: 10, y: 5, width: SCREEN_WIDTH-30, height:20))
    //            titleLbl.textColor = UIColor.darkGray
    //            titleLbl.font = UIFont(name: REGULAR_FONT, size: 13)
    //            titleLbl.text = "All Measurement are in inches"
    //            self.measurementsView.addSubview(titleLbl)
    //
    //            let measurementLbl  = UILabel(frame:CGRect(x: 20, y: 35, width: SCREEN_WIDTH-30, height:30))
    //            measurementLbl.lineBreakMode = .byWordWrapping
    //            measurementLbl.numberOfLines = 0
    //            measurementLbl.font = UIFont(name: REGULAR_FONT, size: 13)
    //            measurementLbl.text = finalMeasurement
    //            let measurementLblHeight = AppUtility.heightForView(text: measurementLbl.text!, font: UIFont(name: REGULAR_FONT, size: 13)!, width: SCREEN_WIDTH-30)
    //            measurementLbl.sizeToFit()
    //            self.measurementViewHeightConstraint.constant = measurementLblHeight + 40
    //            self.measurementsView.addSubview(measurementLbl)
    //        },onError: {
    //            message in
    //            AppUtility.hideLoading(self.view)
    //        })
    //    }
    
    @IBAction func addToCartBtnAction(_ sender: UIButton) {
        //        if self.productDetailsInfo?.addToCartModel?.DisableBuyButton == true {
        //            self.showAlert(alertTitle: "Information", description: "Buying is disabled for this product")
        //            return
        //        }
        let finalDicArray = self.createDictForAddTocartBuyNow()
        AppUtility.showLoading(self.view)
        self.aPIManager.loadAddProductToCart(1, productId: productId, parameters: finalDicArray, onSuccess:{ [weak self]  priceValue in
            if let viewLoc = self?.view, let _ = self {
                let msg = "Cart Updated Successfully".localized
                AppUtility.hideLoading(viewLoc)
                self?.showAlert(alertTitle: "".localized, description: msg)
                print("badge value: \(UserDefaults.standard.value(forKey: "badge") ?? "")")
                DispatchQueue.main.async {
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                            UIView.animate(withDuration: 0.4, animations: {
                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                            }, completion: ({finished in
                                
                                DispatchQueue.main.async {
                                    
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                                            UIView.animate(withDuration: 0.4, animations: {
                                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                                            }, completion: ({finished in
                                                //                                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                                            }))
                                    })
                                }
                                
                            }))
                    })
                }
            }
            }, onError: { [weak self]
                message in
                if let viewLoc = self?.view, let _ = self {
                    AppUtility.hideLoading(viewLoc)
                    let msg = message?.components(separatedBy: "\n")
                    let errorMsg = msg?[0]
                    self?.showAlert(alertTitle: "Information".localized, description: errorMsg ?? "")
                    //                    AppUtility.showToastCustomBlackInView(message: message!, controller: vc)
                }
        })
    }
    
    func createDictForAddTocartBuyNow() -> NSMutableArray {
        self.dictionaryArray = []
        for i in 0  ..< (self.productDetailsInfo?.productAttributes?.count)!  {
            for j in 0  ..< (self.productDetailsInfo?.productAttributes?[i].Values?.count)!  {
                if (self.productDetailsInfo?.productAttributes?[i].Values?[j].IsPreSelected)! {
                    var parameters = Dictionary<String, AnyObject>()
                    
                    parameters["value"] = self.productDetailsInfo?.productAttributes?[i].Values?[j].Id as Any as AnyObject
                    parameters["key"] = String(format: "product_attribute_%d_%d_%d",(self.productDetailsInfo?.productAttributes?[i].ProductId)!,(self.productDetailsInfo?.productAttributes?[i].ProductAttributeId)!,(self.productDetailsInfo?.productAttributes?[i].Id)!) as AnyObject
                    
                    self.dictionaryArray.add(parameters)
                }
            }
        }
        
        let finalDicArray = NSMutableArray ()
        var parameters = Dictionary<String, AnyObject>()
        parameters["value"] = self.productDetailsInfo?.quantityModel?.OrderMinimumQuantity as AnyObject
        parameters["key"] = "addtocart_\(String(productId)).EnteredQuantity" as AnyObject
        finalDicArray.add(parameters)
        finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
        print("finalDicArray from add to cart : \(finalDicArray)")
        //        println_debug("DictionaryArray from add to cart : \(self.dictionaryArray)")
        return finalDicArray
    }
    
    @IBAction func buyNowBtnAction(_ sender: UIButton) {
        if self.productDetailsInfo?.addToCartModel?.DisableBuyButton == true {
            self.showAlert(alertTitle: "Information".localized, description: "Buying is disabled for this product".localized)
            return
        }
        let finalDicArray = self.createDictForAddTocartBuyNow()
        AppUtility.showLoading(self.view)
        self.aPIManager.loadAddProductToCart(1, productId: productId, parameters: finalDicArray, onSuccess:{ [weak self] priceValue in
            if let viewLoc = self?.view, let vc = self {
                AppUtility.hideLoading(viewLoc)
            }
            DispatchQueue.main.async {
                let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
                self?.present(viewController, animated: true, completion: nil)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view, let _ = self {
                    AppUtility.hideLoading(viewLoc)
                    self?.showAlert(alertTitle: "Information".localized, description: message!)
                    //                    AppUtility.showToastCustomBlackInView(message: message!, controller: vc)
                }
        })
    }
    
    @IBAction func NotifyMeAction(_ sender: UIButton) {
        if sender.isSelected == true{
            AppUtility.showToastlocal(message: "Already saved your notify request", view: self.view)
        }
        else{
            sender.isSelected = true
            AppUtility.showToastlocal(message: "Your Notify request saved", view: self.view)
        }
        
    }
    
    private func addToWishList() {
        //        AppUtility.showLoading(self.view)
        self.wishListSelected = true
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
                    }
                }))
        })
        self.aPIManager.loadAddProductToCart(2, productId: productId, parameters: self.dictionaryArray, onSuccess:{ [weak self] priceValue in
            if let viewLoc = self?.view, let vc = self {
                //                let msg = "Added to WishList"
                //                AppUtility.hideLoading(viewLoc)
                //                AppUtility.showToast(msg,view: viewLoc)
                //                AppUtility.showToastCustomBlackInView(message: msg, controller: vc)
                self?.delegate?.addToWishList(id: (self?.productId)!)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view, let vc = self {
                    //                    AppUtility.hideLoading(viewLoc)
                    //                    AppUtility.showToastCustomBlackInView(message: message ?? "", controller: vc)
                    //                    self?.showAlert(alertTitle: "", description: message ?? "")
                }
        })
    }
    
    private func removeFromWishList() {
        //        AppUtility.showLoading(self.view)
        self.wishListSelected = false
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
                    }
                }))
        })
        self.aPIManager.removeProductFromWishList(2, productId: productId, parameters: self.dictionaryArray, onSuccess: { [weak self] in
            //            if let viewLoc = self?.view {
            //                AppUtility.hideLoading(viewLoc)
            //            }
            //            self?.wishListSelected = false
            //            UIView.animate(withDuration: 0.7, animations: {
            //                self?.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
            //                    UIView.animate(withDuration: 0.5, animations: {
            //                        self?.wishListBtn.transform = CGAffineTransform.identity
            //                    }, completion: ({finished in
            //                        if (finished) {
            //                            self?.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
            //                        }
            //                    }))
            //            })
            self?.delegate?.removeToWishList(id: self!.productId)
            }, onError: { [weak self] message in
                if let viewLoc = self?.view, let vc = self {
                    //                    AppUtility.hideLoading(viewLoc)
                    //                    AppUtility.showToastCustomBlackInView(message: message ?? "", controller: vc)
                    //                    self?.showAlert(alertTitle: "", description: message ?? "")
                }
        })
    }
    
    @IBAction func addToWishListAction(_ sender: UIButton) {
        if !wishListSelected {
            self.addToWishList()
        } else {
            self.removeFromWishList()
        }
    }
    
    private func updateShortDescriptionView() {
        var lblHeight: CGFloat = 0
        self.shortDescriptionLbl.frame.size.height = 1
        self.shortDescriptionLbl.frame.size = CGSize.init(width: SCREEN_WIDTH - 30, height: (self.shortDescriptionLbl.attributedText ?? NSAttributedString()).height(containerWidth: SCREEN_WIDTH - 30) + 10)
        lblHeight = self.shortDescriptionLbl.frame.size.height
        outerY += lblHeight
        producAttributesViewHeightConstraint.constant = outerY + 10
    }
    
    private func updateTextView() {
        var lblHeight: CGFloat = 0
        self.textView2.frame.size.height = 1
        self.textView2.frame.size = CGSize.init(width: SCREEN_WIDTH - 20, height: (self.textView2.attributedText ?? NSAttributedString()).height(containerWidth: SCREEN_WIDTH - 20))
        lblHeight = self.textView2.frame.size.height
        //            let scrollableSize = CGSize(width: (self.textView2.frame.size.width - 30), height: lblHeight)
        //            textView2.contentSize = scrollableSize
        outerY += lblHeight
        producAttributesViewHeightConstraint.constant = outerY
        //        self.mainView.isHidden = false
        //        self.bottomView.isHidden = false
        AppUtility.hideLoading(self.view)
    }
    
    
    @IBAction func ShareAction(_ sender: UIButton) {
        let urlParameters = String(format: "%@/productdetails/%d",APIManagerClient.sharedInstance.base_url, self.productId) as String
        DispatchQueue.main.async {
            let activityViewController = UIActivityViewController(activityItems: [urlParameters], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: { })
        }
    }
}

extension ProductDetails: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == productCollectionView1 {
            return self.imageUrlArray.count
        }
        else {
            if self.numberOfSecondCollectionLastIndex != 0 {
                return self.numberOfSecondCollectionLastIndex
            }
            return self.imageUrlArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == productCollectionView1 {
            let cell = productCollectionView1.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell1", for: indexPath) as! ProductDetailsCollectionViewCell1
            cell.productImageView.sd_setImage(with: URL(string: self.imageUrlArray[indexPath.row]))
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: self.imageUrlArray[indexPath.row]), imageView: cell.productImageView, imgUrl: self.imageUrlArray[indexPath.row]) { (image) in }
            
            
            
            
            
            //            AppUtility.getData(from: URL(string: self.imageUrlArray[indexPath.row])!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    cell.productImageView.image = UIImage(data: data)
            //                }
            //            }
            
            
            return cell
        } else {
            let cell = productCollectionView2.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell2", for: indexPath) as! ProductDetailsCollectionViewCell2
            cell.productImageView.sd_setImage(with: URL(string: self.imageUrlArray[indexPath.row]))
            
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: self.imageUrlArray[indexPath.row]), imageView: cell.productImageView, imgUrl: self.imageUrlArray[indexPath.row]) { (image) in }
            
            
            
            
            //            AppUtility.getData(from: URL(string: self.imageUrlArray[indexPath.row])!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    cell.productImageView.image = UIImage(data: data)
            //                }
            //            }
            
            
            
            if self.numberOfSecondCollectionLastIndex != 0 {
                if indexPath.row == self.numberOfSecondCollectionLastIndex-1 {
                    cell.alphaView.isHidden = false
                    cell.moreImageCountLbl.text = "+\(self.moreImagesCount)"
                } else {
                    cell.alphaView.isHidden = true
                    cell.moreImageCountLbl.text = ""
                }
            }else {
                cell.alphaView.isHidden = true
                cell.moreImageCountLbl.text = ""
            }
            
            if checked[indexPath.row] {
                cell.contentView.layer.borderColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 120, blueValue: 235, alpha: 1).cgColor
                cell.contentView.layer.borderWidth = 1
            } else {
                cell.contentView.layer.borderColor = UIColor.colorWithRedValue(redValue: 200, greenValue: 200, blueValue: 200, alpha: 1).cgColor
                cell.contentView.layer.borderWidth = 1
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == productCollectionView1 {
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "ProductDetailsToImagesCollectionSegue", sender: self)
        } else {
            if indexPath.row == self.numberOfSecondCollectionLastIndex-1 {
                self.selectedIndex = indexPath.row
                self.performSegue(withIdentifier: "ProductDetailsToImagesCollectionSegue", sender: self)
            } else {
                scrollToIndexCollection(indexpath: indexPath)
                productCollectionView1.scrollToItem(at: IndexPath(row: indexPath.row, section: 0), at: .right, animated: true)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        DispatchQueue.main.async {
            for cell: UICollectionViewCell in self.productCollectionView1.visibleCells {
                let indexPath: IndexPath? = self.productCollectionView1.indexPath(for: cell)
                if let indexPath = indexPath {
                    for (index,_) in self.checked.enumerated() {
                        self.checked[index] = false
                    }
                    self.checked[indexPath.row] = true
                    if indexPath.row < self.numberOfSecondCollectionLastIndex {
                        self.productCollectionView2.reloadData()
                        self.productCollectionView2.scrollToItem(at: IndexPath(row: indexPath.row, section: 0), at: .right, animated: true)
                    }
                    else {
                        if let cell:ProductDetailsCollectionViewCell2 = self.productCollectionView2.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell2", for: IndexPath(row: self.numberOfSecondCollectionLastIndex-1, section: 0)) as? ProductDetailsCollectionViewCell2 {
                            DispatchQueue.main.async {
                                cell.contentView.layer.borderColor = UIColor.colorWithRedValue(redValue: 200, greenValue: 200, blueValue: 200, alpha: 1).cgColor
                                cell.contentView.layer.borderWidth = 1
                                self.productCollectionView2.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func scrollToIndexCollection(indexpath: IndexPath) {
        for (index,_) in checked.enumerated() {
            checked[index] = false
        }
        checked[indexpath.row] = true
        DispatchQueue.main.async {
            self.productCollectionView2.reloadData()
            self.productCollectionView2.scrollToItem(at: IndexPath(row: indexpath.row, section: 0), at: .right, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productCollectionView1 {
            return CGSize(width: SCREEN_WIDTH, height: productCollectionView1.bounds.height)
        } else {
            return CGSize(width: 70, height: productCollectionView2.bounds.height)
        }
    }
}

extension ProductDetails : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
        let lineLbl = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 2))
        lineLbl.font = UIFont(name: appFont, size: 15.0)
        lineLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        headerView.addSubview(lineLbl)
        let headerLabel = UILabel(frame: CGRect(x: 10, y: 10, width: SCREEN_WIDTH-30, height: 30))
        headerLabel.text = "Related Products".localized
        headerLabel.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
        headerLabel.font = UIFont.init(name: appFont, size: 15)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contentTableView.dequeueReusableCell(withIdentifier: "RelatedProductsTableCell", for: indexPath) as! RelatedProductsTableViewCell
        cell.updateRelatedProducts(relatedProductsInfo: self.relatedProductDetailsInfoArray.productsInfo)
        cell.cellDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREEN_HEIGHT/2.5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension ProductDetails : RelatedProductsCollectionCellDelegate {
    func tapOnRelatedProductCollectionView(collectioncell: ProductListCollectionViewCell?) {
        if let cell = collectioncell {
            if let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails {
                vc.productId = cell.productId
                vc.productName = cell.productName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension ProductDetails : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allowQuantity.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allowQuantity[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.allowQtyTF.text =  "Qty \(allowQuantity[row])"
    }
}

extension ProductDetails : TapOnWishlistBtnOnProductImageCollectionDelegate {
    func tappedOnWishlistBtn(check: Bool) {
        if check {
            wishListSelected = true
            self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
            self.delegate?.addToWishList(id: (self.productId))
        } else {
            wishListSelected = false
            self.delegate?.removeToWishList(id: self.productId)
            self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
        }
    }
}

extension ProductDetails: WKNavigationDelegate{
    func configureWebView(UrlStr: String){
        if UrlStr == ""{
            return
        }
        else{
            DispatchQueue.main.async {
                self.webView?.isHidden = false
                
                //    let request = URLRequest(url: URL(string: UrlStr)!)
                //    self.webView?.load(request)
                AppUtility.showLoading(self.view)
                guard let url = URL(string: UrlStr) else { return }
                let request = URLRequest(url: url)
                
                self.webView = WKWebView(frame: self.view.bounds)
                self.webView?.translatesAutoresizingMaskIntoConstraints = false
                self.webView?.isUserInteractionEnabled = true
                
                self.webView?.navigationDelegate = self
                self.webView?.load(request)
                self.webView?.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
                self.view.addSubview(self.webView!)
                
                //          self.view.sendSubviewToBack(self.webView!)
                // add activity
                //            AppUtility.showLoading(self.view)
                
                //                self.viewWillAppear(true)
            }
        }
    }
    
    
    
    
    //    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
    //         AppUtility.showLoading(self.view)
    //        print(#function)
    //    }
    
    
    //    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    //
    //        webView.evaluateJavaScript("navigator.userAgent", completionHandler: { result, error in
    //            if let userAgent = result as? String {
    //                print(userAgent)
    //                AppUtility.hideLoading(self.view)
    //            }
    //        })
    //    }
    
    
    
    
    //
    //    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    //         AppUtility.hideLoading(self.view)
    //    }
    //
    //    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
    //        AppUtility.hideLoading(self.view)
    //        print(error.localizedDescription)
    //    }
    //    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    //        AppUtility.showLoading(self.view)
    //        print("Strat to load")
    //    }
    //    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    //        AppUtility.hideLoading(self.view)
    //        print("finish to load")
    //    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading" {
            if webView!.isLoading {
                AppUtility.showLoading(self.view)
                
            } else {
                AppUtility.hideLoading(self.view)
                if self.webView?.isHidden == false{
                    
                    let button = UIButton(type: .custom)
                    button.setImage(UIImage(named: "back"), for: .normal)
                    button.addTarget(self, action: #selector(webArenaBackAction), for: .touchUpInside)
                    //            button.frame = CGRect(x: -20, y: 0, width: 10, height: 40)
                    button.imageEdgeInsets.left = -35
                    barButton = UIBarButtonItem(customView: button)
                    self.navigationItem.leftBarButtonItem = barButton
                    
                    
                    //            let webArenaBack = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(webArenaBackAction))
                    //            self.navigationItem.leftBarButtonItem  = webArenaBack
                }
            }
        }
    }
}
