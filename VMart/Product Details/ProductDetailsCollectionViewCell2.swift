//
//  ProductDetailsCollectionViewCell2.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/27/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class ProductDetailsCollectionViewCell2: UICollectionViewCell {
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var alphaView: UIView!
    @IBOutlet var moreImageCountLbl: UILabel!
}
