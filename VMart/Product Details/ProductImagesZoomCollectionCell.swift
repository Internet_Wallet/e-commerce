//
//  ProductImagesZoomCollectionCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 2/13/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class ProductImagesZoomCollectionCell: UICollectionViewCell, UIScrollViewDelegate {
    
    var MINIMUM_SCALE: CGFloat = 1.0
    var MAXIMUM_SCALE: CGFloat = 3.0
    
    
    @IBOutlet var scrollviewCell: UIScrollView!
    @IBOutlet var productImageView: UIImageView!
    
    
    
    override func awakeFromNib() {        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollviewCell.addGestureRecognizer(doubleTapGest)
    }
    
    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrollviewCell.zoomScale == 1 {
            scrollviewCell.zoom(to: zoomRectForScale(scale: scrollviewCell.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollviewCell.setZoomScale(1, animated: true)
        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = productImageView.frame.size.height / scale
        zoomRect.size.width  = productImageView.frame.size.width  / scale
        let newCenter = productImageView.convert(center, from: scrollviewCell)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
}
