//
//  DialogsViewControllerNew.swift
//  sample-chat-swift
//
//  Created by Injoit on 1/28/19.
//  Copyright © 2019 Quickblox. All rights reserved.
//

import UIKit
import Quickblox
import SVProgressHUD


// VAudio/Video Purpose //
import Quickblox
import QuickbloxWebRTC
import PushKit
import   SVProgressHUD
//

struct DialogsConstant {
    static let dialogsPageLimit:Int = 100
    static let segueGoToChat = "goToChat"
    static let selectOpponents = "SelectOpponents"
    static let infoSegue = "PresentInfoViewController"
}

class DialogTableViewCellModel: NSObject {
    
    //MARK: - Properties
    var textLabelText: String = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden = true
    var dialogIcon : UIImage?
    
    
    //MARK: - Life Cycle
    init(dialog: QBChatDialog) {
        super.init()
        
        if let dialogName = dialog.name {
            textLabelText = dialogName
        }
        
        // Unread messages counter label
        if dialog.unreadMessagesCount > 0 {
            var trimmedUnreadMessageCount = ""
            
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            unreadMessagesCounterHiden = false
        } else {
            unreadMessagesCounterLabelText = nil
            unreadMessagesCounterHiden = true
        }
        // Dialog icon
        if dialog.type == .private {
            dialogIcon = UIImage(named: "user")
            
            if dialog.recipientID == -1 {
                return
            }
            // Getting recipient from users.
            if let recipient = ChatManager.instance.storage.user(withID: UInt(dialog.recipientID)),
                let fullName = recipient.fullName {
                self.textLabelText = fullName
            } else {
                ChatManager.instance.loadUser(UInt(dialog.recipientID)) { [weak self] (user) in
                    self?.textLabelText = user?.fullName ?? user?.login ?? ""
                }
            }
        } else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
}

class DialogsViewController: UITableViewController {
    
    
    var dicToCheck = NSMutableDictionary()
    
    var flagState = false
    
    // Audio/Video //
    let  profile = Profile()
    
    //MARK: - IBOutlets
    @IBOutlet private weak var audioCallButton: UIButton!
    @IBOutlet private weak var videoCallButton: UIButton!
    @IBOutlet private weak var headerLbl: UILabel!{
        didSet{
            self.headerLbl.text = self.headerLbl.text?.localized
            self.headerLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    //MARK: - Properties
    lazy private var VdataSource: UsersDataSource = {
        let dataSource = UsersDataSource()
        return dataSource
    }()
    
    lazy private var navViewController: UINavigationController = {
        let navViewController = UINavigationController()
        return navViewController
        
    }()
    
    private weak var session: QBRTCSession?
    lazy private var voipRegistry: PKPushRegistry = {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        return voipRegistry
    }()
    
    
    private var callUUID: UUID?
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask = UIBackgroundTaskIdentifier.invalid
        return backgroundTask
    }()
    
    //
    
    //MARK: - Properties
    private let chatManager = ChatManager.instance
    private var dialogs: [QBChatDialog] = []
    var storeValue = UserDefaults.standard.integer(forKey: "KeyAudioOrVideo")
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dicToCheck.removeAllObjects()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        if profile.isFull == true {
            navigationItem.title = profile.fullName
        }
        
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "SA_STR_LOGOUT".localized,
        //                                                           style: .plain,
        //                                                           target: self,
        //                                                           action: #selector(didTapLogout(_:)))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(didTapLogout(_:)))
        
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
        self.defaultNavigationItem()
        
        if storeValue == 2{
            self.headerLbl.text = "Select a Dialog to Chat"
            let button = UIButton(frame: CGRect(x: self.tableView.frame.width-60, y: self.tableView.frame.height-150, width: 60, height: 60))
            button.setImage(UIImage(named: "edit_blue.png"), for: UIControl.State.normal)
            button.setTitle(" ", for: .normal)
            button.addTarget(self, action: #selector(didTapNewChat(_:)), for: .touchUpInside)
            self.tableView.addSubview(button)
            
        }
        
        showInfoButton()
        
        
        // Audio / Video
        QBRTCClient.instance().add(self)
        
        // Reachability
        if Reachability2.instance.networkConnectionStatus() != NetworkConnectionStatus.notConnection {
            loadUsers()
        }
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])
        
        //
        
    }
    
    
    func defaultNavigationItem(){
        if storeValue == 1{
            let usersButtonItem = UIBarButtonItem(image: UIImage(named: "refresh"),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(didTapRefresh(_:)))
            navigationItem.rightBarButtonItem = usersButtonItem
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadUserData()
        
    }
    
    
    func loadUserData(){
        chatManager.delegate = self
        if QBChat.instance.isConnected == true {
            chatManager.updateStorage()
        }
    }
    
    @IBAction func AddNewChat(_ sender: UIButton){
        SVProgressHUD.show(withStatus: "Working Fine" )
    }
    //MARK: - Actions
    @objc func didTapInfo(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: DialogsConstant.infoSegue, sender: sender)
    }
    
    @objc func didTapNewChat(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: DialogsConstant.selectOpponents, sender: sender)
        
    }
    
    @objc func didTapRefresh(_ sender: UIBarButtonItem) {
        
        self.loadUserData()
    }
    
    @objc func didTapLogout(_ sender: UIBarButtonItem) {
        if QBChat.instance.isConnected == false {
            SVProgressHUD.showError(withStatus: "Error")
            return
        }
        SVProgressHUD.show(withStatus: "SA_STR_LOGOUTING".localized)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        let uuidString = identifierForVendor.uuidString
        #if targetEnvironment(simulator)
        disconnectUser()
        #else
        QBRequest.subscriptions(successBlock: { (response, subscriptions) in
            let subscriptionsUIUD = subscriptions?.first?.deviceUDID
            if subscriptionsUIUD == uuidString {
                QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: uuidString, successBlock: { response in
                    self.disconnectUser()
                }, errorBlock: { error in
                    if let error = error.error {
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                        return
                    }
                    SVProgressHUD.dismiss()
                })
            } else {
                self.disconnectUser()
            }
            
        }) { response in
            if response.status.rawValue == 404 {
                self.disconnectUser()
            }
        }
        #endif
    }
    
    //MARK: - Internal Methods
    private func disconnectUser() {
        QBChat.instance.disconnect(completionBlock: { error in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            self.logOut()
        })
    }
    
    private func logOut() {
        QBRequest.logOut(successBlock: { [weak self] response in
            //ClearProfile
            Profile.clearProfile()
            self?.chatManager.storage.clear()
            self?.dismiss(animated: true, completion: nil)
            //self?.navigationController?.popToRootViewController(animated: false)
            SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
        }) { response in
            debugPrint("[DialogsViewController] logOut error: \(response)")
        }
    }
    
    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogs.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogcell", for: indexPath) as? DialogTableViewCell else {
            return UITableViewCell()
        }
        
        cell.isExclusiveTouch = true
        cell.contentView.isExclusiveTouch = true
        
        cell.tag = indexPath.row
        
        let chatDialog = dialogs[indexPath.row]
        let cellModel = DialogTableViewCellModel(dialog: chatDialog)
        //cell.checkOnlineOffline(user: ChatManager.instance.storage.users[indexPath.row])
        cell.dialogLastMessage.text = chatDialog.lastMessageText
        if chatDialog.lastMessageText == nil && chatDialog.lastMessageID != nil {
            cell.dialogLastMessage.text = "[Attachment]"
        }
        cell.dialogName.text = cellModel.textLabelText
        cell.dialogTypeImage.image = cellModel.dialogIcon
        cell.unreadMessageCounterLabel.text = cellModel.unreadMessagesCounterLabelText
        cell.unreadMessageCounterHolder.isHidden = cellModel.unreadMessagesCounterHiden
        
        if dicToCheck.value(forKey: String(indexPath.row)) != nil{
            if let value = dicToCheck.value(forKey: String(indexPath.row)) as? Bool{
                if value{
                    cell.dialogTypeImage?.image = UIImage.init(named: "correctBlack")
                }
            }
        }else{
            cell.dialogTypeImage?.image = UIImage.init(named: "user")
        }
        
        
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //self.tableView.deselectRow(at: indexPath, animated: true)
        if storeValue == 2{
            
            tableView.deselectRow(at: indexPath, animated: true)
            let dialog = dialogs[indexPath.row]
            performSegue(withIdentifier: "goToChat" , sender: dialog.id)
        }
        else{
            
            if dicToCheck.count>0 {
                
                if let _ = dicToCheck.value(forKey: String(indexPath.row)) as? Bool{
                    dicToCheck.removeObject(forKey: String(indexPath.row))
                    tableView.reloadData()
                    self.configureRefreshBarButton()
                }else{
                    self.configureCallOrVideoBarButton(indexPath: indexPath)
                    //Toast
                    AppUtility.showToastlocal(message: "You can select only one at a time.", view: self.view)
                }
                
            }else{
                dicToCheck.setValue(true, forKey: String(indexPath.row))
                tableView.reloadData()
                self.configureCallOrVideoBarButton(indexPath: indexPath)
            }
        }
    }
    
    func configureRefreshBarButton(){
        navigationItem.rightBarButtonItem = nil
        let usersButtonItem = UIBarButtonItem(image: UIImage(named: "refresh"),
                                              style: .plain,
                                              target: self,
                                              action: #selector(didTapRefresh(_:)))
        navigationItem.rightBarButtonItem = usersButtonItem
    }
    
    
    func configureCallOrVideoBarButton(indexPath: IndexPath){
        navigationItem.rightBarButtonItem = nil
        let AudioBarBtn = UIBarButtonItem(image: UIImage(named: "call"),
                                          style: .plain,
                                          target: self,
                                          action:#selector(didPressAudioCall(_:)))
        AudioBarBtn.tag = indexPath.row
        AudioBarBtn.tintColor = UIColor.white
        let VideoBarBtn = UIBarButtonItem(image: UIImage(named: "video"),
                                          style: .plain,
                                          target: self,
                                          action:#selector(didPressVideoCall(_:)))
        VideoBarBtn.tag = indexPath.row
        VideoBarBtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [AudioBarBtn,VideoBarBtn]
    }
    
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if storeValue == 2{
            self.defaultNavigationItem()
        }
        else{
            //            let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
            //            cellToDeSelect.contentView.backgroundColor = UIColor.green
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if storeValue == 2{
            let dialog = dialogs[indexPath.row]
            if dialog.type == .publicGroup {
                return false
            }
            return true
        }else{
            return false
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,
                            forRowAt indexPath: IndexPath) {
        if storeValue == 2{
            let dialog = dialogs[indexPath.row]
            if editingStyle != .delete || dialog.type == .publicGroup {
                return
            }
            
            let alertController = UIAlertController(title: "SA_STR_WARNING".localized,
                                                    message: "SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized,
                                                    preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel, handler: nil)
            
            let leaveAction = UIAlertAction(title: "SA_STR_DELETE".localized, style: .default) { (action:UIAlertAction) in
                SVProgressHUD.show(withStatus: "SA_STR_DELETING".localized, maskType: .clear)
                
                guard let dialogID = dialog.id else {
                    SVProgressHUD.dismiss()
                    return
                }
                
                if dialog.type == .private {
                    self.chatManager.deleteDialog(withID: dialogID)
                } else {
                    
                    let currentUser = Profile()
                    guard currentUser.isFull == true, let dialogOccupantIDs = dialog.occupantIDs else {
                        return
                    }
                    // group
                    let occupantIDs = dialogOccupantIDs.filter({ $0.intValue != currentUser.ID })
                    dialog.occupantIDs = occupantIDs
                    
                    let message = "User \(currentUser.fullName) " + "SA_STR_USER_HAS_LEFT".localized
                    // Notifies occupants that user left the dialog.
                    self.chatManager.sendLeaveMessage(message, to: dialog, completion: { (error) in
                        if let error = error {
                            debugPrint("[DialogsViewController] sendLeaveMessage error: \(error.localizedDescription)")
                            SVProgressHUD.dismiss()
                            return
                        }
                        self.chatManager.deleteDialog(withID: dialogID)
                    })
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(leaveAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "SA_STR_DELETE".localized
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat" {
            if let chatVC = segue.destination as? ChatViewController {
                chatVC.dialogID = sender as? String
            }
        }
        switch segue.identifier {
        case UsersSegueConstant.settings:
            let settingsViewController = (segue.destination as? UINavigationController)?.topViewController
                as? SessionSettingsViewController
            settingsViewController?.delegate = self
        case UsersSegueConstant.call:
            debugPrint("[UsersViewController] UsersSegueConstant.call")
        default:
            break
        }
        
    }
    
    // MARK: - Helpers
    private func reloadContent() {
        dialogs = chatManager.storage.dialogsSortByUpdatedAt()
        
        tableView.reloadData()
    }
    
    // Audio/Video
    
    /**
     *  Load all (Recursive) users for current room (tag)
     */
    @objc func loadUsers() {
        
        let firstPage = QBGeneralResponsePage(currentPage: 1, perPage: 100)
        QBRequest.users(withExtendedRequest: ["order": "desc date updated_at"],
                        page: firstPage,
                        successBlock: { [weak self] (response, page, users) in
                            self?.VdataSource.update(users: users)
                            
            }, errorBlock: { response in
                debugPrint("[UsersViewController] loadUsers error: \(self.errorMessage(response: response) ?? "")")
        })
        
    }
    
    @IBAction func didPressAudioCall(_ sender: UIBarButtonItem?) {
        
        self.VdataSource.selectUser(at:IndexPath(row: sender?.tag ?? 0, section: 0))
        call(with: QBRTCConferenceType.audio)
        
    }
    
    @IBAction func didPressVideoCall(_ sender: UIBarButtonItem?) {
        
        self.VdataSource.selectUser(at:IndexPath(row: sender?.tag ?? 0, section: 0))
        call(with: QBRTCConferenceType.video)
        
    }
    
    // MARK: - Internal Methods
    private func hasConnectivity() -> Bool {
        
        let status = Reachability2.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView(message: UsersAlertConstant.checkInternet)
            if CallKitManager.instance.isCallStarted() == false {
                CallKitManager.instance.endCall(with: callUUID) {
                    debugPrint("[UsersViewController] endCall")
                }
            }
            return false
        }
        return true
    }
    
    
    private func showAlertView(message: String?) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: UsersAlertConstant.okAction, style: .default,
                                                handler: nil))
        present(alertController, animated: true)
    }
    
    
    @objc func didPressSettingsButton(_ item: UIBarButtonItem?) {
        let settingsStoryboard =  UIStoryboard(name: "Settings", bundle: nil)
        if let settingsController = settingsStoryboard.instantiateViewController(withIdentifier: "SessionSettingsViewController") as? SessionSettingsViewController {
            settingsController.delegate = self
            navigationController?.pushViewController(settingsController, animated: true)
        }
    }
    
    private func call(with conferenceType: QBRTCConferenceType) {
        
        if session != nil {
            return
        }
        
        if hasConnectivity() {
            CallPermissions.check(with: conferenceType) { granted in
                if granted {
                    let opponentsIDs = self.VdataSource.ids(forUsers: self.VdataSource.selectedUsers)
                    
                    //Create new session
                    let session = QBRTCClient.instance().createNewSession(withOpponents: opponentsIDs, with: conferenceType)
                    if session.id.isEmpty == false {
                        self.session = session
                        let uuid = UUID()
                        self.callUUID = uuid
                        
                        CallKitManager.instance.startCall(withUserIDs: opponentsIDs, session: session, uuid: uuid)
                        
                        
                        if let callViewController = UIStoryboard(name: "Call", bundle: nil).instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                            callViewController.session = self.session
                            callViewController.usersDataSource = self.VdataSource
                            callViewController.callUUID = uuid
                            let nav = UINavigationController(rootViewController: callViewController)
                            nav.navigationBar.tintColor = UIColor.white
                            nav.modalPresentationStyle = .fullScreen
                            self.present(nav , animated: false)
                            self.navViewController = nav
                        }
                        let profile = Profile()
                        guard profile.isFull == true else {
                            return
                        }
                        let opponentName = profile.fullName.isEmpty == false ? profile.fullName : "Unknown user"
                        let payload = ["message": "\(opponentName) is calling you.",
                            "ios_voip": "1", UsersConstant.voipEvent: "1"]
                        let data = try? JSONSerialization.data(withJSONObject: payload,
                                                               options: .prettyPrinted)
                        var message = ""
                        if let data = data {
                            message = String(data: data, encoding: .utf8) ?? ""
                        }
                        let event = QBMEvent()
                        event.notificationType = QBMNotificationType.push
                        let arrayUserIDs = opponentsIDs.map({"\($0)"})
                        event.usersIDs = arrayUserIDs.joined(separator: ",")
                        event.type = QBMEventType.oneShot
                        event.message = message
                        QBRequest.createEvent(event, successBlock: { response, events in
                            debugPrint("[UsersViewController] Send voip push - Success")
                        }, errorBlock: { response in
                            debugPrint("[UsersViewController] Send voip push - Error")
                        })
                    } else {
                        SVProgressHUD.showError(withStatus: UsersAlertConstant.shouldLogin)
                    }
                }
            }
        }
    }
    
    private func cancelCallAlert() {
        let alert = UIAlertController(title: UsersAlertConstant.checkInternet, message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            
            CallKitManager.instance.endCall(with: self.callUUID) {
                debugPrint("[UsersViewController] endCall")
                
            }
            self.prepareCloseCall()
        }
        alert.addAction(cancelAction)
        present(alert, animated: false) {
        }
    }
    
    
    //Handle Error
    private func errorMessage(response: QBResponse) -> String? {
        var errorMessage : String
        if response.status.rawValue == 502 {
            errorMessage = "Bad Gateway, please try again"
        } else if response.status.rawValue == 0 {
            errorMessage = "Connection network error, please try again"
        } else {
            guard let qberror = response.error,
                let error = qberror.error else {
                    return nil
            }
            
            errorMessage = error.localizedDescription.replacingOccurrences(of: "(",
                                                                           with: "",
                                                                           options:.caseInsensitive,
                                                                           range: nil)
            errorMessage = errorMessage.replacingOccurrences(of: ")",
                                                             with: "",
                                                             options: .caseInsensitive,
                                                             range: nil)
        }
        return errorMessage
    }
    //
    
}

// MARK: - QBChatDelegate
extension DialogsViewController: QBChatDelegate {
    
    func chatRoomDidReceive(_ message: QBChatMessage, fromDialogID dialogID: String) {
        chatManager.updateDialog(with: dialogID, with: message)
    }
    
    func chatDidReceive(_ message: QBChatMessage) {
        guard let dialogID = message.dialogID else {
            return
        }
        chatManager.updateDialog(with: dialogID, with: message)
    }
    
    func chatDidReceiveSystemMessage(_ message: QBChatMessage) {
        guard let dialogID = message.dialogID else {
            return
        }
        if let _ = chatManager.storage.dialog(withID: dialogID) {
            return
        }
        chatManager.updateDialog(with: dialogID, with: message)
    }
    
    func chatServiceChatDidFail(withStreamError error: Error) {
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatDidAccidentallyDisconnect() {
    }
    
    func chatDidNotConnectWithError(_ error: Error) {
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatDidDisconnectWithError(_ error: Error) {
    }
    
    func chatDidConnect() {
        if QBChat.instance.isConnected == true {
            chatManager.updateStorage()
            SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType: .clear)
        }
    }
    
    func chatDidReconnect() {
        // SVProgressHUD.show(withStatus: "SA_STR_CONNECTED".localized)
        if QBChat.instance.isConnected == true {
            chatManager.updateStorage()
            SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType: .clear)
        }
    }
}

// MARK: - ChatManagerDelegate
extension DialogsViewController: ChatManagerDelegate {
    func chatManager(_ chatManager: ChatManager, didUpdateChatDialog chatDialog: QBChatDialog) {
        reloadContent()
        SVProgressHUD.dismiss()
    }
    
    func chatManager(_ chatManager: ChatManager, didFailUpdateStorage message: String) {
        SVProgressHUD.showError(withStatus: message)
    }
    
    func chatManager(_ chatManager: ChatManager, didUpdateStorage message: String) {
        reloadContent()
        SVProgressHUD.dismiss()
        QBChat.instance.addDelegate(self)
    }
    
    func chatManagerWillUpdateStorage(_ chatManager: ChatManager) {
        if navigationController?.topViewController == self {
            SVProgressHUD.show()
        }
    }
}

// Audio/Video

// MARK: - QBRTCClientDelegate
// MARK: - QBRTCClientDelegate
extension DialogsViewController: QBRTCClientDelegate {
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if CallKitManager.instance.isCallStarted() == false && self.session?.id == session.id && self.session?.initiatorID == userID {
            CallKitManager.instance.endCall(with: callUUID) {
                debugPrint("[UsersViewController] endCall")
            }
            prepareCloseCall()
        }
    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        if self.session != nil {
            session.rejectCall(["reject": "busy"])
            return
        }
        
        self.session = session
        let uuid = UUID()
        callUUID = uuid
        var opponentIDs = [session.initiatorID]
        let profile = Profile()
        guard profile.isFull == true else {
            return
        }
        for userID in session.opponentsIDs {
            if userID.uintValue != profile.ID {
                opponentIDs.append(userID)
            }
        }
        
        var callerName = ""
        var opponentNames = [String]()
        var newUsers = [NSNumber]()
        for userID in opponentIDs {
            
            // Getting recipient from users.
            if let user = VdataSource.user(withID: userID.uintValue),
                let fullName = user.fullName {
                opponentNames.append(fullName)
            } else {
                newUsers.append(userID)
            }
        }
        
        if newUsers.isEmpty == false {
            let loadGroup = DispatchGroup()
            for userID in newUsers {
                loadGroup.enter()
                VdataSource.loadUser(userID.uintValue) { (user) in
                    if let user = user {
                        opponentNames.append(user.fullName ?? user.login ?? "")
                    } else {
                        opponentNames.append("\(userID)")
                    }
                    loadGroup.leave()
                }
            }
            loadGroup.notify(queue: DispatchQueue.main) {
                callerName = opponentNames.joined(separator: ", ")
                self.reportIncomingCall(withUserIDs: opponentIDs, outCallerName: callerName, session: session, uuid: uuid)
            }
        } else {
            callerName = opponentNames.joined(separator: ", ")
            self.reportIncomingCall(withUserIDs: opponentIDs, outCallerName: callerName, session: session, uuid: uuid)
        }
    }
    
    private func reportIncomingCall(withUserIDs userIDs: [NSNumber], outCallerName: String, session: QBRTCSession, uuid: UUID) {
        if hasConnectivity() {
            CallKitManager.instance.reportIncomingCall(withUserIDs: userIDs,
                                                       outCallerName: outCallerName,
                                                       session: session,
                                                       uuid: uuid,
                                                       onAcceptAction: { [weak self] in
                                                        guard let self = self else {
                                                            return
                                                        }
                                                        
                                                        
                                                        
                                                        if let callViewController = UIStoryboard(name: "Call", bundle: nil).instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                                                            
                                                            //                                                        if let callViewController = self.storyboard?.instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                                                            
                                                            callViewController.session = session
                                                            callViewController.usersDataSource = self.VdataSource
                                                            callViewController.callUUID = self.callUUID
                                                            self.navViewController = UINavigationController(rootViewController: callViewController)
                                                            self.navViewController.modalPresentationStyle = .fullScreen
                                                            //                                                            self.navViewController.modalTransitionStyle = .crossDissolve
                                                            self.present(self.navViewController , animated: false)
                                                            
                                                        }
                }, completion: { (end) in
                    debugPrint("[UsersViewController] endCall")
            })
        } else {
            
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if let sessionID = self.session?.id,
            sessionID == session.id {
            if self.navViewController.presentingViewController?.presentedViewController == self.navViewController {
                self.navViewController.view.isUserInteractionEnabled = false
                self.navViewController.dismiss(animated: false)
            }
            CallKitManager.instance.endCall(with: self.callUUID) {
                debugPrint("[UsersViewController] endCall")
                
            }
            prepareCloseCall()
        }
    }
    
    private func prepareCloseCall() {
        self.callUUID = nil
        self.session = nil
        if QBChat.instance.isConnected == false {
            self.connectToChat()
        }
        //self.setupToolbarButtons()
    }
    
    private func connectToChat() {
        let profile = Profile()
        guard profile.isFull == true else {
            return
        }
        
        QBChat.instance.connect(withUserID: profile.ID,
                                password: LoginConstant.defaultPassword,
                                completion: { [weak self] error in
                                    guard let self = self else { return }
                                    if let error = error {
                                        if error._code == QBResponseStatusCode.unAuthorized.rawValue {
                                            self.logoutAction()
                                        } else {
                                            debugPrint("[UsersViewController] login error response:\n \(error.localizedDescription)")
                                        }
                                    } else {
                                        //did Login action
                                        SVProgressHUD.dismiss()
                                    }
        })
    }
}


extension DialogsViewController: PKPushRegistryDelegate {
    // MARK: - PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = pushCredentials.token
        
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
            debugPrint("[UsersViewController] Create Subscription request - Success")
        }, errorBlock: { response in
            debugPrint("[UsersViewController] Create Subscription request - Error")
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { response in
            debugPrint("[UsersViewController] Unregister Subscription request - Success")
        }, errorBlock: { error in
            debugPrint("[UsersViewController] Unregister Subscription request - Error")
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didReceiveIncomingPushWith payload: PKPushPayload,
                      for type: PKPushType) {
        if payload.dictionaryPayload[UsersConstant.voipEvent] != nil {
            let application = UIApplication.shared
            if application.applicationState == .background && backgroundTask == .invalid {
                backgroundTask = application.beginBackgroundTask(expirationHandler: {
                    application.endBackgroundTask(self.backgroundTask)
                    self.backgroundTask = UIBackgroundTaskIdentifier.invalid
                })
            }
            if QBChat.instance.isConnected == false {
                connectToChat()
            }
        }
    }
}

// MARK: - SettingsViewControllerDelegate
extension DialogsViewController: SettingsViewControllerDelegate {
    func settingsViewController(_ vc: SessionSettingsViewController, didPressLogout sender: Any) {
        logoutAction()
    }
    
    private func logoutAction() {
        if QBChat.instance.isConnected == false {
            SVProgressHUD.showError(withStatus: "Error")
            return
        }
        SVProgressHUD.show(withStatus: UsersAlertConstant.logout)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        let uuidString = identifierForVendor.uuidString
        #if targetEnvironment(simulator)
        disconnectUser()
        #else
        QBRequest.subscriptions(successBlock: { (response, subscriptions) in
            
            if let subscriptions = subscriptions {
                for subscription in subscriptions {
                    if let subscriptionsUIUD = subscriptions.first?.deviceUDID,
                        subscriptionsUIUD == uuidString,
                        subscription.notificationChannel == .APNSVOIP {
                        self.unregisterSubscription(forUniqueDeviceIdentifier: uuidString)
                        return
                    }
                }
            }
            self.disconnectUser()
            
        }) { response in
            if response.status.rawValue == 404 {
                self.disconnectUser()
            }
        }
        #endif
    }
    
    private func unregisterSubscription(forUniqueDeviceIdentifier uuidString: String) {
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: uuidString, successBlock: { response in
            self.disconnectUser()
        }, errorBlock: { error in
            if let error = error.error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            SVProgressHUD.dismiss()
        })
    }
}
