//
//  StateTownshipListing.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/16/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

protocol StateTownshipDelegate: class {
    func selectedStateTownshipMethod(type: tapGestureType, selectedText: (String, String))
}

class StateTownshipListing: UIViewController {
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var mainBackgroundView: UIView!
    
    var aPIManager = APIManager()
    weak var delegate: StateTownshipDelegate?
    var selectedType: tapGestureType?
    var countryObject: (String, String)?
    var stateTownShipArray = [Dictionary<String, Any>?]()
    var stateObject = (code: "", name: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainBackgroundView.layer.cornerRadius = 6
        getStateAndTownship()
    }
    
    open func showInView(_ aView: UIView!) {
        self.view.frame = aView.frame
        aView.addSubview(self.view)
        self.showAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate(completionHandler: @escaping (_ isCompleted: Bool) -> Void) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.dismiss(animated: true, completion: nil)
                completionHandler(true)
            }
        })
    }
    
    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        self.removeAnimate() { isCompleted in
            
        }
    }
    
    private func getStateAndTownship() {
        var urlString = ""
        if let type = selectedType, let cuntryObj = countryObject {
            switch type {
            case .state:
                urlString = String(format: "%@/country/getstatesbycountryid/%@", APIManagerClient.sharedInstance.base_url, cuntryObj.0)
            case .townShip:
                urlString = String(format: "%@/country/getshippingallowedcitiesbystateid/%@", APIManagerClient.sharedInstance.base_url, cuntryObj.0)
            case .none, .country:
                urlString = ""
            }
        }
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self](response, success, _) in
            
            DispatchQueue.main.async {
                
                
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["StatusCode"] as? Int, code == 200 {
                            if let dataArray = json["Data"] as? [Dictionary<String, Any>] {
                                self?.stateTownShipArray = dataArray
                            }
                            println_debug(json)
                            
                            self?.listTableView.reloadData()
                            
                        } else {
                            
                        }
                    }
                }
            }
        }
    }
}

extension StateTownshipListing: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stateTownShipArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StateTownshipCell", for: indexPath) as! StateTownshipTableViewCell
        cell.nameLbl.text = self.stateTownShipArray[indexPath.row]?["name"] as? String ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        stateObject.name = self.stateTownShipArray[indexPath.row]?["name"] as? String ?? ""
        stateObject.code = "\(self.stateTownShipArray[indexPath.row]?["id"] as? Int ?? 0)"
        self.removeAnimate() { isCompleted in
            self.delegate?.selectedStateTownshipMethod(type: self.selectedType ?? tapGestureType.none, selectedText: self.stateObject)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class StateTownshipTableViewCell: UITableViewCell {
    @IBOutlet var nameLbl: UILabel!{
        didSet {
            nameLbl?.text = nameLbl?.text?.localized
            self.nameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
